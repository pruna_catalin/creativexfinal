<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * DAOUsers: CreativeX && noValue
 * Date: 9/29/2017
 * Time: 12:58 PM
 */

namespace CreativeX\config;
/*
 * SETTINGS FOR DEVELOPER
 */

//define("DEVELOPER_MODE", 0);
//define("FLUSHCACHE",1);
//define("LOGIN_FRONT", false);
//define("Idle", false);
//define("FRONT_THEME","ctheme");
//define("ADMIN_THEME","admin_3");
//define("LIVE_URL","http://localhost/");
//define("LIVE_PATH","/creativex/creativex/");
//define("LIVE_PATH_IMAGES_TMP",getcwd()."/tmp_image/");
//define("COMPILE_TMP",getcwd()."/compileTmp/");
//define("LIVE_PATH_IMAGES_TMP_LINK","http://".LIVE_URL.LIVE_PATH."tmp_image/");
//define("LIVE_PATH_ADMIN",LIVE_URL.LIVE_PATH."admin/");
//define("LIVE_PATH_ADMIN_FILE_DOWNLOAD",getcwd()."/attachment/");
//define("LIVE_PATH_ADMIN_URL",LIVE_URL.LIVE_PATH."admin/index.php");

class  application
{
    /*
     * SETTINGS FOR DEVELOPER
     */
	const APP_NAME			= "CreativeX FrameWork";
    const DEVELOPER_MODE    = 0; // 1 activate Debugger
    const FLUSHCACHE        = 1; // 0 cache all files (css/js)
    const FRONT_THEME       = "market"; // name folder from app/themes/
    const ADMIN_THEME       = "admin2323"; // name admin folder from app/themes/
    const LIVE_URL_FRONT    = "http://localhost/creativex/creativex/"; // real path for load
    const LIVE_URL_ADMIN    = "http://localhost/creativex/creativex/admin/"; // real path for load
	const PATH_ADMIN_JS     = self::LIVE_URL_FRONT."app/themes/".self::ADMIN_THEME."/assets/js/";  // real path for load JS
	const PATH_ADMIN_CSS    = self::LIVE_URL_FRONT."app/themes/".self::ADMIN_THEME."/assets/css/";  // real path for load CSS
	const PATH_ADMIN_IMAGES = self::LIVE_URL_FRONT."app/themes/".self::ADMIN_THEME."/assets/images/";  // real path for IMAGES
	const PATH_FRONT_JS     = self::LIVE_URL_FRONT."app/themes/".self::FRONT_THEME."/assets/js/";  // real path for load JS
	const PATH_FRONT_CSS    = self::LIVE_URL_FRONT."app/themes/".self::FRONT_THEME."/assets/css/";  // real path for load CSS
	const PATH_FRONT_IMAGES = self::LIVE_URL_FRONT."app/themes/".self::FRONT_THEME."/assets/images/";  // real path for IMAGES
	const RELOAD_PERMISION  = "/creativex/creativex/admin/index.php/reloadPermision/list"; //special route for reload admin permisions in realtime
	const LOADER_ADM_ASSETS = self::LIVE_URL_FRONT."app/themes/".self::ADMIN_THEME."/";
	const BASE_JS			= self::LIVE_URL_FRONT."app/baseAssets/js/"; // base js include
	const BASE_CSS			= self::LIVE_URL_FRONT."app/baseAssets/css/"; // base css include
	const BASE_IMAGES		= self::LIVE_URL_FRONT."app/baseAssets/images/"; // base css include
	const BASE_ASSETS		= self::LIVE_URL_FRONT."app/baseAssets/";
	const BASE_MODEL		= "app/Model/";
	const BASE_CONTROLLER	= "core/Controller/";
	const BASE_ADM_TPL		= "app/themes/".self::ADMIN_THEME."/templates";

	const COMPILE_TMP = "compile/";
	const LANGUAGES = ["en","ro"];
	const MAX_PERPAGE = 10;
	const MAX_PSPAGE = 5;
	
}