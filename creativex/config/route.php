<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * DAOUsers: CreativeX && noValue
 * Date: 9/29/2017
 * Time: 12:55 PM
 */

namespace CreativeX\config;


class route
{
    /*
     * Rules manager
     * simple route "{case}"=> ["NameController","ActionController","NameFolderController","XHR or empty"]
     * complex route "{case}\/[a-zA-z]"=> ["NameController","ActionController","NameFolderController","XHR or empty"]
	 *				"{admin\/index\/edit}\/\d+" => ['SiteController',"actionIndex","admin","",""]
     */
    public static $Urls = [
             "{blank}" =>					 ['SiteController',"actionIndex","frontend","NORMAL","FRONT"],
             "{index}" =>					 ['SiteController',"actionIndex","frontend","",""],
             "{admin\/index}" =>			 ['DashBoardController',"actionIndex","admin","",""],
             "{admin\/users}" =>			 ['UserController',"actionList","admin","",""],
			 "{admin\/users\/delete}" =>	 ['UserController',"actionDelete","admin","XHR",""],
			 "{admin\/users\/edit\/}\d+" =>  ['UserController',"actionEdit","admin","",""],
			 "{admin\/users\/new}" =>		 ['UserController',"actionAdd","admin","",""],
             "{admin\/login}" =>			 ['LoginController',"actionLogin","admin","",""],
             "{admin\/logout}" =>			 ['LoginController',"actionLogin","admin","",""],
			 "{admin\/groups}" =>			 ['GroupController',"actionList","admin","",""],
			 "{admin\/groups\/new}" =>		 ['GroupController',"actionAdd","admin","",""],
			 "{admin\/groups\/edit\/}\d+" => ['GroupController',"actionEdit","admin","",""],
			 "{admin\/groups\/delete}" =>	 ['GroupController',"actionDelete","admin","",""],
			 "{admin\/orders}" =>			 ['OrdersController',"actionList","admin","",""],
			 "{admin\/orders\/new}}" =>	     ['OrdersController',"actionAdd","admin","",""],
			 "{admin\/orders\/edit\/}\d+" => ['OrdersController',"actionEdit","admin","",""],
			 "{admin\/orders\/delete}" =>    ['OrdersController',"actionDelete","admin","",""],
			 "{admin\/orders\/getItems}" =>  ['OrdersController',"actionGetItems","admin","",""],
			 "{admin\/customers}" =>		 ['CustomersController',"actionList","admin","",""],
			 "{admin\/generator}" =>		 ['GeneratorController',"actionMain","admin","",""],
			 "{admin\/generate}" =>			 ['GeneratorController',"actionGenerate","admin","",""],

			 
			 
    ];

}