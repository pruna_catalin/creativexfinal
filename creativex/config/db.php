<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 9/29/2017
 * Time: 12:58 PM
 */

namespace CreativeX\config;

/*
 *
 */

class db
{
    public static $hostname = "localhost";
    public static $port = "3306";
    public static $username = "root";
    public static $password = "";
    public static $database = "radu";
    public static $driver   = "mysql";
    public static $charset  = "utf8";
    public static $emailSupport = "prunacatalin.costin@gmail.com";
}