<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * DAOUsers: CreativeX && noValue
 * Date: 9/29/2017
 * Time: 1:02 PM
 */
namespace  CreativeX\core\controller\front;
use CreativeX\modules\render\View;
class SiteController{
	private static $request, $lang;
	public function __construct($request, $lang = null) {
		self::$request = $request;
		self::$lang = $lang;
	}

	public function actionIndex(){
		
	}
}