<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * DAOUsers: CreativeX && noValue
 * Date: 9/29/2017
 * Time: 1:02 PM
 */

namespace CreativeX\core\controller;

define("CreativeX","Passed");
use CreativeX\modules\request\RequestParser as Request;
use CreativeX\modules\language\Language;
use CreativeX\modules\render\View;
use CreativeX\config\application;
use CreativeX\core\controller\admin\LoginController;
class Controller
{
    public $request;
    public function Init(){
        $this->request = new Request();

        if($this->request->Case != ""){
            self::callFunction();
        }
        //404 for implement

    }
    //Controller body
    private function callFunction(){
		
        $translate = new Language();
        if($this->request->Controller != "" ){
            $controllerString = __NAMESPACE__ ."\\".$this->request->Folder."\\".$this->request->Controller;
			
            $response = new $controllerString($this->request, $translate);
            $content = $response->{$this->request->Action}();
			
            if(!$content instanceof View){
				self::Index($content, $this->request->Case."_activ",
					array($this->request->Case, $this->request->Action),
					$this->request->Folder, \Utils::JsonTest($content) ? "json" : "normal");
            }else{
				// NOTE(Iulian): In which context it is an XHR request?!
                self::Index($content, $this->request->Case."_activ",
					array($this->request->Case, $this->request->Action),
					$this->request->Folder, "normal");
           }
        }
    }
    private static function Index($content, $item, $customArray, $folder, $typeResponse = "normal") {
        $selectMenu = preg_match("/(.*)/",$item,$find);
        if(sizeof($find) > 0 )
            $selectMenu = str_replace("\\","",$find[0]);
        else
            $selectMenu = $item;
		if(LoginController::CheckLogin() && $folder != "frontend"){
		
			if($content instanceof View) {
				
				$view = new View(
					["index",$folder], 
					array(
						"content" =>  $content->render(),
						"selectMenu" => $selectMenu,
						"result" => $customArray,
						"App"=> new application()
					) 
				);

				echo $view->render();
				//$a = $content->render();
				//Debugger::log($a);
			} else{
				// \Debugger::Log($content);
				if(\Utils::JsonTest($content)){ 
					
					echo $content;
				}else{
					if(isset($content['errors']) && $content['errors'] != ""){
						$view = new View(
							["user/login", $folder], 
							array(
								"message" =>  (is_array($content) && $content['message'] == "") ? $content['errors'] : "",
								"App"=> new application()
							) 
						);
					}else{
						$view = new View(
							["index",$folder], 
							array(
								"content" =>  $content,
								"selectMenu" => $selectMenu,
								"result" => $customArray,
								"App"=> new application()
							) 
						);
					}
					
					echo $view->render();
				}
				
			}
		}else{
			if($folder != "frontend"){
				$view = new View(
					["user/login", $folder], 
					array(
						"message" =>  (is_array($content) && $content['message'] == "") ? $content['errors'] : "",
						"App"=> new application()
					) 
				);
			}else{

				$view = new View(
				["index", $folder], 
				array(
						"content" =>  "",
						"selectMenu" => $selectMenu,
						"result" => $customArray,
						"App"=> new application()
							) 
				);
			}
			
			echo $view->render();
		}
      
//        $view = new View("index", array(    "content" => $content,
//                                            "selectMenu" => $selectMenu,
//                                            "customArray" => $customArray
//                                        )
//        );
//        echo $view->render();
    }
}