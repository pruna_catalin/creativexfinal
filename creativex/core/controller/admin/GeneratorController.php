<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * DAOOrder: CreativeX && noValue
 * Date: 9/29/2017
 * Time: 1:02 PM
 */
namespace  CreativeX\core\controller\admin;
use CreativeX\modules\util\Debugger;
use CreativeX\modules\render\View;
use CreativeX\config\application;
use CreativeX\modules\migration\ParseSQL;
class GeneratorController{
	private static $request, $lang;
	public function __construct($request, $lang = null) {
		self::$request = $request;
		self::$lang = $lang;
	}

	public function actionMain(){
		$getData = new ParseSQL();
		$tables = $getData::GetTables();
		$itemList = "";
		
		for($i=0;$i<sizeof($tables);$i++){
			$items =  new View( ["generator/item","admin"],
						  [
							"App"=> new application(),
							"model"=>$tables[$i]->TABLE_NAME,
							"dao"=>"DAO".\Utils::CleanString($tables[$i]->TABLE_NAME,"FirstUp"),
							"controller"=>\Utils::CleanString($tables[$i]->TABLE_NAME,"FirstUp")."Controller",
							"js"=>"PATH:".\Utils::CleanString($tables[$i]->TABLE_NAME,"FirstUp")."<br>FILE:".\Utils::CleanString($tables[$i]->TABLE_NAME,"FirstUp").".js",
							"css"=>"PATH:".\Utils::CleanString($tables[$i]->TABLE_NAME,"FirstUp")."<br>FILE:".\Utils::CleanString($tables[$i]->TABLE_NAME,"FirstUp").".css",
							"route"=>\Utils::CleanString($tables[$i]->TABLE_NAME,"FirstUp")
						   ]
					  );
			$itemList .= $items->render();
		}
		

		$view =  new View( ["generator/main","admin"],
						  [
							"App"=> new application(),
							"items"=>$itemList
						   ]
					  );
        return  $view;
	}
	public function actionGenerate(){
		$result = "";
		$app = new application();
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			$model = [];
			if(isset($params['Model']['folder']) && isset($params['Model']['model'])) {
				if(sizeof($params['Model']['folder']) == sizeof($params['Model']['model'])){
					$size = sizeof($params['Model']['folder']);
					for($i=0;$i<$size;$i++){
						$this->createFiles($params['Model']['folder'][$i],$params['Model']['model'][$i]);
						$model[$params['Model']['model'][$i]] = $params['Model']['folder'][$i];
						$view =  new View( ["generator/generate","admin"],
						  [
							"line"=>"Route: ".$params['Model']['folder'][$i]."/".$params['Model']['model'][$i]."
									Model: ".$params['Model']['model'][$i]."
									DAO: DAO".$params['Model']['model'][$i]."
									Controller: Controller".$params['Model']['model'][$i]."
									<br>"
						   ]
							 );
						$result .= $view->render();
					}
				}
			}
			$this->createLoaderSintax(\Utils::ReadFile($app::BASE_ADM_TPL."/generator/templates/Loader.tpl"),$model);
			$this->createRoute(\Utils::ReadFile($app::BASE_ADM_TPL."/generator/templates/route.tpl"),$model);
		}
		
		return $result;
	}
	private function createFiles($route,$model){
		$app = new application();
		$this->createModel(\Utils::ReadFile($app::BASE_ADM_TPL."/generator/templates/Model.tpl"),$model);
		$this->createDAO(\Utils::ReadFile($app::BASE_ADM_TPL."/generator/templates/DAO.tpl"),$model);
		$this->createController(\Utils::ReadFile($app::BASE_ADM_TPL."/generator/templates/Controller.tpl"),$model,$route);
		$this->createViewList(\Utils::ReadFile($app::BASE_ADM_TPL."/generator/templates/view/list.tpl"), \Utils::ReadFile($app::BASE_ADM_TPL."/generator/templates/view/list_item.tpl"),$model);
		$this->createViewNew(\Utils::ReadFile($app::BASE_ADM_TPL."/generator/templates/view/new.tpl"),$model);
		$this->createViewEdit(\Utils::ReadFile($app::BASE_ADM_TPL."/generator/templates/view/edit.tpl"),$model);
		$this->createCssFile(\Utils::ReadFile($app::BASE_ADM_TPL."/generator/templates/assets/css/css.tpl"),$model);
		$this->createJsFile(\Utils::ReadFile($app::BASE_ADM_TPL."/generator/templates/assets/js/js.tpl"),$model);
					

	}
	private function createRoute($string,$model){
		/* 
   *	"{admin\/users}" =>				['UserController',"actionList","admin","",""],
		"{admin\/users\/delete}" =>		['UserController',"actionDelete","admin","XHR",""],
		"{admin\/users\/edit\/}\d+" =>  ['UserController',"actionEdit","admin","",""],
		"{admin\/users\/new}" =>		['UserController',"actionAdd","admin","",""],
		 */ 
		$route = [];
		$app = new application();
		foreach($model as $key => $value){
			if($value == "front"){
				$route[] = "\n\"{".$value."\/".$key."}\" =>	[\"".\Utils::CleanString($key,"FirstUp")."Controller\",\"actionList\",\"frontend\",\"\",\"\"],
\"{".$value."\/".$key."\/delete}\" =>	[\"".\Utils::CleanString($key,"FirstUp")."Controller\",\"actionDelete\",\"frontend\",\"XHR\",\"\"],\n\"{".$value."\/".$key."\/edit\/}\d+\" => [\"".\Utils::CleanString($key,"FirstUp")."Controller\",\"actionEdit\",\"frontend\",\"\",\"\"],
\"{".$value."\/".$key."\/new}\" =>	[\"".\Utils::CleanString($key,"FirstUp")."Controller\",\"actionAdd\",\"frontend\",\"\",\"\"]";
			}else{
				$route[] = "\n\"{".$value."\/".\Utils::CleanString($key,"FirstUp")."}\" =>	[\"".\Utils::CleanString($key,"FirstUp")."Controller\",\"actionList\",\"".$value."\",\"\",\"\"],
\"{".$value."\/".\Utils::CleanString($key,"FirstUp")."\/delete}\" =>	[\"".\Utils::CleanString($key,"FirstUp")."Controller\",\"actionDelete\",\"".$value."\",\"XHR\",\"\"],
\"{".$value."\/".\Utils::CleanString($key,"FirstUp")."\/edit\/}\d+\" =>	[\"".\Utils::CleanString($key,"FirstUp")."Controller\",\"actionEdit\",\"".$value."\",\"\",\"\"],
\"{".$value."\/".\Utils::CleanString($key,"FirstUp")."\/new}\" =>	[\"".\Utils::CleanString($key,"FirstUp")."Controller\",\"actionAdd\",\"".$value."\",\"\",\"\"]";
			}
		}
		$string = str_replace("ROUTE",implode(",",$route),$string);
		\Utils::WriteToFile($app::BASE_ADM_TPL."/generator/temp/route.php",$string);
	}
	private function createLoaderSintax($File,$model){
		
		$listJs = [];
		$listCss = [];
		$app = new application();
		//Model is array [folder][modelname]
		/*
		*
  *		"admin/users":{
						"actionList":[
										"Users/UserActions.js",
										"bootstrap-toastr/toastr.min.js",
										"ui-toastr.min.js",
										"bootbox/bootbox.min.js"
									]
					}
		 */
		foreach($model as $key => $value){
			$listJs[] = "\"".$value."/".\Utils::CleanString($key,"FirstUp")."\":{\"actionList\":[\"".\Utils::CleanString($key,"FirstUp")."/".\Utils::CleanString($key,"FirstUp").".js\",\"bootstrap-toastr/toastr.min.js\", \"ui-toastr.min.js\", \"bootbox/bootbox.min.js\"]}";
			$listCss[] = "\"".$value."/".\Utils::CleanString($key,"FirstUp")."\":{\"actionList\":[\"".\Utils::CleanString($key,"FirstUp")."/".\Utils::CleanString($key,"FirstUp").".css\"]}";
		}
		$listJsString = implode(",",$listJs);
		$listCssString = implode(",",$listCss); 
		//JS_ROWS , CSS_ROWS
		$File = str_replace("JS_ROWS",$listJsString,$File);
		$File = str_replace("CSS_ROWS",$listCssString,$File);
		
		\Utils::WriteToFile($app::BASE_ADM_TPL."/generator/temp/Loader.json",\Utils::JsonPrettyPrint($File));
		
	}
	private function createJsFile($string,$model){
		$string = str_replace("DATE_CREATE",date("Y-m-d",time()),$string);
		$string = str_replace("TIME_CREATE",date("h:i:s A",time()),$string);
		$string = str_replace("MODEL",\Utils::CleanString($model,"FirstUp"),$string);


		$app = new application();
		if(is_dir($app::BASE_ADM_TPL."/generator/temp/assets/js/".\Utils::CleanString($model,"FirstUp"))){
			\Utils::WriteToFile($app::BASE_ADM_TPL."/generator/temp/assets/js/".\Utils::CleanString($model,"FirstUp")."/".\Utils::CleanString($model,"FirstUp").".js",$string);
		}else{
			//echo $app::BASE_ADM_TPL."/generator/temp/view/".\Utils::CleanString($model,"FirstUp");die;
			if(mkdir("./".$app::BASE_ADM_TPL."/generator/temp/assets/js/".\Utils::CleanString($model,"FirstUp"), 0777, true)){
				\Utils::WriteToFile($app::BASE_ADM_TPL."/generator/temp/assets/js/".\Utils::CleanString($model,"FirstUp")."/".\Utils::CleanString($model,"FirstUp").".js",$string);
			}
		}
		
	}
	private function createCssFile($string,$model){
		$string = str_replace("DATE_CREATE",date("Y-m-d",time()),$string);
		$string = str_replace("TIME_CREATE",date("h:i:s A",time()),$string);
		$app = new application();
		if(is_dir($app::BASE_ADM_TPL."/generator/temp/assets/css/".\Utils::CleanString($model,"FirstUp"))){
			\Utils::WriteToFile($app::BASE_ADM_TPL."/generator/temp/assets/css/".\Utils::CleanString($model,"FirstUp")."/".\Utils::CleanString($model,"FirstUp").".css",$string);
		}else{
			//echo $app::BASE_ADM_TPL."/generator/temp/view/".\Utils::CleanString($model,"FirstUp");die;
			if(mkdir("./".$app::BASE_ADM_TPL."/generator/temp/assets/css/".\Utils::CleanString($model,"FirstUp"), 0777, true)){
				\Utils::WriteToFile($app::BASE_ADM_TPL."/generator/temp/assets/css/".\Utils::CleanString($model,"FirstUp")."/".\Utils::CleanString($model,"FirstUp").".css",$string);
			}
		}
		
	}
	private function createViewEdit($string,$model){
		$getData = new ParseSQL();
		$columnsArr = $getData::GetColumnsFromTable($model);
		#\Debugger::Log($columnsArr);die;
		$string = str_replace("DATE_CREATE",date("Y-m-d",time()),$string);
		$string = str_replace("TIME_CREATE",date("h:i:s A",time()),$string);
		$string = str_replace("MODEL",\Utils::CleanString($model,"FirstUp"),$string);
		$group_item = "";
		$primary = "id";
		for($i=0;$i<sizeof($columnsArr);$i++){ 
			if(isset($columnsArr[$i]->COLUMN_KEY) && $columnsArr[$i]->COLUMN_KEY == "PRI"){ 
				$primary = $columnsArr[$i]->COLUMN_NAME;
			}else{
				$group_item .=\Utils::GenerateTabsSpace(10)."<div class=\"form-group\">\n"
							.\Utils::GenerateTabsSpace(11)."<label for=\"input".$columnsArr[$i]->COLUMN_NAME."\" class=\"col-sm-2 control-label\">{@\$Language->t('".$columnsArr[$i]->COLUMN_NAME."')@}</label>\n".
							\Utils::GenerateTabsSpace(11)."<div class=\"col-sm-10\">\n".
							\Utils::GenerateTabsSpace(12)."<input type=\"text\" id=\"input".$columnsArr[$i]->COLUMN_NAME."\" class=\"form-control\" name=\"Model[".$columnsArr[$i]->COLUMN_NAME."]\" placeholder=\"{@\$Language->t('".$columnsArr[$i]->COLUMN_NAME."')@}\" value=\"{@\$".\Utils::CleanString($model,"FirstUp")."->".$columnsArr[$i]->COLUMN_NAME."@}\" />\n".
							\Utils::GenerateTabsSpace(11)."</div>\n".
							\Utils::GenerateTabsSpace(10)."</div>\n";
			}
		}
		$string = str_replace("GROUP_ITEM",$group_item,$string);
		$string = str_replace("PRIMARY",$primary,$string);
		$app = new application();
		\Utils::WriteToFile($app::BASE_ADM_TPL."/generator/temp/View/".\Utils::CleanString($model,"FirstUp")."/edit.tpl",$string);
	}
	private function createViewNew($string,$model){
		/* GROUP_ITEM 10 tabs
		 * <div class="form-group">
				<label for="inputEmail" class="col-sm-2 control-label">{@$Language->t('email')@}</label>
				<div class="col-sm-10">
				<input type="email" id="inputEmail" class="form-control" name="Model[email]" placeholder="{@$Language->t('email')@}" value="" />
				</div>
			</div>
		*/ 
		$getData = new ParseSQL();
		$columnsArr = $getData::GetColumnsFromTable($model);
		#\Debugger::Log($columnsArr);die;
		$string = str_replace("DATE_CREATE",date("Y-m-d",time()),$string);
		$string = str_replace("TIME_CREATE",date("h:i:s A",time()),$string);
		$string = str_replace("MODEL",\Utils::CleanString($model,"FirstUp"),$string);
		$group_item = "";
		for($i=0;$i<sizeof($columnsArr);$i++){ 
			if(isset($columnsArr[$i]->COLUMN_KEY) && $columnsArr[$i]->COLUMN_KEY == "PRI"){ 
				
			}else{
				$group_item .=\Utils::GenerateTabsSpace(10)."<div class=\"form-group\">\n"
							.\Utils::GenerateTabsSpace(11)."<label for=\"input".$columnsArr[$i]->COLUMN_NAME."\" class=\"col-sm-2 control-label\">{@\$Language->t('".$columnsArr[$i]->COLUMN_NAME."')@}</label>\n".
							\Utils::GenerateTabsSpace(11)."<div class=\"col-sm-10\">\n".
							\Utils::GenerateTabsSpace(12)."<input type=\"text\" id=\"input".$columnsArr[$i]->COLUMN_NAME."\" class=\"form-control\" name=\"Model[".$columnsArr[$i]->COLUMN_NAME."]\" placeholder=\"{@\$Language->t('".$columnsArr[$i]->COLUMN_NAME."')@}\" value=\"\" />\n".
							\Utils::GenerateTabsSpace(11)."</div>\n".
							\Utils::GenerateTabsSpace(10)."</div>\n";
			}
		}
		$string = str_replace("GROUP_ITEM",$group_item,$string);
		$app = new application();
		\Utils::WriteToFile($app::BASE_ADM_TPL."/generator/temp/View/".\Utils::CleanString($model,"FirstUp")."/new.tpl",$string);
	}
	private function createViewList($string,$stringItem,$model){
		//MODEL,TH_TABLE
		/*
		 * <th style="width: 20px">
			#<input type="text" class="form-control" name="Model[search][id]"  value="{@$filter['id']@}"/>
			</th>
			<td>{@$user->id@}</td> FOR ITEMS
		*/
		$getData = new ParseSQL();
		$columnsArr = $getData::GetColumnsFromTable($model);
		#\Debugger::Log($columnsArr);die;
		$string = str_replace("DATE_CREATE",date("Y-m-d",time()),$string);
		$string = str_replace("TIME_CREATE",date("h:i:s A",time()),$string);
		$string = str_replace("MODEL",\Utils::CleanString($model,"FirstUp"),$string);
		$stringItem = str_replace("MODEL",\Utils::CleanString($model,"FirstUp"),$stringItem);
		$primary = "id";
		$th_table = "";
		$td_table = "";
		for($i=0;$i<sizeof($columnsArr);$i++){ 
			if(isset($columnsArr[$i]->COLUMN_KEY) && $columnsArr[$i]->COLUMN_KEY == "PRI"){
				$primary = $columnsArr[$i]->COLUMN_NAME; 
				$th_table .="\t\t\t\t\t\t<th style=\"width: 20px\">
								#<input type=\"text\" class=\"form-control\" name=\"Model[search][".$columnsArr[$i]->COLUMN_NAME."]\"  value=\"{@\$filter['".$columnsArr[$i]->COLUMN_NAME."']@}\"/>
						</th>\n";
			}else{
				$th_table .="\t\t\t\t\t\t<th style=\"width: 20px\">
								<input type=\"text\" class=\"form-control\" name=\"Model[search][".$columnsArr[$i]->COLUMN_NAME."]\"  value=\"{@\$filter['".$columnsArr[$i]->COLUMN_NAME."']@}\"/>
						</th>\n";
			}
			$td_table .="\t<td>{@\$".\Utils::CleanString($model,"FirstUp")."->".$columnsArr[$i]->COLUMN_NAME."@}</td>\n";

		}
		$string = str_replace("TH_TABLE",$th_table,$string);
		$stringItem = str_replace("TD_ITEM",$td_table,$stringItem);
		$stringItem = str_replace("PRIMARY",$primary,$stringItem);
		$app = new application();
		
		if(is_dir($app::BASE_ADM_TPL."/generator/temp/View/".\Utils::CleanString($model,"FirstUp"))){
			\Utils::WriteToFile($app::BASE_ADM_TPL."/generator/temp/View/".\Utils::CleanString($model,"FirstUp")."/list.tpl",$string);
			\Utils::WriteToFile($app::BASE_ADM_TPL."/generator/temp/View/".\Utils::CleanString($model,"FirstUp")."/list_item.tpl",$stringItem);
		}else{
			//echo $app::BASE_ADM_TPL."/generator/temp/view/".\Utils::CleanString($model,"FirstUp");die;
			if(mkdir("./".$app::BASE_ADM_TPL."/generator/temp/View/".\Utils::CleanString($model,"FirstUp"), 0777, true)){
				\Utils::WriteToFile($app::BASE_ADM_TPL."/generator/temp/View/".\Utils::CleanString($model,"FirstUp")."/list.tpl",$string);
				\Utils::WriteToFile($app::BASE_ADM_TPL."/generator/temp/View/".\Utils::CleanString($model,"FirstUp")."/list_item.tpl",$stringItem);
			}
		}


	}
	private function createController($string,$model,$folder){
		//DATE_CREATE , TIME_CREATE , MODEL,  | PRIMARY, VALUE_FILTER, ISSET_PARAMS , COLUMN_DATA_MODEL, FOLDER
		/*if(isset($params['Model']['search']['email']))
		if($params['Model']['search']['email'] != "")
		$dataUser->email['like'] = ["%".$params['Model']['search']['email']."%"];
		 */
		/*
		 * if(!DAOMODEL::Find("username='".$params['Model']['username']."' AND id !=".$id)){
		$dataUser->username = $params['Model']['username'];
		}else{
		$dataUser->username = $user->username;
		$message = "User is already exist";
		}
		 */
		$getData = new ParseSQL();
		$columnsArr = $getData::GetColumnsFromTable($model);
		$string = str_replace("DATE_CREATE",date("Y-m-d",time()),$string);
		$string = str_replace("TIME_CREATE",date("h:i:s A",time()),$string);
		$string = str_replace("MODEL",\Utils::CleanString($model,"FirstUp"),$string);
		$string = str_replace("NAMESPACE_GENERATE","<?php\nnamespace CreativeX\core\controller\admin;\n",$string);
		$primary = "id";
		$value_filter = [];
		$issetParams = "";
		$column_data_model = [];
		for($i=0;$i<sizeof($columnsArr);$i++){ 
			if(isset($columnsArr[$i]->COLUMN_KEY) && $columnsArr[$i]->COLUMN_KEY == "PRI"){
				$primary = $columnsArr[$i]->COLUMN_NAME; 
			}
			#\Debugger::Log($columnsArr[$i]);
			if(isset($columnsArr[$i]->DATA_TYPE)){
				if($columnsArr[$i]->DATA_TYPE == "int"){
					$issetParams .= "\t\t\t\tif(isset(\$params['Model']['search']['".$columnsArr[$i]->COLUMN_NAME."']))\n\t\t\t\t\tif(\$params['Model']['search']['".$columnsArr[$i]->COLUMN_NAME."'] != \"\")\n\t\t\t\t\t\t\$data".\Utils::CleanString($model,"FirstUp")."->".$columnsArr[$i]->COLUMN_NAME." =  is_numeric(\$params['Model']['search']['".$columnsArr[$i]->COLUMN_NAME."']) ? \$params['Model']['search']['".$columnsArr[$i]->COLUMN_NAME."'] : 0;\n";
				}else{
					$issetParams .= "\t\t\t\tif(isset(\$params['Model']['search']['".$columnsArr[$i]->COLUMN_NAME."']))\n\t\t\t\t\tif(\$params['Model']['search']['".$columnsArr[$i]->COLUMN_NAME."'] != \"\")\n\t\t\t\t\t\t\$data".\Utils::CleanString($model,"FirstUp")."->".$columnsArr[$i]->COLUMN_NAME."['like'] = [\"%\".\$params['Model']['search']['".$columnsArr[$i]->COLUMN_NAME."'].\"%\"];\n";
				}
			}
			$column_data_model[] = "\t\t\t\t\$data".\Utils::CleanString($model,"FirstUp")."->".$columnsArr[$i]->COLUMN_NAME." = \$params['Model']['search']['".$columnsArr[$i]->COLUMN_NAME."'];";
			//IS_NULLABLE
   
			#$columns_set_data .="\t\t\t\$".$columnsArr[$i]->COLUMN_NAME." = \$data->".$columnsArr[$i]->COLUMN_NAME.";\n";
			$value_filter[] = "\"".$columnsArr[$i]->COLUMN_NAME."\"=> \"\"";
		}
		$string = str_replace("PRIMARY",$primary,$string);
		$string = str_replace("ISSET_PARAMS",$issetParams,$string);
		$string = str_replace("VALUE_FILTER",implode(" ,",$value_filter),$string);
		$string = str_replace("COLUMN_DATA",implode(" \n",$column_data_model),$string);
		$string = str_replace("FOLDER",$folder,$string);

		$app = new application();
		\Utils::WriteToFile($app::BASE_ADM_TPL."/generator/temp/Controller/".\Utils::CleanString($model,"FirstUp")."Controller.php",$string);
	}
	private function createDAO($string,$model){
		//DATE_CREATE , TIME_CREATE , MODEL,  | PRIMARY, TABLE, COLUMNS_SET_DATA, LIST_VAR_COLUMNS
		$getData = new ParseSQL();
		$columnsArr = $getData::GetColumnsFromTable($model);
		$string = str_replace("DATE_CREATE",date("Y-m-d",time()),$string);
		$string = str_replace("TIME_CREATE",date("h:i:s A",time()),$string);
		$string = str_replace("MODEL",\Utils::CleanString($model,"FirstUp"),$string);
		$primary = "id";
		$columns_set_data = "";
		$list_var_columns  = [];
		for($i=0;$i<sizeof($columnsArr);$i++){ 
			if(isset($columnsArr[$i]->COLUMN_KEY) && $columnsArr[$i]->COLUMN_KEY == "PRI"){
				$primary = $columnsArr[$i]->COLUMN_NAME; 
			}
			$columns_set_data .="\t\t\t\$".$columnsArr[$i]->COLUMN_NAME." = \$data->".$columnsArr[$i]->COLUMN_NAME.";\n";
			$list_var_columns[] = "\$".$columnsArr[$i]->COLUMN_NAME;
		}
		$string = str_replace("PRIMARY",$primary,$string);
		$string = str_replace("TABLE",$model,$string);
		$string = str_replace("COLUMNS_SET_DATA",$columns_set_data,$string);
		$string = str_replace("LIST_VAR_COLUMNS",implode(" ,",$list_var_columns),$string);


		$app = new application();
		\Utils::WriteToFile($app::BASE_ADM_TPL."/generator/temp/DAO/DAO".\Utils::CleanString($model,"FirstUp").".php",$string);
	}
	private function createModel($string,$model){
		//DATE_CREATE , TIME_CREATE , MODEL, PUBLIC_COLUMNS_NULL, COLUMNS_NULL_CONSTRUCT, SET_FUNCTION_COLUMN, FUNCTION_COLUMN
		$getData = new ParseSQL();
		$columnsArr = $getData::GetColumnsFromTable($model);
		$string = str_replace("DATE_CREATE",date("Y-m-d",time()),$string);
		$string = str_replace("TIME_CREATE",date("h:i:s A",time()),$string);
		$string = str_replace("MODEL",\Utils::CleanString($model,"FirstUp"),$string);
		$public_columns_null = "";
		$columns_null_construct = [];
		$set_function_column = "";
		$function_column = "";
		for($i=0;$i<sizeof($columnsArr);$i++){
			$public_columns_null .="\tpublic \$".$columnsArr[$i]->COLUMN_NAME." = NULL;\n";
			$columns_null_construct[] = "\$".$columnsArr[$i]->COLUMN_NAME." = NULL";
			$set_function_column .= "\t\t\t\$this->set".\Utils::CleanString($columnsArr[$i]->COLUMN_NAME,"FirstUp")."(\$".$columnsArr[$i]->COLUMN_NAME.");\n";
			$function_column  .="\tpublic function set".\Utils::CleanString($columnsArr[$i]->COLUMN_NAME,"FirstUp")."(\$".$columnsArr[$i]->COLUMN_NAME."){ \n \t\t\$this->".$columnsArr[$i]->COLUMN_NAME." = "."\$".$columnsArr[$i]->COLUMN_NAME.";\n \t\treturn \$this; \n\t}\n";
			$function_column  .="\tpublic function get".\Utils::CleanString($columnsArr[$i]->COLUMN_NAME,"FirstUp")."(){ \n \t\treturn \$this->".$columnsArr[$i]->COLUMN_NAME."; \n\t}\n";
		}
		$string = str_replace("PUBLIC_COLUMNS_NULL",$public_columns_null,$string);
		$string = str_replace("COLUMNS_NULL_CONSTRUCT",implode(" ,",$columns_null_construct),$string);
		$string = str_replace("SET_FUNCTION_COLUMN",$set_function_column,$string);
		$string = str_replace("FUNCTION_COLUMN",$function_column,$string);
		$app = new application();
		\Utils::WriteToFile($app::BASE_ADM_TPL."/generator/temp/Data/Data".\Utils::CleanString($model,"FirstUp").".php",$string);
	}
}