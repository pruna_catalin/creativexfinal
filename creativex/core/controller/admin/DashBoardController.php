<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * DAOUsers: CreativeX && noValue
 * Date: 9/29/2017
 * Time: 1:02 PM
 */
namespace  CreativeX\core\controller\admin;

use CreativeX\Model\DAO\DAOUsers;
use CreativeX\Model\DAO\DAOCustomer;
use CreativeX\Model\DAO\DAOOrder;
use CreativeX\modules\render\View;
use CreativeX\Model\Data\DataUser;
use CreativeX\Model\Data\DataCustomer;
use CreativeX\Model\Data\DataOrder;
use CreativeX\modules\migration\ParseSql;
class DashBoardController{
    private $translate = array();
    private $params = array();
    public function __construct() {
        if(sizeof(func_get_args()) > 1){ $this->params = func_get_args()[0] ; $this->translate = func_get_args()[1];}
        if(sizeof(func_get_args()) == 1) { $this->params = func_get_args()[0] ; }
    }


    public function actionIndex(){

		//echo \Html::Link("javascript:void();","IDLINK","forma","CLICK ME",["onclick"=>"alert(/pressme2/);"]);
		//echo \Html::Input("text","form-control",["name1","name2","name3"],"testVAL2",["onclick"=>"alert(/pressme2/);"]);

		//echo \Html::Radio(["id1","id2"],"form-control",["name1"],["yes"=>"","no"=>"checked"],["onclick"=>"alert(/pressme2/);"]);
		//echo \Html::Checkbox("id1","form-control",["radio1"],["yes"=>"checked"],["onclick"=>"alert(/pressme2/);"]);
		//echo \Html::DateInput("text","form-control",["name1"],date("Y-d-m",time()),["onclick"=>"alert(/pressme2/);"]);
		//echo \Html::Select2("idEvent","form-control",["country"],["en"=>["Engleza","selected"],"fr"=>["Franceza","selected"]],[],"multiple");
		//echo \Html::DatePicker("idPicker","",["name1"],"27.02.2017","calendar",[]);

		DAOUsers::$order		= "id DESC";
		DAOCustomer::$order		= "id DESC";
		DAOOrder::$order		= "id DESC";
		$totalCustomer			= DAOCustomer::Count();
		$totalusers				= DAOUsers::Count();     
		$totalOrders			= DAOOrder::Count();
		$objectOrders			= new DataOrder();
		$objectOrders->status	= "paid";
		$totalOrdersPaid		= DAOOrder::FindAll($objectOrders);

        $view =  new View( ["dashboard/top","admin"],
						   [
							"totalUsers"=>$totalusers,
							"totalCustomers"=>$totalCustomer,
							"totalOrders"=>$totalOrders,
							"totalOrdersPaid"=> ($totalOrdersPaid) ? count($totalOrdersPaid) : 0
							]
                       );
        return  $view;
    }
    public function actionContact(){
		ParseSql::Start();
       // return json_encode(array("me"=>"Hello my friend i am just a simple answer"));
    }
}