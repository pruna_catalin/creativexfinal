<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * DAOUsers: CreativeX && noValue
 * Date: 9/29/2017
 * Time: 1:02 PM
 */
namespace  CreativeX\core\controller\admin;
use CreativeX\Model\Data\DataGroup;
use CreativeX\Model\Data\DataPermision;
use CreativeX\Model\DAO\DAOGroup;
use CreativeX\Model\DAO\DAOPermision;
use CreativeX\modules\util\Debugger;
use CreativeX\modules\render\View;
use CreativeX\config\application;
use CreativeX\config\route;
class GroupController{
	private static $request, $lang;
	public function __construct($request, $lang = null) {
		self::$request = $request;
		self::$lang = $lang;
	}
	public function actionList(){
		$dataGroup = new DataGroup();
		$ValueFilter = ["id"=>"","name"=>""];
		$limit = "0,".application::MAX_PERPAGE;
		$totalGroups = DAOGroup::Count();
		$page_no = 1;
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['search'])) {
				#$model->username['in'] = ["admin", "departament"];
				#$model->email['or'] = ["departament@creativex.ro", "23"];
				if(isset($params['Model']['search']['name']))
					if($params['Model']['search']['name'] != "")
						$dataGroup->name['like'] = ["%".$params['Model']['search']['name']."%"];
				$ValueFilter = \Utils::GetValueFromModel($ValueFilter,$params['Model']['search']);
				
			}
			if(isset($params['Model']['pagination'])){
				if(is_numeric($params['Model']['pagination'])){
					if($params['Model']['pagination'] < 1){
						$page_no = 1;
					}else{
						$page_no = $params['Model']['pagination'];
					}
					$page_no -= 1;
					$limit = ($page_no * application::MAX_PERPAGE).",".application::MAX_PERPAGE;
				}
			}

		}
		DAOGroup::$order = "id DESC";
		$groups = DAOGroup::FindAll($dataGroup,$limit);
		$groupList = "";
		if($groups){
			foreach($groups as $group){
				$child = new View( ["group/list_item","admin"],
						  [
							"group"=>$group,
							"App"=> new application()
						   ]
					  );
				$groupList .= $child->render();
			}
		}
		$view =  new View( ["group/list","admin"],
						  [
							"totalGroups"=>$totalGroups,
							"items"=>$groupList,
							"App"=> new application(),
							"filter"=>$ValueFilter,
							"page_no"=>$page_no
						   ]
					  );
        return  $view;
	}
	public function actionDelete(){
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['delete'])){
				$id = is_numeric($params['Model']['delete']) ? $params['Model']['delete'] : 0;
				if(DAOGroup::Find("id=".$id)){
					if(DAOGroup::Delete("id=".$id)){
						return json_encode(["success"=>true]);
					}
					return json_encode(["success"=>false,"message"=>self::$lang->t('usernotfound')]);
				}else{
					return json_encode(["success"=>false,"message"=>self::$lang->t('usernotfound')]);
				}
			}
		}
		exit;
	}
	public function actionAdd(){
		$message = "";
		$params = "";
		$groupid= 0;
		$group = DAOGroup::FindAll('');
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && $params['Model'] != ""){
				$dataGroup = new DataGroup();
				$check = false;				
				if(!DAOGroup::Find("name='".$params['Model']['name']."'")){
					$dataGroup->name = $params['Model']['name'];
				}else{
					$message = "Group is already exist";
					$check = true;
				}
				if(!$check){
					$groupid = DAOGroup::Insert($dataGroup);
					if($groupid){
						\Utils::Redirect("admin/groups/edit/".$groupid,true);
					}
				}
			}					
		}
		$view =  new View(	["group/new","admin"],
								["message"=>$message,
								"group"=>$group,
								"App"=> new application(),
								"params"=>$params
							]
					  );
		return  $view;
	}
	public function actionEdit(){
		$message = "";
		$group = 0;
		$params = "";
		//\Debugger::Log(self::$request);
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && self::$request->Method[1] != "POST"){
				$id = is_numeric($params['Model']) ? $params['Model'] : 0;
				$group = DAOGroup::Find("id=".$id);
				if(!$group) return self::$lang->t('usernotfound');
			}else{
				$success = false;
				$id = is_numeric($params['Model']['id']) ? $params['Model']['id'] : 0;
				$group = DAOGroup::Find("id=".$id);
				if(!$group) return self::$lang->t('usernotfound');
				$dataGroup = new DataGroup();
				$dataGroup->id = $params['Model']['id'];
				$dataGroup->id_permision = $group->id_permision;
				if(!DAOGroup::Find("name='".$params['Model']['name']."' AND id !=".$id)){
					$dataGroup->name = $params['Model']['name'];
				}else{
					$dataGroup->name = $group->name;
					$message = "Group is already exist";
				}
				$dataGroup->create_by = $_SESSION['UserId'];
				$dataGroup->modified_by = $_SESSION['UserId'];
				$dataGroup->create_at = date("Y-m-d",time());
				$dataGroup->modified_at = date("Y-m-d",time());
				if(DAOGroup::Update($dataGroup,"id")){
					$success = true;
				}
				$dataPermision = new DataPermision();
				$dataPermision->id = $group->id_permision;
				$dataPermision->permision_list = json_encode(array("permision"=>
							[$params['Model']['permision']['list']]
							));
				$dataPermision->create_by = $_SESSION['UserId'];
				$dataPermision->modified_by = $_SESSION['UserId'];
				$dataPermision->create_at = date("Y-m-d",time());
				$dataPermision->modified_at = date("Y-m-d",time());
				$updatePermision = DAOPermision::Update($dataPermision,"id");
				if($updatePermision){
					$success = true;
				}
				
				if($success)
					\Utils::Redirect("admin/groups/edit/".$group->id,true);

				//\Utils::Debug(self::$request);
			}					
		}
		$view =  new View(	["group/edit","admin"],
								[
								"message"=>$message,
								"group"=>$group,
								"App"=> new application(),
								"params"=>$params,
								"dataList"=>$this->getPermision($group->id_permision),
								"permision"=>$group->id_permision
							]
					  );
		return  $view;
	}
	private function getpermision($id_permision){			
		$findPermsion = DAOPermision::Find("id=".$id_permision);
		$contentChildren = "";
		$returnListString = "";
		if($findPermsion ){
			$listPermision = json_decode($findPermsion->permision_list,true);
			$returnListArr = $this->ParsePermision($listPermision['permision']);
			if(sizeof($returnListArr) > 0){
				foreach($returnListArr as $item => $value){
					for($i=0;$i<sizeof($value);$i++){	
						$contentChildren .='{"text": "'.$value[$i][1][0].'","state": {"selected": '.(($value[$i][1][1] == 1 )? "true" : "false").'},"data":"'.bin2hex($value[$i][0]).'"},';
					}

					$view_list = new View(["group/permision","admin"],
										["nameController"=>$item,"childrenList"=>$contentChildren]
									);
					$contentChildren = "";
					$returnListString .= $view_list->render();
				}
			}
		}
		
		return $returnListString;
	}
	private  function ParsePermision($checkPermsion){
		$arrListPermsion = array(); // param[0]->route,param[1]->name Controller
		$split = explode(",",$checkPermsion[0]);
		$checkPermsion = $split;
		#\Debugger::Log($checkPermsion);
		foreach(Route::$Urls as $permision => $value){
			if(sizeof($value) == 3 || sizeof($value) == 5){
				if($value[2] == "admin"){
					if(is_array($checkPermsion)){
						if(in_array(bin2hex($permision),$checkPermsion)){
								$arrListPermsion[$value[0]][] = array($permision,array($value[1],1));
							}else{
								$arrListPermsion[$value[0]][] = array($permision,array($value[1],0));
							}						
					}else{
						$arrListPermsion[$value[0]][] = array($permision,array($value[1],0));
					}
				}
			}
		}
		return $arrListPermsion;
		
	}
}