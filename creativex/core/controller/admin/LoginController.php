<?php
namespace  CreativeX\core\controller\admin;

use CreativeX\Model\DAO\DAOUsers;
use CreativeX\Model\DAO\DAOGroup;
use CreativeX\Model\DAO\DAOPermision;
use CreativeX\Model\Data\DataUser;
use CreativeX\config\application;
use CreativeX\modules\util;
use CreativeX\modules\render\View;

class LoginController{
	private static $request, $lang;
	public function __construct($request, $lang = null) {
		self::$request = $request;
		self::$lang = $lang;
	}

    public static function CheckLogin(){
        $expireAfter = 30;
		if(isset($_SESSION['last_action'])){
			
			$secondsInactive = time() - $_SESSION['last_action'];
			//Convert our minutes into seconds.
			$expireAfterSeconds = $expireAfter * 60;
			//Check to see if they have been inactive for too long.
			if($secondsInactive >= $expireAfterSeconds){
				//User has been inactive for too long.
				//Kill their session.
				session_destroy();
			}
		}else{
				$_SESSION['loginAccess'] = "";
		}
        return isset($_SESSION['loginAccess']) && $_SESSION['loginAccess'] == "Passed";
    }

    public static function actionLogin() {
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;

			$currentUrl = isset($params['currentUrl']) ? "/".$params['currentUrl'] : "";

			if(!empty($params['Model']['username']) && !empty($params['Model']['password'])){
				$username = htmlentities($params['Model']['username']);
				$password = htmlentities($params['Model']['password']);
				$model = new DataUser('');
				$daoUser = new DAOUsers('');
				$model->username = $username;
				$model->password  = md5($password);
				$sql = $daoUser::Find($model);
					
				if($sql){
					$modelGroup= new DAOGroup('');
					$groupUser = $modelGroup::Find("id=".$sql->id_group);
					if($groupUser){
						$modelPermision= new DAOPermision('');
						$sqlPermision = $modelPermision::Find("id='".$groupUser->id_permision."'");
						$_SESSION['permision'] = $sqlPermision->permision_list;
						$_SESSION['loginAccess'] = "Passed";
						$_SESSION['Username'] = $sql->username;
						$_SESSION['UserId'] =  $sql->id;
						$_SESSION['GroupId'] = $sql->id_group;
						$_SESSION['language'] = \Utils::GetIfExistsOrDefault(
							$params['Model']['language'], application::LANGUAGES, "en"
						);
						$_SESSION['last_action'] = time();	
						//setcookie("language",$arg_list['language'],strtotime( '+30 days' ));
						\Utils::Redirect("admin/index",true);			
					}else{
						return  ["message"=>"","errors"=>"You dont have permision","case"=>$currentUrl];
					}
						
				}else{
					$_SESSION['Username'] = "";
					$_SESSION['loginAccess'] = "";
					$_SESSION['permision'] = "";
					session_destroy();
					return  ["message"=>"","errors"=>"Login Incorect","case"=>$currentUrl];
						
				}
			} else{
				return  ["message"=>"","errors"=>"Enter Username And Password"];
			}
		} else{
			return  ["message"=>"", "errors"=> "Trespassing imaginary boundaries !"];
		}
    }

    public static function actionLogout(){
		$_SESSION['loginAccess'] = "";
		session_destroy();
		\Utils::Redirect("admin/login",true);	
    }
}


?>
