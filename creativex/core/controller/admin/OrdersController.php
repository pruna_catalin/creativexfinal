<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * DAOOrder: CreativeX && noValue
 * Date: 9/29/2017
 * Time: 1:02 PM
 */
namespace  CreativeX\core\controller\admin;
use CreativeX\Model\DAO\DAOOrder;
use CreativeX\Model\DAO\DAOCustomer;
use CreativeX\Model\DAO\DAOItem;
use CreativeX\Model\Data\DataOrder;
use CreativeX\Model\Data\DataCustomer;
use CreativeX\Model\Data\DataProfile;
use CreativeX\modules\util\Debugger;
use CreativeX\modules\render\View;
use CreativeX\config\application;
use CreativeX\Model\Data\DataItem;
class OrdersController{
	private static $request, $lang;
	public function __construct($request, $lang = null) {
		self::$request = $request;
		self::$lang = $lang;
	}
	public function actionList(){
		$dataOrder = new DataOrder();
		$ValueFilter = ["id"=>"","customer"=>"","item"=>"","status"=>""];
		$limit = "0,".application::MAX_PERPAGE;
		$totalOrders = DAOOrder::Count();
		$listIdCustomer = [];
		$page_no = 1;
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['search'])) {
				#$model->Ordername['in'] = ["admin", "departament"];
				#$model->email['or'] = ["departament@creativex.ro", "23"];
				if(isset($params['Model']['search']['customer'])){
					if($params['Model']['search']['customer'] != ""){
						$getCustomer = DAOCustomer::FindCustomerByName($params['Model']['search']['customer']);
						//\Debugger::log($getCustomer);die;
						foreach($getCustomer as $idCustomer){
							array_push($listIdCustomer,$idCustomer->id);
						}
						
						if(sizeof($listIdCustomer) > 0 ){
							$dataOrder->id_customer['in'] = $listIdCustomer;
						}
					}					
				}
				
				if(isset($params['Model']['search']['item']))
					if($params['Model']['search']['item'] != "")
						$dataOrder->id_item = is_numeric($params['Model']['search']['item']) ? $params['Model']['search']['item'] : 0;
				if(isset($params['Model']['search']['status']))
					if($params['Model']['search']['status'] != "")
						$dataOrder->status = $params['Model']['search']['status'];
				$ValueFilter = \Utils::GetValueFromModel($ValueFilter,$params['Model']['search']);
			}
			if(isset($params['Model']['pagination'])){
				if(is_numeric($params['Model']['pagination'])){
					if($params['Model']['pagination'] < 1){
						$page_no = 1;
					}else{
						$page_no = $params['Model']['pagination'];
					}
					$page_no -= 1;
					$limit = ($page_no * application::MAX_PERPAGE).",".application::MAX_PERPAGE;
				}
			}

		}
		DAOOrder::$order = "id ASC";
		
		$Orders = DAOOrder::FindAll($dataOrder,$limit);
		
		$OrderList = "";
		if($Orders){
			foreach($Orders as $Order){
				$customer =  DAOCustomer::FindOrderCustomer($Order->id_customer);
				$items = DAOItem::FindAll("id IN(".$Order->id_item.")");
				$item = [];
				foreach($items  as $itemChild){
					$item[] = $itemChild->name;
				}
				$child = new View( ["order/list_item","admin"],
						  [
							"order"=>$Order,
							"customer"=>($customer) ? $customer : "",
							"item"=>($item) ? $item : "",
							"App"=> new application()
						   ]
					  );
				$OrderList .= $child->render();
			}
		}
		$view =  new View( ["order/list","admin"],
						  [
							"totalOrders"=>$totalOrders,
							"items"=>$OrderList,
							"App"=> new application(),
							"filter"=>$ValueFilter,
							"page_no"=>$page_no
						   ]
					  );
        return  $view;
	}
	public function actionEdit(){
		$order = "";
		$message = "";
		$params = "";
		$customer = "";
		$items = "";
		DAOItem::$order = "id ASC";
		$totalItems = DAOItem::FindAll("");
		#\Debugger::Log(self::$request);die;
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && self::$request->Method[1] != "POST"){
				$id = is_numeric($params['Model']) ? $params['Model'] : 0;
				$order = DAOOrder::Find("id=".$id);
				if(!$order) return self::$lang->t('ordernotfound');
				$customer = DAOCustomer::FindOrderCustomer($order->id_customer);
				if(!$customer) $customer = new DataCustomer();
				$items = DAOItem::FindAll("id IN(".$order->id_item.")");
				if(!$items) $items = new DataItem();
			}else{
				
				$success = false;
				$id = is_numeric($params['Model']['id']) ? $params['Model']['id'] : 0;
				$order = DAOOrder::Find("id=".$id);
				if(!$order) return self::$lang->t('ordernotfound');
				if(isset($params['Model']['itemAdd'])  && $params['Model']['itemAdd'] != 0){
					$idItem = is_numeric($params['Model']['itemAdd']) ? $params['Model']['itemAdd'] : 0;
					$item = DAOItem::Find("id=".$idItem);
					if($item) {
						array_push($params['Model']['id_item'],$params['Model']['itemAdd']);
						array_push($params['Model']['quantity'],$params['Model']['quantityAdd']);

						$customer = DAOCustomer::FindOrderCustomer($order->id_customer);
						$dataOrder = new DataOrder();
						$dataOrder->id = $id;
						$dataOrder->id_customer = $order->id_customer;
						$dataOrder->id_item = implode(",",$params['Model']['id_item']);
						$dataOrder->quantity  = implode(",",$params['Model']['quantity']);
						$dataOrder->price  = $params['Model']['price'] + $params['Model']['priceAdd'];
						$dataOrder->status  = $params['Model']['status'];
						$dataOrder->modified_by = $_SESSION['UserId'];
						$dataOrder->modified_at = date("Y-m-d",time());
						
						if(DAOOrder::Update($dataOrder,"id")){
							$success = true;
						}
						if($success)
							\Utils::Redirect("admin/orders/edit/".$order->id,true);
					}
				}
				$items = DAOItem::FindAll("id IN(".$order->id_item.")");
				if(!$items) $items = new DataItem();
				$customer = DAOCustomer::FindOrderCustomer($order->id_customer);
				$dataOrder = new DataOrder();
				$dataOrder->id = $id;
				$dataOrder->id_customer = $order->id_customer;
				$dataOrder->id_item = implode(",",$params['Model']['id_item']);
				$dataOrder->quantity  = implode(",",$params['Model']['quantity']);
				$dataOrder->price  = $params['Model']['price'];
				$dataOrder->status  = $params['Model']['status'];
				$dataOrder->modified_by = $_SESSION['UserId'];
				$dataOrder->modified_at = date("Y-m-d",time());
				if(DAOOrder::Update($dataOrder,"id")){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/orders/edit/".$order->id,true);
			}
			$view =  new View(	["order/edit","admin"],
								["order"=>$order,
								"items"=>$items,
								"totalItems"=>$totalItems,
								"customer"=>$customer,
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params]
					  );
			return  $view;
		}
	}
	public function actionGetItems(){
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			$id = is_numeric($params['Model']['id_item']) ? $params['Model']['id_item'] : 0;
			$item = DAOItem::Find("id=".$id);
			if($item)
				return json_encode(["success"=>true,"value"=>$item->quantity,"price"=>$item->price]);
			else
				return json_encode(["success"=>false,"message"=>self::$lang->t('itemnotfound')]);
		}
		return json_encode(["success"=>false,"message"=>self::$lang->t('itemnotfound')]);
	}
	public function actionDelete(){
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['delete'])){
				$id = is_numeric($params['Model']['delete']) ? $params['Model']['delete'] : 0;
				if(DAOOrder::Find("id=".$id)){
					if(DAOOrder::Delete("id=".$id)){
						return json_encode(["success"=>true]);
					}
					return json_encode(["success"=>false,"message"=>self::$lang->t('usernotfound')]);
				}else{
					return json_encode(["success"=>false,"message"=>self::$lang->t('usernotfound')]);
				}
			}
		}
		exit;
	}
}