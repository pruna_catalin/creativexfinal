<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * DAOOrder: CreativeX && noValue
 * Date: 9/29/2017
 * Time: 1:02 PM
 */
namespace  CreativeX\core\controller\admin;
use CreativeX\Model\Data\DataCustomer;
use CreativeX\Model\DAO\DAOCustomer;
use CreativeX\modules\util\Debugger;
use CreativeX\modules\render\View;
use CreativeX\config\application;
class CustomersController{
	private static $request, $lang;
	public function __construct($request, $lang = null) {
		self::$request = $request;
		self::$lang = $lang;
	}
	public function actionList(){
		$dataUser = new DataCustomer();
		$ValueFilter = ["id"=>"","username"=>"","email"=>"","status"=>""];
		$limit = "0,".application::MAX_PERPAGE;
		$totalUsers = DAOCustomer::Count();
		$page_no = 1;
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['search'])) {
				#$model->username['in'] = ["admin", "departament"];
				#$model->email['or'] = ["departament@creativex.ro", "23"];
				if(isset($params['Model']['search']['email']))
					if($params['Model']['search']['email'] != "")
						$dataUser->email['like'] = ["%".$params['Model']['search']['email']."%"];
				if(isset($params['Model']['search']['username']))
					if($params['Model']['search']['username'] != "")
						$dataUser->username['like'] = ["%".$params['Model']['search']['username']."%"];
				if(isset($params['Model']['search']['status']))
					if($params['Model']['search']['status'] != "")
						$dataUser->active = is_numeric($params['Model']['search']['status']) ? $params['Model']['search']['status'] : 1; ;
				/*#OPTIONALSTART
				if(isset($this->params['Model']['orderTable'])) {
				if(isset($this->params['Model']['orderTable']['id'])) {
				$model->orderBy("id ".$this->params['Model']['orderTable']['id']);
				}
				}
				 */#OPTIONALEND
				$ValueFilter = \Utils::GetValueFromModel($ValueFilter,$params['Model']['search']);
				#\Debugger::log($ValueFilter);
			}
			if(isset($params['Model']['pagination'])){
				if(is_numeric($params['Model']['pagination'])){
					if($params['Model']['pagination'] < 1){
						$page_no = 1;
					}else{
						$page_no = $params['Model']['pagination'];
					}
					$page_no -= 1;
					$limit = ($page_no * application::MAX_PERPAGE).",".application::MAX_PERPAGE;
				}
			}

		}
		DAOCustomer::$order = "id ASC";
		
		#\Debugger::log($limit);
		$users = DAOCustomer::FindAll($dataUser,$limit);
		
		$userList = "";
		if($users){
			foreach($users as $user){
				$child = new View( ["user/list_item","admin"],
						  [
							"user"=>$user,
							"App"=> new application()
						   ]
					  );
				$userList .= $child->render();
			}
		}
		$view =  new View( ["user/list","admin"],
						  [
							"totalUsers"=>$totalUsers,
							"items"=>$userList,
							"App"=> new application(),
							"filter"=>$ValueFilter,
							"page_no"=>$page_no
						   ]
					  );
        return  $view;
	}
}