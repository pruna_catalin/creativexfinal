<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * DAOUsers: CreativeX && noValue
 * Date: 9/29/2017
 * Time: 1:02 PM
 */
namespace  CreativeX\core\controller\admin;
use CreativeX\Model\DAO\DAOUsers;
use CreativeX\Model\DAO\DAOGroup;
use CreativeX\Model\DAO\DAOProfile;
use CreativeX\Model\Data\DataUser;
use CreativeX\Model\Data\DataProfile;
use CreativeX\modules\util\Debugger;
use CreativeX\modules\render\View;
use CreativeX\config\application;
class UserController{
	private static $request, $lang;
	public function __construct($request, $lang = null) {
		self::$request = $request;
		self::$lang = $lang;
	}
	public function actionList(){
		//username,email,nume,password
		$dataUser = new DataUser();
		$ValueFilter = ["id"=>"","username"=>"","email"=>"","status"=>""];
		$limit = "0,".application::MAX_PERPAGE;
		$totalUsers = DAOUsers::Count();
		$page_no = 1;
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['search'])) {
				if(isset($params['Model']['search']['email']))
					if($params['Model']['search']['email'] != "")
						$dataUser->email['like'] = ["%".$params['Model']['search']['email']."%"];
				if(isset($params['Model']['search']['username']))
					if($params['Model']['search']['username'] != "")
						$dataUser->username['like'] = ["%".$params['Model']['search']['username']."%"];
				if(isset($params['Model']['search']['status']))
					if($params['Model']['search']['status'] != "")
						$dataUser->active = is_numeric($params['Model']['search']['status']) ? $params['Model']['search']['status'] : 1; ;$ValueFilter = \Utils::GetValueFromModel($ValueFilter,$params['Model']['search']);			
			}
			if(isset($params['Model']['pagination'])){
				if(is_numeric($params['Model']['pagination'])){
					if($params['Model']['pagination'] < 1){
						$page_no = 1;
					}else{
						$page_no = $params['Model']['pagination'];
					}
					$page_no -= 1;
					$limit = ($page_no * application::MAX_PERPAGE).",".application::MAX_PERPAGE;
				}
			}

		}
		DAOUsers::$order = "id ASC";
		#\Debugger::log($limit);
		$users = DAOUsers::FindAll($dataUser,$limit);
		$userList = "";
		if($users){
			foreach($users as $user){
				$child = new View( ["user/list_item","admin"], ["user"=>$user,"App"=> new application()]);
				$userList .= $child->render();
			}
		}
		$view =  new View( ["user/list","admin"], ["totalUsers"=>$totalUsers,"items"=>$userList,"App"=> new application(),"filter"=>$ValueFilter,"page_no"=>$page_no]);
        return  $view;
	}
	public function actionDelete(){
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['delete'])){
				$id = is_numeric($params['Model']['delete']) ? $params['Model']['delete'] : 0;
				if(DAOUsers::Find("id=".$id)){
					if(DAOUsers::Delete("id=".$id)){
						return json_encode(["success"=>true]);
					}
					return json_encode(["success"=>false,"message"=>self::$lang->t('usernotfound')]);
				}else{
					return json_encode(["success"=>false,"message"=>self::$lang->t('usernotfound')]);
				}
			}
		}
		exit;
	}
	public function actionEdit(){
		$user = "";
		$profile = "";
		$message = "";
		$group = "";
		$params = "";
		$group = DAOGroup::FindAll('');
		
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && self::$request->Method[1] != "POST"){
				$id = is_numeric($params['Model']) ? $params['Model'] : 0;
				$user = DAOUsers::Find("id=".$id);
				if(!$user) return self::$lang->t('usernotfound');
				$profile = DAOProfile::Find("id=".$user->id_profile);
				
				if(!$profile) $profile = new DataProfile();
			}else{
				$id = is_numeric($params['Model']['id']) ? $params['Model']['id'] : 0;
				$user = DAOUsers::Find("id=".$id);
				if(!$user) return self::$lang->t('usernotfound');
				$dataUser = new DataUser();
				$dataUser->id = $params['Model']['id'];
				$dataUser->id_profile = $user->id_profile;
				$dataUser->id_group = is_numeric($params['Model']['id_group']) ? $params['Model']['id_group'] : $user->id_group;

				if(!DAOUsers::Find("username='".$params['Model']['username']."' AND id !=".$id)){
					$dataUser->username = $params['Model']['username'];
				}else{
					$dataUser->username = $user->username;
					$message = "User is already exist";
				}
				if(!DAOUsers::Find("email='".$params['Model']['email']."' AND id !=".$id)){	
					$dataUser->email = $params['Model']['email'];
				}else{
					$dataUser->email = $user->email;
					$message = "Email is already exist";
				}
				$dataUser->active = is_numeric($params['Model']['active']) ? $params['Model']['active'] : 0;
				$dataUser->reload_permisions = 1;
				$dataUser->password = ($params['Model']['password'] != "") ? \Utils::HashPassword($params['Model']['password']) : $user->password;
				$profile = DAOProfile::Find("id=".$user->id_profile);
				
				if(!$profile) $profile = new DataProfile();
				$success = false;
				if(DAOUsers::Update($dataUser,"id")){
					$success = true;
				}
				$profile->id = $user->id_profile;
				$profile->name =  $params['Model']['name'];
				$profile->skill = $params['Model']['skill'];
				$profile->language = $params['Model']['language'];
				if(DAOProfile::Update($profile,"id")){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/users/edit/".$user->id,true);

				//\Utils::Debug(self::$request);
			}					
		}
		$view =  new View(	["user/edit","admin"],
								["user"=>$user,
								"profile"=>$profile,
								"message"=>$message,
								"group"=>$group,
								"App"=> new application(),
								"params"=>$params]
					  );
		return  $view;
	}
	public function actionAdd(){
		$user = "";
		$profile = "";
		$message = "";
		$group = "";
		$params = "";
		$userid= 0;
		$group = DAOGroup::FindAll('');
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && $params['Model'] != ""){
				$dataUser = new DataUser();
				$check = false;
				$success = false;
				
				if(!DAOUsers::Find("username='".$params['Model']['username']."'")){

					$dataUser->username = $params['Model']['username'];
				}else{
					$message = "User is already exist";
					$check = true;
				}
				if(!DAOUsers::Find("email='".$params['Model']['email']."'")){	
					$dataUser->email = $params['Model']['email'];
				}else{
					$message = "Email is already exist";
					$check = true;
				}

				if(!$check){
					$profile = new DataProfile();
					$profile->name =  $params['Model']['name'];
					$profile->skill = $params['Model']['skill'];
					$profile->language = $params['Model']['language'];
					$profile_id = DAOProfile::Insert($profile);
					if($profile_id){
						$dataUser->id_profile = $profile_id;
						$dataUser->id_group = is_numeric($params['Model']['id_group']) ? $params['Model']['id_group'] : 0;

						$dataUser->active = is_numeric($params['Model']['active']) ? $params['Model']['active'] : 0;
						$dataUser->reload_permisions = 1;
						$dataUser->password = ($params['Model']['password'] != "") ? \Utils::HashPassword($params['Model']['password']) : md5("password");
						$user = DAOUsers::Insert($dataUser);
						if($user){
							$userid = $user;
							$success = true;
						}
					}
					


				}
				if($success)
					\Utils::Redirect("admin/users/edit/".$userid,true);
			}					
		}
		$view =  new View(	["user/new","admin"],
								["user"=>"",
								"profile"=>"",
								"message"=>$message,
								"group"=>$group,
								"App"=> new application(),
								"params"=>$params
							]
					  );
		return  $view;
	}
}