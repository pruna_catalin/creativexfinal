<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 10/4/2017
 * Time: 8:35 AM
 */

namespace CreativeX\modules\storage;
class Storage{
	/* 
	 * Can check and create new session
	 * @return true if is valid session
	 */
	public function Session($name = NULL, $value =  NULL){
		$result = false;
		if(isset($_SESSION)){
			if($name != NULL && $value =  NULL){
				if(isset($_SESSION[$name])){
					return $_SESSION[$name];
				}else{
					return false;
				}
			}else{
				if($value != NULL){
					$_SESSION[$name] =  $value;
					return true;
				}
			}
		}
		return $result;
	}
	/* 
	 *
	 * @return array(object)/json list
	 * 
	 */
	public function AllSession($type = "array"){
		$result =["name",];
		if($type == "array"){
			foreach($_SESSION as $session){
				
			}
		}
		return $result;
	}
	/* 
	 *
	 * @return true if session was delete
	 */
	public function DeleteSession($name = NULL,$clear = false){
		$result = false;
		if(isset($_SESSION)){
			if($name != NULL && !$clear){
				if(isset($_SESSION[$name])){
					unset($_SESSION[$name]);
					return true;
				}
			}else if($name != NULL){
				$_SESSION[$name] = "";
				return true;
			}
		}
		return $result;
	}
	/* 
	 *
	 * @return true if all sessions was delete
	 */
	public function DeleteAllSession(){
		$result = false;
		if(isset($_SESSION)){
			session_destroy();
			return true;
		}
		return $result;
	}
}