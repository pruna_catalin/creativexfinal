<?php
namespace CreativeX\modules\migration;
use CreativeX\modules\util\Debugger;
/**
 * ParseSQL short summary.
 *
 * ParseSQL description.
 *
 * @version 1.0
 * @author CreativeX
 */
class ParseSQL extends Migration{
	private static $Tables = [];
	private static $TableStructure = [];
	private static $Columns = [];
	private static $ColumnStructure = []; 
	public static function Start(){
		self::GetTables();
		self::GetColumns();
	}
	
	/*
	 * Get All Information for each table find in current database
	 */
	public static function GetTables(){
		$sql = self::DB();
		$localTables = [];
		$sql->select("* from information_schema.TABLES where  table_schema = '".$sql->getDatabase()."'")->prepare();
		$result = $sql->execute()->fetchAll(\PDO::FETCH_OBJ);
		self::$Tables = $result;
		for($i=0;$i<sizeof(self::$Tables);$i++){
			array_push($localTables,self::$Tables[$i]);
			self::SetInformationTable(self::$Tables[$i]);
		}
		return $localTables;
	}
	/*
	 * Set information for each table
	 */
	private static function SetInformationTable($table){
		$structure = ["table_name"=>(isset($table->TABLE_NAME) ? $table->TABLE_NAME : ""),"engine"=>(isset($table->ENGINE) ? $table->ENGINE: "") ,"table_collation"=>(isset($table->TABLE_COLLATION) ? $table->TABLE_COLLATION : "")];
		self::$TableStructure[] = $structure;
	}

	/*
	 * Get All Information for each column find in current database
	 */
	private static function GetColumns(){
		
		for($i = 0;$i < sizeOf(self::$TableStructure); $i++){
			$sql = self::DB();
			$sql->select("* from information_schema.COLUMNS where  table_schema = '".$sql->getDatabase()."' AND table_name= '" . self::$TableStructure[$i]['table_name'] . "'")->prepare();
			$result = $sql->execute()->fetchAll(\PDO::FETCH_OBJ);
			self::$Columns = $result;
			for($j=0;$j< sizeOf(self::$Columns);$j++){
				self::$ColumnStructure[] = self::SetInformationColumns(self::$Columns[$j],self::$TableStructure[$i]['table_name']);
			}
			//Debugger::Log(self::$Columns);die;
		}
	}
	public static function GetColumnsFromTable($table){
		$columns = [];
		$sql = self::DB();
		$sql->select("* from information_schema.COLUMNS where  table_schema = '".$sql->getDatabase()."' AND table_name= '" . $table."'")->prepare();
		$result = $sql->execute()->fetchAll(\PDO::FETCH_OBJ);
		self::$Columns = $result;
		for($j=0;$j< sizeOf(self::$Columns);$j++){
			array_push($columns,self::$Columns[$j]);
		}
		return $columns;
	}
	private static function SetInformationColumns($column,$table){
		
		$relationTable = self::GetRelations($table);
		$structure = [	
						"column_name"=>(isset($column->COLUMN_NAME)? $column->COLUMN_NAME : ""),
						"is_null"=>(isset($column->IS_NULLABLE) ? $column->IS_NULLABLE : ""),
						"data_type"=>(isset($column->DATA_TYPE) ? $column->DATA_TYPE : "") ,
						"lenght"=>(isset($column->CHARACTER_MAXIMUM_LENGTH)  ? $column->CHARACTER_MAXIMUM_LENGTH : ""),
						"column_key"=>(isset($column->COLUMN_KEY) ? $column->COLUMN_KEY : ""),
						"relation"=>(sizeof($relationTable) > 0 ? self::CheckRelationType($relationTable,$column) : ""),
						"collation"=>(isset($column->COLLATION)  ? $column->COLLATION : "NONE") ,
						"extra"=>(isset($column->EXTRA) ? $column->EXTRA : ""),
						"privileges"=>(isset($column->PRIVILEGES) ? $column->PRIVILEGES :  "") 
					];
		 
		
		return $structure;
	}
	private static function CheckRelationType($tableRel,$column){
		$getRel = self::hasRelation($tableRel, $column);

		if($getRel == null) {
			if($column->COLUMN_KEY == "UNI"){
				return "one-to-one";
			}
			if($column->COLUMN_KEY == "MUL") {
				return "one-to-many";
			}

		} else {
			foreach($tableRel as $rel) {
				if($rel->column == $column->COLUMN_NAME) {
					if($column->COLUMN_KEY == "PRI" &&  $rel->column_key == "PRI"){
						if($column->COLUMN_KEY != "UNI" && $rel->column_key != "UNI")
							return "many-to-many";
					}
				}
			}
			return "ToBeDefined";
		}
		return "ToBeDefined";
	}
	private static function hasRelation($tableRel,$column){
		foreach($tableRel as $rel){
			if($rel->column == $column->COLUMN_NAME){
				return $rel;
			}
		}
		return null;
	}
	/*
	 * Get all relations from database (inherit from interface)
	 * return @list object with all relations
	 */
	public static function GetRelations($table = ""){
		$db = self::DB();
		if($table != ""){
			$db->select("t1.*,t2.*")->from("information_schema.REFERENTIAL_CONSTRAINTS as t1 left join information_schema.KEY_COLUMN_USAGE as t2 ON t1.TABLE_NAME = t2.TABLE_NAME AND t1.CONSTRAINT_NAME =  t2.CONSTRAINT_NAME ")->where("t1.CONSTRAINT_SCHEMA ='".$db->getDatabase()."' AND t1.TABLE_NAME='".$table."'")->prepare();
		}else{
			$db->select("*")->from("information_schema.KEY_COLUMN_USAGE")->where("CONSTRAINT_SCHEMA ='".$db->getDatabase()."'")->prepare();
		}
		$sql = $db->execute();
		$result = [];
		foreach($sql->fetchAll(\PDO::FETCH_OBJ) as $table) {
			$obj = (object)["table" => $table->TABLE_NAME,"column"=>$table->COLUMN_NAME, "reference_table" => $table->REFERENCED_TABLE_NAME,"reference_column" => $table->REFERENCED_COLUMN_NAME, "onupdate" => (isset($table->UPDATE_RULE) ? $table->UPDATE_RULE : "") , "ondelete" => (isset($table->DELETE_RULE)?$table->DELETE_RULE: ""), "constraint_name" => $table->CONSTRAINT_NAME];
			$result[] = $obj;
		}

		return $result;
	}
}