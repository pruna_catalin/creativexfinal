<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 9/29/2017
 * Time: 1:04 PM
 */
namespace  CreativeX\modules\language;
use CreativeX\modules\language\i18n\en;
class Language
{
	public   function t($text){
		if(isset($_SESSION['language'])){
			$language = "CreativeX\\modules\\language\\i18n\\".$_SESSION['language'];
			$translateFile = new $language();
			if(isset($translateFile->translate->{$text})){
				return $translateFile->translate->{$text};
			}else{
				return "Translation Not found ";
			}
		}else{
			$translateFile = new en();
			if(isset($translateFile->translate->{$text})){
				return $translateFile->translate->{$text};
			}else{
				return "Translation Not found ";
			}
		}
        
    }
}