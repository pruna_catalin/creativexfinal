<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 9/29/2017
 * Time: 1:04 PM
 */

namespace CreativeX\modules\language\i18n;


class en
{
	public $translate;
	public function __construct() {
		$this->translate = (object)array(
										//ADD BUTTONS
										"add_button"=>"Add",
										"add_menu"=>"Add Menu",
										"add_group"=>"Add Group",
										"add_user"=>"Add User",
										"add_files"=>"Add File",
										"add_forum"=>"Add Forum",
										"add_blog"=>"Add blog",
										"add_category"=>"Add Category",
										"generate"=>"Generate",
										//EDIT BUTTONS
										"edit_button"=>"Edit",
										//DELETE BUTTONS
										"delete_button"=>"Delete",
										"delete_image"=>"Remove Image",
										//SAVE BUTTONS
										"save_button"=>"Save",
										//UPDATE BUTTONS
										"update_button"=>"Update",
										//EXPORT BUTTONS
										"export_button"=>"Export",
										"export_excel"=>"Export to Excel",
										"export_csv"=>"Export to CSV",
										"export_xml"=>"Export to XML",
										//SUBMIT BUTTONS
										"submit_button"=>"Submit",
										//SEARCH BUTTONS
										"search_button"=>"Search",
										//RESET BUTTONS
										"reset_button"=>"Reset",
										//VIEW  BUTTONS
										"view_button"=>"View",
										"view_buttonPDF"=>"View PDF",
										//More Info BUTTONS
										"more_info"=>"More Info",
										//OPTIONS YES/NO
										"option_yes"=>"Yes",
										"option_no"=>"No",
										//LIST TABLES
										"list"=>"List",
										"list_backuppermision"=>"Backuppermision list",
										"list_ncategory"=>"Nblogcategory list",
										"list_ncity"=>"City list",
										"list_nconstant"=>"Nconstant list",
										"list_nfile"=>"Nfile list",
										"list_nimage"=>"Nimage list",
										"list_nmenu"=>"Nmenu list",
										"list_nmirror"=>"Nmirror list",
										"list_nnews"=>"Nnews list",
										"list_nnewsletter"=>"Nnewsletter list",
										"list_npages"=>"Npages list",
										"list_nprovince"=>"Nprovince list",
										"list_nslider"=>"Nslider list",
										"list_ntype"=>"Ntype list",
										"list_page"=>"Pages List",
										"list_file"=>"File List",
										"list_slider"=>"Slider List",
										"list_menu"=>"Menu List",
										"list_user"=>"User List",
										"list_users"=>"Users List",
										"list_city"=>"City",
										"list_permisions"=>"Permisions List",
										"list_group"=>"Group List",
										"list_forum"=>"Forum List",
										"list_blog"=>"Blog List",
										"list_forums"=>"List forums",
										"list_blog_category"=>"List category blog",
										"list_orders"=>"List Orders",
										"list_customers"=>"List Customers",
										//NOTE 
										"note"=>"NOTE: ",
										"note_delete"=>"All delete actions are permantly",
										"note_support_image"=>"Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only ",
										"note_forms_request"=>" ( All the fields are requested * )",
										//SELECT TEXT
										"select_image"=>"Select Image",
										"select_combo"=>"Select Combo",
										//CHANGE TEXT
										"change_image"=>"Change Image",
										//MESSAGE CUSTOM
										"usernotfound"=>"User Not Found!",
										//FIELD
										"password"=>"Password",
										"same_password"=>"Same Password",
										"privacy_settings"=>"Privacy Settings ",
										"first_name"=>"First Name",
										"last_name"=>"Last Name",
										"mobile_number"=>"Mobile Number",
										"about_me"=>"About Me",
										"username"=>"Username",
										"settings"=>"Settings",
										"global_info"=>"Global Settings",
										"email_settings"=>"Email Settings",
										"security_settings"=>"Security Settings",
										"social_settings"=>"Social Settings",
										"language_front"=>"Language Front",
										"language_admin"=>"Language Admin",
										"maintenance"=>"Maintenance",
										"title"=>"Title",
										"sub_title"=>"Sub title",
										"name_group"=>"Group Name",
										"users"=>"Users",
										"wiki"=>"Wiki",
										"forum"=>"Forum",
										"group"=>"Group",
										"email"=>"Email",
										"profile_account"=>"Profile Account",
										"info"=>"Info",
										"name"=>"Name",
										"parent"=>"Parent",
										"description"=>"Description",
										"category"=>"Category",
										"blog"=>"Blog",
										"presentation"=>"CreativeX Framework",
										"status"=>"Status",
										"active"=>"Active",
										"skill"=>"Skill",
										"language"=>"Language",
										"actions"=>"Actions",
										//DETAILS FOR SMTP
										"host_smtp"=>"Host SMTP",
										"port_smtp"=>"Port SMTP",
										"secure_smtp"=>"Secure SMTP",
										"email_system"=>"Email System",
										"name_email_system"=>"Name for Email",
										"subject_email_system"=>"Subject for Email",
										"body_email_system"=>"Body for Email",
										//BREADCRUMB
										"home"=>"Home",
										//MESSAGES
										"messages"=>"Messages",
										"message_system"=>"System Message",
										"message_add_success"=>"Add Success Message ",
										"message_add_error"=>"Oops!  there is a problem! ",
										"message_edit_success"=>"Edit occured with success ",
										"message_edit_error"=>"Can't edit this! ",
										"message_delete_question"=>"Are you sure want delete this?",
										"message_delete_success"=>"Delete Success",
										"message_delete_error"=>"Oops!  there is a problem!",
										"message_formular_success"=>"Message was sent successfully",
										//TABLE COLUMNS 
										"table_column_id"=>"Id",
										"table_column_id_parent"=>"Id Parent",
										"table_column_name"=>"Name",
										"table_column_order"=>"Order",
										"table_column_url_seo"=>"Url Seo",
										"table_column_actions"=>"Actions",
										"table_column_username"=>"Username",
										"table_column_groupname"=>"Group Name",
										//FORM
										"form_name"=>"Name",
										"form_error_firstname"=>"Insert Name",
										"form_error_lastname"=>"Insert Last Name",
										"form_error_phonemobile"=>"Mobile Phone",
										"form_error_ocupation"=>"Occupation",
										"form_error_aboutme"=>"Insert About me",
										"form_error_image"=>"Image",
										"form_error_password"=>"Insert Password",
										"form_error_samepassword"=>"The Pasword it's not the same",
										"form_error_type"=>"Select type",
										"form_error_name"=>"Insert Name",
										"form_error_username"=>"Insert Username",
										"form_error_description"=>"Insert Description",
										"form_error_phone"=>"Insert Phone Number",
										"form_error_email"=>"Insert Email",
										"form_error_privacy"=>"You must agree to Text Privacy",
										"form_error_information"=>"You must accept the information",
										"form_error_incorectemail"=>"Incorrect Email",
										"form_error_city"=>"Insert City",
										"form_error_name_already_exists"=>"Name is already used",
										//social facebok,twiter etc..
										"social_facebook"=>"Facebook",
										"social_instagram"=>"Instagram",
										"social_twiter"=>"Twiter",
										"social_linkedin"=>"Linkedin",
										//FOOTER
										"footer_details"=>"Footer Details",
										//MENU
										"menu_file_share"=>"File Share",
										"menu_home"=>"Home",
										"menu_permision"=>"Permisions",
										"total_order_paid"=>"Total Order Paid",
										"total_orders_paid"=>"Total Orders Paid",
										"total_order"=>"Total Order",
										"total_orders"=>"Total Orders",
										"total_customers"=>"Total Customers",
										"user_registration"=>"User Registrations",
										"quantity"=>"Quantity",
										"price"=>"Price",
										"price_total"=>"Total Price",
										"total"=>"Total",
										"select"=>"Select",
										"logout"=>"Logout",

										"bill_info"=>"Info Bill",
										"product_list"=>"Product List",
										"customer_name"=>"Customer Name",
										"customer_address"=>"Customer Address",
										"customer_email"=>"Customer Email",
										"customer_phone"=>"Customer Phone",
										"customer_company"=>"Customer Company",
										//GENERATOR
										"ModelData"=>"Model Data",
										"DAO"=>"DAO",
										"Controller"=>"Controller",
										"Loader"=>"Loader",
										"Loader"=>"Loader",
										"JS"=>"JS",
										"CSS"=>"CSS",
										"Route"=>"Route",
										"Destination"=>"Destination"
										);
	}
}