<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 10/4/2017
 * Time: 8:35 AM
 */

namespace CreativeX\modules\request;
use CreativeX\config\route;
use CreativeX\config\application;

class request {
	public $Case = "";
	public $Controller = "";
	public $Action = "";
	public $Folder = "";
	public $Method = "";
	public $RequestType = "";
	public $Args = [];
}

class RequestParser extends request
{
	public function __construct() {
		$this->Init();
	}

    private function Init(){
		//\Utils::Debug($_SERVER);
		$requestXHR = !is_null(\Utils::GetIfExistsOrDefault('HTTP_X_REQUESTED_WITH', $_SERVER, null,true));
		if(!isset($_GET['case'])){
			$_GET['case'] = "blank";
		}
        if(isset($_GET['case'])){
			
            if(sizeof($_GET['case']) > 0 ){
                foreach(route::$Urls as $key => $route){
					if(sizeof($route) < 4) {
						throw new \TException("Route '". print_r($route, true) ."' has an invalid number of arguments.",
							11100, \ErrorLevel::EL_NOTICE);
					}
					
                    $string = "";
                    $regex =  "";
                    if(preg_match("/\{(.*)\}/",$key,$findKey)){
						
                        $string = $findKey[1];
                        if(preg_match("/\}(.*)/",$key,$findRegex)){
                            $regex = $findRegex[1];
                        }
						
						if( ($regex == "" && $string == str_replace("/","\/",$_GET['case'])) ||
							(!empty($regex) && preg_match("/".$string.$regex."/",$_GET['case'])) ) {

							$argsRegex = preg_match("/".$regex."/",$_GET['case'] , $find) ? array("Model" =>$find[0]) : "";
							
							if(\Utils::GetIfExistsOrDefault(3, $route, null, true) == "XHR" && !$requestXHR) die;
							$this->Case = str_replace("\\","",$string);
							$this->Controller = $route[0];
							$this->Action = $route[1];
							$this->Folder = $route[2];
							$this->Method = [$route[3],$_SERVER['REQUEST_METHOD']];
							$this->Args = isset($_POST) ? (!empty($_POST) ? $_POST : $argsRegex) : [];
							#echo $this->Action ;die;
						}
                    }
                }
            }
        }
    }

}