<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * DAOUsers: CreativeX && noValue
 * Date: 10/9/2017
 * Time: 9:23 AM
 */

namespace CreativeX\modules\render;
use CreativeX\config\application;
use CreativeX\modules\language\Language;
class View
{
    const DEFAULT_TEMPLATE = "default.cx";

    protected $template = self::DEFAULT_TEMPLATE;
    public $fields = array();
    protected $addChilds = array();
    public function __construct($template = [],  $fields = []) {
        if (!empty($fields)) {
            foreach ($fields as $name => $value) {
                $this->$name = $value;
            }
        }
        if (sizeof($template) == 2) {
            $this->template = $template;
        }else{
            \Debugger::log("Directory load is not specify for this template" , "Warning");
        }
    }
    public function add($addChilds){

    }
    public function render() {
        $application =  new application();
		$this->fields['Language'] = new Language();
        if (sizeof($this->template) == 2) {
            $template = $this->template;
			$tmpRender  = "";
            if ($template[1] == "frontend") {
                $tmpRender = "./app/themes/" . $application::FRONT_THEME . "/templates/" . $template[0] . ".tpl";
            } else {
                $tmpRender = "./app/themes/" . $application::ADMIN_THEME . "/templates/" . $template[0] . ".tpl";
            }
            if (!is_file($tmpRender) || !is_readable($tmpRender)) {
                \Debugger::log("use " . $tmpRender, "Warning");
            }
            extract($this->fields);
            ob_start();
			include $this->evalSintax($tmpRender);
           // $content = Utils::ReadFile($template);
			return ob_get_clean();
			//return $content; // ob_get_clean();
        }
    }

    public function __set($name, $value) {
        $this->fields[$name] = $value;
        return $this;
    }

    public function __get($name) {
        if (!isset($this->fields[$name])) {
            \Debugger::log("Unable to get the field ". $this->fields[$name] , "Warning");
        }
        $field = $this->fields[$name];
        return $field instanceof Closure ? $field($this) : $field;
    }

    public function __isset($name) {
        return isset($this->fields[$name]);
    }

    public function __unset($name) {
        if (!isset($this->fields[$name])) {
            \Debugger::log("Unable to get the field ". $this->fields[$name] , "Warning");
        }
        unset($this->fields[$name]);
        return $this;
    }

//    public function render() {
//        $this->fields["translate"] = new Lang();
//        $this->fields["Application"] = new App();
//        extract($this->fields);
//        ob_start();
//        $this->evalSintax($this->template);
//        include $this->evalSintax($this->template);
//        return ob_get_clean();
//    }
    private function evalSintax($template){
        $parseTemplate = $template;
		$application =  new application();
        $content = \Utils::ReadFile($template);
        $content = str_replace(array("{@","@}"), array("<?php echo "," ?>"), $content);
        $content = str_replace("@InjectHtmlElements", "<?php use CreativeX\modules\html\Html as Html;  ?>", $content);
        $content = str_replace("@ProtectCreativeX", "<?php (!defined('CreativeX')) ? exit() : '';  ?>", $content);
        \Utils::WriteToFile($application::COMPILE_TMP."/Tmp.php", $content);
        return $application::COMPILE_TMP."/Tmp.php";

    }
}