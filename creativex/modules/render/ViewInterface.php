<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 10/11/2017
 * Time: 12:23 PM
 */

namespace CreativeX\modules\render;


interface ViewInterface
{
    public function setTemplate($template);
    public function __set($field, $value);
    public function __get($field);
    public function __isset($field);
    public function __unset($field);

}
