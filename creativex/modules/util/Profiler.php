<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 9/29/2017
 * Time: 1:05 PM
 */

class Timer {
    public $_StartTime = 0, $_Length = 0;
    public $_Profile = null;

    function __construct($start, $len) {
        $this->_StartTime = $start; $this->_Length = $len;
    }
}
class Profiler {
    private $_Flag = false, $_ForceStop = false;
    public $_StartTime = 0;
    public $_StartLine = 0, $_EndLine = 0;
    public $_FileName = "", $_ClassPath = "";

    function __construct() {
        $dbg = debug_backtrace();
        $call = array_shift($dbg);
        $this->_FileName = $call['file'];
        $this->_StartLine = $call['line'];
        $dbg = array_shift($dbg);
        if(!empty($dbg['class']) && !empty($dbg['type'])) {
            $this->_ClassPath .= "(";
            if($dbg['type'] == '::') $this->_ClassPath .= "static ";
            if($dbg['type'] == '->') $this->_ClassPath .= "object ";
            $this->_ClassPath .= $dbg['class'];
            $this->_ClassPath .= ")";
            $this->_ClassPath .= $dbg['type'];
        }

        $this->_ClassPath .= $dbg['function'];

        $this->_StartTime = microtime(true);
    }

    function __destruct() {
        if(!$this->_Flag) {
            $Timer = new Timer($this->_StartTime, microtime(true) - $this->_StartTime);
            $Timer->_Profile = $this;
            array_push(Debugger::$Timers, $Timer);

            $dbg = debug_backtrace();
            $call = array_shift($dbg);
            if($this->_ForceStop) $call = array_shift($dbg);
            $this->_EndLine = $call['line'];

        }
    }

    function D() {
        $this->_ForceStop = true;
        $this->__destruct();
        $this->_Flag = true;
    }
}