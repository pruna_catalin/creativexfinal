<?php

class ErrorLevel {
	const
	EL_NONE = 0,
	EL_NOTICE = 1,
	EL_DEPRECATED = 2,
	EL_PARSE = 3,
	EL_WARNING = 4,
	EL_EXCEPTION = 5,
	EL_ERROR = 6,
	EL_UNKNOWN = 7;
}

class TException extends \Exception {
	public $TLevel, $TCode, $TMessage, $TFile, $TLine, $TTrace;
	function __construct($errstr, $errcode = 0, $errlevel = ErrorLevel::EL_EXCEPTION, $errtrace = NULL, $errfile = NULL, $errline = NULL) {
		$this->TLevel = $errlevel;
		$this->TCode = $errcode;
		$this->TMessage = $errstr;

		if($errfile == NULL) $this->TFile = $this->file;
		else $this->TFile = $errfile;

		if($errline == NULL) $this->TLine = $this->line;
		else $this->TLine = $errline;

		if($errtrace == NULL) $errtrace = $this->getTrace();
		
		array_unshift($errtrace, 
			array('function'=> '_error_handler', 
				'file'=> $this->TFile, 
				'line'=> $this->TLine
			)
		);
		$this->TTrace = $errtrace;
	}
}

class Debugger {
	private static $Logs = array();
	public static $ModeOn = false;
	public static $StackTrace = true;
	public static $OneError = false;

	public static $Timers = array();
	
	public static function Log($message, $title = "") {
		$log_message = "";
		if(is_array($message)) {
			$log_message = print_r($message, true);
		} else if(is_bool($message)) {
			$log_message = $message ? "TRUE" : "FALSE";
		} else if(is_object($message)) {
			$log_message = print_r($message, true)."";
		} else if(is_null($message)) {
			$log_message = "NULL";
		} else {
			$log_message = $message;
		}

		$caller = debug_backtrace();
		$caller = array_shift($caller);

		$file = ''; $line = '';
		if(!empty($caller)) {
			$file =  $caller['file'];
			$line =  $caller['line'];
		} 
		self::log_message(ErrorLevel::EL_NOTICE, 0, self::GetBasePath($file, BASE_DIR), $line, $log_message, $title);
	}

	public static function _exception_handler($ex) {
		if($ex instanceof TException) {
			self::_handler($ex->TLevel, $ex->TCode, $ex->TFile, $ex->TLine, $ex->TMessage, $ex->TTrace);
		} else {
			$trace = $ex->getTrace();
			array_unshift($trace, 
				array('function'=> '_error_handler', 
					'file' => $ex->getFile(), 
					'line' => $ex->getLine()
				)
			);
			self::_handler(ErrorLevel::EL_EXCEPTION, $ex->getCode(), $ex->getFile(), 
			$ex->getLine(), $ex->getMessage(), $trace);
		}
		
		if(self::$OneError) die;
	}

	public static function _error_handler($err_code, $err_message, $err_file, $err_line, $err_context) {
		$err_level = ErrorLevel::EL_NONE;
		switch ($err_code) {
			case E_ERROR: $err_level = ErrorLevel::EL_ERROR; break;
			case E_WARNING: $err_level = ErrorLevel::EL_WARNING; break;
			case E_PARSE: $err_level = ErrorLevel::EL_PARSE; break;
			case E_DEPRECATED: $err_level = ErrorLevel::EL_DEPRECATED; break;
			case E_NOTICE: $err_level = ErrorLevel::EL_NOTICE; break;
			default: $err_level = ErrorLevel::EL_UNKNOWN; break;
		}

		$trace = debug_backtrace();
		array_shift($trace);
		self::_exception_handler(new TException($err_message, $err_code, $err_level, $trace, $err_file, $err_line));
		if(self::$OneError) die;
	}

	public static function _exit_script_handler() {
		if (self::$ModeOn) {
			$last_error = error_get_last();
			if(!empty($last_error)) {
				$err_code = $last_error['type'];
				$err_file = $last_error['file'];
				$err_line = $last_error['line'];
				$err_message = $last_error['message'];
				
				$trace = debug_backtrace();
				array_shift($trace);
				self::_exception_handler(new TException($err_message, $err_code, ErrorLevel::EL_ERROR, $trace, $err_file, $err_line));
			}

			if(!empty(self::$Logs) > 0) {
				$style = ".debugger {height:50%; background-color: #111;color:#fff; bottom:2%; left:10px; right:10px; position:fixed; margin:0px; margin-right:20px; margin-left:20px; padding-left:20px; padding-right: 20px;box-shadow: 0px 0px 5px #000;border-radius: 10px;border: 2px solid black; white-space: pre-wrap;overflow-y:hidden;z-index:999999;} 
				.debugger:hover { box-sizing:border-box; bottom:2%; height:96%; background-color: #000;cursor: pointer;border: 2px solid red; box-shadow: 0px 0px 25px #000;z-index:999999;overflow-y:auto}";

				if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') { 
					$dbg_content = "<pre id='debugger' class='debugger'>". implode("<br>", self::$Logs) ."<br><br>";

					foreach(self::$Timers as $timer) {
						$profiler = $timer->_Profile;
						$dbg_content .= "In ". self::GetBasePath($profiler->_FileName, BASE_DIR) ."<br>";
						$dbg_content .= "<font color='yellow'>".$profiler->_ClassPath. "</font><br>Between lines <font color='#1E90FF'>". $profiler->_StartLine. "-" .$profiler->_EndLine."</font> took <font color='red'>". round($timer->_Length , 3). "</font>sec<br><br>"; 
					}

					$dbg_content .= "</pre>";
					echo ", \"debugger\"=".json_encode($dbg_content)."}";
				} else {
					echo "<style>".$style."</style>";
					echo "<pre id='debugger' class='debugger'>".implode("<br>", self::$Logs)."<br><br>";

					foreach(self::$Timers as $timer) {
						$profiler = $timer->_Profile;
						echo "In ". self::GetBasePath($profiler->_FileName, BASE_DIR) ."<br>";
						echo "<font color='yellow'>".$profiler->_ClassPath. "</font><br>Between lines <font color='#1E90FF'>". $profiler->_StartLine. "-" .$profiler->_EndLine."</font> took <font color='red'>". round($timer->_Length , 3). "</font>sec<br><br>"; 
					}
					echo "</pre>";
				}
			} else {
				if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') { 
					//echo "}";
				}
			}
		}
	}

	private static function _handler($_err_level, $_err_code, $_err_file, $_err_line, $_err_message, $_err_trace) {		
		if(self::$StackTrace) {
			$inc = 0;
			$message = '';
			foreach($_err_trace as $trace) {
				if(array_key_exists('function', $trace) && array_key_exists('file', $trace)) {
					if($trace['function'] === '_error_handler') {
						self::log_message($_err_level, $_err_code, self::GetBasePath($trace['file'], BASE_DIR), $trace['line'], $_err_message);
						continue;
					}

					$file_name = self::GetBasePath($trace['file'], BASE_DIR); 
					$message .= "[".$inc."] In ". $file_name;
					$message .= " (line <font color='red'>".$trace['line']."</font>)<br>";
					
					$function_class = empty($trace['class']) ? "" : $trace['class'];
					$function_params_values = $trace['args'];
					$function_params_list = self::function_get_params($trace['function'], $function_class);

					$params = array();
					foreach($function_params_list as $var => $def) {
						$raw_val = array_shift($function_params_values);
						$content = self::function_get_param_value($raw_val);
						$s = true;
						$def = self::function_get_default_param($def);
						array_push($params, ($s? "<a><span title='".$content."'>" : "" )."\$".$var.$def ."".($s? "</span></a>" : "" ));
					}

					if(!empty($trace['type']) && !empty($function_class)) {
						$struct_type = "Undefined";
						if($trace['type'] == '->') $struct_type = "<font color='blue'>object</font>";
						else if($trace['type'] == '::') $struct_type = "<font color='orange'>static</font>";

						$message .= " -> (<font color='orange'>".$struct_type."</font> <font color='yellow'>".
						$trace['class']."</font>)".$trace['type']."<font color='cyan'>".$trace['function']."</font>(<font color='purple'>".implode(", ", $params)."</font>)<br>";
					} else {
						$message .= " -> <font color='#1E90FF'>".$trace['function']."</font>(<font color='purple'>".implode(", ", $params)."</font>)<br>";
					}

					$inc++;
				}
			}
			
			if(!empty($message)) self::log_message(ErrorLevel::EL_NONE, 0, null, 0, $message);
		} else {
			self::log_message($_err_level, $_err_code, self::GetBasePath($_err_file, BASE_DIR), $_err_line, $_err_message);
		}
	}
	
	private static function log_message($_err_level, $_err_code, $_err_file, $_err_line, $_err_message, $_err_title = "") {
		$log_title = '';
		$log_color = '';
		switch ($_err_level) {
			case ErrorLevel::EL_ERROR: $log_title = 'Error'; $log_color = 'red'; break;
			case ErrorLevel::EL_EXCEPTION: $log_title = 'Exception'; $log_color = '#456496'; break;
			case ErrorLevel::EL_WARNING: $log_title = 'Warning'; $log_color = 'yellow'; break;
			case ErrorLevel::EL_PARSE: $log_title = 'Parse'; $log_color = 'orange'; break;
			case ErrorLevel::EL_DEPRECATED: $log_title = 'Deprecated'; $log_color = 'purple'; break;
			case ErrorLevel::EL_NOTICE: $log_title = 'Notice'; $log_color = 'cyan'; break;
			case ErrorLevel::EL_UNKNOWN: $log_title = 'Unknown'; $log_color = 'maroon'; break;
		}
		
		$logged = "";

		if(!empty($log_title)) {
			$logged .= "<b><font color='".$log_color."'>";
			$logged .= "(".$log_title.") ";
			$logged .= $_err_title . "</font>";
			if($_err_level != ErrorLevel::EL_NOTICE) 
				$logged .= "(Code ".$_err_code.")";
			$logged .= "</b><br>";
		} else {
			if(!empty($_err_title))
				$logged .= "<b><font color='".$log_color."'>". $_err_title . "</font></b><br>";
		}

		if(!empty($_err_file)) {
			$logged .= "In\t". $_err_file . "(line <font color='red'>".$_err_line."</font>)<br>";
		}

		if(!empty($_err_message)) {
			if($_err_level == ErrorLevel::EL_NONE)
				$logged .= $_err_message;
			else if($_err_level == ErrorLevel::EL_NOTICE)
				$logged .= "<font color='#778768'>".$_err_message."</font><br>";
			else 
				$logged .= "Msg\t<font color='#825600'>".$_err_message."</font><br>";
		} 

		if(!empty($logged)) {
			array_push(self::$Logs, $logged);
		}
	}

	private static function function_get_params($funcName, $className = ""){
		$attribute_names = [];

		if(empty($className)) {
			if(function_exists($funcName)){
				$fx = new \ReflectionFunction($funcName);
				foreach ($fx->getParameters() as $param){
					$attribute_names[$param->name] = NULL;
					if ($param->isOptional()){
						if($fx->isInternal())
							$attribute_names[$param->name] = "[Optional]";
						else
							$attribute_names[$param->name] = $param->getDefaultValue();
					}
				}
			}			
		} else {
			if(method_exists($className, $funcName)){
				$fx = new \ReflectionMethod($className, $funcName);

				foreach ($fx->getParameters() as $param){
					$attribute_names[$param->name] = NULL;
					if ($param->isOptional()){
						if($fx->isInternal())
							$attribute_names[$param->name] = "[Optional]";
						else
							$attribute_names[$param->name] = $param->getDefaultValue();
					}
				}           
			}
		}

		return $attribute_names;
	}
	
	private static function function_get_default_param($mixed) {
		if(empty($mixed)) return "";
		$result = "=";
		if(is_string($mixed) || is_numeric($mixed)) { $result .= $mixed; }
		else if(is_bool($mixed)) { $result .= $mixed ? "true":"false"; }
		else if(is_array($mixed)) { $result .= "array()"; }
		else $result .= gettype($mixed);
		return $result;
	}

	private static function function_get_param_value($mixed) {
		$result = "";
		if(is_bool($mixed)) { $result .= $mixed ? "true":"false"; }
		else $result .= htmlentities(print_r($mixed, true));
		return $result;
	}

	private static function GetBasePath($path, $root) {
		if(!is_dir($root) || $path == '/') return $path;
		
		$result = array_filter(explode("/", str_replace("\\", "/", $path)), 
				function ($_) { return !empty($_); });
		$base = array_filter(explode("/", str_replace("\\", "/", realpath($root))), 
				function ($_) { return !empty($_); });
		$result_idx = array_values($result);
		$base_idx = array_values($base);
		
		while(sizeof($result_idx) > 0 && sizeof($base_idx) > 0) {
			if($result_idx[0] == $base_idx[0]) { array_shift($result_idx); array_shift($base_idx); }
			else break;
		}
		return (sizeof($result_idx) > 0 ? "" : "./") . implode("/", $result_idx);
	}
}
