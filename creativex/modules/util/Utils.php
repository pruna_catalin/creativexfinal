<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * DAOUsers: CreativeX && noValue
 * Date: 10/9/2017
 * Time: 5:40 PM
 */
use CreativeX\config\application;
class Utils
{

    public static function WriteToFile($file,$content,$append = false){
        $fp = fopen($file, $append ? "a+b" : "wb");
        if($fp !== FALSE){
			#fwrite($fp, pack("CCC",0xef,0xbb,0xbf)); 
            fwrite($fp, str_replace(" "," ",$content));
            fclose($fp);
        }
    }
    public static function ReadFile($file){
        $fp = fopen($file, "r");
        $content = "";
        if($fp){
            while (($line = fgets($fp)) !== false) {
                $content .= $line;
            }
            fclose($fp);
        }
        return $content;
    }



    public static function GenerateRandomString($length){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public static function GenerateRandomInteger($lenght){
        return rand(pow(10,$lenght),9 * pow(10,$lenght));
    }
    public static function Debug($param) {
        echo '<pre>';
        print_r($param);
        echo '</pre>';
    }

    public static function Dump($param) {
        echo '<pre>';
        print_r(var_dump($param));
        echo '</pre>';
    }

    public static function DiffTime($date_end, $date_start) {
        $start = new DateTime($date_start);
        $end = new DateTime($date_end);
        $interval = date_diff($start, $end);
        return $interval->format('%a');
    }

    public static function FormatBytes($bytes, $precision = 2) {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        // Uncomment one of the following alternatives
        $bytes /= pow(1024, $pow);
        // $bytes /= (1 << (10 * $pow));

        return round($bytes, $precision) . ' ' . $units[$pow];
    }
    public static function RemoveEmptyObjects($array){
        $array_return = [];
        foreach($array as $item => $value){
            if (!empty($value) || $value != "") {
                $array_return[$item] = $value;
            }
        }
        return $array_return;
    }
    public static function GetBasePath($path, $root) {
        if(!is_dir($root) || $path == '/') return $path;

        $result = array_filter(explode("/", str_replace("\\", "/", $path)),
            function ($_) { return !empty($_); });
        $base = array_filter(explode("/", str_replace("\\", "/", realpath($root))),
            function ($_) { return !empty($_); });
        $result_idx = array_values($result);
        $base_idx = array_values($base);

        while(sizeof($result_idx) > 0 && sizeof($base_idx) > 0) {
            if($result_idx[0] == $base_idx[0]) { array_shift($result_idx); array_shift($base_idx); }
            else break;
        }
        return (sizeof($result_idx > 0) ? "" : "./") . implode("/", $result_idx);
    }
	public static function JsonTest($json){
		$content = false;
		if(is_string($json)){
			$content = json_decode($json);
			if($content == null) $content = false;			
		}
		
		return $content;
	}

	public static function GetIfExistsOrDefault($needle, $haystack, $default = null, $map = false) {
		if($map)
			return array_key_exists($needle, $haystack) ? $haystack[$needle] : $default;
		else
			return in_array($needle, $haystack, true) ? $needle : $default;
	} 
	public static function HashPassword($string, $method = "MD5", $compare = null){
		if($string != ""){
			if($method == "MD5" && $compare != null){
				if($string == $compare) return true;
				else return false;
			}else if($method == "MD5"){
				return md5($string);
			}else if($method == "AES"){
				return md5($string);
			}else{
				return false;
			}

		}
		return false;
	}
	public static function Redirect($link,$short = false){
		if($link != ""){
			if($short){
				$app =	new application();
				header("Location: ".$app::LIVE_URL_FRONT.$link);
			}else{
				header("Location: ".$link);
			}
			exit;
		}
	}
	public static function GetValueFromModel($model,$params){
		$valueArray =[];
		foreach($model as $key => $value){
			if(isset($params[$key]))
				$valueArray[$key] = $params[$key];
			else
				$valueArray[$key] = "";
		}
			
		
		return $valueArray;
	}
	public static function CleanString($string,$returnType){
		if($returnType == "FirstUp"){
			return ucfirst(preg_replace("/\W+|\_/", "", $string));
		}else{
			return preg_replace("/\W+|\_/", "", $string);
		}
	}
	public static function GenerateTabsSpace($nr = 0){
		$content = "";
		for($i=0;$i < $nr;$i++)
			$content .= "\t";
		return $content;
	}
	public static function JsonPrettyPrint( $json )
	{
		$result = '';
		$level = 0;
		$in_quotes = false;
		$in_escape = false;
		$ends_line_level = NULL;
		$json_length = strlen( $json );

		for( $i = 0; $i < $json_length; $i++ ) {
			$char = $json[$i];
			$new_line_level = NULL;
			$post = "";
			if( $ends_line_level !== NULL ) {
				$new_line_level = $ends_line_level;
				$ends_line_level = NULL;
			}
			if ( $in_escape ) {
				$in_escape = false;
			} else if( $char === '"' ) {
				$in_quotes = !$in_quotes;
			} else if( ! $in_quotes ) {
				switch( $char ) {
					case '}':
					case ']':
						$level--;
						$ends_line_level = NULL;
						$new_line_level = $level;
						break;

					case '{':
					case '[':
						$level++;
					case ',':
						$ends_line_level = $level;
						break;

					case ':':
						$post = " ";
						break;

					case " ":
					case "\t":
					case "\n":
					case "\r":
						$char = "";
						$ends_line_level = $new_line_level;
						$new_line_level = NULL;
						break;
				}
			} else if ( $char === '\\' ) {
				$in_escape = true;
			}
			if( $new_line_level !== NULL ) {
				$result .= "\n".str_repeat( "\t", $new_line_level );
			}
			$result .= $char.$post;
		}

		return $result;
	}
}