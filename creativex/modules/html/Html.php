<?php

/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 2017-12-04
 * Time: 05:41:27 PM
 */
class Html{
	public function __construct(){
		
	}
	/* ACCEPT ONLY text OR password OR hidden OR submit type
	 * @type string
	 * @class string
	 * @name array
	 * @value string
	 * @events array
	 */ 
	public static function Input($type= "text",$class= "form-control",$name = [],$value = "",$events = []){
		$content = "<input";
		if($type == "text" || $type == "password" || $type == "hidden" || $type == "submit"){
			if(!empty($type)){
				if(!empty($class)){
					$content .=" type=\"".$type."\" class=\"".$class."\"";
				}else{
					$content .=" type=\"".$type."\" ";
				}
			}else{
				$content .=" type=\"text\"";
			}

			if(sizeof($name) > 0){
				$content .= " name=\"Model";
				for($i=0;$i<sizeof($name);$i++){
					$content .="[".$name[$i]."]";
				}
				$content .= "\" ";
			}
			if(!empty($value)){
				$content .=" value=\"".$value."\" ";
			}
			if(sizeof($events) > 0){
				foreach($events as $key => $val){
					$content .= $key."=\"".$val."\" ";
				}
			}			
		}
		$content .=" />";
		return $content;
	}
	/* 
	 * @href string or JS
	 * @id string
	 * @class string
	 * @value string
	 * @events array
	 */ 
	public static function Link($href = "",$id = "",$class= "form-control",$value= "" ,$events = [] ){
		$content = "<a";
		if(!empty($href)){
			$content .=" href=\"".$href."\"";
		}
		if(!empty($id)){
			$content .=" id=\"".$id."\"";
		}
		if(!empty($class)){
			$content .=" class=\"".$class."\"";
		}
		
		if(sizeof($events) > 0){
			foreach($events as $key => $val){
				$content .= $key."=\"".$val."\" ";
			}
		}
		if(!empty($value)){
			$content .=" >".$value;
		}
		$content .= "</a>";
		return $content;
	}
	/* 
	 * @id array
	 * @class string
	 * @name array
	 * @value array
	 * @events array
	 */ 
	public static function Radio($id = [],$class= "radio-inline",$name = [],$value = [],$events = []){
		$content = " <label class=\"".$class."\">";
		if(sizeof($value) > 0){
			$x = 0;
			foreach($value as $key => $val){
				$content .= "<input type=\"radio\"";
				
				if(sizeof($id) > 0 && $x < sizeof($id)){
					$content .=" id=\"".$id[$x]."\"";
				}
				if(sizeof($name) > 0){
					$content .= " name=\"Model";
					for($i=0;$i<sizeof($name);$i++){
						$content .="[".$name[$i]."]";
					}
					$content .= "\" ";
				}
				if(sizeof($events) > 0){
					foreach($events as $keyEv => $valEv){
						$content .= $keyEv."=\"".$valEv."\" ";
					}
				}	
				if($val == "checked"){
					$content .= " checked=\"true\" ";
				}
				$content .=" >".$key." ";
				$x++;
			}
		}
		$content .= "</label>";
		return $content;
	}
	/* 
	 * @id string
	 * @class string
	 * @name array
	 * @value array
	 * @events array
	 */ 
	public static function Checkbox($id = "",$class= "form-control",$name = [],$value = [],$events = []){
		$content = "";
		$content .= "<input type=\"checkbox\"";
		if(!empty($class)){
			$content .=" class=\"".$class."\"";
		}
		if(!empty($id)){
			$content .=" id=\"".$id."\"";
		}
		if(sizeof($name) > 0){
			$content .= " name=\"Model";
			for($i=0;$i<sizeof($name);$i++){
				$content .="[".$name[$i]."]";
			}
			$content .= "\" ";
		}
		if(sizeof($events) > 0){
			foreach($events as $keyEv => $valEv){
				$content .= $keyEv."=\"".$valEv."\" ";
			}
		}	
		foreach($value as $key=>$val){
			if($val == "checked"){
				$content .= " checked=\"true\" ";
			}
			$content .=" >".$key." ";
		}
		return $content;
	}
	/* 
	 * @id string
	 * @class string
	 * @name array
	 * @value string
	 * @events array
	 */ 
	public static function DateInput($id = "",$class= "form-control",$name = [],$value = "",$events = []){
		//data-date="" data-date-format="DD MMMM YYYY" value="2015-08-09"
		$content = "<input type=\"date\"";
		if(!empty($id)){
			$content .="  id=\"".$id."\"";
		}
		if(!empty($class)){
			$content .="  class=\"".$class."\"";
		}
		if(sizeof($name) > 0){
			$content .= " name=\"Model";
			for($i=0;$i<sizeof($name);$i++){
				$content .="[".$name[$i]."]";
			}
			$content .= "\" ";
		}
		if(!empty($value)){
			$content .="  value=\"".$value."\"";
		}
		if(sizeof($events) > 0){
			foreach($events as $key => $val){
				$content .= $key."=\"".$val."\" ";
			}
		}
		$content .=" />";
		return $content;
	}
	/* 
	 * @id string
	 * @class string
	 * @name array
	 * @value string
	 * @icon string
	 * @events array
	 */ 
	public static function DatePicker($id = "" ,$class="input-group date",$name = [] ,$value = "", $icon = "calendar",$events = []){
		$content = "<div ";
		if(!empty($class)){
			$content .=" class=\"".$class."\"";
		}else{
			$content .= " class=\"input-group date\" ";
		}
		if(!empty($id)){
			$content .=" id=\"".$id."\"";
		}
		$content .= " > ";
		$content .="<input type=\"text\" class=\"form-control\" value=\"".$value."\"";
		if(sizeof($name) > 0){
			$content .= " name=\"Model";
			for($i=0;$i<sizeof($name);$i++){
				$content .="[".$name[$i]."]";
			}
			$content .= "\" ";
		}
		if(sizeof($events) > 0){
			foreach($events as $key => $val){
				$content .= $key."=\"".$val."\" ";
			}
		}
		$content .= " />";
		if(!empty($icon)){
			$content .="<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-".$icon."\"></span></span>";
		}
		$content .= " </div>";
		return $content;
	}
	/* 
	 * @id string
	 * @class string
	 * @name array
	 * @value [key=>[value,selected/no]] array
	 * @events array
	 * @displayType (multiple or not)
	 */ 
	public static function Select2($id = "",$class = "form-control",$name = [] ,$value = [] ,$events = [],$displayType = ""){
		$content = "<select ";
		if(!empty($class)){
			$content .=" class=\"".$class."\"";
		}
		if(!empty($id)){
			$content .=" id=\"".$id."\"";
		}
		if(sizeof($name) > 0){
			$content .= " name=\"Model";
			for($i=0;$i<sizeof($name);$i++){
				$content .="[".$name[$i]."]";
			}
			if($displayType == "multiple"){
				$content .= "[]\" ";
			}else{
				$content .= "\" ";
			}
		}
		if(sizeof($events) > 0){

			foreach($events as $keyEv => $valEv){
				$content .= $keyEv."=\"".$valEv."\" ";
			}
		}	
		//option selected simple 
		
		if($displayType == "multiple"){
			$content .=" multiple=\"multiple\">";
		}else{
			$content .=" >";
		}
		$options = "";
		if(sizeof($value) > 0 ){
			foreach($value as $valKey => $val){
				$options .= "<option ";
				if(sizeof($val) == 2){
					if($val[1] == "selected"){
						$options .= " value=\"".$valKey."\" selected>";
					}else{
						$options .= " value=\"".$valKey."\">";
					}
					$options .= $val[0]."</option> ";
				}
			}
		}
		$content .= $options;
		$content .= "</select>";
		return $content;
	}


}