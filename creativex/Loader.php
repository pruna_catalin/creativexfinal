<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * DAOUsers: CreativeX && noValue
 * Date: 9/29/2017
 * Time: 1:06 PM
 */
namespace CreativeX;

use CreativeX\core\controller\Controller;
     
define("BASE_DIR", __DIR__ . DIRECTORY_SEPARATOR);

$Global_Namespace = array(
	'Debugger' => 'CreativeX\modules\util\Debugger',
	'Profiler' => 'CreativeX\modules\util\Profiler',
	'Utils' => 'CreativeX\modules\util\Utils',
	"Html" => "CreativeX\modules\html\Html"
);

class Loader
{

    public static function register()
    {

        session_start();
        chdir(BASE_DIR);
        spl_autoload_register(array(__CLASS__, '_autoload'));
        \Debugger::$ModeOn = true;
        #Debugger::$OneError = true; // show only 1 error

        set_exception_handler(array("\Debugger", "_exception_handler"));
        set_error_handler(array("\Debugger", '_error_handler'));
        register_shutdown_function(array("\Debugger", '_exit_script_handler'));
        
		$object = new Controller();
        $object->Init();
    }

    private static function _autoload($classPath)
    {
		global $Global_Namespace;

        if (strpos($classPath, __NAMESPACE__. "\\") === 0) {
            $class = str_replace(__NAMESPACE__. '\\', '', $classPath);
            $classToPath = str_replace('\\', '/', $class);
			
            if (!self::require_file(BASE_DIR . $classToPath . '.php')) {
                \Debugger::Log("Class " . $classPath . " not found!");
            }
        } else {
			if(key_exists($classPath, $Global_Namespace)) {
				$class = str_replace(__NAMESPACE__. '\\', '', $Global_Namespace[$classPath]);
				$classToPath = str_replace('\\', '/', $class);

				if (!self::require_file(BASE_DIR . $classToPath . '.php')) {
					\Debugger::Log("Class " . $classPath . " not found!");
				}
			}
		}
    }

    private static function require_file($path)
    {
        $filePath = "";
        if (is_array($path)) {
            $filePath = implode(DIRECTORY_SEPARATOR, $path);
        } else
            $filePath = $path;

        if (file_exists($filePath)) {
            require_once $filePath;
            return true;
        } else {
            return false;
        }
    }
}