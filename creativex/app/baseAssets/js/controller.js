/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * DAOUsers: CreativeX && noValue
 */
function checkForms(form){
    var formElement = $(form)[0];
    for(i=0;i<formElement.length;i++){
        if(formElement[i].nodeName.toLowerCase() !== "button"){
            if(formElement[i].value === ""){
                toastr.error(formElement[i].attributes['messageerror'].textContent);
                return false;
            }else{
                if(formElement[i].id === "same_password"){
                    if($("#password").val() !== formElement[i].value){
                        toastr.error(formElement[i].attributes['messageerror'].textContent);
                        return false;
                    }
                }
                if(formElement[i].nodeName.toLowerCase() === "select" && formElement[i].value === "0" ){
                    toastr.error(formElement[i].attributes['messageerror'].textContent);
                    return false;
                }
            }
        }
    }
    return true;
}