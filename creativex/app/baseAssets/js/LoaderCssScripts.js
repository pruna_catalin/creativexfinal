/**
 * Created by CreativeXGenerator
 * User     : Pruna Catalin
 * WebSite  : www.upg-ploiesti.ro
 * Platform : CreativeX-Framework
 */
$.ajax({
    url: LivePathLoader + 'Loader.json' + "?r=" + (new Date()).getTime(),
	async: true,
	dataType: 'json',
	success: function (response) {
		response.Css.global.list.forEach(function(script) {
		    includeCss(LivePathThemeCss + script);
		});
		
		for(var i in response.Css.private){
			var script = response.Css.private[i];
			if(i === caseController){
				for(var j in script){
					var list = script[j];
					if(j === actionController){
						list.forEach(function(s){
						    includeCss(LivePathThemeCss + s);
						});
					}
				}
			}
		}
		
	}
});
function includeCss(p_file) {
    $("<link href='" + p_file + "?r=" + (new Date()).getTime() + "' type='text/css' rel='stylesheet'></link>").appendTo(document.head);
	//$("LoadCSS").html($("LoadCSS").html() + "<link href='"+p_file+"' type='text/css' rel='stylesheet'></link>");
	//console.log("ORDER :",p_file);
	//(devMode || flushCache) ? $( "<link href='"+p_file+"?r="+(new Date()).getTime()+"' type='text/css' rel='stylesheet'></link>").appendTo(document.head): $( "<link href='"+p_file+"' type='text/css' rel='stylesheet'></link>").appendTo(document.head);
	
	
}

