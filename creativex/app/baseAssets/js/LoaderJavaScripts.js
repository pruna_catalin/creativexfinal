/**
 * Created by CreativeXGenerator
 * User     : Pruna Catalin
 * WebSite  : www.upg-ploiesti.ro
 * Platform : CreativeX-Framework
 */
var i = 0;
$.ajax({
    url: LivePathLoader + 'Loader.json' + "?r=" + (new Date()).getTime(),
	async: true,
	dataType: 'json',
	success: function (response) {
		response.JavaScripts.global.list.forEach(function(script) {
		    includeJS(LivePathThemeJs + script);
		});
		for(var i in response.JavaScripts.private){
			var script = response.JavaScripts.private[i];
			if(i === caseController){
				for(var j in script){
					var list = script[j];
					if(j === actionController){
						list.forEach(function(s){
						    includeJS(LivePathThemeJs + s);
						});
						break;
					}
				}
			}
		}
		$("#removeMeHeader").show();
		$("#removeMeContent").show();
		$(".loader").hide();
	}
});
function includeJS(p_file) {
    $("<script src='" + p_file + "?r=" + (new Date()).getTime() + "' type='text/javascript'></script>").appendTo(document.head);
	//console.log(p_file);
	//(devMode || flushCache) ? $("<script src='"+p_file+"?r="+(new Date()).getTime()+"' type='text/javascript'></script>").appendTo(document.body) : $("<script src='"+p_file+"' type='text/javascript'></script>").appendTo(document.body);
		
    //$("LoadJS").html("<script src='"+p_file+"' type='text/javascript'></script>");
   // EvalContent(p_file);
}
function EvalContent(p_file) {
    $.ajax({
        url: p_file,
        async: true,
        dataType: 'text',
        success: function (response) {
            //function a(rep) {
            try {
                eval(response);
            } catch (e) {
                console.log(e.name, ": ", LivePathAjax + p_file, "(line ", e.lineNumber, ")\n", e.message);
            }


            //}
            //a(response);
            //console.log(document.CreativeX.actionDelete());
        }
    });
}

