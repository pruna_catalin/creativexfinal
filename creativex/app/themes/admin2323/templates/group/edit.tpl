﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<section class="content-header">
  <h1>
    Edit
    <small>{@$group->name@}</small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="{@$App::LIVE_URL_ADMIN@}index">
        <i class="fa fa-dashboard"></i> Home
      </a>
    </li>
    <li class="active">
      <a href="{@$App::LIVE_URL_ADMIN@}groups"> {@$Language->t('list_group')@}</a>
    </li>
  </ol>
</section>
<section class="content">
  <form class="form-horizontal" action="{@$App::LIVE_URL_ADMIN@}groups/edit/{@$group->id@}" method="POST">
	  <input type="hidden" name="Model[id]"  value="{@$group->id@}">
    <input type="hidden" value="{@ $permision; @}"  name="Model[permision][id]">
     <input type="hidden" value="" id="permision_list" name="Model[permision][list]">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">{@$Language->t('note'). "".$message@}</h3>
        </div>
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <br>
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tab_1" data-toggle="tab">{@$Language->t('global_info')@}</a>
                </li>
             
			        </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputName" class="col-sm-2 control-label">{@$Language->t('name')@}</label>
                      <div class="col-sm-8">
                        <input type="text" id="inputName" class="form-control" name="Model[name]"
                        placeholder="{@$Language->t('name')@}" value="{@$group->name@}">
                         
                      </div>
                      <div class="col-sm-2">
                        <button type="submit" class="btn btn-info pull-right">{@$Language->t('save_button')@}</button>
                      </div>
                    </div>
                    <div id="tree_2" class="tree-demo"></div>
                  </div>
                  <!-- /.box-body -->
                
                  <!-- /.box-footer -->

                </div>
                
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
          </div>
        <!-- /.box-header -->
        <!-- form start -->

      </div>
    </form>
  <script>
    var dataList = [{@ $dataList; @}];
    var listPermision = [];
  </script>
</section>