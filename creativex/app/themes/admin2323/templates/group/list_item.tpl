﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<tr>
	<td>{@$group->id@}</td>
	<td>{@$group->name@}</td>
	<td>
    <a class="btn btn-info" href="{@$App::LIVE_URL_ADMIN@}groups/edit/{@$group->id@}">{@$Language->t('edit_button')@}</a>
    <a class="btn btn-danger"  onclick="actionDelete('groups/delete',{@$group->id@});">{@$Language->t('delete_button')@}</a>
  </td>
</tr>
              