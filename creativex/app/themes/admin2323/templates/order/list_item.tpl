﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<tr>
	<td>{@$order->id@}</td>
	<td>{@$customer->name@}</td>
	<td>{@($item) ? implode(" , ",$item) : ""@}</td>
	<td>{@$order->status@}</td>
	<td >
      <a class="btn btn-warning" href="{@$App::LIVE_URL_ADMIN@}orders/view/{@$order->id@}">{@$Language->t('view_button')@}</a>
      <a class="btn btn-info" href="{@$App::LIVE_URL_ADMIN@}orders/edit/{@$order->id@}">{@$Language->t('edit_button')@}</a>
      <a class="btn btn-danger"  onclick="actionDelete('orders/delete',{@$order->id@});">{@$Language->t('delete_button')@}</a>
  </td>
</tr>
              