﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<section class="content-header">
  <h1>
    Edit
    <small>{@$order->id@}</small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="{@$App::LIVE_URL_ADMIN@}index">
        <i class="fa fa-dashboard"></i> Home
      </a>
    </li>
    <li class="active">
      <a href="{@$App::LIVE_URL_ADMIN@}orders"> {@$Language->t('list_orders')@}</a>
    </li>
  </ol>
</section>
<section class="content">
  <form class="form-horizontal" action="{@$App::LIVE_URL_ADMIN@}orders/edit/{@$order->id@}" method="POST">
    <input type="hidden" name="Model[id]"  value="{@$order->id@}" />
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">{@$Language->t('note'). "".$message@}</h3>
          <button type="submit" class="btn btn-info pull-right col-lg-4">{@$Language->t('save_button')@}</button>
        </div>
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <br>
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tab_1" data-toggle="tab">{@$Language->t('bill_info')@}</a>
                </li>
                <li>
                  <a href="#tab_2" data-toggle="tab">{@$Language->t('product_list')@}</a>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputStatus" class="col-sm-2 control-label">{@$Language->t('status')@}</label>
                      <div class="col-sm-10">
                        <select class="form-control select2"  name="Model[status]">
                            <?php 
                              if($order->status == "paid")
                                echo '<option value="paid" selected="selected">Paid</option>
                                      <option value="unpaid">Unpaid</option>';
                                else
                                echo '<option value="paid">Paid</option>
                                      <option value="unpaid" selected="selected">Unpaid</option>';
                            ?>
                        </select>
                         
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputCustomer" class="col-sm-2 control-label">{@$Language->t('customer_name')@}</label>
                      <div class="col-sm-10">
                        <input type="text" id="inputCustomer" class="form-control"  placeholder="{@$customer->name@}" readonly="true" />
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="inputCustomerAddress" class="col-sm-2 control-label">{@$Language->t('customer_address')@}</label>
                      <div class="col-sm-10">
                        <input type="text" id="inputCustomerAddress" class="form-control"  placeholder="{@$customer->address@}" readonly="true" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputCustomerEmail" class="col-sm-2 control-label">{@$Language->t('customer_email')@}</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputCustomerEmail" placeholder="{@$customer->email@}" readonly="true" />
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="inputCustomerPhone" class="col-sm-2 control-label">{@$Language->t('customer_phone')@}</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputCustomerPhone" placeholder="{@$customer->phone@}" readonly="true" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputCustomerCompany" class="col-sm-2 control-label">{@$Language->t('customer_company')@}</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputCustomerCompany" placeholder="{@$customer->company@}" readonly="true" />
                      </div>
                    </div>
                  </div>
                  <!-- /.box-body -->
                
                  <!-- /.box-footer -->

                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">
                  <div class="box-body">
                    <div class="form-group">
                      <div class="col-sm-12">
                        <h2>{@$Language->t('price_total')@} : <input type="text" id="calcTotal" value="{@$order->price@}" readonly="true" style="border: none;" name="Model[price]"/></h2>
                        
                        <table class="table table-bordered">
                          <tr>
                            <td>
                              Item
                            </td>
                            <td>
                              <select class="form-control select2"  name="Model[itemAdd]" onchange="GetItemSpecification(this,{@$order->id@});">
                                <option value="">{@$Language->t('select')@}</option>
                              <?php  
                                     foreach($totalItems as $item){ ?>
                                        {@ '<option value="'.$item->id.'">'.$item->name.'</option>' @};
                              <?php } ?>
                              </select>
                            
                            </td>
                            <td>
                              Quantity
                            </td>
                            <td>
                              <input type="text" class="form-control" name="Model[quantityAdd]" value="0" readonly="true" id="quantityAdd"/>
                            </td>
                            <td>
                              Price
                            </td>
                            <td>
                              <input type="text" class="form-control" name="Model[priceAdd]" value="0" readonly="true" id="priceAdd"/>
                            </td>
                            <td>
                              <button type="submit" class="btn btn-success">{@$Language->t('add_button')@}</button>
                            </td>
                         </tr>
                        </table>
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th>NR.</th>
                              <th>{@$Language->t('name')@}</th>
                              <th>{@$Language->t('quantity')@}</th>
                              <th>{@$Language->t('price')@}</th>
                              <th>{@$Language->t('total')@}</th>
                            </tr>
                          </thead>
                          <?php
                            $i = 0 ;
                            $itemsString = "";
                            $orderQ = explode(",",$order->quantity);
                            $orderI = explode(",",$order->id_item);
									          foreach($items as $item){
										            $itemsString .='
                                        <tr>
                                          <td>'.($i + 1).'</td>                                        
                                          <td><input type="hidden" value="'.$item->id.'" name="Model[id_item][]"/>'.$item->name.'</td>
                                         <td> 
                                            <input type="text" class="form-control col-sm-4" name="Model[quantity][]" 
                                                value="'.$orderQ[$i].'" onblur="calculate(this,'.$i.','.$item->price.')"/>
                                          </td>
                                          <td id="priceItem_'.$i.'">'.$item->price.'</td>
                                          <td id="totalPrice_'.$i.'">'.($item->price * $orderQ[$i]) .'</td>
                                      <tr>';
                              $i++;
									          }
                           
                           echo $itemsString;
								          ?>
                          <input type="hidden" id="totalCount" value="{@($i - 1)@}"/>
                        </table>
                      </div>
                    </div>

                  </div>
                  <!-- /.box-body -->
                
                  <!-- /.box-footer -->
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
          </div>
        <!-- /.box-header -->
        <!-- form start -->

      </div>
    </form>
</section>