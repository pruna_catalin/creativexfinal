<?php
namespace CreativeX\config;
/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: DATE_CREATE
 * Time: TIME_CREATE
 */
class route
{
    /*
     * Rules manager
     * simple route "{case}"=> ["NameController","ActionController","NameFolderController","XHR or empty"]
     * complex route "{case}\/[a-zA-z]"=> ["NameController","ActionController","NameFolderController","XHR or empty"]
	 *				"{admin\/index\/edit}\/\d+" => ['SiteController',"actionIndex","admin","",""]
     */
    public static $Urls = [
             "{blank}" =>					 ['SiteController',"actionIndex","frontend","NORMAL","FRONT"],
             "{index}" =>					 ['SiteController',"actionIndex","frontend","",""],
             "{admin\/index}" =>			 ['DashBoardController',"actionIndex","admin","",""],
			 "{admin\/login}" =>			 ['LoginController',"actionLogin","admin","",""],
			 ROUTE,
			 "{admin\/generator}" =>		 ['GeneratorController',"actionMain","admin","",""],
			 "{admin\/generate}" =>			 ['GeneratorController',"actionGenerate","admin","",""],

			 
			 
    ];

}