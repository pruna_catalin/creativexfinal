<?php
namespace CreativeX\Model\DAO;
use CreativeX\Model\Data\DataMODEL;
use CreativeX\Model\Drivers\SqlPDO;
use CreativeX\modules\util\Debugger;
/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: DATE_CREATE
 * Time: TIME_CREATE
 */
class DAOMODEL extends DAOAbstract {
	public static $order = "PRIMARY ASC";
	private static function TableName() {
		return "TABLE";
	}

	/* 
     * @params  condition , resultType  = 'assoc|num|object|lazy' , null or empty return OBJECT
	 * @ return false on rows 0/ SUCCESS RETURN MODEL WITH DATA
	 */
	public static function Find($data, $resultType = NULL) {
		$db = new SqlPDO('');
		$result = [];
		if ($data instanceof DataMODEL) {
			$result = DAOTools::FindByModel($db, $data, self::tableName(), self::$order)->fetch($db::SwitchResult($resultType));
		} else {
			if ($data != "") {
				$db->select('*')->from(self::tableName())->where($data)->orderby(self::$order)->prepare();
				$request = $db->execute();
				$result = $request->fetch($db::SwitchResult($resultType));
			}
		}
		return $result;
	}

	/* 
     * @params  condition , limit / row , resultType  = 'assoc|num|object|lazy' , null or empty return OBJECT
     * @ return false on rows 0
     */
	public static function FindAll($data, $limit = "", $resultType = NULL) {
		$db = new SqlPDO('');
		$result = [];
		$prepare = [];
		if ($data instanceof DataMODEL) {
			$prepare = DAOTools::FindByModel($db, $data, self::tableName(), self::$order,$limit)->fetchAll($db::SwitchResult($resultType));
		} else {
			if ($data != "") {
				if($limit != "")
					$db->select('*')->from(self::tableName())->where($data)->orderby(self::$order)->limit($limit)->prepare();
				else
					$db->select('*')->from(self::tableName())->where($data)->orderby(self::$order)->prepare();
				$request = $db->execute();
				$prepare = $request->fetchAll($db::SwitchResult($resultType));
			}else{
				if($limit != "")
					$db->select('*')->from(self::tableName())->orderby(self::$order)->limit($limit)->prepare();
				else
					$db->select('*')->from(self::tableName())->orderby(self::$order)->prepare();
				$request = $db->execute();
				$prepare = $request->fetchAll($db::SwitchResult($resultType));
            }
		}
		
        foreach($prepare as $data){
COLUMNS_SET_DATA
            $prepareObject = new DataMODEL(null,LIST_VAR_COLUMNS);
            array_push($result,new DataMODEL($prepareObject,LIST_VAR_COLUMNS));
        }
		return (sizeof($result) == 0 ) ? FALSE : $result;
	}
    /* 
     * @params  condition | MODEL   
     * @ return false on FAILD
     */
	public static function Insert($model) {
		$dataParse = DAOTools::InsertModel($model);
		$data = array('input' => $dataParse[0]);
		if(sizeof($dataParse[0]) > 0 ){
			$sql = new SqlPDO($data);
			$sql->insert(self::tableName())->columns($dataParse[1])
				->values($dataParse[2])->prepare();
			$sql->execute();
			return $sql->lastInsertId();
		}else{
			return FALSE;
		}
	}
    /* 
     * @params  condition | MODEL
     * @ return false on FAILD
     */
	public static function Update($model,$condition) {
		$dataParse = DAOTools::UpdateModel($model,$condition);
		if(sizeof($dataParse[1]) == 0){
			$data = $model;
		}else{
			$data = array('prepare' => array(':SET' =>$dataParse[0],':WHERE' => $condition.'=:'.$condition),
				'input' => $dataParse[1]);
		}
		$sql = new SqlPDO($data);
		$sql->update(self::tableName())->prepare();
        $result = $sql->execute();
		return ($result->rowCount() > 0 ) ? TRUE : FALSE;
	}

	public static function Delete($data) {
		$sql = new SqlPDO('');
		$sql->delete("")->from(self::tableName())->where($data)->prepare();
        $result = $sql->execute();
		return ($result->rowCount() > 0 ) ? TRUE : FALSE;
	}

	public static function Count(){
		$db = new SqlPDO('');
		$db->select('*')->from(self::tableName())->orderby(self::$order)->prepare();
		$request = $db->execute();
		$prepare = $request->fetchAll();
		return count($prepare);
	}
    


}