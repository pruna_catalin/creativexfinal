﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<section class="content-header">
  <h1>
    {@$Language->t('list_MODEL')@}
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="{@$App::LIVE_URL_ADMIN@}index">
        <i class="fa fa-dashboard"></i> Home
      </a>
    </li>
    <li class="active">MODEL</li>
  </ol>
</section>
<section class="content">
  <form class="form-horizontal" action="{@$App::LIVE_URL_ADMIN@}MODEL" method="POST" style="text-align:center">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title  pull-left">Total : {@$totalMODEL@}</h3>
        <a href="{@$App::LIVE_URL_ADMIN@}MODEL/new" class="btn btn-success pull-right">{@$Language->t('add_button')@}</a>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-bordered">
          <tr>
TH_TABLE
            <th>
              {@$Language->t('actions')@}<br/>
              <input type="submit" class="btn btn-info pull-left btn-sm" style="width:40%" value="{@$Language->t('search_button')@}"/>
              <a class="btn btn-warning pull-left btn-sm col-md-offset-1" style="width:50%" onclick="location.href = '{@$App::LIVE_URL_ADMIN@}MODEL';">{@$Language->t('reset_button')@}</a>
            </th>
          </tr>
          {@$item@}
        </table>
      </div>
      <!-- /.box-body -->
      <?php
  $contents = "";
    for($i=0; $i< $totalMODEL / $App::MAX_PERPAGE; $i++) {
      if($i == 0 || ($page_no > $i - $App::MAX_PSPAGE && $page_no < $i + $App::MAX_PSPAGE) || $i == $totalMODEL/$App::MAX_PERPAGE -1) {
			    if($page_no == $i) {
				    $contents .= '<li class="active"><button class="btn btn-danger btn-xs" type="submit" name="Model[pagination]" value="'.($i+1).'">'.($i+1).'</button></li>';
          }else {
            $contents .= '<li><button class="btn btn-info btn-xs" type="submit" name="Model[pagination]" value="'.($i+1).'">'.($i+1).'</button></li>';
          }
      } else {

      //if(($i == $page_no - $ps_page || $i == $page_no + $ps_page) && $i != 0)

      //$contents .= '...';

      }

    }
  ?>
      <div class="box-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-right">
          <li>
            <button class="btn btn-info btn-xs" type="submit" name="Model[pagination]" value="0">&laquo;</button>
          </li>
          {@$contents@}
          <li>
            <button class="btn btn-info btn-xs" type="submit" name="Model[pagination]" value="{@ceil($totalMODEL / $App::MAX_PERPAGE)@}">&raquo;</button>
          </li>
        </ul>
      </div>
    </div>
  </form>
  <script>
    var deleteMessage = "{@$Language->t('message_delete_question')@}";
    var faildMessage = "{@$Language->t('message_delete_error')@}";
  </script>
</section>