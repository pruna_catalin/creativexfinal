/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: DATE_CREATE
 * Time: TIME_CREATE
 */
$(document).ready(function(){
	
});

function actionDelete(action,id){
	bootbox.confirm(deleteMessage, function(result) {
		if (result === false) {} else {
			$.ajax({
			    url: action,
				type: 'POST', 
				data : "Model[delete]="+id,    
				dataType:'json', 
				success : function(data) {  
					if (data.success) {
					    location.href = "MODEL";
					}else{
					    toastr.error(data.message);
					}
				}
			});
		}
	});
}