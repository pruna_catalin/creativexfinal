﻿<?php
namespace CreativeX\core\controller;
use CreativeX\Model\DAO\DAOMODEL;
use CreativeX\Model\Data\DataMODEL;
use CreativeX\modules\util\Debugger;
use CreativeX\modules\render\View;
use CreativeX\config\application;
/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: DATE_CREATE
 * Time: TIME_CREATE
 */
class MODELController{
	private static $request, $lang;
	public function __construct($request, $lang = null) {
		self::$request = $request;
		self::$lang = $lang;
	}
	public function actionList(){
		$dataMODEL = new DataMODEL();
		$ValueFilter = [VALUE_FILTER];
		$limit = "0,".application::MAX_PERPAGE;
		$totalMODEL = DAOMODEL::Count();
		$page_no = 1;
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['search'])) {
ISSET_PARAMS
				
			}
			if(isset($params['Model']['pagination'])){
				if(is_numeric($params['Model']['pagination'])){
					if($params['Model']['pagination'] < 1){
						$page_no = 1;
					}else{
						$page_no = $params['Model']['pagination'];
					}
					$page_no -= 1;
					$limit = ($page_no * application::MAX_PERPAGE).",".application::MAX_PERPAGE;
				}
			}

		}
		DAOMODEL::$order = "PRIMARY ASC";
		#\Debugger::log($limit);
		$MODEL = DAOMODEL::FindAll($dataMODEL,$limit);
		$MODELList = "";
		if($MODEL){
			foreach($MODEL as $item){
				$child = new View( ["MODEL/list_item","FOLDER"], ["item"=>$item,"App"=> new application()]);
				$MODELList .= $child->render();
			}
		}
		$view =  new View( ["MODEL/list","FOLDER"], ["totalItems"=>$totalMODEL,"items"=>$MODELList,
													"App"=> new application(),"filter"=>$ValueFilter,"page_no"=>$page_no]);
        return  $view;
	}
	public function actionDelete(){
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['delete'])){
				$PRIMARY = is_numeric($params['Model']['delete']) ? $params['Model']['delete'] : 0;
				if(DAOMODEL::Find("PRIMARY=".$PRIMARY)){
					if(DAOMODEL::Delete("PRIMARY=".$PRIMARY)){
						return json_encode(["success"=>true]);
					}
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}else{
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}
			}
		}
		exit;
	}
	public function actionEdit(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && self::$request->Method[1] != "POST"){
				$PRIMARY = is_numeric($params['Model']) ? $params['Model'] : 0;
				$model = DAOMODEL::Find("PRIMARY=".$PRIMARY);
				if(!$model) return self::$lang->t('notfound');
			}else{
				$PRIMARY = is_numeric($params['Model']['PRIMARY']) ? $params['Model']['PRIMARY'] : 0;
				$model = DAOMODEL::Find("PRIMARY=".$PRIMARY);
				if(!$model) return self::$lang->t('notfound');
				$dataMODEL = new DataMODEL();
COLUMN_DATA
				$success = false;
				if(DAOMODEL::Update($dataMODEL,"PRIMARY")){
					$success = true;
				}
				if($success)
					\Utils::Redirect("FOLDER/MODEL/edit/".$dataMODEL->PRIMARY,true);

				//\Utils::Debug(self::$request);
			}					
		}
		$view =  new View(	["MODEL/edit","FOLDER"],
								["MODEL"=>$model,
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params]
					  );
		return  $view;
	}
	public function actionAdd(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && $params['Model'] != ""){
				$dataMODEL = new DataMODEL();
COLUMN_DATA	
				$model = DAOMODEL::Insert($dataMODEL);
				$success =	false;
				if($model){
					$success = true;
				}
				if($success)
					\Utils::Redirect("FOLDER/MODEL/edit/".$model,true);
			}					
		}
		$view =  new View(	["MODEL/new","FOLDER"],
								["MODEL"=>"",
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params
							]
					  );
		return  $view;
	}
	
}