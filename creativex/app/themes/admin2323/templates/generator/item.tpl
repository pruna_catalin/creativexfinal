﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<tr>
	<td class="pull-left">
		<select class="" name="Model[folder][]">
			<option value="admin">admin</option>
			<option value="front">front</option>
		</select>
		{@$route@}
	</td>
	<td class="text-center">
		<input type="hidden" name="Model[model][]" value="{@$model@}">
		{@$model@}
	</td>
	<td class="text-center">{@$dao@}</td>
	<td class="text-center">{@$controller@}</td>
	<td class="text-center">{@$js@}</td>
	<td class="text-center">{@$css@}</td>
</tr>