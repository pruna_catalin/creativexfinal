﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<form  action="{@$App::LIVE_URL_ADMIN@}generate" method="POST">
	<div class="col-lg-12">
		<div class="box box-primary">
		  <div class="box-header with-border">
			<h3 class="box-title"><button class="btn btn-success">{@$Language->t('generate')@}</button></h3>
			<div class="box-tools pull-right">
			  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
			  </button>
			  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
			</div>
		  </div>
		  <!-- /.box-header -->
		  <div class="box-body">
			 <table class="table table-bordered">
				<th class="text-center">
					{@$Language->t('Route')@}
				</th>
				<th class="text-center">
					{@$Language->t('ModelData')@}
				</th>
				<th class="text-center">
					{@$Language->t('DAO')@}
				</th>
				<th class="text-center">
					{@$Language->t('Controller')@}
				</th>				
				<th class="text-center">
					{@$Language->t('JS')@}
				</th>
				<th class="text-center">
					{@$Language->t('CSS')@}
				</th>
				{@$items@}
			</table>
		  </div>
		  <!-- /.box-body -->
		</div>
	</div>
</form>