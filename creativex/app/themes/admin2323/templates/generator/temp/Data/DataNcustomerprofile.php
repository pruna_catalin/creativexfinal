<?php
namespace CreativeX\Model\Data;
/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 2017-12-05
 * Time: 03:39:42 PM
 */
class DataNcustomerprofile
{
	public $id = NULL;
	public $name = NULL;
	public $address = NULL;
	public $email = NULL;
	public $phone = NULL;
	public $company = NULL;
	public $create_by = NULL;
	public $create_at = NULL;
	public $modified_by = NULL;
	public $modified_at = NULL;


    public function __construct($class = NULL ,$id = NULL ,$name = NULL ,$address = NULL ,$email = NULL ,$phone = NULL ,$company = NULL ,$create_by = NULL ,$create_at = NULL ,$modified_by = NULL ,$modified_at = NULL){
        if(is_a($class, __CLASS__)) {
			$this->setId($id);
			$this->setName($name);
			$this->setAddress($address);
			$this->setEmail($email);
			$this->setPhone($phone);
			$this->setCompany($company);
			$this->setCreateby($create_by);
			$this->setCreateat($create_at);
			$this->setModifiedby($modified_by);
			$this->setModifiedat($modified_at);

        }
    }
	public function setId($id){ 
 		$this->id = $id;
 		return $this; 
	}
	public function getId(){ 
 		return $this->id; 
	}
	public function setName($name){ 
 		$this->name = $name;
 		return $this; 
	}
	public function getName(){ 
 		return $this->name; 
	}
	public function setAddress($address){ 
 		$this->address = $address;
 		return $this; 
	}
	public function getAddress(){ 
 		return $this->address; 
	}
	public function setEmail($email){ 
 		$this->email = $email;
 		return $this; 
	}
	public function getEmail(){ 
 		return $this->email; 
	}
	public function setPhone($phone){ 
 		$this->phone = $phone;
 		return $this; 
	}
	public function getPhone(){ 
 		return $this->phone; 
	}
	public function setCompany($company){ 
 		$this->company = $company;
 		return $this; 
	}
	public function getCompany(){ 
 		return $this->company; 
	}
	public function setCreateby($create_by){ 
 		$this->create_by = $create_by;
 		return $this; 
	}
	public function getCreateby(){ 
 		return $this->create_by; 
	}
	public function setCreateat($create_at){ 
 		$this->create_at = $create_at;
 		return $this; 
	}
	public function getCreateat(){ 
 		return $this->create_at; 
	}
	public function setModifiedby($modified_by){ 
 		$this->modified_by = $modified_by;
 		return $this; 
	}
	public function getModifiedby(){ 
 		return $this->modified_by; 
	}
	public function setModifiedat($modified_at){ 
 		$this->modified_at = $modified_at;
 		return $this; 
	}
	public function getModifiedat(){ 
 		return $this->modified_at; 
	}

}