<?php
namespace CreativeX\Model\Data;
/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 2017-12-04
 * Time: 05:27:07 PM
 */
class DataSuser
{
	public $id = NULL;
	public $id_group = NULL;
	public $id_profile = NULL;
	public $username = NULL;
	public $password = NULL;
	public $email = NULL;
	public $active = NULL;
	public $reload_permisions = NULL;


    public function __construct($class = NULL ,$id = NULL ,$id_group = NULL ,$id_profile = NULL ,$username = NULL ,$password = NULL ,$email = NULL ,$active = NULL ,$reload_permisions = NULL){
        if(is_a($class, __CLASS__)) {
			$this->setId($id);
			$this->setIdgroup($id_group);
			$this->setIdprofile($id_profile);
			$this->setUsername($username);
			$this->setPassword($password);
			$this->setEmail($email);
			$this->setActive($active);
			$this->setReloadpermisions($reload_permisions);

        }
    }
	public function setId($id){ 
 		$this->id = $id;
 		return $this; 
	}
	public function getId(){ 
 		return $this->id; 
	}
	public function setIdgroup($id_group){ 
 		$this->id_group = $id_group;
 		return $this; 
	}
	public function getIdgroup(){ 
 		return $this->id_group; 
	}
	public function setIdprofile($id_profile){ 
 		$this->id_profile = $id_profile;
 		return $this; 
	}
	public function getIdprofile(){ 
 		return $this->id_profile; 
	}
	public function setUsername($username){ 
 		$this->username = $username;
 		return $this; 
	}
	public function getUsername(){ 
 		return $this->username; 
	}
	public function setPassword($password){ 
 		$this->password = $password;
 		return $this; 
	}
	public function getPassword(){ 
 		return $this->password; 
	}
	public function setEmail($email){ 
 		$this->email = $email;
 		return $this; 
	}
	public function getEmail(){ 
 		return $this->email; 
	}
	public function setActive($active){ 
 		$this->active = $active;
 		return $this; 
	}
	public function getActive(){ 
 		return $this->active; 
	}
	public function setReloadpermisions($reload_permisions){ 
 		$this->reload_permisions = $reload_permisions;
 		return $this; 
	}
	public function getReloadpermisions(){ 
 		return $this->reload_permisions; 
	}

}