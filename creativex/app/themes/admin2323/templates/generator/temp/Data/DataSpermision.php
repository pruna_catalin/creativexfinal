<?php
namespace CreativeX\Model\Data;
/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 2017-12-05
 * Time: 03:39:43 PM
 */
class DataSpermision
{
	public $id = NULL;
	public $permision_list = NULL;
	public $create_by = NULL;
	public $create_at = NULL;
	public $modified_by = NULL;
	public $modified_at = NULL;


    public function __construct($class = NULL ,$id = NULL ,$permision_list = NULL ,$create_by = NULL ,$create_at = NULL ,$modified_by = NULL ,$modified_at = NULL){
        if(is_a($class, __CLASS__)) {
			$this->setId($id);
			$this->setPermisionlist($permision_list);
			$this->setCreateby($create_by);
			$this->setCreateat($create_at);
			$this->setModifiedby($modified_by);
			$this->setModifiedat($modified_at);

        }
    }
	public function setId($id){ 
 		$this->id = $id;
 		return $this; 
	}
	public function getId(){ 
 		return $this->id; 
	}
	public function setPermisionlist($permision_list){ 
 		$this->permision_list = $permision_list;
 		return $this; 
	}
	public function getPermisionlist(){ 
 		return $this->permision_list; 
	}
	public function setCreateby($create_by){ 
 		$this->create_by = $create_by;
 		return $this; 
	}
	public function getCreateby(){ 
 		return $this->create_by; 
	}
	public function setCreateat($create_at){ 
 		$this->create_at = $create_at;
 		return $this; 
	}
	public function getCreateat(){ 
 		return $this->create_at; 
	}
	public function setModifiedby($modified_by){ 
 		$this->modified_by = $modified_by;
 		return $this; 
	}
	public function getModifiedby(){ 
 		return $this->modified_by; 
	}
	public function setModifiedat($modified_at){ 
 		$this->modified_at = $modified_at;
 		return $this; 
	}
	public function getModifiedat(){ 
 		return $this->modified_at; 
	}

}