<?php
namespace CreativeX\Model\Data;
/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 2017-12-05
 * Time: 03:39:43 PM
 */
class DataSprofile
{
	public $id = NULL;
	public $id_city = NULL;
	public $id_country = NULL;
	public $name = NULL;
	public $skill = NULL;
	public $language = NULL;


    public function __construct($class = NULL ,$id = NULL ,$id_city = NULL ,$id_country = NULL ,$name = NULL ,$skill = NULL ,$language = NULL){
        if(is_a($class, __CLASS__)) {
			$this->setId($id);
			$this->setIdcity($id_city);
			$this->setIdcountry($id_country);
			$this->setName($name);
			$this->setSkill($skill);
			$this->setLanguage($language);

        }
    }
	public function setId($id){ 
 		$this->id = $id;
 		return $this; 
	}
	public function getId(){ 
 		return $this->id; 
	}
	public function setIdcity($id_city){ 
 		$this->id_city = $id_city;
 		return $this; 
	}
	public function getIdcity(){ 
 		return $this->id_city; 
	}
	public function setIdcountry($id_country){ 
 		$this->id_country = $id_country;
 		return $this; 
	}
	public function getIdcountry(){ 
 		return $this->id_country; 
	}
	public function setName($name){ 
 		$this->name = $name;
 		return $this; 
	}
	public function getName(){ 
 		return $this->name; 
	}
	public function setSkill($skill){ 
 		$this->skill = $skill;
 		return $this; 
	}
	public function getSkill(){ 
 		return $this->skill; 
	}
	public function setLanguage($language){ 
 		$this->language = $language;
 		return $this; 
	}
	public function getLanguage(){ 
 		return $this->language; 
	}

}