<?php
namespace CreativeX\Model\Data;
/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 2017-12-05
 * Time: 03:39:42 PM
 */
class DataNorder
{
	public $id = NULL;
	public $id_customer = NULL;
	public $id_item = NULL;
	public $quantity = NULL;
	public $price = NULL;
	public $status = NULL;
	public $create_by = NULL;
	public $create_at = NULL;
	public $modified_by = NULL;
	public $modified_at = NULL;


    public function __construct($class = NULL ,$id = NULL ,$id_customer = NULL ,$id_item = NULL ,$quantity = NULL ,$price = NULL ,$status = NULL ,$create_by = NULL ,$create_at = NULL ,$modified_by = NULL ,$modified_at = NULL){
        if(is_a($class, __CLASS__)) {
			$this->setId($id);
			$this->setIdcustomer($id_customer);
			$this->setIditem($id_item);
			$this->setQuantity($quantity);
			$this->setPrice($price);
			$this->setStatus($status);
			$this->setCreateby($create_by);
			$this->setCreateat($create_at);
			$this->setModifiedby($modified_by);
			$this->setModifiedat($modified_at);

        }
    }
	public function setId($id){ 
 		$this->id = $id;
 		return $this; 
	}
	public function getId(){ 
 		return $this->id; 
	}
	public function setIdcustomer($id_customer){ 
 		$this->id_customer = $id_customer;
 		return $this; 
	}
	public function getIdcustomer(){ 
 		return $this->id_customer; 
	}
	public function setIditem($id_item){ 
 		$this->id_item = $id_item;
 		return $this; 
	}
	public function getIditem(){ 
 		return $this->id_item; 
	}
	public function setQuantity($quantity){ 
 		$this->quantity = $quantity;
 		return $this; 
	}
	public function getQuantity(){ 
 		return $this->quantity; 
	}
	public function setPrice($price){ 
 		$this->price = $price;
 		return $this; 
	}
	public function getPrice(){ 
 		return $this->price; 
	}
	public function setStatus($status){ 
 		$this->status = $status;
 		return $this; 
	}
	public function getStatus(){ 
 		return $this->status; 
	}
	public function setCreateby($create_by){ 
 		$this->create_by = $create_by;
 		return $this; 
	}
	public function getCreateby(){ 
 		return $this->create_by; 
	}
	public function setCreateat($create_at){ 
 		$this->create_at = $create_at;
 		return $this; 
	}
	public function getCreateat(){ 
 		return $this->create_at; 
	}
	public function setModifiedby($modified_by){ 
 		$this->modified_by = $modified_by;
 		return $this; 
	}
	public function getModifiedby(){ 
 		return $this->modified_by; 
	}
	public function setModifiedat($modified_at){ 
 		$this->modified_at = $modified_at;
 		return $this; 
	}
	public function getModifiedat(){ 
 		return $this->modified_at; 
	}

}