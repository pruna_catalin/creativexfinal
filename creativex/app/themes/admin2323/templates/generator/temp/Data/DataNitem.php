<?php
namespace CreativeX\Model\Data;
/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 2017-12-05
 * Time: 03:39:42 PM
 */
class DataNitem
{
	public $id = NULL;
	public $id_page = NULL;
	public $name = NULL;
	public $image = NULL;
	public $quantity = NULL;
	public $description = NULL;
	public $price = NULL;
	public $discount = NULL;
	public $special = NULL;
	public $create_by = NULL;
	public $create_at = NULL;
	public $modified_by = NULL;
	public $modified_at = NULL;


    public function __construct($class = NULL ,$id = NULL ,$id_page = NULL ,$name = NULL ,$image = NULL ,$quantity = NULL ,$description = NULL ,$price = NULL ,$discount = NULL ,$special = NULL ,$create_by = NULL ,$create_at = NULL ,$modified_by = NULL ,$modified_at = NULL){
        if(is_a($class, __CLASS__)) {
			$this->setId($id);
			$this->setIdpage($id_page);
			$this->setName($name);
			$this->setImage($image);
			$this->setQuantity($quantity);
			$this->setDescription($description);
			$this->setPrice($price);
			$this->setDiscount($discount);
			$this->setSpecial($special);
			$this->setCreateby($create_by);
			$this->setCreateat($create_at);
			$this->setModifiedby($modified_by);
			$this->setModifiedat($modified_at);

        }
    }
	public function setId($id){ 
 		$this->id = $id;
 		return $this; 
	}
	public function getId(){ 
 		return $this->id; 
	}
	public function setIdpage($id_page){ 
 		$this->id_page = $id_page;
 		return $this; 
	}
	public function getIdpage(){ 
 		return $this->id_page; 
	}
	public function setName($name){ 
 		$this->name = $name;
 		return $this; 
	}
	public function getName(){ 
 		return $this->name; 
	}
	public function setImage($image){ 
 		$this->image = $image;
 		return $this; 
	}
	public function getImage(){ 
 		return $this->image; 
	}
	public function setQuantity($quantity){ 
 		$this->quantity = $quantity;
 		return $this; 
	}
	public function getQuantity(){ 
 		return $this->quantity; 
	}
	public function setDescription($description){ 
 		$this->description = $description;
 		return $this; 
	}
	public function getDescription(){ 
 		return $this->description; 
	}
	public function setPrice($price){ 
 		$this->price = $price;
 		return $this; 
	}
	public function getPrice(){ 
 		return $this->price; 
	}
	public function setDiscount($discount){ 
 		$this->discount = $discount;
 		return $this; 
	}
	public function getDiscount(){ 
 		return $this->discount; 
	}
	public function setSpecial($special){ 
 		$this->special = $special;
 		return $this; 
	}
	public function getSpecial(){ 
 		return $this->special; 
	}
	public function setCreateby($create_by){ 
 		$this->create_by = $create_by;
 		return $this; 
	}
	public function getCreateby(){ 
 		return $this->create_by; 
	}
	public function setCreateat($create_at){ 
 		$this->create_at = $create_at;
 		return $this; 
	}
	public function getCreateat(){ 
 		return $this->create_at; 
	}
	public function setModifiedby($modified_by){ 
 		$this->modified_by = $modified_by;
 		return $this; 
	}
	public function getModifiedby(){ 
 		return $this->modified_by; 
	}
	public function setModifiedat($modified_at){ 
 		$this->modified_at = $modified_at;
 		return $this; 
	}
	public function getModifiedat(){ 
 		return $this->modified_at; 
	}

}