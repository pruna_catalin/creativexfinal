<?php
namespace CreativeX\Model\Data;
/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 2017-12-04
 * Time: 05:27:07 PM
 */
class DataNcategory
{
	public $id = NULL;
	public $id_page = NULL;
	public $name = NULL;
	public $url_seo = NULL;
	public $description = NULL;
	public $create_by = NULL;
	public $create_at = NULL;
	public $modified_by = NULL;
	public $modified_at = NULL;


    public function __construct($class = NULL ,$id = NULL ,$id_page = NULL ,$name = NULL ,$url_seo = NULL ,$description = NULL ,$create_by = NULL ,$create_at = NULL ,$modified_by = NULL ,$modified_at = NULL){
        if(is_a($class, __CLASS__)) {
			$this->setId($id);
			$this->setIdpage($id_page);
			$this->setName($name);
			$this->setUrlseo($url_seo);
			$this->setDescription($description);
			$this->setCreateby($create_by);
			$this->setCreateat($create_at);
			$this->setModifiedby($modified_by);
			$this->setModifiedat($modified_at);

        }
    }
	public function setId($id){ 
 		$this->id = $id;
 		return $this; 
	}
	public function getId(){ 
 		return $this->id; 
	}
	public function setIdpage($id_page){ 
 		$this->id_page = $id_page;
 		return $this; 
	}
	public function getIdpage(){ 
 		return $this->id_page; 
	}
	public function setName($name){ 
 		$this->name = $name;
 		return $this; 
	}
	public function getName(){ 
 		return $this->name; 
	}
	public function setUrlseo($url_seo){ 
 		$this->url_seo = $url_seo;
 		return $this; 
	}
	public function getUrlseo(){ 
 		return $this->url_seo; 
	}
	public function setDescription($description){ 
 		$this->description = $description;
 		return $this; 
	}
	public function getDescription(){ 
 		return $this->description; 
	}
	public function setCreateby($create_by){ 
 		$this->create_by = $create_by;
 		return $this; 
	}
	public function getCreateby(){ 
 		return $this->create_by; 
	}
	public function setCreateat($create_at){ 
 		$this->create_at = $create_at;
 		return $this; 
	}
	public function getCreateat(){ 
 		return $this->create_at; 
	}
	public function setModifiedby($modified_by){ 
 		$this->modified_by = $modified_by;
 		return $this; 
	}
	public function getModifiedby(){ 
 		return $this->modified_by; 
	}
	public function setModifiedat($modified_at){ 
 		$this->modified_at = $modified_at;
 		return $this; 
	}
	public function getModifiedat(){ 
 		return $this->modified_at; 
	}

}