<?php
namespace CreativeX\Model\Data;
/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 2017-12-05
 * Time: 03:39:43 PM
 */
class DataNpromotion
{
	public $id = NULL;
	public $name = NULL;
	public $discount = NULL;
	public $create_by = NULL;
	public $create_at = NULL;
	public $modified_by = NULL;
	public $modifed_at = NULL;


    public function __construct($class = NULL ,$id = NULL ,$name = NULL ,$discount = NULL ,$create_by = NULL ,$create_at = NULL ,$modified_by = NULL ,$modifed_at = NULL){
        if(is_a($class, __CLASS__)) {
			$this->setId($id);
			$this->setName($name);
			$this->setDiscount($discount);
			$this->setCreateby($create_by);
			$this->setCreateat($create_at);
			$this->setModifiedby($modified_by);
			$this->setModifedat($modifed_at);

        }
    }
	public function setId($id){ 
 		$this->id = $id;
 		return $this; 
	}
	public function getId(){ 
 		return $this->id; 
	}
	public function setName($name){ 
 		$this->name = $name;
 		return $this; 
	}
	public function getName(){ 
 		return $this->name; 
	}
	public function setDiscount($discount){ 
 		$this->discount = $discount;
 		return $this; 
	}
	public function getDiscount(){ 
 		return $this->discount; 
	}
	public function setCreateby($create_by){ 
 		$this->create_by = $create_by;
 		return $this; 
	}
	public function getCreateby(){ 
 		return $this->create_by; 
	}
	public function setCreateat($create_at){ 
 		$this->create_at = $create_at;
 		return $this; 
	}
	public function getCreateat(){ 
 		return $this->create_at; 
	}
	public function setModifiedby($modified_by){ 
 		$this->modified_by = $modified_by;
 		return $this; 
	}
	public function getModifiedby(){ 
 		return $this->modified_by; 
	}
	public function setModifedat($modifed_at){ 
 		$this->modifed_at = $modifed_at;
 		return $this; 
	}
	public function getModifedat(){ 
 		return $this->modifed_at; 
	}

}