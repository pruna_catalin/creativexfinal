<?php
namespace CreativeX\Model\DAO;
use CreativeX\Model\Data\DataSprofile;
use CreativeX\Model\Drivers\SqlPDO;
use CreativeX\modules\util\Debugger;
/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 2017-12-05
 * Time: 03:39:43 PM
 */
class DAOSprofile extends DAOAbstract {
	public static $order = "id ASC";
	private static function TableName() {
		return "s_profile";
	}

	/* 
     * @params  condition , resultType  = 'assoc|num|object|lazy' , null or empty return OBJECT
	 * @ return false on rows 0/ SUCCESS RETURN Sprofile WITH DATA
	 */
	public static function Find($data, $resultType = NULL) {
		$db = new SqlPDO('');
		$result = [];
		if ($data instanceof DataSprofile) {
			$result = DAOTools::FindByModel($db, $data, self::tableName(), self::$order)->fetch($db::SwitchResult($resultType));
		} else {
			if ($data != "") {
				$db->select('*')->from(self::tableName())->where($data)->orderby(self::$order)->prepare();
				$request = $db->execute();
				$result = $request->fetch($db::SwitchResult($resultType));
			}
		}
		return $result;
	}

	/* 
     * @params  condition , limit / row , resultType  = 'assoc|num|object|lazy' , null or empty return OBJECT
     * @ return false on rows 0
     */
	public static function FindAll($data, $limit = "", $resultType = NULL) {
		$db = new SqlPDO('');
		$result = [];
		$prepare = [];
		if ($data instanceof DataSprofile) {
			$prepare = DAOTools::FindByModel($db, $data, self::tableName(), self::$order,$limit)->fetchAll($db::SwitchResult($resultType));
		} else {
			if ($data != "") {
				if($limit != "")
					$db->select('*')->from(self::tableName())->where($data)->orderby(self::$order)->limit($limit)->prepare();
				else
					$db->select('*')->from(self::tableName())->where($data)->orderby(self::$order)->prepare();
				$request = $db->execute();
				$prepare = $request->fetchAll($db::SwitchResult($resultType));
			}else{
				if($limit != "")
					$db->select('*')->from(self::tableName())->orderby(self::$order)->limit($limit)->prepare();
				else
					$db->select('*')->from(self::tableName())->orderby(self::$order)->prepare();
				$request = $db->execute();
				$prepare = $request->fetchAll($db::SwitchResult($resultType));
            }
		}
		
        foreach($prepare as $data){
			$id = $data->id;
			$id_city = $data->id_city;
			$id_country = $data->id_country;
			$name = $data->name;
			$skill = $data->skill;
			$language = $data->language;

            $prepareObject = new DataSprofile(null,$id ,$id_city ,$id_country ,$name ,$skill ,$language);
            array_push($result,new DataSprofile($prepareObject,$id ,$id_city ,$id_country ,$name ,$skill ,$language));
        }
		return (sizeof($result) == 0 ) ? FALSE : $result;
	}
    /* 
     * @params  condition | Sprofile   
     * @ return false on FAILD
     */
	public static function Insert($model) {
		$dataParse = DAOTools::InsertModel($model);
		$data = array('input' => $dataParse[0]);
		if(sizeof($dataParse[0]) > 0 ){
			$sql = new SqlPDO($data);
			$sql->insert(self::tableName())->columns($dataParse[1])
				->values($dataParse[2])->prepare();
			$sql->execute();
			return $sql->lastInsertId();
		}else{
			return FALSE;
		}
	}
    /* 
     * @params  condition | Sprofile
     * @ return false on FAILD
     */
	public static function Update($model,$condition) {
		$dataParse = DAOTools::UpdateModel($model,$condition);
		if(sizeof($dataParse[1]) == 0){
			$data = $model;
		}else{
			$data = array('prepare' => array(':SET' =>$dataParse[0],':WHERE' => $condition.'=:'.$condition),
				'input' => $dataParse[1]);
		}
		$sql = new SqlPDO($data);
		$sql->update(self::tableName())->prepare();
        $result = $sql->execute();
		return ($result->rowCount() > 0 ) ? TRUE : FALSE;
	}

	public static function Delete($data) {
		$sql = new SqlPDO('');
		$sql->delete("")->from(self::tableName())->where($data)->prepare();
        $result = $sql->execute();
		return ($result->rowCount() > 0 ) ? TRUE : FALSE;
	}

	public static function Count(){
		$db = new SqlPDO('');
		$db->select('*')->from(self::tableName())->orderby(self::$order)->prepare();
		$request = $db->execute();
		$prepare = $request->fetchAll();
		return count($prepare);
	}
    


}