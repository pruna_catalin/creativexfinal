﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<tr>
	<td>{@$Npage->id@}</td>
	<td>{@$Npage->id_promotion@}</td>
	<td>{@$Npage->name@}</td>
	<td>{@$Npage->create_by@}</td>
	<td>{@$Npage->create_at@}</td>
	<td>{@$Npage->modified_by@}</td>
	<td>{@$Npage->modified_at@}</td>

	<td>
      <a class="btn btn-info" href="{@$App::LIVE_URL_ADMIN@}Npage/edit/{@$Npage->id@}">{@$Language->t('edit_button')@}</a>
      <a class="btn btn-danger" onclick="actionDelete('Npage/delete',{@$Npage->id@});">{@$Language->t('delete_button')@}</a>
  </td>
</tr>
              