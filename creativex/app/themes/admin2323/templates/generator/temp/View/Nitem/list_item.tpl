﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<tr>
	<td>{@$Nitem->id@}</td>
	<td>{@$Nitem->id_page@}</td>
	<td>{@$Nitem->name@}</td>
	<td>{@$Nitem->image@}</td>
	<td>{@$Nitem->quantity@}</td>
	<td>{@$Nitem->description@}</td>
	<td>{@$Nitem->price@}</td>
	<td>{@$Nitem->discount@}</td>
	<td>{@$Nitem->special@}</td>
	<td>{@$Nitem->create_by@}</td>
	<td>{@$Nitem->create_at@}</td>
	<td>{@$Nitem->modified_by@}</td>
	<td>{@$Nitem->modified_at@}</td>

	<td>
      <a class="btn btn-info" href="{@$App::LIVE_URL_ADMIN@}Nitem/edit/{@$Nitem->id@}">{@$Language->t('edit_button')@}</a>
      <a class="btn btn-danger" onclick="actionDelete('Nitem/delete',{@$Nitem->id@});">{@$Language->t('delete_button')@}</a>
  </td>
</tr>
              