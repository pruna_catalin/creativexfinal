﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<tr>
	<td>{@$Ncustomer->id@}</td>
	<td>{@$Ncustomer->id_profile@}</td>
	<td>{@$Ncustomer->username@}</td>
	<td>{@$Ncustomer->password@}</td>
	<td>{@$Ncustomer->activation_link@}</td>
	<td>{@$Ncustomer->create_by@}</td>
	<td>{@$Ncustomer->create_at@}</td>
	<td>{@$Ncustomer->modified_by@}</td>
	<td>{@$Ncustomer->modified_at@}</td>

	<td>
      <a class="btn btn-info" href="{@$App::LIVE_URL_ADMIN@}Ncustomer/edit/{@$Ncustomer->id@}">{@$Language->t('edit_button')@}</a>
      <a class="btn btn-danger" onclick="actionDelete('Ncustomer/delete',{@$Ncustomer->id@});">{@$Language->t('delete_button')@}</a>
  </td>
</tr>
              