﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<section class="content-header">
  <h1>
    Add
    <small>Norder</small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="{@$App::LIVE_URL_ADMIN@}index">
        <i class="fa fa-dashboard"></i> Home
      </a>
    </li>
    <li class="active">
      <a href="{@$App::LIVE_URL_ADMIN@}Norder"> {@$Language->t('list_Norder')@}</a>
    </li>
  </ol>
</section>
<section class="content">
  <form class="form-horizontal" action="{@$App::LIVE_URL_ADMIN@}Norder/new" method="POST">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">{@$Language->t('note'). "".$message@}</h3>
        </div>
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <br>
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tab_1" data-toggle="tab">{@$Language->t('global_info')@}</a>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                  <div class="box-body">
										<div class="form-group">
											<label for="inputid_customer" class="col-sm-2 control-label">{@$Language->t('id_customer')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputid_customer" class="form-control" name="Model[id_customer]" placeholder="{@$Language->t('id_customer')@}" value="" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputid_item" class="col-sm-2 control-label">{@$Language->t('id_item')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputid_item" class="form-control" name="Model[id_item]" placeholder="{@$Language->t('id_item')@}" value="" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputquantity" class="col-sm-2 control-label">{@$Language->t('quantity')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputquantity" class="form-control" name="Model[quantity]" placeholder="{@$Language->t('quantity')@}" value="" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputprice" class="col-sm-2 control-label">{@$Language->t('price')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputprice" class="form-control" name="Model[price]" placeholder="{@$Language->t('price')@}" value="" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputstatus" class="col-sm-2 control-label">{@$Language->t('status')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputstatus" class="form-control" name="Model[status]" placeholder="{@$Language->t('status')@}" value="" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputcreate_by" class="col-sm-2 control-label">{@$Language->t('create_by')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputcreate_by" class="form-control" name="Model[create_by]" placeholder="{@$Language->t('create_by')@}" value="" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputcreate_at" class="col-sm-2 control-label">{@$Language->t('create_at')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputcreate_at" class="form-control" name="Model[create_at]" placeholder="{@$Language->t('create_at')@}" value="" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputmodified_by" class="col-sm-2 control-label">{@$Language->t('modified_by')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputmodified_by" class="form-control" name="Model[modified_by]" placeholder="{@$Language->t('modified_by')@}" value="" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputmodified_at" class="col-sm-2 control-label">{@$Language->t('modified_at')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputmodified_at" class="form-control" name="Model[modified_at]" placeholder="{@$Language->t('modified_at')@}" value="" />
											</div>
										</div>

                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-info pull-right">{@$Language->t('save_button')@}</button>
                  </div>
                  <!-- /.box-footer -->

                </div>
                <!-- /.tab-pane -->
                  <!-- /.box-body -->
                  
                  <!-- /.box-footer -->
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
          </div>
        <!-- /.box-header -->
        <!-- form start -->

      </div>
    </form>
</section>