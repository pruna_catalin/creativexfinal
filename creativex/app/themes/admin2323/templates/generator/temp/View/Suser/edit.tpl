﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<section class="content-header">
  <h1>
    Edit
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="{@$App::LIVE_URL_ADMIN@}index">
        <i class="fa fa-dashboard"></i> Home
      </a>
    </li>
    <li class="active">
      <a href="{@$App::LIVE_URL_ADMIN@}Suser"> {@$Language->t('list_Suser')@}</a>
    </li>
  </ol>
</section>
<section class="content">
  <form class="form-horizontal" action="{@$App::LIVE_URL_ADMIN@}Suser/edit/{@$Suser->id@}" method="POST">
    <input type="hidden" name="Model[id]"  value="{@$Suser->id@}">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">{@$Language->t('note'). "".$message@}</h3>
        </div>
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <br>
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tab_1" data-toggle="tab">{@$Language->t('global_info')@}</a>
                </li>
               
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                  <div class="box-body">
										<div class="form-group">
											<label for="inputid_group" class="col-sm-2 control-label">{@$Language->t('id_group')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputid_group" class="form-control" name="Model[id_group]" placeholder="{@$Language->t('id_group')@}" value="{@$Suser->id_group@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputid_profile" class="col-sm-2 control-label">{@$Language->t('id_profile')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputid_profile" class="form-control" name="Model[id_profile]" placeholder="{@$Language->t('id_profile')@}" value="{@$Suser->id_profile@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputusername" class="col-sm-2 control-label">{@$Language->t('username')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputusername" class="form-control" name="Model[username]" placeholder="{@$Language->t('username')@}" value="{@$Suser->username@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputpassword" class="col-sm-2 control-label">{@$Language->t('password')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputpassword" class="form-control" name="Model[password]" placeholder="{@$Language->t('password')@}" value="{@$Suser->password@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputemail" class="col-sm-2 control-label">{@$Language->t('email')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputemail" class="form-control" name="Model[email]" placeholder="{@$Language->t('email')@}" value="{@$Suser->email@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputactive" class="col-sm-2 control-label">{@$Language->t('active')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputactive" class="form-control" name="Model[active]" placeholder="{@$Language->t('active')@}" value="{@$Suser->active@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputreload_permisions" class="col-sm-2 control-label">{@$Language->t('reload_permisions')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputreload_permisions" class="form-control" name="Model[reload_permisions]" placeholder="{@$Language->t('reload_permisions')@}" value="{@$Suser->reload_permisions@}" />
											</div>
										</div>

                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-info pull-right">{@$Language->t('save_button')@}</button>
                  </div>
                  <!-- /.box-footer -->

                </div>
                <!-- /.tab-pane -->
                  <!-- /.box-body -->
                  
                  <!-- /.box-footer -->
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
          </div>
        <!-- /.box-header -->
        <!-- form start -->

      </div>
    </form>
</section>