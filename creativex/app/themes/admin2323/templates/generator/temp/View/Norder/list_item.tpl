﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<tr>
	<td>{@$Norder->id@}</td>
	<td>{@$Norder->id_customer@}</td>
	<td>{@$Norder->id_item@}</td>
	<td>{@$Norder->quantity@}</td>
	<td>{@$Norder->price@}</td>
	<td>{@$Norder->status@}</td>
	<td>{@$Norder->create_by@}</td>
	<td>{@$Norder->create_at@}</td>
	<td>{@$Norder->modified_by@}</td>
	<td>{@$Norder->modified_at@}</td>

	<td>
      <a class="btn btn-info" href="{@$App::LIVE_URL_ADMIN@}Norder/edit/{@$Norder->id@}">{@$Language->t('edit_button')@}</a>
      <a class="btn btn-danger" onclick="actionDelete('Norder/delete',{@$Norder->id@});">{@$Language->t('delete_button')@}</a>
  </td>
</tr>
              