﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<section class="content-header">
  <h1>
    Add
    <small>Sprofile</small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="{@$App::LIVE_URL_ADMIN@}index">
        <i class="fa fa-dashboard"></i> Home
      </a>
    </li>
    <li class="active">
      <a href="{@$App::LIVE_URL_ADMIN@}Sprofile"> {@$Language->t('list_Sprofile')@}</a>
    </li>
  </ol>
</section>
<section class="content">
  <form class="form-horizontal" action="{@$App::LIVE_URL_ADMIN@}Sprofile/new" method="POST">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">{@$Language->t('note'). "".$message@}</h3>
        </div>
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <br>
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tab_1" data-toggle="tab">{@$Language->t('global_info')@}</a>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                  <div class="box-body">
										<div class="form-group">
											<label for="inputid_city" class="col-sm-2 control-label">{@$Language->t('id_city')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputid_city" class="form-control" name="Model[id_city]" placeholder="{@$Language->t('id_city')@}" value="" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputid_country" class="col-sm-2 control-label">{@$Language->t('id_country')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputid_country" class="form-control" name="Model[id_country]" placeholder="{@$Language->t('id_country')@}" value="" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputname" class="col-sm-2 control-label">{@$Language->t('name')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputname" class="form-control" name="Model[name]" placeholder="{@$Language->t('name')@}" value="" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputskill" class="col-sm-2 control-label">{@$Language->t('skill')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputskill" class="form-control" name="Model[skill]" placeholder="{@$Language->t('skill')@}" value="" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputlanguage" class="col-sm-2 control-label">{@$Language->t('language')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputlanguage" class="form-control" name="Model[language]" placeholder="{@$Language->t('language')@}" value="" />
											</div>
										</div>

                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-info pull-right">{@$Language->t('save_button')@}</button>
                  </div>
                  <!-- /.box-footer -->

                </div>
                <!-- /.tab-pane -->
                  <!-- /.box-body -->
                  
                  <!-- /.box-footer -->
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
          </div>
        <!-- /.box-header -->
        <!-- form start -->

      </div>
    </form>
</section>