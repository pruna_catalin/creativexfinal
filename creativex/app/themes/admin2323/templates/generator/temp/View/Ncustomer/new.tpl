﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<section class="content-header">
  <h1>
    Add
    <small>Ncustomer</small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="{@$App::LIVE_URL_ADMIN@}index">
        <i class="fa fa-dashboard"></i> Home
      </a>
    </li>
    <li class="active">
      <a href="{@$App::LIVE_URL_ADMIN@}Ncustomer"> {@$Language->t('list_Ncustomer')@}</a>
    </li>
  </ol>
</section>
<section class="content">
  <form class="form-horizontal" action="{@$App::LIVE_URL_ADMIN@}Ncustomer/new" method="POST">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">{@$Language->t('note'). "".$message@}</h3>
        </div>
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <br>
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tab_1" data-toggle="tab">{@$Language->t('global_info')@}</a>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                  <div class="box-body">
										<div class="form-group">
											<label for="inputid_profile" class="col-sm-2 control-label">{@$Language->t('id_profile')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputid_profile" class="form-control" name="Model[id_profile]" placeholder="{@$Language->t('id_profile')@}" value="" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputusername" class="col-sm-2 control-label">{@$Language->t('username')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputusername" class="form-control" name="Model[username]" placeholder="{@$Language->t('username')@}" value="" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputpassword" class="col-sm-2 control-label">{@$Language->t('password')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputpassword" class="form-control" name="Model[password]" placeholder="{@$Language->t('password')@}" value="" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputactivation_link" class="col-sm-2 control-label">{@$Language->t('activation_link')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputactivation_link" class="form-control" name="Model[activation_link]" placeholder="{@$Language->t('activation_link')@}" value="" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputcreate_by" class="col-sm-2 control-label">{@$Language->t('create_by')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputcreate_by" class="form-control" name="Model[create_by]" placeholder="{@$Language->t('create_by')@}" value="" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputcreate_at" class="col-sm-2 control-label">{@$Language->t('create_at')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputcreate_at" class="form-control" name="Model[create_at]" placeholder="{@$Language->t('create_at')@}" value="" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputmodified_by" class="col-sm-2 control-label">{@$Language->t('modified_by')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputmodified_by" class="form-control" name="Model[modified_by]" placeholder="{@$Language->t('modified_by')@}" value="" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputmodified_at" class="col-sm-2 control-label">{@$Language->t('modified_at')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputmodified_at" class="form-control" name="Model[modified_at]" placeholder="{@$Language->t('modified_at')@}" value="" />
											</div>
										</div>

                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-info pull-right">{@$Language->t('save_button')@}</button>
                  </div>
                  <!-- /.box-footer -->

                </div>
                <!-- /.tab-pane -->
                  <!-- /.box-body -->
                  
                  <!-- /.box-footer -->
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
          </div>
        <!-- /.box-header -->
        <!-- form start -->

      </div>
    </form>
</section>