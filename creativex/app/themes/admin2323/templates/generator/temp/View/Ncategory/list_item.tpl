﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<tr>
	<td>{@$Ncategory->id@}</td>
	<td>{@$Ncategory->id_page@}</td>
	<td>{@$Ncategory->name@}</td>
	<td>{@$Ncategory->url_seo@}</td>
	<td>{@$Ncategory->description@}</td>
	<td>{@$Ncategory->create_by@}</td>
	<td>{@$Ncategory->create_at@}</td>
	<td>{@$Ncategory->modified_by@}</td>
	<td>{@$Ncategory->modified_at@}</td>

	<td>
      <a class="btn btn-info" href="{@$App::LIVE_URL_ADMIN@}Ncategory/edit/{@$Ncategory->id@}">{@$Language->t('edit_button')@}</a>
      <a class="btn btn-danger" onclick="actionDelete('Ncategory/delete',{@$Ncategory->id@});">{@$Language->t('delete_button')@}</a>
  </td>
</tr>
              