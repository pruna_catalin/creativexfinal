﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<tr>
	<td>{@$Ncustomerprofile->id@}</td>
	<td>{@$Ncustomerprofile->name@}</td>
	<td>{@$Ncustomerprofile->address@}</td>
	<td>{@$Ncustomerprofile->email@}</td>
	<td>{@$Ncustomerprofile->phone@}</td>
	<td>{@$Ncustomerprofile->company@}</td>
	<td>{@$Ncustomerprofile->create_by@}</td>
	<td>{@$Ncustomerprofile->create_at@}</td>
	<td>{@$Ncustomerprofile->modified_by@}</td>
	<td>{@$Ncustomerprofile->modified_at@}</td>

	<td>
      <a class="btn btn-info" href="{@$App::LIVE_URL_ADMIN@}Ncustomerprofile/edit/{@$Ncustomerprofile->id@}">{@$Language->t('edit_button')@}</a>
      <a class="btn btn-danger" onclick="actionDelete('Ncustomerprofile/delete',{@$Ncustomerprofile->id@});">{@$Language->t('delete_button')@}</a>
  </td>
</tr>
              