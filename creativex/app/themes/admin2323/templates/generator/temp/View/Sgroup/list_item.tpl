﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<tr>
	<td>{@$Sgroup->id@}</td>
	<td>{@$Sgroup->id_permision@}</td>
	<td>{@$Sgroup->name@}</td>
	<td>{@$Sgroup->create_by@}</td>
	<td>{@$Sgroup->create_at@}</td>
	<td>{@$Sgroup->modified_by@}</td>
	<td>{@$Sgroup->modified_at@}</td>

	<td>
      <a class="btn btn-info" href="{@$App::LIVE_URL_ADMIN@}Sgroup/edit/{@$Sgroup->id@}">{@$Language->t('edit_button')@}</a>
      <a class="btn btn-danger" onclick="actionDelete('Sgroup/delete',{@$Sgroup->id@});">{@$Language->t('delete_button')@}</a>
  </td>
</tr>
              