﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<tr>
	<td>{@$Nsocial->id@}</td>
	<td>{@$Nsocial->id_profile@}</td>
	<td>{@$Nsocial->name@}</td>
	<td>{@$Nsocial->link@}</td>
	<td>{@$Nsocial->create_by@}</td>
	<td>{@$Nsocial->create_at@}</td>
	<td>{@$Nsocial->modified_by@}</td>
	<td>{@$Nsocial->modified_at@}</td>

	<td>
      <a class="btn btn-info" href="{@$App::LIVE_URL_ADMIN@}Nsocial/edit/{@$Nsocial->id@}">{@$Language->t('edit_button')@}</a>
      <a class="btn btn-danger" onclick="actionDelete('Nsocial/delete',{@$Nsocial->id@});">{@$Language->t('delete_button')@}</a>
  </td>
</tr>
              