﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<section class="content-header">
  <h1>
    {@$Language->t('list_Npromotion')@}
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="{@$App::LIVE_URL_ADMIN@}index">
        <i class="fa fa-dashboard"></i> Home
      </a>
    </li>
    <li class="active">Npromotion</li>
  </ol>
</section>
<section class="content">
  <form class="form-horizontal" action="{@$App::LIVE_URL_ADMIN@}Npromotion" method="POST" style="text-align:center">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title  pull-left">Total : {@$totalNpromotion@}</h3>
        <a href="{@$App::LIVE_URL_ADMIN@}Npromotion/new" class="btn btn-success pull-right">{@$Language->t('add_button')@}</a>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-bordered">
          <tr>
						<th style="width: 20px">
								#<input type="text" class="form-control" name="Model[search][id]"  value="{@$filter['id']@}"/>
						</th>
						<th style="width: 20px">
								<input type="text" class="form-control" name="Model[search][name]"  value="{@$filter['name']@}"/>
						</th>
						<th style="width: 20px">
								<input type="text" class="form-control" name="Model[search][discount]"  value="{@$filter['discount']@}"/>
						</th>
						<th style="width: 20px">
								<input type="text" class="form-control" name="Model[search][create_by]"  value="{@$filter['create_by']@}"/>
						</th>
						<th style="width: 20px">
								<input type="text" class="form-control" name="Model[search][create_at]"  value="{@$filter['create_at']@}"/>
						</th>
						<th style="width: 20px">
								<input type="text" class="form-control" name="Model[search][modified_by]"  value="{@$filter['modified_by']@}"/>
						</th>
						<th style="width: 20px">
								<input type="text" class="form-control" name="Model[search][modifed_at]"  value="{@$filter['modifed_at']@}"/>
						</th>

            <th>
              {@$Language->t('actions')@}<br/>
              <input type="submit" class="btn btn-info pull-left btn-sm" style="width:40%" value="{@$Language->t('search_button')@}"/>
              <a class="btn btn-warning pull-left btn-sm col-md-offset-1" style="width:50%" onclick="location.href = '{@$App::LIVE_URL_ADMIN@}Npromotion';">{@$Language->t('reset_button')@}</a>
            </th>
          </tr>
          {@$item@}
        </table>
      </div>
      <!-- /.box-body -->
      <?php
  $contents = "";
    for($i=0; $i< $totalNpromotion / $App::MAX_PERPAGE; $i++) {
      if($i == 0 || ($page_no > $i - $App::MAX_PSPAGE && $page_no < $i + $App::MAX_PSPAGE) || $i == $totalNpromotion/$App::MAX_PERPAGE -1) {
			    if($page_no == $i) {
				    $contents .= '<li class="active"><button class="btn btn-danger btn-xs" type="submit" name="Model[pagination]" value="'.($i+1).'">'.($i+1).'</button></li>';
          }else {
            $contents .= '<li><button class="btn btn-info btn-xs" type="submit" name="Model[pagination]" value="'.($i+1).'">'.($i+1).'</button></li>';
          }
      } else {

      //if(($i == $page_no - $ps_page || $i == $page_no + $ps_page) && $i != 0)

      //$contents .= '...';

      }

    }
  ?>
      <div class="box-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-right">
          <li>
            <button class="btn btn-info btn-xs" type="submit" name="Model[pagination]" value="0">&laquo;</button>
          </li>
          {@$contents@}
          <li>
            <button class="btn btn-info btn-xs" type="submit" name="Model[pagination]" value="{@ceil($totalNpromotion / $App::MAX_PERPAGE)@}">&raquo;</button>
          </li>
        </ul>
      </div>
    </div>
  </form>
  <script>
    var deleteMessage = "{@$Language->t('message_delete_question')@}";
    var faildMessage = "{@$Language->t('message_delete_error')@}";
  </script>
</section>