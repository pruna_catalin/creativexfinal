﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<section class="content-header">
  <h1>
    Add
    <small>Npromotion</small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="{@$App::LIVE_URL_ADMIN@}index">
        <i class="fa fa-dashboard"></i> Home
      </a>
    </li>
    <li class="active">
      <a href="{@$App::LIVE_URL_ADMIN@}Npromotion"> {@$Language->t('list_Npromotion')@}</a>
    </li>
  </ol>
</section>
<section class="content">
  <form class="form-horizontal" action="{@$App::LIVE_URL_ADMIN@}Npromotion/new" method="POST">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">{@$Language->t('note'). "".$message@}</h3>
        </div>
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <br>
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tab_1" data-toggle="tab">{@$Language->t('global_info')@}</a>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                  <div class="box-body">
										<div class="form-group">
											<label for="inputname" class="col-sm-2 control-label">{@$Language->t('name')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputname" class="form-control" name="Model[name]" placeholder="{@$Language->t('name')@}" value="" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputdiscount" class="col-sm-2 control-label">{@$Language->t('discount')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputdiscount" class="form-control" name="Model[discount]" placeholder="{@$Language->t('discount')@}" value="" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputcreate_by" class="col-sm-2 control-label">{@$Language->t('create_by')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputcreate_by" class="form-control" name="Model[create_by]" placeholder="{@$Language->t('create_by')@}" value="" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputcreate_at" class="col-sm-2 control-label">{@$Language->t('create_at')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputcreate_at" class="form-control" name="Model[create_at]" placeholder="{@$Language->t('create_at')@}" value="" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputmodified_by" class="col-sm-2 control-label">{@$Language->t('modified_by')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputmodified_by" class="form-control" name="Model[modified_by]" placeholder="{@$Language->t('modified_by')@}" value="" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputmodifed_at" class="col-sm-2 control-label">{@$Language->t('modifed_at')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputmodifed_at" class="form-control" name="Model[modifed_at]" placeholder="{@$Language->t('modifed_at')@}" value="" />
											</div>
										</div>

                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-info pull-right">{@$Language->t('save_button')@}</button>
                  </div>
                  <!-- /.box-footer -->

                </div>
                <!-- /.tab-pane -->
                  <!-- /.box-body -->
                  
                  <!-- /.box-footer -->
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
          </div>
        <!-- /.box-header -->
        <!-- form start -->

      </div>
    </form>
</section>