﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<section class="content-header">
  <h1>
    Edit
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="{@$App::LIVE_URL_ADMIN@}index">
        <i class="fa fa-dashboard"></i> Home
      </a>
    </li>
    <li class="active">
      <a href="{@$App::LIVE_URL_ADMIN@}Nitem"> {@$Language->t('list_Nitem')@}</a>
    </li>
  </ol>
</section>
<section class="content">
  <form class="form-horizontal" action="{@$App::LIVE_URL_ADMIN@}Nitem/edit/{@$Nitem->id@}" method="POST">
    <input type="hidden" name="Model[id]"  value="{@$Nitem->id@}">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">{@$Language->t('note'). "".$message@}</h3>
        </div>
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <br>
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tab_1" data-toggle="tab">{@$Language->t('global_info')@}</a>
                </li>
               
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                  <div class="box-body">
										<div class="form-group">
											<label for="inputid_page" class="col-sm-2 control-label">{@$Language->t('id_page')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputid_page" class="form-control" name="Model[id_page]" placeholder="{@$Language->t('id_page')@}" value="{@$Nitem->id_page@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputname" class="col-sm-2 control-label">{@$Language->t('name')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputname" class="form-control" name="Model[name]" placeholder="{@$Language->t('name')@}" value="{@$Nitem->name@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputimage" class="col-sm-2 control-label">{@$Language->t('image')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputimage" class="form-control" name="Model[image]" placeholder="{@$Language->t('image')@}" value="{@$Nitem->image@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputquantity" class="col-sm-2 control-label">{@$Language->t('quantity')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputquantity" class="form-control" name="Model[quantity]" placeholder="{@$Language->t('quantity')@}" value="{@$Nitem->quantity@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputdescription" class="col-sm-2 control-label">{@$Language->t('description')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputdescription" class="form-control" name="Model[description]" placeholder="{@$Language->t('description')@}" value="{@$Nitem->description@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputprice" class="col-sm-2 control-label">{@$Language->t('price')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputprice" class="form-control" name="Model[price]" placeholder="{@$Language->t('price')@}" value="{@$Nitem->price@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputdiscount" class="col-sm-2 control-label">{@$Language->t('discount')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputdiscount" class="form-control" name="Model[discount]" placeholder="{@$Language->t('discount')@}" value="{@$Nitem->discount@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputspecial" class="col-sm-2 control-label">{@$Language->t('special')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputspecial" class="form-control" name="Model[special]" placeholder="{@$Language->t('special')@}" value="{@$Nitem->special@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputcreate_by" class="col-sm-2 control-label">{@$Language->t('create_by')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputcreate_by" class="form-control" name="Model[create_by]" placeholder="{@$Language->t('create_by')@}" value="{@$Nitem->create_by@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputcreate_at" class="col-sm-2 control-label">{@$Language->t('create_at')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputcreate_at" class="form-control" name="Model[create_at]" placeholder="{@$Language->t('create_at')@}" value="{@$Nitem->create_at@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputmodified_by" class="col-sm-2 control-label">{@$Language->t('modified_by')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputmodified_by" class="form-control" name="Model[modified_by]" placeholder="{@$Language->t('modified_by')@}" value="{@$Nitem->modified_by@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputmodified_at" class="col-sm-2 control-label">{@$Language->t('modified_at')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputmodified_at" class="form-control" name="Model[modified_at]" placeholder="{@$Language->t('modified_at')@}" value="{@$Nitem->modified_at@}" />
											</div>
										</div>

                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-info pull-right">{@$Language->t('save_button')@}</button>
                  </div>
                  <!-- /.box-footer -->

                </div>
                <!-- /.tab-pane -->
                  <!-- /.box-body -->
                  
                  <!-- /.box-footer -->
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
          </div>
        <!-- /.box-header -->
        <!-- form start -->

      </div>
    </form>
</section>