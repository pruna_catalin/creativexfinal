﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<section class="content-header">
  <h1>
    Edit
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="{@$App::LIVE_URL_ADMIN@}index">
        <i class="fa fa-dashboard"></i> Home
      </a>
    </li>
    <li class="active">
      <a href="{@$App::LIVE_URL_ADMIN@}Ncustomerprofile"> {@$Language->t('list_Ncustomerprofile')@}</a>
    </li>
  </ol>
</section>
<section class="content">
  <form class="form-horizontal" action="{@$App::LIVE_URL_ADMIN@}Ncustomerprofile/edit/{@$Ncustomerprofile->id@}" method="POST">
    <input type="hidden" name="Model[id]"  value="{@$Ncustomerprofile->id@}">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">{@$Language->t('note'). "".$message@}</h3>
        </div>
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <br>
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tab_1" data-toggle="tab">{@$Language->t('global_info')@}</a>
                </li>
               
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                  <div class="box-body">
										<div class="form-group">
											<label for="inputname" class="col-sm-2 control-label">{@$Language->t('name')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputname" class="form-control" name="Model[name]" placeholder="{@$Language->t('name')@}" value="{@$Ncustomerprofile->name@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputaddress" class="col-sm-2 control-label">{@$Language->t('address')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputaddress" class="form-control" name="Model[address]" placeholder="{@$Language->t('address')@}" value="{@$Ncustomerprofile->address@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputemail" class="col-sm-2 control-label">{@$Language->t('email')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputemail" class="form-control" name="Model[email]" placeholder="{@$Language->t('email')@}" value="{@$Ncustomerprofile->email@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputphone" class="col-sm-2 control-label">{@$Language->t('phone')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputphone" class="form-control" name="Model[phone]" placeholder="{@$Language->t('phone')@}" value="{@$Ncustomerprofile->phone@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputcompany" class="col-sm-2 control-label">{@$Language->t('company')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputcompany" class="form-control" name="Model[company]" placeholder="{@$Language->t('company')@}" value="{@$Ncustomerprofile->company@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputcreate_by" class="col-sm-2 control-label">{@$Language->t('create_by')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputcreate_by" class="form-control" name="Model[create_by]" placeholder="{@$Language->t('create_by')@}" value="{@$Ncustomerprofile->create_by@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputcreate_at" class="col-sm-2 control-label">{@$Language->t('create_at')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputcreate_at" class="form-control" name="Model[create_at]" placeholder="{@$Language->t('create_at')@}" value="{@$Ncustomerprofile->create_at@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputmodified_by" class="col-sm-2 control-label">{@$Language->t('modified_by')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputmodified_by" class="form-control" name="Model[modified_by]" placeholder="{@$Language->t('modified_by')@}" value="{@$Ncustomerprofile->modified_by@}" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputmodified_at" class="col-sm-2 control-label">{@$Language->t('modified_at')@}</label>
											<div class="col-sm-10">
												<input type="text" id="inputmodified_at" class="form-control" name="Model[modified_at]" placeholder="{@$Language->t('modified_at')@}" value="{@$Ncustomerprofile->modified_at@}" />
											</div>
										</div>

                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-info pull-right">{@$Language->t('save_button')@}</button>
                  </div>
                  <!-- /.box-footer -->

                </div>
                <!-- /.tab-pane -->
                  <!-- /.box-body -->
                  
                  <!-- /.box-footer -->
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
          </div>
        <!-- /.box-header -->
        <!-- form start -->

      </div>
    </form>
</section>