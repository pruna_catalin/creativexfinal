/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 2017-12-05
 * Time: 03:39:43 PM
 */
$(document).ready(function(){
	
});

function actionDelete(action,id){
	bootbox.confirm(deleteMessage, function(result) {
		if (result === false) {} else {
			$.ajax({
			    url: action,
				type: 'POST', 
				data : "Model[delete]="+id,    
				dataType:'json', 
				success : function(data) {  
					if (data.success) {
					    location.href = "Npromotion";
					}else{
					    toastr.error(data.message);
					}
				}
			});
		}
	});
}