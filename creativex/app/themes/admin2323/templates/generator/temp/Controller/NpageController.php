<?php
namespace CreativeX\core\controller\admin;
use CreativeX\Model\DAO\DAONpage;
use CreativeX\Model\Data\DataNpage;
use CreativeX\modules\util\Debugger;
use CreativeX\modules\render\View;
use CreativeX\config\application;
/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 2017-12-05
 * Time: 03:39:43 PM
 */
class NpageController{
	private static $request, $lang;
	public function __construct($request, $lang = null) {
		self::$request = $request;
		self::$lang = $lang;
	}
	public function actionList(){
		$dataNpage = new DataNpage();
		$ValueFilter = ["id"=> "" ,"id_promotion"=> "" ,"name"=> "" ,"create_by"=> "" ,"create_at"=> "" ,"modified_by"=> "" ,"modified_at"=> ""];
		$limit = "0,".application::MAX_PERPAGE;
		$totalNpage = DAONpage::Count();
		$page_no = 1;
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['search'])) {
				if(isset($params['Model']['search']['id']))
					if($params['Model']['search']['id'] != "")
						$dataNpage->id =  is_numeric($params['Model']['search']['id']) ? $params['Model']['search']['id'] : 0;
				if(isset($params['Model']['search']['id_promotion']))
					if($params['Model']['search']['id_promotion'] != "")
						$dataNpage->id_promotion =  is_numeric($params['Model']['search']['id_promotion']) ? $params['Model']['search']['id_promotion'] : 0;
				if(isset($params['Model']['search']['name']))
					if($params['Model']['search']['name'] != "")
						$dataNpage->name['like'] = ["%".$params['Model']['search']['name']."%"];
				if(isset($params['Model']['search']['create_by']))
					if($params['Model']['search']['create_by'] != "")
						$dataNpage->create_by =  is_numeric($params['Model']['search']['create_by']) ? $params['Model']['search']['create_by'] : 0;
				if(isset($params['Model']['search']['create_at']))
					if($params['Model']['search']['create_at'] != "")
						$dataNpage->create_at['like'] = ["%".$params['Model']['search']['create_at']."%"];
				if(isset($params['Model']['search']['modified_by']))
					if($params['Model']['search']['modified_by'] != "")
						$dataNpage->modified_by =  is_numeric($params['Model']['search']['modified_by']) ? $params['Model']['search']['modified_by'] : 0;
				if(isset($params['Model']['search']['modified_at']))
					if($params['Model']['search']['modified_at'] != "")
						$dataNpage->modified_at['like'] = ["%".$params['Model']['search']['modified_at']."%"];

				
			}
			if(isset($params['Model']['pagination'])){
				if(is_numeric($params['Model']['pagination'])){
					if($params['Model']['pagination'] < 1){
						$page_no = 1;
					}else{
						$page_no = $params['Model']['pagination'];
					}
					$page_no -= 1;
					$limit = ($page_no * application::MAX_PERPAGE).",".application::MAX_PERPAGE;
				}
			}

		}
		DAONpage::$order = "id ASC";
		#\Debugger::log($limit);
		$Npage = DAONpage::FindAll($dataNpage,$limit);
		$NpageList = "";
		if($Npage){
			foreach($Npage as $item){
				$child = new View( ["Npage/list_item","admin"], ["Npage"=>$item,"App"=> new application()]);
				$NpageList .= $child->render();
			}
		}
		$view =  new View( ["Npage/list","admin"], ["totalNpage"=>$totalNpage,"items"=>$NpageList,
													"App"=> new application(),"filter"=>$ValueFilter,"page_no"=>$page_no]);
        return  $view;
	}
	public function actionDelete(){
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['delete'])){
				$id = is_numeric($params['Model']['delete']) ? $params['Model']['delete'] : 0;
				if(DAONpage::Find("id=".$id)){
					if(DAONpage::Delete("id=".$id)){
						return json_encode(["success"=>true]);
					}
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}else{
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}
			}
		}
		exit;
	}
	public function actionEdit(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && self::$request->Method[1] != "POST"){
				$id = is_numeric($params['Model']) ? $params['Model'] : 0;
				$model = DAONpage::Find("id=".$id);
				if(!$model) return self::$lang->t('notfound');
			}else{
				$id = is_numeric($params['Model']['id']) ? $params['Model']['id'] : 0;
				$model = DAONpage::Find("id=".$id);
				if(!$model) return self::$lang->t('notfound');
				$dataNpage = new DataNpage();
				$dataNpage->id = $params['Model']['search']['id']; 
				$dataNpage->id_promotion = $params['Model']['search']['id_promotion']; 
				$dataNpage->name = $params['Model']['search']['name']; 
				$dataNpage->create_by = $params['Model']['search']['create_by']; 
				$dataNpage->create_at = $params['Model']['search']['create_at']; 
				$dataNpage->modified_by = $params['Model']['search']['modified_by']; 
				$dataNpage->modified_at = $params['Model']['search']['modified_at'];
				$success = false;
				if(DAONpage::Update($dataNpage,"id")){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/Npage/edit/".$dataNpage->id,true);

				//\Utils::Debug(self::$request);
			}					
		}
		$view =  new View(	["Npage/edit","admin"],
								["Npage"=>$model,
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params]
					  );
		return  $view;
	}
	public function actionAdd(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && $params['Model'] != ""){
				$dataNpage = new DataNpage();
				$dataNpage->id = $params['Model']['search']['id']; 
				$dataNpage->id_promotion = $params['Model']['search']['id_promotion']; 
				$dataNpage->name = $params['Model']['search']['name']; 
				$dataNpage->create_by = $params['Model']['search']['create_by']; 
				$dataNpage->create_at = $params['Model']['search']['create_at']; 
				$dataNpage->modified_by = $params['Model']['search']['modified_by']; 
				$dataNpage->modified_at = $params['Model']['search']['modified_at'];	
				$model = DAONpage::Insert($dataNpage);
				$success =	false;
				if($model){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/Npage/edit/".$model,true);
			}					
		}
		$view =  new View(	["Npage/new","admin"],
								["Npage"=>"",
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params
							]
					  );
		return  $view;
	}
	
}