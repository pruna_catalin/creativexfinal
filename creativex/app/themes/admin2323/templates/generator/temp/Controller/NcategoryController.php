<?php
namespace CreativeX\core\controller\admin;
use CreativeX\Model\DAO\DAONcategory;
use CreativeX\Model\Data\DataNcategory;
use CreativeX\modules\util\Debugger;
use CreativeX\modules\render\View;
use CreativeX\config\application;
/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 2017-12-05
 * Time: 03:39:42 PM
 */
class NcategoryController{
	private static $request, $lang;
	public function __construct($request, $lang = null) {
		self::$request = $request;
		self::$lang = $lang;
	}
	public function actionList(){
		$dataNcategory = new DataNcategory();
		$ValueFilter = ["id"=> "" ,"id_page"=> "" ,"name"=> "" ,"url_seo"=> "" ,"description"=> "" ,"create_by"=> "" ,"create_at"=> "" ,"modified_by"=> "" ,"modified_at"=> ""];
		$limit = "0,".application::MAX_PERPAGE;
		$totalNcategory = DAONcategory::Count();
		$page_no = 1;
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['search'])) {
				if(isset($params['Model']['search']['id']))
					if($params['Model']['search']['id'] != "")
						$dataNcategory->id =  is_numeric($params['Model']['search']['id']) ? $params['Model']['search']['id'] : 0;
				if(isset($params['Model']['search']['id_page']))
					if($params['Model']['search']['id_page'] != "")
						$dataNcategory->id_page =  is_numeric($params['Model']['search']['id_page']) ? $params['Model']['search']['id_page'] : 0;
				if(isset($params['Model']['search']['name']))
					if($params['Model']['search']['name'] != "")
						$dataNcategory->name['like'] = ["%".$params['Model']['search']['name']."%"];
				if(isset($params['Model']['search']['url_seo']))
					if($params['Model']['search']['url_seo'] != "")
						$dataNcategory->url_seo['like'] = ["%".$params['Model']['search']['url_seo']."%"];
				if(isset($params['Model']['search']['description']))
					if($params['Model']['search']['description'] != "")
						$dataNcategory->description['like'] = ["%".$params['Model']['search']['description']."%"];
				if(isset($params['Model']['search']['create_by']))
					if($params['Model']['search']['create_by'] != "")
						$dataNcategory->create_by =  is_numeric($params['Model']['search']['create_by']) ? $params['Model']['search']['create_by'] : 0;
				if(isset($params['Model']['search']['create_at']))
					if($params['Model']['search']['create_at'] != "")
						$dataNcategory->create_at['like'] = ["%".$params['Model']['search']['create_at']."%"];
				if(isset($params['Model']['search']['modified_by']))
					if($params['Model']['search']['modified_by'] != "")
						$dataNcategory->modified_by =  is_numeric($params['Model']['search']['modified_by']) ? $params['Model']['search']['modified_by'] : 0;
				if(isset($params['Model']['search']['modified_at']))
					if($params['Model']['search']['modified_at'] != "")
						$dataNcategory->modified_at['like'] = ["%".$params['Model']['search']['modified_at']."%"];

				
			}
			if(isset($params['Model']['pagination'])){
				if(is_numeric($params['Model']['pagination'])){
					if($params['Model']['pagination'] < 1){
						$page_no = 1;
					}else{
						$page_no = $params['Model']['pagination'];
					}
					$page_no -= 1;
					$limit = ($page_no * application::MAX_PERPAGE).",".application::MAX_PERPAGE;
				}
			}

		}
		DAONcategory::$order = "id ASC";
		#\Debugger::log($limit);
		$Ncategory = DAONcategory::FindAll($dataNcategory,$limit);
		$NcategoryList = "";
		if($Ncategory){
			foreach($Ncategory as $item){
				$child = new View( ["Ncategory/list_item","admin"], ["Ncategory"=>$item,"App"=> new application()]);
				$NcategoryList .= $child->render();
			}
		}
		$view =  new View( ["Ncategory/list","admin"], ["totalNcategory"=>$totalNcategory,"items"=>$NcategoryList,
													"App"=> new application(),"filter"=>$ValueFilter,"page_no"=>$page_no]);
        return  $view;
	}
	public function actionDelete(){
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['delete'])){
				$id = is_numeric($params['Model']['delete']) ? $params['Model']['delete'] : 0;
				if(DAONcategory::Find("id=".$id)){
					if(DAONcategory::Delete("id=".$id)){
						return json_encode(["success"=>true]);
					}
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}else{
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}
			}
		}
		exit;
	}
	public function actionEdit(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && self::$request->Method[1] != "POST"){
				$id = is_numeric($params['Model']) ? $params['Model'] : 0;
				$model = DAONcategory::Find("id=".$id);
				if(!$model) return self::$lang->t('notfound');
			}else{
				$id = is_numeric($params['Model']['id']) ? $params['Model']['id'] : 0;
				$model = DAONcategory::Find("id=".$id);
				if(!$model) return self::$lang->t('notfound');
				$dataNcategory = new DataNcategory();
				$dataNcategory->id = $params['Model']['search']['id']; 
				$dataNcategory->id_page = $params['Model']['search']['id_page']; 
				$dataNcategory->name = $params['Model']['search']['name']; 
				$dataNcategory->url_seo = $params['Model']['search']['url_seo']; 
				$dataNcategory->description = $params['Model']['search']['description']; 
				$dataNcategory->create_by = $params['Model']['search']['create_by']; 
				$dataNcategory->create_at = $params['Model']['search']['create_at']; 
				$dataNcategory->modified_by = $params['Model']['search']['modified_by']; 
				$dataNcategory->modified_at = $params['Model']['search']['modified_at'];
				$success = false;
				if(DAONcategory::Update($dataNcategory,"id")){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/Ncategory/edit/".$dataNcategory->id,true);

				//\Utils::Debug(self::$request);
			}					
		}
		$view =  new View(	["Ncategory/edit","admin"],
								["Ncategory"=>$model,
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params]
					  );
		return  $view;
	}
	public function actionAdd(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && $params['Model'] != ""){
				$dataNcategory = new DataNcategory();
				$dataNcategory->id = $params['Model']['search']['id']; 
				$dataNcategory->id_page = $params['Model']['search']['id_page']; 
				$dataNcategory->name = $params['Model']['search']['name']; 
				$dataNcategory->url_seo = $params['Model']['search']['url_seo']; 
				$dataNcategory->description = $params['Model']['search']['description']; 
				$dataNcategory->create_by = $params['Model']['search']['create_by']; 
				$dataNcategory->create_at = $params['Model']['search']['create_at']; 
				$dataNcategory->modified_by = $params['Model']['search']['modified_by']; 
				$dataNcategory->modified_at = $params['Model']['search']['modified_at'];	
				$model = DAONcategory::Insert($dataNcategory);
				$success =	false;
				if($model){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/Ncategory/edit/".$model,true);
			}					
		}
		$view =  new View(	["Ncategory/new","admin"],
								["Ncategory"=>"",
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params
							]
					  );
		return  $view;
	}
	
}