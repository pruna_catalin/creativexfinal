<?php
namespace CreativeX\core\controller\admin;
use CreativeX\Model\DAO\DAONitem;
use CreativeX\Model\Data\DataNitem;
use CreativeX\modules\util\Debugger;
use CreativeX\modules\render\View;
use CreativeX\config\application;
/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 2017-12-05
 * Time: 03:39:42 PM
 */
class NitemController{
	private static $request, $lang;
	public function __construct($request, $lang = null) {
		self::$request = $request;
		self::$lang = $lang;
	}
	public function actionList(){
		$dataNitem = new DataNitem();
		$ValueFilter = ["id"=> "" ,"id_page"=> "" ,"name"=> "" ,"image"=> "" ,"quantity"=> "" ,"description"=> "" ,"price"=> "" ,"discount"=> "" ,"special"=> "" ,"create_by"=> "" ,"create_at"=> "" ,"modified_by"=> "" ,"modified_at"=> ""];
		$limit = "0,".application::MAX_PERPAGE;
		$totalNitem = DAONitem::Count();
		$page_no = 1;
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['search'])) {
				if(isset($params['Model']['search']['id']))
					if($params['Model']['search']['id'] != "")
						$dataNitem->id =  is_numeric($params['Model']['search']['id']) ? $params['Model']['search']['id'] : 0;
				if(isset($params['Model']['search']['id_page']))
					if($params['Model']['search']['id_page'] != "")
						$dataNitem->id_page =  is_numeric($params['Model']['search']['id_page']) ? $params['Model']['search']['id_page'] : 0;
				if(isset($params['Model']['search']['name']))
					if($params['Model']['search']['name'] != "")
						$dataNitem->name['like'] = ["%".$params['Model']['search']['name']."%"];
				if(isset($params['Model']['search']['image']))
					if($params['Model']['search']['image'] != "")
						$dataNitem->image['like'] = ["%".$params['Model']['search']['image']."%"];
				if(isset($params['Model']['search']['quantity']))
					if($params['Model']['search']['quantity'] != "")
						$dataNitem->quantity =  is_numeric($params['Model']['search']['quantity']) ? $params['Model']['search']['quantity'] : 0;
				if(isset($params['Model']['search']['description']))
					if($params['Model']['search']['description'] != "")
						$dataNitem->description['like'] = ["%".$params['Model']['search']['description']."%"];
				if(isset($params['Model']['search']['price']))
					if($params['Model']['search']['price'] != "")
						$dataNitem->price['like'] = ["%".$params['Model']['search']['price']."%"];
				if(isset($params['Model']['search']['discount']))
					if($params['Model']['search']['discount'] != "")
						$dataNitem->discount =  is_numeric($params['Model']['search']['discount']) ? $params['Model']['search']['discount'] : 0;
				if(isset($params['Model']['search']['special']))
					if($params['Model']['search']['special'] != "")
						$dataNitem->special =  is_numeric($params['Model']['search']['special']) ? $params['Model']['search']['special'] : 0;
				if(isset($params['Model']['search']['create_by']))
					if($params['Model']['search']['create_by'] != "")
						$dataNitem->create_by =  is_numeric($params['Model']['search']['create_by']) ? $params['Model']['search']['create_by'] : 0;
				if(isset($params['Model']['search']['create_at']))
					if($params['Model']['search']['create_at'] != "")
						$dataNitem->create_at['like'] = ["%".$params['Model']['search']['create_at']."%"];
				if(isset($params['Model']['search']['modified_by']))
					if($params['Model']['search']['modified_by'] != "")
						$dataNitem->modified_by =  is_numeric($params['Model']['search']['modified_by']) ? $params['Model']['search']['modified_by'] : 0;
				if(isset($params['Model']['search']['modified_at']))
					if($params['Model']['search']['modified_at'] != "")
						$dataNitem->modified_at['like'] = ["%".$params['Model']['search']['modified_at']."%"];

				
			}
			if(isset($params['Model']['pagination'])){
				if(is_numeric($params['Model']['pagination'])){
					if($params['Model']['pagination'] < 1){
						$page_no = 1;
					}else{
						$page_no = $params['Model']['pagination'];
					}
					$page_no -= 1;
					$limit = ($page_no * application::MAX_PERPAGE).",".application::MAX_PERPAGE;
				}
			}

		}
		DAONitem::$order = "id ASC";
		#\Debugger::log($limit);
		$Nitem = DAONitem::FindAll($dataNitem,$limit);
		$NitemList = "";
		if($Nitem){
			foreach($Nitem as $item){
				$child = new View( ["Nitem/list_item","admin"], ["Nitem"=>$item,"App"=> new application()]);
				$NitemList .= $child->render();
			}
		}
		$view =  new View( ["Nitem/list","admin"], ["totalNitem"=>$totalNitem,"items"=>$NitemList,
													"App"=> new application(),"filter"=>$ValueFilter,"page_no"=>$page_no]);
        return  $view;
	}
	public function actionDelete(){
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['delete'])){
				$id = is_numeric($params['Model']['delete']) ? $params['Model']['delete'] : 0;
				if(DAONitem::Find("id=".$id)){
					if(DAONitem::Delete("id=".$id)){
						return json_encode(["success"=>true]);
					}
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}else{
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}
			}
		}
		exit;
	}
	public function actionEdit(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && self::$request->Method[1] != "POST"){
				$id = is_numeric($params['Model']) ? $params['Model'] : 0;
				$model = DAONitem::Find("id=".$id);
				if(!$model) return self::$lang->t('notfound');
			}else{
				$id = is_numeric($params['Model']['id']) ? $params['Model']['id'] : 0;
				$model = DAONitem::Find("id=".$id);
				if(!$model) return self::$lang->t('notfound');
				$dataNitem = new DataNitem();
				$dataNitem->id = $params['Model']['search']['id']; 
				$dataNitem->id_page = $params['Model']['search']['id_page']; 
				$dataNitem->name = $params['Model']['search']['name']; 
				$dataNitem->image = $params['Model']['search']['image']; 
				$dataNitem->quantity = $params['Model']['search']['quantity']; 
				$dataNitem->description = $params['Model']['search']['description']; 
				$dataNitem->price = $params['Model']['search']['price']; 
				$dataNitem->discount = $params['Model']['search']['discount']; 
				$dataNitem->special = $params['Model']['search']['special']; 
				$dataNitem->create_by = $params['Model']['search']['create_by']; 
				$dataNitem->create_at = $params['Model']['search']['create_at']; 
				$dataNitem->modified_by = $params['Model']['search']['modified_by']; 
				$dataNitem->modified_at = $params['Model']['search']['modified_at'];
				$success = false;
				if(DAONitem::Update($dataNitem,"id")){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/Nitem/edit/".$dataNitem->id,true);

				//\Utils::Debug(self::$request);
			}					
		}
		$view =  new View(	["Nitem/edit","admin"],
								["Nitem"=>$model,
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params]
					  );
		return  $view;
	}
	public function actionAdd(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && $params['Model'] != ""){
				$dataNitem = new DataNitem();
				$dataNitem->id = $params['Model']['search']['id']; 
				$dataNitem->id_page = $params['Model']['search']['id_page']; 
				$dataNitem->name = $params['Model']['search']['name']; 
				$dataNitem->image = $params['Model']['search']['image']; 
				$dataNitem->quantity = $params['Model']['search']['quantity']; 
				$dataNitem->description = $params['Model']['search']['description']; 
				$dataNitem->price = $params['Model']['search']['price']; 
				$dataNitem->discount = $params['Model']['search']['discount']; 
				$dataNitem->special = $params['Model']['search']['special']; 
				$dataNitem->create_by = $params['Model']['search']['create_by']; 
				$dataNitem->create_at = $params['Model']['search']['create_at']; 
				$dataNitem->modified_by = $params['Model']['search']['modified_by']; 
				$dataNitem->modified_at = $params['Model']['search']['modified_at'];	
				$model = DAONitem::Insert($dataNitem);
				$success =	false;
				if($model){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/Nitem/edit/".$model,true);
			}					
		}
		$view =  new View(	["Nitem/new","admin"],
								["Nitem"=>"",
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params
							]
					  );
		return  $view;
	}
	
}