<?php
namespace CreativeX\core\controller\admin;
use CreativeX\Model\DAO\DAOSpermision;
use CreativeX\Model\Data\DataSpermision;
use CreativeX\modules\util\Debugger;
use CreativeX\modules\render\View;
use CreativeX\config\application;
/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 2017-12-05
 * Time: 03:39:43 PM
 */
class SpermisionController{
	private static $request, $lang;
	public function __construct($request, $lang = null) {
		self::$request = $request;
		self::$lang = $lang;
	}
	public function actionList(){
		$dataSpermision = new DataSpermision();
		$ValueFilter = ["id"=> "" ,"permision_list"=> "" ,"create_by"=> "" ,"create_at"=> "" ,"modified_by"=> "" ,"modified_at"=> ""];
		$limit = "0,".application::MAX_PERPAGE;
		$totalSpermision = DAOSpermision::Count();
		$page_no = 1;
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['search'])) {
				if(isset($params['Model']['search']['id']))
					if($params['Model']['search']['id'] != "")
						$dataSpermision->id =  is_numeric($params['Model']['search']['id']) ? $params['Model']['search']['id'] : 0;
				if(isset($params['Model']['search']['permision_list']))
					if($params['Model']['search']['permision_list'] != "")
						$dataSpermision->permision_list['like'] = ["%".$params['Model']['search']['permision_list']."%"];
				if(isset($params['Model']['search']['create_by']))
					if($params['Model']['search']['create_by'] != "")
						$dataSpermision->create_by =  is_numeric($params['Model']['search']['create_by']) ? $params['Model']['search']['create_by'] : 0;
				if(isset($params['Model']['search']['create_at']))
					if($params['Model']['search']['create_at'] != "")
						$dataSpermision->create_at['like'] = ["%".$params['Model']['search']['create_at']."%"];
				if(isset($params['Model']['search']['modified_by']))
					if($params['Model']['search']['modified_by'] != "")
						$dataSpermision->modified_by =  is_numeric($params['Model']['search']['modified_by']) ? $params['Model']['search']['modified_by'] : 0;
				if(isset($params['Model']['search']['modified_at']))
					if($params['Model']['search']['modified_at'] != "")
						$dataSpermision->modified_at['like'] = ["%".$params['Model']['search']['modified_at']."%"];

				
			}
			if(isset($params['Model']['pagination'])){
				if(is_numeric($params['Model']['pagination'])){
					if($params['Model']['pagination'] < 1){
						$page_no = 1;
					}else{
						$page_no = $params['Model']['pagination'];
					}
					$page_no -= 1;
					$limit = ($page_no * application::MAX_PERPAGE).",".application::MAX_PERPAGE;
				}
			}

		}
		DAOSpermision::$order = "id ASC";
		#\Debugger::log($limit);
		$Spermision = DAOSpermision::FindAll($dataSpermision,$limit);
		$SpermisionList = "";
		if($Spermision){
			foreach($Spermision as $item){
				$child = new View( ["Spermision/list_item","admin"], ["Spermision"=>$item,"App"=> new application()]);
				$SpermisionList .= $child->render();
			}
		}
		$view =  new View( ["Spermision/list","admin"], ["totalSpermision"=>$totalSpermision,"items"=>$SpermisionList,
													"App"=> new application(),"filter"=>$ValueFilter,"page_no"=>$page_no]);
        return  $view;
	}
	public function actionDelete(){
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['delete'])){
				$id = is_numeric($params['Model']['delete']) ? $params['Model']['delete'] : 0;
				if(DAOSpermision::Find("id=".$id)){
					if(DAOSpermision::Delete("id=".$id)){
						return json_encode(["success"=>true]);
					}
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}else{
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}
			}
		}
		exit;
	}
	public function actionEdit(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && self::$request->Method[1] != "POST"){
				$id = is_numeric($params['Model']) ? $params['Model'] : 0;
				$model = DAOSpermision::Find("id=".$id);
				if(!$model) return self::$lang->t('notfound');
			}else{
				$id = is_numeric($params['Model']['id']) ? $params['Model']['id'] : 0;
				$model = DAOSpermision::Find("id=".$id);
				if(!$model) return self::$lang->t('notfound');
				$dataSpermision = new DataSpermision();
				$dataSpermision->id = $params['Model']['search']['id']; 
				$dataSpermision->permision_list = $params['Model']['search']['permision_list']; 
				$dataSpermision->create_by = $params['Model']['search']['create_by']; 
				$dataSpermision->create_at = $params['Model']['search']['create_at']; 
				$dataSpermision->modified_by = $params['Model']['search']['modified_by']; 
				$dataSpermision->modified_at = $params['Model']['search']['modified_at'];
				$success = false;
				if(DAOSpermision::Update($dataSpermision,"id")){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/Spermision/edit/".$dataSpermision->id,true);

				//\Utils::Debug(self::$request);
			}					
		}
		$view =  new View(	["Spermision/edit","admin"],
								["Spermision"=>$model,
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params]
					  );
		return  $view;
	}
	public function actionAdd(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && $params['Model'] != ""){
				$dataSpermision = new DataSpermision();
				$dataSpermision->id = $params['Model']['search']['id']; 
				$dataSpermision->permision_list = $params['Model']['search']['permision_list']; 
				$dataSpermision->create_by = $params['Model']['search']['create_by']; 
				$dataSpermision->create_at = $params['Model']['search']['create_at']; 
				$dataSpermision->modified_by = $params['Model']['search']['modified_by']; 
				$dataSpermision->modified_at = $params['Model']['search']['modified_at'];	
				$model = DAOSpermision::Insert($dataSpermision);
				$success =	false;
				if($model){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/Spermision/edit/".$model,true);
			}					
		}
		$view =  new View(	["Spermision/new","admin"],
								["Spermision"=>"",
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params
							]
					  );
		return  $view;
	}
	
}