<?php
namespace CreativeX\core\controller\admin;
use CreativeX\Model\DAO\DAONorder;
use CreativeX\Model\Data\DataNorder;
use CreativeX\modules\util\Debugger;
use CreativeX\modules\render\View;
use CreativeX\config\application;
/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 2017-12-05
 * Time: 03:39:42 PM
 */
class NorderController{
	private static $request, $lang;
	public function __construct($request, $lang = null) {
		self::$request = $request;
		self::$lang = $lang;
	}
	public function actionList(){
		$dataNorder = new DataNorder();
		$ValueFilter = ["id"=> "" ,"id_customer"=> "" ,"id_item"=> "" ,"quantity"=> "" ,"price"=> "" ,"status"=> "" ,"create_by"=> "" ,"create_at"=> "" ,"modified_by"=> "" ,"modified_at"=> ""];
		$limit = "0,".application::MAX_PERPAGE;
		$totalNorder = DAONorder::Count();
		$page_no = 1;
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['search'])) {
				if(isset($params['Model']['search']['id']))
					if($params['Model']['search']['id'] != "")
						$dataNorder->id =  is_numeric($params['Model']['search']['id']) ? $params['Model']['search']['id'] : 0;
				if(isset($params['Model']['search']['id_customer']))
					if($params['Model']['search']['id_customer'] != "")
						$dataNorder->id_customer =  is_numeric($params['Model']['search']['id_customer']) ? $params['Model']['search']['id_customer'] : 0;
				if(isset($params['Model']['search']['id_item']))
					if($params['Model']['search']['id_item'] != "")
						$dataNorder->id_item['like'] = ["%".$params['Model']['search']['id_item']."%"];
				if(isset($params['Model']['search']['quantity']))
					if($params['Model']['search']['quantity'] != "")
						$dataNorder->quantity['like'] = ["%".$params['Model']['search']['quantity']."%"];
				if(isset($params['Model']['search']['price']))
					if($params['Model']['search']['price'] != "")
						$dataNorder->price['like'] = ["%".$params['Model']['search']['price']."%"];
				if(isset($params['Model']['search']['status']))
					if($params['Model']['search']['status'] != "")
						$dataNorder->status['like'] = ["%".$params['Model']['search']['status']."%"];
				if(isset($params['Model']['search']['create_by']))
					if($params['Model']['search']['create_by'] != "")
						$dataNorder->create_by =  is_numeric($params['Model']['search']['create_by']) ? $params['Model']['search']['create_by'] : 0;
				if(isset($params['Model']['search']['create_at']))
					if($params['Model']['search']['create_at'] != "")
						$dataNorder->create_at['like'] = ["%".$params['Model']['search']['create_at']."%"];
				if(isset($params['Model']['search']['modified_by']))
					if($params['Model']['search']['modified_by'] != "")
						$dataNorder->modified_by =  is_numeric($params['Model']['search']['modified_by']) ? $params['Model']['search']['modified_by'] : 0;
				if(isset($params['Model']['search']['modified_at']))
					if($params['Model']['search']['modified_at'] != "")
						$dataNorder->modified_at['like'] = ["%".$params['Model']['search']['modified_at']."%"];

				
			}
			if(isset($params['Model']['pagination'])){
				if(is_numeric($params['Model']['pagination'])){
					if($params['Model']['pagination'] < 1){
						$page_no = 1;
					}else{
						$page_no = $params['Model']['pagination'];
					}
					$page_no -= 1;
					$limit = ($page_no * application::MAX_PERPAGE).",".application::MAX_PERPAGE;
				}
			}

		}
		DAONorder::$order = "id ASC";
		#\Debugger::log($limit);
		$Norder = DAONorder::FindAll($dataNorder,$limit);
		$NorderList = "";
		if($Norder){
			foreach($Norder as $item){
				$child = new View( ["Norder/list_item","admin"], ["Norder"=>$item,"App"=> new application()]);
				$NorderList .= $child->render();
			}
		}
		$view =  new View( ["Norder/list","admin"], ["totalNorder"=>$totalNorder,"items"=>$NorderList,
													"App"=> new application(),"filter"=>$ValueFilter,"page_no"=>$page_no]);
        return  $view;
	}
	public function actionDelete(){
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['delete'])){
				$id = is_numeric($params['Model']['delete']) ? $params['Model']['delete'] : 0;
				if(DAONorder::Find("id=".$id)){
					if(DAONorder::Delete("id=".$id)){
						return json_encode(["success"=>true]);
					}
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}else{
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}
			}
		}
		exit;
	}
	public function actionEdit(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && self::$request->Method[1] != "POST"){
				$id = is_numeric($params['Model']) ? $params['Model'] : 0;
				$model = DAONorder::Find("id=".$id);
				if(!$model) return self::$lang->t('notfound');
			}else{
				$id = is_numeric($params['Model']['id']) ? $params['Model']['id'] : 0;
				$model = DAONorder::Find("id=".$id);
				if(!$model) return self::$lang->t('notfound');
				$dataNorder = new DataNorder();
				$dataNorder->id = $params['Model']['search']['id']; 
				$dataNorder->id_customer = $params['Model']['search']['id_customer']; 
				$dataNorder->id_item = $params['Model']['search']['id_item']; 
				$dataNorder->quantity = $params['Model']['search']['quantity']; 
				$dataNorder->price = $params['Model']['search']['price']; 
				$dataNorder->status = $params['Model']['search']['status']; 
				$dataNorder->create_by = $params['Model']['search']['create_by']; 
				$dataNorder->create_at = $params['Model']['search']['create_at']; 
				$dataNorder->modified_by = $params['Model']['search']['modified_by']; 
				$dataNorder->modified_at = $params['Model']['search']['modified_at'];
				$success = false;
				if(DAONorder::Update($dataNorder,"id")){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/Norder/edit/".$dataNorder->id,true);

				//\Utils::Debug(self::$request);
			}					
		}
		$view =  new View(	["Norder/edit","admin"],
								["Norder"=>$model,
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params]
					  );
		return  $view;
	}
	public function actionAdd(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && $params['Model'] != ""){
				$dataNorder = new DataNorder();
				$dataNorder->id = $params['Model']['search']['id']; 
				$dataNorder->id_customer = $params['Model']['search']['id_customer']; 
				$dataNorder->id_item = $params['Model']['search']['id_item']; 
				$dataNorder->quantity = $params['Model']['search']['quantity']; 
				$dataNorder->price = $params['Model']['search']['price']; 
				$dataNorder->status = $params['Model']['search']['status']; 
				$dataNorder->create_by = $params['Model']['search']['create_by']; 
				$dataNorder->create_at = $params['Model']['search']['create_at']; 
				$dataNorder->modified_by = $params['Model']['search']['modified_by']; 
				$dataNorder->modified_at = $params['Model']['search']['modified_at'];	
				$model = DAONorder::Insert($dataNorder);
				$success =	false;
				if($model){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/Norder/edit/".$model,true);
			}					
		}
		$view =  new View(	["Norder/new","admin"],
								["Norder"=>"",
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params
							]
					  );
		return  $view;
	}
	
}