<?php
namespace CreativeX\core\controller\admin;
use CreativeX\Model\DAO\DAONsocial;
use CreativeX\Model\Data\DataNsocial;
use CreativeX\modules\util\Debugger;
use CreativeX\modules\render\View;
use CreativeX\config\application;
/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 2017-12-05
 * Time: 03:39:43 PM
 */
class NsocialController{
	private static $request, $lang;
	public function __construct($request, $lang = null) {
		self::$request = $request;
		self::$lang = $lang;
	}
	public function actionList(){
		$dataNsocial = new DataNsocial();
		$ValueFilter = ["id"=> "" ,"id_profile"=> "" ,"name"=> "" ,"link"=> "" ,"create_by"=> "" ,"create_at"=> "" ,"modified_by"=> "" ,"modified_at"=> ""];
		$limit = "0,".application::MAX_PERPAGE;
		$totalNsocial = DAONsocial::Count();
		$page_no = 1;
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['search'])) {
				if(isset($params['Model']['search']['id']))
					if($params['Model']['search']['id'] != "")
						$dataNsocial->id =  is_numeric($params['Model']['search']['id']) ? $params['Model']['search']['id'] : 0;
				if(isset($params['Model']['search']['id_profile']))
					if($params['Model']['search']['id_profile'] != "")
						$dataNsocial->id_profile =  is_numeric($params['Model']['search']['id_profile']) ? $params['Model']['search']['id_profile'] : 0;
				if(isset($params['Model']['search']['name']))
					if($params['Model']['search']['name'] != "")
						$dataNsocial->name['like'] = ["%".$params['Model']['search']['name']."%"];
				if(isset($params['Model']['search']['link']))
					if($params['Model']['search']['link'] != "")
						$dataNsocial->link['like'] = ["%".$params['Model']['search']['link']."%"];
				if(isset($params['Model']['search']['create_by']))
					if($params['Model']['search']['create_by'] != "")
						$dataNsocial->create_by =  is_numeric($params['Model']['search']['create_by']) ? $params['Model']['search']['create_by'] : 0;
				if(isset($params['Model']['search']['create_at']))
					if($params['Model']['search']['create_at'] != "")
						$dataNsocial->create_at['like'] = ["%".$params['Model']['search']['create_at']."%"];
				if(isset($params['Model']['search']['modified_by']))
					if($params['Model']['search']['modified_by'] != "")
						$dataNsocial->modified_by =  is_numeric($params['Model']['search']['modified_by']) ? $params['Model']['search']['modified_by'] : 0;
				if(isset($params['Model']['search']['modified_at']))
					if($params['Model']['search']['modified_at'] != "")
						$dataNsocial->modified_at['like'] = ["%".$params['Model']['search']['modified_at']."%"];

				
			}
			if(isset($params['Model']['pagination'])){
				if(is_numeric($params['Model']['pagination'])){
					if($params['Model']['pagination'] < 1){
						$page_no = 1;
					}else{
						$page_no = $params['Model']['pagination'];
					}
					$page_no -= 1;
					$limit = ($page_no * application::MAX_PERPAGE).",".application::MAX_PERPAGE;
				}
			}

		}
		DAONsocial::$order = "id ASC";
		#\Debugger::log($limit);
		$Nsocial = DAONsocial::FindAll($dataNsocial,$limit);
		$NsocialList = "";
		if($Nsocial){
			foreach($Nsocial as $item){
				$child = new View( ["Nsocial/list_item","admin"], ["Nsocial"=>$item,"App"=> new application()]);
				$NsocialList .= $child->render();
			}
		}
		$view =  new View( ["Nsocial/list","admin"], ["totalNsocial"=>$totalNsocial,"items"=>$NsocialList,
													"App"=> new application(),"filter"=>$ValueFilter,"page_no"=>$page_no]);
        return  $view;
	}
	public function actionDelete(){
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['delete'])){
				$id = is_numeric($params['Model']['delete']) ? $params['Model']['delete'] : 0;
				if(DAONsocial::Find("id=".$id)){
					if(DAONsocial::Delete("id=".$id)){
						return json_encode(["success"=>true]);
					}
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}else{
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}
			}
		}
		exit;
	}
	public function actionEdit(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && self::$request->Method[1] != "POST"){
				$id = is_numeric($params['Model']) ? $params['Model'] : 0;
				$model = DAONsocial::Find("id=".$id);
				if(!$model) return self::$lang->t('notfound');
			}else{
				$id = is_numeric($params['Model']['id']) ? $params['Model']['id'] : 0;
				$model = DAONsocial::Find("id=".$id);
				if(!$model) return self::$lang->t('notfound');
				$dataNsocial = new DataNsocial();
				$dataNsocial->id = $params['Model']['search']['id']; 
				$dataNsocial->id_profile = $params['Model']['search']['id_profile']; 
				$dataNsocial->name = $params['Model']['search']['name']; 
				$dataNsocial->link = $params['Model']['search']['link']; 
				$dataNsocial->create_by = $params['Model']['search']['create_by']; 
				$dataNsocial->create_at = $params['Model']['search']['create_at']; 
				$dataNsocial->modified_by = $params['Model']['search']['modified_by']; 
				$dataNsocial->modified_at = $params['Model']['search']['modified_at'];
				$success = false;
				if(DAONsocial::Update($dataNsocial,"id")){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/Nsocial/edit/".$dataNsocial->id,true);

				//\Utils::Debug(self::$request);
			}					
		}
		$view =  new View(	["Nsocial/edit","admin"],
								["Nsocial"=>$model,
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params]
					  );
		return  $view;
	}
	public function actionAdd(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && $params['Model'] != ""){
				$dataNsocial = new DataNsocial();
				$dataNsocial->id = $params['Model']['search']['id']; 
				$dataNsocial->id_profile = $params['Model']['search']['id_profile']; 
				$dataNsocial->name = $params['Model']['search']['name']; 
				$dataNsocial->link = $params['Model']['search']['link']; 
				$dataNsocial->create_by = $params['Model']['search']['create_by']; 
				$dataNsocial->create_at = $params['Model']['search']['create_at']; 
				$dataNsocial->modified_by = $params['Model']['search']['modified_by']; 
				$dataNsocial->modified_at = $params['Model']['search']['modified_at'];	
				$model = DAONsocial::Insert($dataNsocial);
				$success =	false;
				if($model){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/Nsocial/edit/".$model,true);
			}					
		}
		$view =  new View(	["Nsocial/new","admin"],
								["Nsocial"=>"",
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params
							]
					  );
		return  $view;
	}
	
}