<?php
namespace CreativeX\core\controller\admin;
use CreativeX\Model\DAO\DAONcustomerprofile;
use CreativeX\Model\Data\DataNcustomerprofile;
use CreativeX\modules\util\Debugger;
use CreativeX\modules\render\View;
use CreativeX\config\application;
/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 2017-12-05
 * Time: 03:39:42 PM
 */
class NcustomerprofileController{
	private static $request, $lang;
	public function __construct($request, $lang = null) {
		self::$request = $request;
		self::$lang = $lang;
	}
	public function actionList(){
		$dataNcustomerprofile = new DataNcustomerprofile();
		$ValueFilter = ["id"=> "" ,"name"=> "" ,"address"=> "" ,"email"=> "" ,"phone"=> "" ,"company"=> "" ,"create_by"=> "" ,"create_at"=> "" ,"modified_by"=> "" ,"modified_at"=> ""];
		$limit = "0,".application::MAX_PERPAGE;
		$totalNcustomerprofile = DAONcustomerprofile::Count();
		$page_no = 1;
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['search'])) {
				if(isset($params['Model']['search']['id']))
					if($params['Model']['search']['id'] != "")
						$dataNcustomerprofile->id =  is_numeric($params['Model']['search']['id']) ? $params['Model']['search']['id'] : 0;
				if(isset($params['Model']['search']['name']))
					if($params['Model']['search']['name'] != "")
						$dataNcustomerprofile->name['like'] = ["%".$params['Model']['search']['name']."%"];
				if(isset($params['Model']['search']['address']))
					if($params['Model']['search']['address'] != "")
						$dataNcustomerprofile->address['like'] = ["%".$params['Model']['search']['address']."%"];
				if(isset($params['Model']['search']['email']))
					if($params['Model']['search']['email'] != "")
						$dataNcustomerprofile->email['like'] = ["%".$params['Model']['search']['email']."%"];
				if(isset($params['Model']['search']['phone']))
					if($params['Model']['search']['phone'] != "")
						$dataNcustomerprofile->phone['like'] = ["%".$params['Model']['search']['phone']."%"];
				if(isset($params['Model']['search']['company']))
					if($params['Model']['search']['company'] != "")
						$dataNcustomerprofile->company['like'] = ["%".$params['Model']['search']['company']."%"];
				if(isset($params['Model']['search']['create_by']))
					if($params['Model']['search']['create_by'] != "")
						$dataNcustomerprofile->create_by =  is_numeric($params['Model']['search']['create_by']) ? $params['Model']['search']['create_by'] : 0;
				if(isset($params['Model']['search']['create_at']))
					if($params['Model']['search']['create_at'] != "")
						$dataNcustomerprofile->create_at['like'] = ["%".$params['Model']['search']['create_at']."%"];
				if(isset($params['Model']['search']['modified_by']))
					if($params['Model']['search']['modified_by'] != "")
						$dataNcustomerprofile->modified_by =  is_numeric($params['Model']['search']['modified_by']) ? $params['Model']['search']['modified_by'] : 0;
				if(isset($params['Model']['search']['modified_at']))
					if($params['Model']['search']['modified_at'] != "")
						$dataNcustomerprofile->modified_at['like'] = ["%".$params['Model']['search']['modified_at']."%"];

				
			}
			if(isset($params['Model']['pagination'])){
				if(is_numeric($params['Model']['pagination'])){
					if($params['Model']['pagination'] < 1){
						$page_no = 1;
					}else{
						$page_no = $params['Model']['pagination'];
					}
					$page_no -= 1;
					$limit = ($page_no * application::MAX_PERPAGE).",".application::MAX_PERPAGE;
				}
			}

		}
		DAONcustomerprofile::$order = "id ASC";
		#\Debugger::log($limit);
		$Ncustomerprofile = DAONcustomerprofile::FindAll($dataNcustomerprofile,$limit);
		$NcustomerprofileList = "";
		if($Ncustomerprofile){
			foreach($Ncustomerprofile as $item){
				$child = new View( ["Ncustomerprofile/list_item","admin"], ["Ncustomerprofile"=>$item,"App"=> new application()]);
				$NcustomerprofileList .= $child->render();
			}
		}
		$view =  new View( ["Ncustomerprofile/list","admin"], ["totalNcustomerprofile"=>$totalNcustomerprofile,"items"=>$NcustomerprofileList,
													"App"=> new application(),"filter"=>$ValueFilter,"page_no"=>$page_no]);
        return  $view;
	}
	public function actionDelete(){
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['delete'])){
				$id = is_numeric($params['Model']['delete']) ? $params['Model']['delete'] : 0;
				if(DAONcustomerprofile::Find("id=".$id)){
					if(DAONcustomerprofile::Delete("id=".$id)){
						return json_encode(["success"=>true]);
					}
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}else{
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}
			}
		}
		exit;
	}
	public function actionEdit(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && self::$request->Method[1] != "POST"){
				$id = is_numeric($params['Model']) ? $params['Model'] : 0;
				$model = DAONcustomerprofile::Find("id=".$id);
				if(!$model) return self::$lang->t('notfound');
			}else{
				$id = is_numeric($params['Model']['id']) ? $params['Model']['id'] : 0;
				$model = DAONcustomerprofile::Find("id=".$id);
				if(!$model) return self::$lang->t('notfound');
				$dataNcustomerprofile = new DataNcustomerprofile();
				$dataNcustomerprofile->id = $params['Model']['search']['id']; 
				$dataNcustomerprofile->name = $params['Model']['search']['name']; 
				$dataNcustomerprofile->address = $params['Model']['search']['address']; 
				$dataNcustomerprofile->email = $params['Model']['search']['email']; 
				$dataNcustomerprofile->phone = $params['Model']['search']['phone']; 
				$dataNcustomerprofile->company = $params['Model']['search']['company']; 
				$dataNcustomerprofile->create_by = $params['Model']['search']['create_by']; 
				$dataNcustomerprofile->create_at = $params['Model']['search']['create_at']; 
				$dataNcustomerprofile->modified_by = $params['Model']['search']['modified_by']; 
				$dataNcustomerprofile->modified_at = $params['Model']['search']['modified_at'];
				$success = false;
				if(DAONcustomerprofile::Update($dataNcustomerprofile,"id")){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/Ncustomerprofile/edit/".$dataNcustomerprofile->id,true);

				//\Utils::Debug(self::$request);
			}					
		}
		$view =  new View(	["Ncustomerprofile/edit","admin"],
								["Ncustomerprofile"=>$model,
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params]
					  );
		return  $view;
	}
	public function actionAdd(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && $params['Model'] != ""){
				$dataNcustomerprofile = new DataNcustomerprofile();
				$dataNcustomerprofile->id = $params['Model']['search']['id']; 
				$dataNcustomerprofile->name = $params['Model']['search']['name']; 
				$dataNcustomerprofile->address = $params['Model']['search']['address']; 
				$dataNcustomerprofile->email = $params['Model']['search']['email']; 
				$dataNcustomerprofile->phone = $params['Model']['search']['phone']; 
				$dataNcustomerprofile->company = $params['Model']['search']['company']; 
				$dataNcustomerprofile->create_by = $params['Model']['search']['create_by']; 
				$dataNcustomerprofile->create_at = $params['Model']['search']['create_at']; 
				$dataNcustomerprofile->modified_by = $params['Model']['search']['modified_by']; 
				$dataNcustomerprofile->modified_at = $params['Model']['search']['modified_at'];	
				$model = DAONcustomerprofile::Insert($dataNcustomerprofile);
				$success =	false;
				if($model){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/Ncustomerprofile/edit/".$model,true);
			}					
		}
		$view =  new View(	["Ncustomerprofile/new","admin"],
								["Ncustomerprofile"=>"",
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params
							]
					  );
		return  $view;
	}
	
}