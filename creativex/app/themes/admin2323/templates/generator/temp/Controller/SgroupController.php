<?php
namespace CreativeX\core\controller\admin;
use CreativeX\Model\DAO\DAOSgroup;
use CreativeX\Model\Data\DataSgroup;
use CreativeX\modules\util\Debugger;
use CreativeX\modules\render\View;
use CreativeX\config\application;
/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 2017-12-05
 * Time: 03:39:43 PM
 */
class SgroupController{
	private static $request, $lang;
	public function __construct($request, $lang = null) {
		self::$request = $request;
		self::$lang = $lang;
	}
	public function actionList(){
		$dataSgroup = new DataSgroup();
		$ValueFilter = ["id"=> "" ,"id_permision"=> "" ,"name"=> "" ,"create_by"=> "" ,"create_at"=> "" ,"modified_by"=> "" ,"modified_at"=> ""];
		$limit = "0,".application::MAX_PERPAGE;
		$totalSgroup = DAOSgroup::Count();
		$page_no = 1;
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['search'])) {
				if(isset($params['Model']['search']['id']))
					if($params['Model']['search']['id'] != "")
						$dataSgroup->id =  is_numeric($params['Model']['search']['id']) ? $params['Model']['search']['id'] : 0;
				if(isset($params['Model']['search']['id_permision']))
					if($params['Model']['search']['id_permision'] != "")
						$dataSgroup->id_permision =  is_numeric($params['Model']['search']['id_permision']) ? $params['Model']['search']['id_permision'] : 0;
				if(isset($params['Model']['search']['name']))
					if($params['Model']['search']['name'] != "")
						$dataSgroup->name['like'] = ["%".$params['Model']['search']['name']."%"];
				if(isset($params['Model']['search']['create_by']))
					if($params['Model']['search']['create_by'] != "")
						$dataSgroup->create_by =  is_numeric($params['Model']['search']['create_by']) ? $params['Model']['search']['create_by'] : 0;
				if(isset($params['Model']['search']['create_at']))
					if($params['Model']['search']['create_at'] != "")
						$dataSgroup->create_at['like'] = ["%".$params['Model']['search']['create_at']."%"];
				if(isset($params['Model']['search']['modified_by']))
					if($params['Model']['search']['modified_by'] != "")
						$dataSgroup->modified_by =  is_numeric($params['Model']['search']['modified_by']) ? $params['Model']['search']['modified_by'] : 0;
				if(isset($params['Model']['search']['modified_at']))
					if($params['Model']['search']['modified_at'] != "")
						$dataSgroup->modified_at['like'] = ["%".$params['Model']['search']['modified_at']."%"];

				
			}
			if(isset($params['Model']['pagination'])){
				if(is_numeric($params['Model']['pagination'])){
					if($params['Model']['pagination'] < 1){
						$page_no = 1;
					}else{
						$page_no = $params['Model']['pagination'];
					}
					$page_no -= 1;
					$limit = ($page_no * application::MAX_PERPAGE).",".application::MAX_PERPAGE;
				}
			}

		}
		DAOSgroup::$order = "id ASC";
		#\Debugger::log($limit);
		$Sgroup = DAOSgroup::FindAll($dataSgroup,$limit);
		$SgroupList = "";
		if($Sgroup){
			foreach($Sgroup as $item){
				$child = new View( ["Sgroup/list_item","admin"], ["Sgroup"=>$item,"App"=> new application()]);
				$SgroupList .= $child->render();
			}
		}
		$view =  new View( ["Sgroup/list","admin"], ["totalSgroup"=>$totalSgroup,"items"=>$SgroupList,
													"App"=> new application(),"filter"=>$ValueFilter,"page_no"=>$page_no]);
        return  $view;
	}
	public function actionDelete(){
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['delete'])){
				$id = is_numeric($params['Model']['delete']) ? $params['Model']['delete'] : 0;
				if(DAOSgroup::Find("id=".$id)){
					if(DAOSgroup::Delete("id=".$id)){
						return json_encode(["success"=>true]);
					}
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}else{
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}
			}
		}
		exit;
	}
	public function actionEdit(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && self::$request->Method[1] != "POST"){
				$id = is_numeric($params['Model']) ? $params['Model'] : 0;
				$model = DAOSgroup::Find("id=".$id);
				if(!$model) return self::$lang->t('notfound');
			}else{
				$id = is_numeric($params['Model']['id']) ? $params['Model']['id'] : 0;
				$model = DAOSgroup::Find("id=".$id);
				if(!$model) return self::$lang->t('notfound');
				$dataSgroup = new DataSgroup();
				$dataSgroup->id = $params['Model']['search']['id']; 
				$dataSgroup->id_permision = $params['Model']['search']['id_permision']; 
				$dataSgroup->name = $params['Model']['search']['name']; 
				$dataSgroup->create_by = $params['Model']['search']['create_by']; 
				$dataSgroup->create_at = $params['Model']['search']['create_at']; 
				$dataSgroup->modified_by = $params['Model']['search']['modified_by']; 
				$dataSgroup->modified_at = $params['Model']['search']['modified_at'];
				$success = false;
				if(DAOSgroup::Update($dataSgroup,"id")){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/Sgroup/edit/".$dataSgroup->id,true);

				//\Utils::Debug(self::$request);
			}					
		}
		$view =  new View(	["Sgroup/edit","admin"],
								["Sgroup"=>$model,
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params]
					  );
		return  $view;
	}
	public function actionAdd(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && $params['Model'] != ""){
				$dataSgroup = new DataSgroup();
				$dataSgroup->id = $params['Model']['search']['id']; 
				$dataSgroup->id_permision = $params['Model']['search']['id_permision']; 
				$dataSgroup->name = $params['Model']['search']['name']; 
				$dataSgroup->create_by = $params['Model']['search']['create_by']; 
				$dataSgroup->create_at = $params['Model']['search']['create_at']; 
				$dataSgroup->modified_by = $params['Model']['search']['modified_by']; 
				$dataSgroup->modified_at = $params['Model']['search']['modified_at'];	
				$model = DAOSgroup::Insert($dataSgroup);
				$success =	false;
				if($model){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/Sgroup/edit/".$model,true);
			}					
		}
		$view =  new View(	["Sgroup/new","admin"],
								["Sgroup"=>"",
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params
							]
					  );
		return  $view;
	}
	
}