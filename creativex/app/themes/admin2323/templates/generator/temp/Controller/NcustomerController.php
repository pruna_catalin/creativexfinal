<?php
namespace CreativeX\core\controller\admin;
use CreativeX\Model\DAO\DAONcustomer;
use CreativeX\Model\Data\DataNcustomer;
use CreativeX\modules\util\Debugger;
use CreativeX\modules\render\View;
use CreativeX\config\application;
/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 2017-12-05
 * Time: 03:39:42 PM
 */
class NcustomerController{
	private static $request, $lang;
	public function __construct($request, $lang = null) {
		self::$request = $request;
		self::$lang = $lang;
	}
	public function actionList(){
		$dataNcustomer = new DataNcustomer();
		$ValueFilter = ["id"=> "" ,"id_profile"=> "" ,"username"=> "" ,"password"=> "" ,"activation_link"=> "" ,"create_by"=> "" ,"create_at"=> "" ,"modified_by"=> "" ,"modified_at"=> ""];
		$limit = "0,".application::MAX_PERPAGE;
		$totalNcustomer = DAONcustomer::Count();
		$page_no = 1;
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['search'])) {
				if(isset($params['Model']['search']['id']))
					if($params['Model']['search']['id'] != "")
						$dataNcustomer->id =  is_numeric($params['Model']['search']['id']) ? $params['Model']['search']['id'] : 0;
				if(isset($params['Model']['search']['id_profile']))
					if($params['Model']['search']['id_profile'] != "")
						$dataNcustomer->id_profile =  is_numeric($params['Model']['search']['id_profile']) ? $params['Model']['search']['id_profile'] : 0;
				if(isset($params['Model']['search']['username']))
					if($params['Model']['search']['username'] != "")
						$dataNcustomer->username['like'] = ["%".$params['Model']['search']['username']."%"];
				if(isset($params['Model']['search']['password']))
					if($params['Model']['search']['password'] != "")
						$dataNcustomer->password['like'] = ["%".$params['Model']['search']['password']."%"];
				if(isset($params['Model']['search']['activation_link']))
					if($params['Model']['search']['activation_link'] != "")
						$dataNcustomer->activation_link['like'] = ["%".$params['Model']['search']['activation_link']."%"];
				if(isset($params['Model']['search']['create_by']))
					if($params['Model']['search']['create_by'] != "")
						$dataNcustomer->create_by =  is_numeric($params['Model']['search']['create_by']) ? $params['Model']['search']['create_by'] : 0;
				if(isset($params['Model']['search']['create_at']))
					if($params['Model']['search']['create_at'] != "")
						$dataNcustomer->create_at['like'] = ["%".$params['Model']['search']['create_at']."%"];
				if(isset($params['Model']['search']['modified_by']))
					if($params['Model']['search']['modified_by'] != "")
						$dataNcustomer->modified_by =  is_numeric($params['Model']['search']['modified_by']) ? $params['Model']['search']['modified_by'] : 0;
				if(isset($params['Model']['search']['modified_at']))
					if($params['Model']['search']['modified_at'] != "")
						$dataNcustomer->modified_at['like'] = ["%".$params['Model']['search']['modified_at']."%"];

				
			}
			if(isset($params['Model']['pagination'])){
				if(is_numeric($params['Model']['pagination'])){
					if($params['Model']['pagination'] < 1){
						$page_no = 1;
					}else{
						$page_no = $params['Model']['pagination'];
					}
					$page_no -= 1;
					$limit = ($page_no * application::MAX_PERPAGE).",".application::MAX_PERPAGE;
				}
			}

		}
		DAONcustomer::$order = "id ASC";
		#\Debugger::log($limit);
		$Ncustomer = DAONcustomer::FindAll($dataNcustomer,$limit);
		$NcustomerList = "";
		if($Ncustomer){
			foreach($Ncustomer as $item){
				$child = new View( ["Ncustomer/list_item","admin"], ["Ncustomer"=>$item,"App"=> new application()]);
				$NcustomerList .= $child->render();
			}
		}
		$view =  new View( ["Ncustomer/list","admin"], ["totalNcustomer"=>$totalNcustomer,"items"=>$NcustomerList,
													"App"=> new application(),"filter"=>$ValueFilter,"page_no"=>$page_no]);
        return  $view;
	}
	public function actionDelete(){
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['delete'])){
				$id = is_numeric($params['Model']['delete']) ? $params['Model']['delete'] : 0;
				if(DAONcustomer::Find("id=".$id)){
					if(DAONcustomer::Delete("id=".$id)){
						return json_encode(["success"=>true]);
					}
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}else{
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}
			}
		}
		exit;
	}
	public function actionEdit(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && self::$request->Method[1] != "POST"){
				$id = is_numeric($params['Model']) ? $params['Model'] : 0;
				$model = DAONcustomer::Find("id=".$id);
				if(!$model) return self::$lang->t('notfound');
			}else{
				$id = is_numeric($params['Model']['id']) ? $params['Model']['id'] : 0;
				$model = DAONcustomer::Find("id=".$id);
				if(!$model) return self::$lang->t('notfound');
				$dataNcustomer = new DataNcustomer();
				$dataNcustomer->id = $params['Model']['search']['id']; 
				$dataNcustomer->id_profile = $params['Model']['search']['id_profile']; 
				$dataNcustomer->username = $params['Model']['search']['username']; 
				$dataNcustomer->password = $params['Model']['search']['password']; 
				$dataNcustomer->activation_link = $params['Model']['search']['activation_link']; 
				$dataNcustomer->create_by = $params['Model']['search']['create_by']; 
				$dataNcustomer->create_at = $params['Model']['search']['create_at']; 
				$dataNcustomer->modified_by = $params['Model']['search']['modified_by']; 
				$dataNcustomer->modified_at = $params['Model']['search']['modified_at'];
				$success = false;
				if(DAONcustomer::Update($dataNcustomer,"id")){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/Ncustomer/edit/".$dataNcustomer->id,true);

				//\Utils::Debug(self::$request);
			}					
		}
		$view =  new View(	["Ncustomer/edit","admin"],
								["Ncustomer"=>$model,
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params]
					  );
		return  $view;
	}
	public function actionAdd(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && $params['Model'] != ""){
				$dataNcustomer = new DataNcustomer();
				$dataNcustomer->id = $params['Model']['search']['id']; 
				$dataNcustomer->id_profile = $params['Model']['search']['id_profile']; 
				$dataNcustomer->username = $params['Model']['search']['username']; 
				$dataNcustomer->password = $params['Model']['search']['password']; 
				$dataNcustomer->activation_link = $params['Model']['search']['activation_link']; 
				$dataNcustomer->create_by = $params['Model']['search']['create_by']; 
				$dataNcustomer->create_at = $params['Model']['search']['create_at']; 
				$dataNcustomer->modified_by = $params['Model']['search']['modified_by']; 
				$dataNcustomer->modified_at = $params['Model']['search']['modified_at'];	
				$model = DAONcustomer::Insert($dataNcustomer);
				$success =	false;
				if($model){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/Ncustomer/edit/".$model,true);
			}					
		}
		$view =  new View(	["Ncustomer/new","admin"],
								["Ncustomer"=>"",
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params
							]
					  );
		return  $view;
	}
	
}