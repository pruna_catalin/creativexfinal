<?php
namespace CreativeX\core\controller\admin;
use CreativeX\Model\DAO\DAOSuser;
use CreativeX\Model\Data\DataSuser;
use CreativeX\modules\util\Debugger;
use CreativeX\modules\render\View;
use CreativeX\config\application;
/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 2017-12-05
 * Time: 03:39:43 PM
 */
class SuserController{
	private static $request, $lang;
	public function __construct($request, $lang = null) {
		self::$request = $request;
		self::$lang = $lang;
	}
	public function actionList(){
		$dataSuser = new DataSuser();
		$ValueFilter = ["id"=> "" ,"id_group"=> "" ,"id_profile"=> "" ,"username"=> "" ,"password"=> "" ,"email"=> "" ,"active"=> "" ,"reload_permisions"=> ""];
		$limit = "0,".application::MAX_PERPAGE;
		$totalSuser = DAOSuser::Count();
		$page_no = 1;
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['search'])) {
				if(isset($params['Model']['search']['id']))
					if($params['Model']['search']['id'] != "")
						$dataSuser->id =  is_numeric($params['Model']['search']['id']) ? $params['Model']['search']['id'] : 0;
				if(isset($params['Model']['search']['id_group']))
					if($params['Model']['search']['id_group'] != "")
						$dataSuser->id_group =  is_numeric($params['Model']['search']['id_group']) ? $params['Model']['search']['id_group'] : 0;
				if(isset($params['Model']['search']['id_profile']))
					if($params['Model']['search']['id_profile'] != "")
						$dataSuser->id_profile =  is_numeric($params['Model']['search']['id_profile']) ? $params['Model']['search']['id_profile'] : 0;
				if(isset($params['Model']['search']['username']))
					if($params['Model']['search']['username'] != "")
						$dataSuser->username['like'] = ["%".$params['Model']['search']['username']."%"];
				if(isset($params['Model']['search']['password']))
					if($params['Model']['search']['password'] != "")
						$dataSuser->password['like'] = ["%".$params['Model']['search']['password']."%"];
				if(isset($params['Model']['search']['email']))
					if($params['Model']['search']['email'] != "")
						$dataSuser->email['like'] = ["%".$params['Model']['search']['email']."%"];
				if(isset($params['Model']['search']['active']))
					if($params['Model']['search']['active'] != "")
						$dataSuser->active =  is_numeric($params['Model']['search']['active']) ? $params['Model']['search']['active'] : 0;
				if(isset($params['Model']['search']['reload_permisions']))
					if($params['Model']['search']['reload_permisions'] != "")
						$dataSuser->reload_permisions =  is_numeric($params['Model']['search']['reload_permisions']) ? $params['Model']['search']['reload_permisions'] : 0;

				
			}
			if(isset($params['Model']['pagination'])){
				if(is_numeric($params['Model']['pagination'])){
					if($params['Model']['pagination'] < 1){
						$page_no = 1;
					}else{
						$page_no = $params['Model']['pagination'];
					}
					$page_no -= 1;
					$limit = ($page_no * application::MAX_PERPAGE).",".application::MAX_PERPAGE;
				}
			}

		}
		DAOSuser::$order = "id ASC";
		#\Debugger::log($limit);
		$Suser = DAOSuser::FindAll($dataSuser,$limit);
		$SuserList = "";
		if($Suser){
			foreach($Suser as $item){
				$child = new View( ["Suser/list_item","admin"], ["Suser"=>$item,"App"=> new application()]);
				$SuserList .= $child->render();
			}
		}
		$view =  new View( ["Suser/list","admin"], ["totalSuser"=>$totalSuser,"items"=>$SuserList,
													"App"=> new application(),"filter"=>$ValueFilter,"page_no"=>$page_no]);
        return  $view;
	}
	public function actionDelete(){
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['delete'])){
				$id = is_numeric($params['Model']['delete']) ? $params['Model']['delete'] : 0;
				if(DAOSuser::Find("id=".$id)){
					if(DAOSuser::Delete("id=".$id)){
						return json_encode(["success"=>true]);
					}
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}else{
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}
			}
		}
		exit;
	}
	public function actionEdit(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && self::$request->Method[1] != "POST"){
				$id = is_numeric($params['Model']) ? $params['Model'] : 0;
				$model = DAOSuser::Find("id=".$id);
				if(!$model) return self::$lang->t('notfound');
			}else{
				$id = is_numeric($params['Model']['id']) ? $params['Model']['id'] : 0;
				$model = DAOSuser::Find("id=".$id);
				if(!$model) return self::$lang->t('notfound');
				$dataSuser = new DataSuser();
				$dataSuser->id = $params['Model']['search']['id']; 
				$dataSuser->id_group = $params['Model']['search']['id_group']; 
				$dataSuser->id_profile = $params['Model']['search']['id_profile']; 
				$dataSuser->username = $params['Model']['search']['username']; 
				$dataSuser->password = $params['Model']['search']['password']; 
				$dataSuser->email = $params['Model']['search']['email']; 
				$dataSuser->active = $params['Model']['search']['active']; 
				$dataSuser->reload_permisions = $params['Model']['search']['reload_permisions'];
				$success = false;
				if(DAOSuser::Update($dataSuser,"id")){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/Suser/edit/".$dataSuser->id,true);

				//\Utils::Debug(self::$request);
			}					
		}
		$view =  new View(	["Suser/edit","admin"],
								["Suser"=>$model,
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params]
					  );
		return  $view;
	}
	public function actionAdd(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && $params['Model'] != ""){
				$dataSuser = new DataSuser();
				$dataSuser->id = $params['Model']['search']['id']; 
				$dataSuser->id_group = $params['Model']['search']['id_group']; 
				$dataSuser->id_profile = $params['Model']['search']['id_profile']; 
				$dataSuser->username = $params['Model']['search']['username']; 
				$dataSuser->password = $params['Model']['search']['password']; 
				$dataSuser->email = $params['Model']['search']['email']; 
				$dataSuser->active = $params['Model']['search']['active']; 
				$dataSuser->reload_permisions = $params['Model']['search']['reload_permisions'];	
				$model = DAOSuser::Insert($dataSuser);
				$success =	false;
				if($model){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/Suser/edit/".$model,true);
			}					
		}
		$view =  new View(	["Suser/new","admin"],
								["Suser"=>"",
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params
							]
					  );
		return  $view;
	}
	
}