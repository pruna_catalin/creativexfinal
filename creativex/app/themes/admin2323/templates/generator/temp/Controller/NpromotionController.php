<?php
namespace CreativeX\core\controller\admin;
use CreativeX\Model\DAO\DAONpromotion;
use CreativeX\Model\Data\DataNpromotion;
use CreativeX\modules\util\Debugger;
use CreativeX\modules\render\View;
use CreativeX\config\application;
/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 2017-12-05
 * Time: 03:39:43 PM
 */
class NpromotionController{
	private static $request, $lang;
	public function __construct($request, $lang = null) {
		self::$request = $request;
		self::$lang = $lang;
	}
	public function actionList(){
		$dataNpromotion = new DataNpromotion();
		$ValueFilter = ["id"=> "" ,"name"=> "" ,"discount"=> "" ,"create_by"=> "" ,"create_at"=> "" ,"modified_by"=> "" ,"modifed_at"=> ""];
		$limit = "0,".application::MAX_PERPAGE;
		$totalNpromotion = DAONpromotion::Count();
		$page_no = 1;
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['search'])) {
				if(isset($params['Model']['search']['id']))
					if($params['Model']['search']['id'] != "")
						$dataNpromotion->id =  is_numeric($params['Model']['search']['id']) ? $params['Model']['search']['id'] : 0;
				if(isset($params['Model']['search']['name']))
					if($params['Model']['search']['name'] != "")
						$dataNpromotion->name['like'] = ["%".$params['Model']['search']['name']."%"];
				if(isset($params['Model']['search']['discount']))
					if($params['Model']['search']['discount'] != "")
						$dataNpromotion->discount =  is_numeric($params['Model']['search']['discount']) ? $params['Model']['search']['discount'] : 0;
				if(isset($params['Model']['search']['create_by']))
					if($params['Model']['search']['create_by'] != "")
						$dataNpromotion->create_by =  is_numeric($params['Model']['search']['create_by']) ? $params['Model']['search']['create_by'] : 0;
				if(isset($params['Model']['search']['create_at']))
					if($params['Model']['search']['create_at'] != "")
						$dataNpromotion->create_at['like'] = ["%".$params['Model']['search']['create_at']."%"];
				if(isset($params['Model']['search']['modified_by']))
					if($params['Model']['search']['modified_by'] != "")
						$dataNpromotion->modified_by =  is_numeric($params['Model']['search']['modified_by']) ? $params['Model']['search']['modified_by'] : 0;
				if(isset($params['Model']['search']['modifed_at']))
					if($params['Model']['search']['modifed_at'] != "")
						$dataNpromotion->modifed_at['like'] = ["%".$params['Model']['search']['modifed_at']."%"];

				
			}
			if(isset($params['Model']['pagination'])){
				if(is_numeric($params['Model']['pagination'])){
					if($params['Model']['pagination'] < 1){
						$page_no = 1;
					}else{
						$page_no = $params['Model']['pagination'];
					}
					$page_no -= 1;
					$limit = ($page_no * application::MAX_PERPAGE).",".application::MAX_PERPAGE;
				}
			}

		}
		DAONpromotion::$order = "id ASC";
		#\Debugger::log($limit);
		$Npromotion = DAONpromotion::FindAll($dataNpromotion,$limit);
		$NpromotionList = "";
		if($Npromotion){
			foreach($Npromotion as $item){
				$child = new View( ["Npromotion/list_item","admin"], ["Npromotion"=>$item,"App"=> new application()]);
				$NpromotionList .= $child->render();
			}
		}
		$view =  new View( ["Npromotion/list","admin"], ["totalNpromotion"=>$totalNpromotion,"items"=>$NpromotionList,
													"App"=> new application(),"filter"=>$ValueFilter,"page_no"=>$page_no]);
        return  $view;
	}
	public function actionDelete(){
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['delete'])){
				$id = is_numeric($params['Model']['delete']) ? $params['Model']['delete'] : 0;
				if(DAONpromotion::Find("id=".$id)){
					if(DAONpromotion::Delete("id=".$id)){
						return json_encode(["success"=>true]);
					}
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}else{
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}
			}
		}
		exit;
	}
	public function actionEdit(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && self::$request->Method[1] != "POST"){
				$id = is_numeric($params['Model']) ? $params['Model'] : 0;
				$model = DAONpromotion::Find("id=".$id);
				if(!$model) return self::$lang->t('notfound');
			}else{
				$id = is_numeric($params['Model']['id']) ? $params['Model']['id'] : 0;
				$model = DAONpromotion::Find("id=".$id);
				if(!$model) return self::$lang->t('notfound');
				$dataNpromotion = new DataNpromotion();
				$dataNpromotion->id = $params['Model']['search']['id']; 
				$dataNpromotion->name = $params['Model']['search']['name']; 
				$dataNpromotion->discount = $params['Model']['search']['discount']; 
				$dataNpromotion->create_by = $params['Model']['search']['create_by']; 
				$dataNpromotion->create_at = $params['Model']['search']['create_at']; 
				$dataNpromotion->modified_by = $params['Model']['search']['modified_by']; 
				$dataNpromotion->modifed_at = $params['Model']['search']['modifed_at'];
				$success = false;
				if(DAONpromotion::Update($dataNpromotion,"id")){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/Npromotion/edit/".$dataNpromotion->id,true);

				//\Utils::Debug(self::$request);
			}					
		}
		$view =  new View(	["Npromotion/edit","admin"],
								["Npromotion"=>$model,
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params]
					  );
		return  $view;
	}
	public function actionAdd(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && $params['Model'] != ""){
				$dataNpromotion = new DataNpromotion();
				$dataNpromotion->id = $params['Model']['search']['id']; 
				$dataNpromotion->name = $params['Model']['search']['name']; 
				$dataNpromotion->discount = $params['Model']['search']['discount']; 
				$dataNpromotion->create_by = $params['Model']['search']['create_by']; 
				$dataNpromotion->create_at = $params['Model']['search']['create_at']; 
				$dataNpromotion->modified_by = $params['Model']['search']['modified_by']; 
				$dataNpromotion->modifed_at = $params['Model']['search']['modifed_at'];	
				$model = DAONpromotion::Insert($dataNpromotion);
				$success =	false;
				if($model){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/Npromotion/edit/".$model,true);
			}					
		}
		$view =  new View(	["Npromotion/new","admin"],
								["Npromotion"=>"",
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params
							]
					  );
		return  $view;
	}
	
}