<?php
namespace CreativeX\core\controller\admin;
use CreativeX\Model\DAO\DAOSprofile;
use CreativeX\Model\Data\DataSprofile;
use CreativeX\modules\util\Debugger;
use CreativeX\modules\render\View;
use CreativeX\config\application;
/*
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 2017-12-05
 * Time: 03:39:43 PM
 */
class SprofileController{
	private static $request, $lang;
	public function __construct($request, $lang = null) {
		self::$request = $request;
		self::$lang = $lang;
	}
	public function actionList(){
		$dataSprofile = new DataSprofile();
		$ValueFilter = ["id"=> "" ,"id_city"=> "" ,"id_country"=> "" ,"name"=> "" ,"skill"=> "" ,"language"=> ""];
		$limit = "0,".application::MAX_PERPAGE;
		$totalSprofile = DAOSprofile::Count();
		$page_no = 1;
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['search'])) {
				if(isset($params['Model']['search']['id']))
					if($params['Model']['search']['id'] != "")
						$dataSprofile->id =  is_numeric($params['Model']['search']['id']) ? $params['Model']['search']['id'] : 0;
				if(isset($params['Model']['search']['id_city']))
					if($params['Model']['search']['id_city'] != "")
						$dataSprofile->id_city =  is_numeric($params['Model']['search']['id_city']) ? $params['Model']['search']['id_city'] : 0;
				if(isset($params['Model']['search']['id_country']))
					if($params['Model']['search']['id_country'] != "")
						$dataSprofile->id_country =  is_numeric($params['Model']['search']['id_country']) ? $params['Model']['search']['id_country'] : 0;
				if(isset($params['Model']['search']['name']))
					if($params['Model']['search']['name'] != "")
						$dataSprofile->name['like'] = ["%".$params['Model']['search']['name']."%"];
				if(isset($params['Model']['search']['skill']))
					if($params['Model']['search']['skill'] != "")
						$dataSprofile->skill['like'] = ["%".$params['Model']['search']['skill']."%"];
				if(isset($params['Model']['search']['language']))
					if($params['Model']['search']['language'] != "")
						$dataSprofile->language['like'] = ["%".$params['Model']['search']['language']."%"];

				
			}
			if(isset($params['Model']['pagination'])){
				if(is_numeric($params['Model']['pagination'])){
					if($params['Model']['pagination'] < 1){
						$page_no = 1;
					}else{
						$page_no = $params['Model']['pagination'];
					}
					$page_no -= 1;
					$limit = ($page_no * application::MAX_PERPAGE).",".application::MAX_PERPAGE;
				}
			}

		}
		DAOSprofile::$order = "id ASC";
		#\Debugger::log($limit);
		$Sprofile = DAOSprofile::FindAll($dataSprofile,$limit);
		$SprofileList = "";
		if($Sprofile){
			foreach($Sprofile as $item){
				$child = new View( ["Sprofile/list_item","admin"], ["Sprofile"=>$item,"App"=> new application()]);
				$SprofileList .= $child->render();
			}
		}
		$view =  new View( ["Sprofile/list","admin"], ["totalSprofile"=>$totalSprofile,"items"=>$SprofileList,
													"App"=> new application(),"filter"=>$ValueFilter,"page_no"=>$page_no]);
        return  $view;
	}
	public function actionDelete(){
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args; 
			if(isset($params['Model']['delete'])){
				$id = is_numeric($params['Model']['delete']) ? $params['Model']['delete'] : 0;
				if(DAOSprofile::Find("id=".$id)){
					if(DAOSprofile::Delete("id=".$id)){
						return json_encode(["success"=>true]);
					}
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}else{
					return json_encode(["success"=>false,"message"=>self::$lang->t('notfound')]);
				}
			}
		}
		exit;
	}
	public function actionEdit(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && self::$request->Method[1] != "POST"){
				$id = is_numeric($params['Model']) ? $params['Model'] : 0;
				$model = DAOSprofile::Find("id=".$id);
				if(!$model) return self::$lang->t('notfound');
			}else{
				$id = is_numeric($params['Model']['id']) ? $params['Model']['id'] : 0;
				$model = DAOSprofile::Find("id=".$id);
				if(!$model) return self::$lang->t('notfound');
				$dataSprofile = new DataSprofile();
				$dataSprofile->id = $params['Model']['search']['id']; 
				$dataSprofile->id_city = $params['Model']['search']['id_city']; 
				$dataSprofile->id_country = $params['Model']['search']['id_country']; 
				$dataSprofile->name = $params['Model']['search']['name']; 
				$dataSprofile->skill = $params['Model']['search']['skill']; 
				$dataSprofile->language = $params['Model']['search']['language'];
				$success = false;
				if(DAOSprofile::Update($dataSprofile,"id")){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/Sprofile/edit/".$dataSprofile->id,true);

				//\Utils::Debug(self::$request);
			}					
		}
		$view =  new View(	["Sprofile/edit","admin"],
								["Sprofile"=>$model,
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params]
					  );
		return  $view;
	}
	public function actionAdd(){
		$model = "";
		$message = "";
		$params = "";
		if(sizeof(self::$request->Args) > 0) {
			$params = self::$request->Args;
			if(isset($params['Model']) && $params['Model'] != ""){
				$dataSprofile = new DataSprofile();
				$dataSprofile->id = $params['Model']['search']['id']; 
				$dataSprofile->id_city = $params['Model']['search']['id_city']; 
				$dataSprofile->id_country = $params['Model']['search']['id_country']; 
				$dataSprofile->name = $params['Model']['search']['name']; 
				$dataSprofile->skill = $params['Model']['search']['skill']; 
				$dataSprofile->language = $params['Model']['search']['language'];	
				$model = DAOSprofile::Insert($dataSprofile);
				$success =	false;
				if($model){
					$success = true;
				}
				if($success)
					\Utils::Redirect("admin/Sprofile/edit/".$model,true);
			}					
		}
		$view =  new View(	["Sprofile/new","admin"],
								["Sprofile"=>"",
								"message"=>$message,
								"App"=> new application(),
								"params"=>$params
							]
					  );
		return  $view;
	}
	
}