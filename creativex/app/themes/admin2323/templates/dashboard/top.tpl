<?php (!defined('CreativeX')) ? exit() : '';  ?>

<?php

//echo Html::Link("javascript:void();","IDLINK","forma","CLICK ME",["onclick"=>"alert(/pressme2/);"]);
//echo Html::Input("text","form-control",["name1","name2","name3"],"testVAL2",["onclick"=>"alert(/pressme2/);"]);
//echo Html::Radio(["id1","id2"],"form-control",["name1"],["yes"=>"checked","no"=>""],[]);
//echo Html::Checkbox("id1","",["radio1"],["yes"=>"checked"],["onclick"=>"alert(/pressme2/);"]);
//echo Html::DateInput("text","form-control",["name1"],date("Y-d-m",time()),["onclick"=>"alert(/pressme2/);"]);
//echo Html::Select2("idEvent","form-control",["country"],["en"=>["Engleza","selected"],"fr"=>["Franceza","selected"]],[],"multiple");
echo   Html::DatePicker("idPicker","",["name1"],"27.02.2018","calendar",[]);
?>
<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="#">
        <i class="fa fa-dashboard"></i> Home
      </a>
    </li>
    <li class="active">Dashboard</li>
  </ol>
</section>
<section class="content">
  <div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>{@$totalOrders@}</h3>

                <p>{@$Language->t('total_orders')@}</p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">{@$Language->t('more_info')@} <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3>{@$totalOrdersPaid@}<sup style="font-size: 20px">%</sup></h3>

                <p>{@$Language->t('total_orders_paid')@}</p>
            </div>
            <div class="icon">
                <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">{@$Language->t('more_info')@} <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>{@$totalUsers@}</h3>

                <p>{@$Language->t('user_registration')@}</p>
            </div>
            <div class="icon">
                <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">{@$Language->t('more_info')@} <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3>{@$totalCustomers@}</h3>

                <p>{@$Language->t('total_customers')@}</p>
            </div>
            <div class="icon">
                <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">{@$Language->t('more_info')@} <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
  </div>
</section>