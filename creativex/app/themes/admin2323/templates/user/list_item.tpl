﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<tr>
	<td>{@$user->id@}</td>
	<td>{@$user->username@}</td>
	<td>{@$user->email@}</td>
	<td>{@$user->active@}</td>
	<td >
      <a class="btn btn-info" href="{@$App::LIVE_URL_ADMIN@}users/edit/{@$user->id@}">{@$Language->t('edit_button')@}</a>
      <a class="btn btn-danger"  onclick="actionDelete('users/delete',{@$user->id@});">{@$Language->t('delete_button')@}</a>
  </td>
</tr>
              