﻿<?php (!defined('CreativeX')) ? exit() : ""; ?>
<section class="content-header">
  <h1>
    Add
    <small>User</small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="{@$App::LIVE_URL_ADMIN@}index">
        <i class="fa fa-dashboard"></i> Home
      </a>
    </li>
    <li class="active">
      <a href="{@$App::LIVE_URL_ADMIN@}users"> {@$Language->t('list_users')@}</a>
    </li>
  </ol>
</section>
<section class="content">
  <form class="form-horizontal" action="{@$App::LIVE_URL_ADMIN@}users/new" method="POST">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">{@$Language->t('note'). "".$message@}</h3>
        </div>
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <br>
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tab_1" data-toggle="tab">{@$Language->t('global_info')@}</a>
                </li>
                <li>
                  <a href="#tab_2" data-toggle="tab">{@$Language->t('profile_account')@}</a>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail" class="col-sm-2 control-label">{@$Language->t('email')@}</label>
                      <div class="col-sm-10">
                        <input type="email" id="inputEmail" class="form-control" name="Model[email]"
                        placeholder="{@$Language->t('email')@}" value="">
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="inputUsername" class="col-sm-2 control-label">{@$Language->t('username')@}</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="Model[username]" id="inputUsername"
                          placeholder="{@$Language->t('username')@}" value="">
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="inputPassword" class="col-sm-2 control-label">{@$Language->t('password')@}</label>
                      <div class="col-sm-10">
                        <input type="password" class="form-control" id="inputPassword"
                          placeholder="{@$Language->t('password')@}" name="Model[password]">
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="inputGroup" class="col-sm-2 control-label">{@$Language->t('group')@}</label>
                      <div class="col-sm-10">
                        <select class="form-control select2" id="inputGroup" name="Model[id_group]">
                          <?php
									foreach($group as $groupItem){
										echo '<option value="'.$groupItem->id.'">'.$groupItem->name.'</option>';
									}
								?>

                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputActive" class="col-sm-2 control-label">{@$Language->t('active')@}</label>
                      <div class="col-sm-10">
                        <label>
                          <input name="Model[active]" id="inputActive" value="1"  checked  type="radio" />Yes
                            <input name="Model[active]" id="inputActive" value="0" type="radio"/>No
						</label>
                      </div>
                    </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-info pull-right">{@$Language->t('save_button')@}</button>
                  </div>
                  <!-- /.box-footer -->

                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputName" class="col-sm-2 control-label">{@$Language->t('name')@}</label>
                      <div class="col-sm-10">
                        <input type="text" id="inputName" class="form-control" name="Model[name]"
                        placeholder="{@$Language->t('name')@}" value="">
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="inputUsername" class="col-sm-2 control-label">{@$Language->t('skill')@}</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="Model[skill]" id="inputUsername"
                          placeholder="{@$Language->t('skill')@}" value="">
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="inputLanguage" class="col-sm-2 control-label">{@$Language->t('language')@}</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="Model[language]" id="inputLanguage"
                          placeholder="{@$Language->t('language')@}" value="">
                        </div>
                    </div>

                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-info pull-right">{@$Language->t('save_button')@}</button>
                  </div>
                  <!-- /.box-footer -->
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
          </div>
        <!-- /.box-header -->
        <!-- form start -->

      </div>
    </form>
</section>