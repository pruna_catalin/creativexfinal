﻿<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{@ $App::BASE_CSS @}bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{@ $App::BASE_CSS @}font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{@ $App::BASE_CSS @}ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{@ $App::BASE_CSS @}admin.css">
  <link rel="stylesheet" href="{@ $App::BASE_CSS @}skin-blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#" class="logo"><b>{@$App::APP_NAME@}</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">{@$message@}</p>

    <form action="{@$App::LIVE_URL_ADMIN@}login" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" name="Model[username]">
        
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="Model[password]">
      
      </div>
	  <div class="form-group has-feedback">
        <select  class="form-control" placeholder="language" name="Model[language]">
			<option value='en'>English</option>
			<option value='ro'>Romana</option>
		</select>
       
      </div>
      <div class="row">
        
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="{@ $App::BASE_JS @}jquery.min.js"></script>
<!-- iCheck -->
<script src="{@ $App::BASE_JS @}bootstrap.min.js"></script>
</body>
</html>
