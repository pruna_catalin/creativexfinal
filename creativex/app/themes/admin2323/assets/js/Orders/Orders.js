﻿/**
 * Created by CreativeXGenerator
 * User     : Pruna Catalin
 * WebSite  : www.upg-ploiesti.ro
 * Platform : CreativeX-Framework
 */
$(document).ready(function () {

});
function GetItemSpecification( obj, redirectId) {
 
    $.ajax({
        url: "../getItems",
        type: 'POST',
        data: "Model[id_item]=" + obj.value,
        dataType: 'json',
        success: function (data) {
            if (data.success) {
              
                $("#quantityAdd").val(data.value);
                $("#priceAdd").val(data.price);
                //location.href = "orders/edit/" + redirectId;
            } else {
                toastr.error(data.message);
            }
        }
    });
}
function actionDelete(action, id) {
    bootbox.confirm(deleteMessage, function (result) {
        if (result === false) { } else {
            $.ajax({
                url: action,
                type: 'POST',
                data: "Model[delete]=" + id,
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        location.href = "orders";
                    } else {
                        toastr.error(data.message);
                    }
                }
            });
        }
    });
}

function calculate(obj,item,price) {
    var total = $("#totalCount")[0].value;
    var priceTotal = 0;  
    $("#totalPrice_" + item).html(obj.value * price);
    for (i = 0; i <= total; i++) {
        priceTotal += parseInt($("#totalPrice_" + i).html());
        
    }
    $("#calcTotal").val(priceTotal);
    console.log(priceTotal);
}