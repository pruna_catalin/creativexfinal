/**
 * Created by CreativeXGenerator
 * User     : Pruna Catalin
 * WebSite  : www.upg-ploiesti.ro
 * Platform : CreativeX-Framework
 */
$(document).ready(function(){
	
});

function actionDelete(action,id){
	bootbox.confirm(deleteMessage, function(result) {
		if (result === false) {} else {
			$.ajax({
			    url: action,
				type: 'POST', 
				data : "Model[delete]="+id,    
				dataType:'json', 
				success : function(data) {  
					if (data.success) {
					    location.href = "users";
					}else{
					    toastr.error(data.message);
					}
				}
			});
		}
	});
}
