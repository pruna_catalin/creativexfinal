/**
 * Created by CreativeXGenerator
 * User     : Pruna Catalin
 * WebSite  : www.upg-ploiesti.ro
 * Platform : CreativeX-Framework
 */
$(document).ready(function(){
	init.push(function () {
	// Single select
	Get_Value_From_Model();
		
	});
// Single select
	init.push(function () {
		if (! $('html').hasClass('ie8')) {
			$('#summernote-example').summernote({
				height: 200,
				tabsize: 2,
				codemirror: {
					theme: 'monokai'
				}
			});
		}		
	});
	$('#jq-datatables-example').dataTable( {
		 "bSort": false
	});
});
$(".dropzone_form").each(function(){
	var elemZone = $(this);
		elemZone.dropzone({ 	
	    url : "index.php?case=scomponents",
		init :function (){
			var id = this.element.id;
			this.on("processing", function(file) {
				this.options.url = "index.php?case=scomponents&add=true&upload=true";
			});
			this.on("success", function(file, responseText) {
				responseText = jQuery.parseJSON(responseText);
				$("#image").val(responseText.image_upload);
				elemZone.removeAttr("src").attr("src",responseText.image+"?"+Math.floor(Math.random()*1000));
				$.growl.notice({
					message: "Success Add"
				});	
			// Now attach this new element some where in your page
		  });
		  this.on("faild", function(file, responseText) {
				$.growl.notice({
					message: "Faild Add"
				});	
			// Now attach this new element some where in your page
		  });
		
		}
		,
		renameFilename: function (filename) {
			return new Date().getTime() + '_' + filename;
		}
		
	});
	});
$("td#dropzone_id").each(function(){
	var elemName = $(".dropzone"+$(this).attr('data'));	
	$(".dropzone"+$(this).attr('data')).dropzone({ 	
	    url : "index.php?case=scomponents",
		init :function (){
			var id = this.element.id;
			this.on("processing", function(file) {
				this.options.url = "index.php?case=scomponents&upload="+id;
			});
			this.on("success", function(file, responseText) {
				responseText = jQuery.parseJSON(responseText);
				elemName.attr("src",responseText.image+"?"+Math.floor(Math.random()*1000));
				$.growl.notice({
					message: "Success Add"
				});	
			// Now attach this new element some where in your page
		  });
		  this.on("faild", function(file, responseText) {
				$.growl.notice({
					message: "Faild Add"
				});	
			// Now attach this new element some where in your page
		  });
		
		},
		renameFilename: function (filename) {
			if(elemName.attr("data-id") !== "" || elemName.attr("noimage.gif")){
				return elemName.attr("data-id");
			}else{
				return new Date().getTime() + '_' + filename;
			}
            
        }
		
	});
	
});
function Get_Value_From_Model(){
	$.ajax({
		url : "index.php?case=scomponents&ajax_combo=true",                          
		type: 'GET', 
		data : '',
		dataType:'json',                   
		success : function(data) {  
			if (data.success) {
				$("#cascade_nr_0").html(data.content);
			}
		}
	});
}
function Check_Model_Cascade(model,item,id_object,id_object_next) {	
	var id = item.value;
	$.ajax({
		url : "index.php?case=model&ajax_get_id="+id,                          
		type: 'POST', 
		data : id,
		dataType:'json',                   
		success : function(data) {  
			if (data.success) {
				/*ENABLE_CASCADE*/
				if(id_object == "null"){
					$("#"+id_object_next).prop('disabled', false); 
					$("#"+id_object_next).html(data.content);
				}
			}
		}
	});
}

function add_scomponents(form){
	var formData = "";
	formData = $("#"+form).serialize();

   /*REMOVE_IFNOT_FILE formData = form; REMOVE_IFNOT_FILE*/
	/*REMOVE_IF_FILE formData = new FormData(form); formData.append('file', $('input[type=file]')[0].files[0]); REMOVE_IF_FILE*/
	$.ajax({
		url : "index.php?case=scomponents&add=true",                          
		type: 'POST', 
		data : formData,
/*REMOVE_IF_FILE
        async: false,
		processData: false,
		cache: false,
		contentType: false,
*/
		dataType:'json',                   
		success : function(data) {  
			if (data.success) {
				$.growl.notice({
					message: "scomponents added!"
				});				
			}else{
				$.growl.danger({
					message: "Fail to add scomponents!"
				});
			}
		}
	});
	return false;
}
function update_scomponents(id,form){
	var formData = "";
	formData = $("#"+form).serialize();
	$.ajax({
		url : "index.php?case=scomponents&update="+id,                          
		type: 'POST', 
		data : formData,
      
		dataType:'json',                   
		success : function(data) {  
			if (data.success) {
				$.growl.notice({
					message: "Success"
				});
			}else{
				$.growl.danger({
					message: "Save Faild"
				});
			}
		}
	});
	return false;
}
function delete_scomponents(id){
	bootbox.confirm("Are you sure to delete this?",function(result) {
			if(result == false){} else {
				$.ajax({
					url : "index.php?case=scomponents&delete="+id,                          
					type: 'POST', 
					data : "id="+id,
					dataType:'json',                   
					success : function(data) {  
						if (data.success && data.content === 1) {
							refresh_table();
						}
					}
				});
			}
		});
}
function refresh_table(){
	$.ajax({
		url : "index.php?case=scomponents&refresh_table=true",                          
		type: 'GET', 
		data : "",
		dataType:'json',                   
		success : function(data) {  
			if (data.success) {
				$("#refresh_table").html(data.content);
				$.growl.notice({
					message: "Success"
				});	
			}
		}
	});
}
