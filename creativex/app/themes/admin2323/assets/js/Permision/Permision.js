/**
 * Created by CreativeXGenerator
 * User     : Pruna Catalin
 * WebSite  : www.upg-ploiesti.ro
 * Platform : CreativeX-Framework
 */
$(document).ready(function(){
	var handleSample2 = function () {
		if(dataList !== "null"){
			$('#tree_2').jstree({
					'plugins': ["wholerow", "checkbox", "types"],
					'core': {
						"themes" : {
							"responsive": false
						},
						'data': dataList
					},
					"types" : {
						"default" : {
							"icon" : "fa fa-folder icon-state-warning icon-lg"
						},
						"file" : {
							"icon" : "fa fa-file icon-state-warning icon-lg"
						}
					}
				});
			};
		}
		handleSample2();
});
$('#tree_2').on("changed.jstree", function (e, data) {
	if(data.action === "select_node"){
		if(data.node.parent === "#"){
			var getAllObject  = data.instance._model.data;
			$.each(getAllObject,function(index,element){
				if(typeof element.data !== 'undefined' && element.data !== null && element.state.selected === true){
				    if (listPermision.indexOf(element.data) === -1) {
				        console.log(element.data);
				        listPermision.push(element.data);
					}					
				}
			});
			$("#permision_list").val(listPermision.join(","));
			
		}else{
			listPermision.push(data.node.data);
			$("#permision_list").val(listPermision.join(","));
			
		}
		
	}else if(data.action === "deselect_node"){
		if(data.node.parent === "#"){
			var getAllObject  = data.selected;
			var oldList =  data.instance._model.data;
			listPermision = [];
			$.each(getAllObject,function(index,element){
				if(oldList[element].data !== null){
				    listPermision.push(oldList[element].data);
				}
			});
			$("#permision_list").val(listPermision.join(","));
			
		}else{			
			listPermision.splice(listPermision.indexOf(data.node.data),1);
			$("#permision_list").val(listPermision.join(","));
			
		}
	}
	if(data.action === "ready"){
		var getAllObject  = data.instance._model.data;
		$.each(getAllObject,function(index,element){
			if(typeof element.data !== 'undefined' && element.data !== null && element.state.selected === true){
			    listPermision.push(element.data);
				$("#permision_list").val(listPermision.join(","));
			}
		});
	}
  
});

// Single select

