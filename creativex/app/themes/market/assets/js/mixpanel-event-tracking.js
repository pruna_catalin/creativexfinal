/* Start : Global Variables */
	var CONSTANT_USER_EMAIL = '';
	var CONSTANT_SESSION_ID = '';
/* End : Global Variables */


/* Start Event: Register Super Properties */
	function mixpanelSetSuperProperties(properties){
		if(CONSTANT_ENV == 'live')
			mixpanel.register_once(properties);
	}
/* End Event: Register Super Properties */


/* Start Event: Identify User */
	function mixpanelIdentifyUser(email){

		if(CONSTANT_ENV == 'live' && CONSTANT_USER_EMAIL == ''){
			mixpanel.alias(email);
			mixpanel.identify(email);
			CONSTANT_USER_EMAIL = email;
			mixpanel.people.set({ "$email": email });
		}
	}
/* End Event: Identify User */


/* Start Event: Add to Cart */
	function mixpanelAddToCart(item){

		if(CONSTANT_ENV == 'live'){
			mixpanelSetSuperProperties({session_id: item.session_id});

			var source = item.source;
			if(source == '') source = 'web';

			mixpanel.track("add_to_cart",
		    {
		    	"service": item.service,
		    	"quantity": item.quantity,
		    	"price": item.price,
		    	"source": source
		    }
			);

			// time Event
			mixpanel.time_event('completed_checkout');
		}
	}
/* End Event: Add to Cart */


/* Start Event: Completed Checkout */
	function mixpanelCompletedCheckout(total, txn_id, payment_method){

		if(CONSTANT_ENV == 'live'){
			mixpanel.track("completed_checkout",
		    {
		    	"total": total,
		    	"txn_id": txn_id,
		    	"payment_method": payment_method
		    }
			);
		}
	}
/* End Event: Completed Checkout */


/* Start Event: Logged In */
	function mixpanelUserLogIn(email){

		if(CONSTANT_ENV == 'live'){
			mixpanelIdentifyUser(email);
			mixpanel.track("logged_in");
		}
	}
/* End Event: Logged In */


/* Start Event: Logged Out */
	function mixpanelLogout(){

		console.log('logging you out...');

		if(CONSTANT_ENV == 'live')
			mixpanel.track('logged_out');

		window.location.href = base_url+'/logout';

		// if(CONSTANT_ENV == 'live')
			// mixpanel.reset();
	}
/* End Event: Logged Out */


/* Start Event: Sign Up */
	function mixpanelUserSignUp(email){

		if(CONSTANT_ENV == 'live'){
			mixpanelIdentifyUser(email);
			mixpanel.track("sign_up");
		}
	}
/* End Event: Sign Up */