/**

*

*  This file should contain helper functions only.

*  I annotated some of my code down below.

*

**/



$.fn.display_error = function(msg){

  $('.msg').addClass('error').html(msg);

}



$.fn.display_message = function(msg){

  $('.msg').removeClass('error').html(msg);

}



$.fn.display_loading = function(optional_message){

  $('.reyco-vince-harvey').html('<div id="loader-container"><img src="assets/images/ajax-loader.gif" /></div>');

}



$.fn.highlight_service = function(service){

  $('.service').removeClass('active');

  $('.s-' + service).addClass('active');

}



$.fn.change_header_color = function(service){

  $('.top-wrapper').removeClass('twitter');

  $('.top-wrapper').removeClass('youtube');

  $('.top-wrapper').removeClass('vimeo');

  $('.top-wrapper').removeClass('soundcloud');

  $('.top-wrapper').addClass(service);



  $('.top-wrapper-main').removeClass('twitter');

  $('.top-wrapper-main').removeClass('youtube');

  $('.top-wrapper-main').removeClass('vimeo');

  $('.top-wrapper-main').removeClass('soundcloud');

  $('.top-wrapper-main').addClass(service);



  $('.hero-headline').removeClass('twitter');

  $('.hero-headline').removeClass('youtube');

  $('.hero-headline').removeClass('vimeo');

  $('.hero-headline').removeClass('soundcloud');

  $('.hero-headline').addClass(service);

}
