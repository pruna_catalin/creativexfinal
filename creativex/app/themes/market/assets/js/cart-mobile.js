function saveCartEmailMobile(){
  var buyer_email = $('.js-mob-buyer-email').val();
  $.post(base_url+'/cart/update-buyer-email', { buyer_email:buyer_email }, function(data) {}, 'json');
}

function checkEmailRestrictedMobile(){
  var buyer_email = $('.js-mob-buyer-email').val();
  $.post(base_url+'/cart/check-input-not-restricted', { input:buyer_email }, function(data) {
    if(data.response == 1){
      alert('Email provided is restricted.');
      $('.js-mob-buyer-email').val('');
    }
  }, 'json');
}

function getPriceandDeliveryMobile(sub_service_id, quantity, cart_id) {
  $.post(base_url+'/cart/get-price-and-delivery', { 'sub_service_id': sub_service_id, 'quantity': quantity }, function(data) {
      //update item price
      var price = parseFloat(data.price);
      $('.js-mob-item-price-'+cart_id).html(parseFloat(data.price));
      $('.js-item-price-'+cart_id).html(price.toFixed(2));

      //update remove button with the new price
      $('.js-remove-btn-'+cart_id).removeAttr('onclick');
      $('.js-remove-btn-'+cart_id).attr('onclick', 'removeFromCart('+cart_id+', '+parseFloat(data.price)+')');

      updateCartTotalMobile();

  },'json');
}

function updateCartTotalMobile() {
  var total = 0;
  var discount = $('.js-discount-coupon-percent').val();
  var total_discount = 0;
  var grand_total = 0;
  var ctr = 0;
  var discountable_amt = 0;

  $.each($('.js-item-price'), function(index, ds){
    ds = $(ds);
    total = total + parseFloat(ds.html());
    total_without_discount = total;

    if(!$(ds).hasClass('no-discount')){
      discountable_amt = discountable_amt + parseFloat(ds.html());
    }

    ctr++;
  });

  grand_total = total;
  if(discount != '' && typeof discount != 'undefined'){
    total_discount = parseFloat(discountable_amt) * parseFloat(discount);
    grand_total = parseFloat(total) - parseFloat(total_discount);
  }

  $('.js-cart-total').html(parseFloat(grand_total).toFixed(2));
  $('.js-bonus-coins').html(parseFloat(total).toFixed(0));
  $('.cart_total').html(parseFloat(grand_total).toFixed(2));
  $('.num-items').html('('+ctr+')');
}

function saveResourceMobile(id,sub_service_id){
  var resource = $('.js-mob-cart-item-url-'+id).val();

  $.post(base_url+'/cart/edit-cart-resource', { 'id': id, 'sub_service_id': sub_service_id, 'resource': resource }, function(data) {console.log(data);
    if(data == 1) {
      console.log('ye');
    }
  });
}

function checkResourceRestrictedMobile(id){
  var input = $('.js-mob-cart-item-url-'+id).val();
  $.post(base_url+'/cart/check-input-not-restricted', { input:input }, function(data) {
    console.log(data);
    if(data.response == 1){
      alert('URL/Username provided is restricted.');
      $('.js-mob-cart-item-url'+id).val('');
    }
  }, 'json');
}

function removeFromCartMobile(cart_id, price) {
  $('.js-mob-cart-item-'+cart_id).fadeOut(function() { $(this).remove(); updateCartTotalMobile(); });
  $.post(base_url+'/cart/remove-from-cart', { 'id': cart_id }, function(data) {
    if(data.status == 1) {
      //update total cart
      updateCartTotalMobile();
    }
  });
}

function changeServiceQuantityOptionMobile(service_id, sub_service_id) {
  $.ajax(
    {
      method: 'post',
      url: base_url+'/cart/upsell-quantities',
      data: { 'sub_service_id': sub_service_id },
      success: function(data) {
        console.log(data);
        var first_price = 0; var count = 0; var is_daily = 0;
        $('.q_service_mobile_'+service_id).children().remove();
        $.each(data.packages, function(key,value) {
          if(!count){
            first_price = value.price;
            if(value.delivery_days == 'Daily' || value.delivery_days == 'daily') is_daily = 1;
          }
          $('.q_service_mobile_'+service_id).append($('<option value="'+value.quantity+':'+parseFloat(value.price)+'">'+value.quantity+' ($'+parseFloat(value.price)+') </option>'))
          count++;
        });
        if(is_daily) $('.js-add-to-cart-btn-'+service_id).addClass('js-daily-service');
        else $('.js-add-to-cart-btn-'+service_id).removeClass('js-daily-service');
        $('.js-selected-price-display-'+service_id).html(parseFloat(first_price).toFixed());
      },
      error: function(data) {
        console.log(data);
      }
    }
    );
}

function addToCartMobile(service_name, sub_service_id, sub_service_name, quantityandprice, source, deal_delivery_days, ab_variation, resource) {

  var mp_source = source;
  if(source == 'cart') source = 'web';

  $('.shop-empty').fadeOut();
  var email = $('.js-mob-buyer-email').val();
  $('body').css('cursor', 'wait');
  $('.btn-green-addtocart').css('cursor', 'wait');
  var quantity_arr = quantityandprice.split(':');
  var quantity = quantity_arr[0];
  var price = parseFloat(quantity_arr[1])

  if($('.btn-green-addtocart'+service_name).hasClass('js-daily-service')){
    window.location = base_url + '/buy-'+quantity+'-'+service_name+'-'+sub_service_name.split(' ').join('-').toLowerCase();
  }
  else{
    var item =  {
        'service_name': service_name,
        'sub_service_name': sub_service_name,
        'sub_service_id': sub_service_id,
        'quantity': quantity,
        'price': price,
        'email': email,
        's': source,
        'deal_delivery_days': deal_delivery_days,
        'resource' : resource
    };
    $.post(base_url+'/cart/add-to-cart', item, function(data) {

      console.log(data);

      // this condition will add a "no-discount" class to subservices under the Mother's day promo
      var can_discount_class = '';
      if(!data.can_discount) can_discount_class = 'no-discount';

      // tracking in mixpanel
      var cart_item = {service: service_name +' '+ sub_service_name, quantity: quantity, source: mp_source, price: price, session_id: data.cart_session_id}
      mixpanelAddToCart(cart_item);


      if(typeof(data.id) == 'number') {
        if((item.service_name=='twitter' && item.sub_service_name=='followers') || (item.service_name=='instagram' && item.sub_service_name=='followers'))
          placeholder = 'Enter '+item.service_name.charAt(0).toUpperCase()+item.service_name.slice(1)+' username';
        else if(item.service_name=='youtube' && item.sub_service_name=='dislikes')
          placeholder = 'Enter Youtube Video URL';
        else
          placeholder = 'Enter '+item.service_name.charAt(0).toUpperCase()+item.service_name.slice(1)+' URL';

        options = '';

        $.each(data.quantity_list, function(qty, val){
          if(item.quantity == val)
            options += '<option selected="selected" value="'+val+'">'+val+' '+item.service_name+' '+item.sub_service_name+'</option>';
          else
            options += '<option value="'+val+'">'+val+' '+item.service_name+' '+item.sub_service_name+'</option>';
        });

        new_item = '<div class="cart-item cart-item-dtls clearfix js-cart-item-'+data.id+'">';
        // icon
        new_item += '<div class="col-md-1 col-sm-1 co3-prod hidden-xs"><img src="'+base_url+'/assets/images/prod-'+item.service_name+'.png"></div>';
        // item url input field
        new_item += '<div class="col-md-5 col-sm-4 cart-item-input-field mob-no-pad"><ul class="co3-qty-serv-url-user"><li class="ie-cart-label"><img src="'+base_url+'/assets/images/prod-'+item.service_name+'.png" alt="'+item.service_name+'" class="visible-xs cart-item-icon"><span class="js-item-desc-qty-'+data.id+'">'+item.quantity+'</span> '+item.service_name.charAt(0).toUpperCase() + item.service_name.slice(1)+' '+item.sub_service_name.charAt(0).toUpperCase() + item.sub_service_name.slice(1)+' <img class="dn lighting-'+data.id+'" src="'+base_url+'/assets/images/img-high-retention.png" alt="high retention" title="high retention"></li><li class="ie-email"><input class="warning js-cart-item-url-'+data.id+' js-input-field" type="text" value="" placeholder="'+placeholder+'" style="color:#7C7C7C;" onfocus="if(this.placeholder==\''+placeholder+'\'){this.placeholder=\'\';this.style.color=\'#000000\';}" onblur="if(this.placeholder==\'\'){this.placeholder=\''+placeholder+'\';this.style.color=\'#7C7C7C\';}saveResource('+data.id+', '+data.sub_service_id+')"><span class="dn js-save'+data.id+'"><a href="javascript:void(0)" onclick="saveResource('+data.id+', '+data.sub_service_id+')"><img src="'+base_url+'/assets/images/btn-save-input.png"></a></span><i class="fa fa-ok dn js-ok'+data.id+'"></i></li>'; //</ul></div>

        // mobile quantity selection
        new_item += '<li class="ie-qty ie-qty-mob visible-xs"><div><select classjs-item-quantity-selection-'+data.id+' onchange="getPriceandDeliveryMobile('+data.sub_service_id+', this.value, '+data.id+');updateQuantity('+data.id+', '+data.sub_service_id+', this.value)">'+options+'</select></div></li>';


        new_item += '</ul></div>';

        new_item += '<div class="col-md-2 col-sm-2 ie-price-mob"><ul class="co2-price"><li class="ie-price"><div>$<span class="js-item-price-'+data.id+' js-item-price '+can_discount_class+'">'+item.price.toFixed(2)+'</span></div></li></ul></div>';

        new_item += '<ul class="co3-delete"><li><a href="javascript:void(0)" class="js-remove-btn-'+data.id+'" onclick="removeFromCart('+data.id+','+item.price+')"><i class="fa fa-remove"></i></a></li></ul>';

        // $('.js-order-list-mobile').append(new_item);
        $('.js-order-list').prepend(new_item);

        $('#item'+data.id).fadeOut().fadeIn();

        item.cart_id = data.id;
        item.delivery = data.delivery_days;
        item.quantity_list = data.quantity_list;

        updateCartTotalMobile();

        $('body').css('cursor', 'auto');
        $('.btn-green-addtocart').css('cursor', 'auto');
          return false;
      }
    },'json');
  }
}

$(document).ready(function(){
  $('.confirm-checkout-fund-mobile').click(function(){

    //check if logged in
    $.post(base_url+'/user/check-logged-in', {}, function(data){
      if(data.success){ console.log('data');
        if(parseFloat($('.js-mob-cart-total').text()) < 1){ return false; }
        var inputs = $('.js-mob-cart-item-url').filter(function(){ return $(this).val() == ''; });

        if(inputs.length){
          inputs.addClass('warning');
          alert('Ooops! Something is missing. Please make sure to enter your email, URL or usernamee.');
        }
        else{
          checkEnoughFund();
        }
      }
      else{
        $('.hide-pass').removeClass('hide');
      }
    });
  });

  $('.btn-login-mobile').click(function(){
    var email = $('.js-mob-buyer-email').val();
    var password = $('.js-mob-password').val();

    $.post(base_url+'/session/login', { email:email, password:password }, function(data){
      if(data.success == 'You\'re now login'){
        $('.hide-pass, .mobile-login').addClass('hide');
        $('.js-mob-user-email').html(data.email);
        $('.js-mob-user-funds').html(data.funds);
        $('.logged').removeClass('hide');
      }
    });
  });

  $('.confirm-checkout-btn-mobile').click(function(){
    var inputs = $('.js-mob-cart-item-url').filter(function(){ return $(this).val() == ''; });

    if(inputs.length){
      inputs.addClass('warning');
      alert('Ooops! Something is missing. Please make sure to enter your email, URL or username.');
    }else{
      $('#myModal').modal('show');
      window.location = base_url + '/cart/go-checkout';
    }
  });
});