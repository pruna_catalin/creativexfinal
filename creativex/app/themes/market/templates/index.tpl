<!DOCTYPE html>
<html lang="en-US">
   <head>
      <title>Buy Real Marketing | Buy Twitter Followers & Youtube Views</title>
      <meta name="description" content="Buy Real Marketing Is The Leading Online Marketplace To Buy Real Youtube Views, Instagram Followers, Twitter Followers, Fb Likes, Comments & More.">
      <meta name="copyright" content="2009 - 2017 Buy Real Marketing">
      <meta name="email" content="Email: hello@buyrealmarketing.com">
      <meta name="Distribution" content="Global">
      <meta name="Rating" content="General">
      <meta name="robots" content="index,follow">
      <link rel="canonical" href="index.htm">
      <meta name="robots" content="noodp,noydir">
      <meta name="Revisit-after" content="1 Day">
      <meta name="expires" content="never">
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <!-- Chrome, Firefox OS, Opera -->
      <!-- Mobile Browsers Address Bar Color -->
      <meta name="theme-color" content="#00B0EF">
      <link rel="shortcut icon" href="{@$App::PATH_FRONT_IMAGES@}favicon.ico">
      <link rel="author" href="https://plus.google.com/106648742468077903610">
      <link media="all" type="text/css" rel="stylesheet" href="{@$App::PATH_FRONT_CSS@}bootstrap.min.css">
      <link media="all" type="text/css" rel="stylesheet" href="{@$App::PATH_FRONT_CSS@}common-min.css">
      <link media="all" type="text/css" rel="stylesheet" href="{@$App::PATH_FRONT_CSS@}style-min.css">
      <link media="all" type="text/css" rel="stylesheet" href="{@$App::PATH_FRONT_CSS@}reset.min.css">
      <link media="all" type="text/css" rel="stylesheet" href="{@$App::PATH_FRONT_CSS@}font-awesome.min.css">
      <link media="all" type="text/css" rel="stylesheet" href="{@$App::PATH_FRONT_CSS@}jquery.mmenu.min.css">
      <link media="all" type="text/css" rel="stylesheet" href="{@$App::PATH_FRONT_CSS@}flexslider.min.css">
      <link media="all" type="text/css" rel="stylesheet" href="{@$App::PATH_FRONT_CSS@}media-queries-min.css">
      <link media="all" type="text/css" rel="stylesheet" href="{@$App::PATH_FRONT_CSS@}media-min.css">
      <!-- Themes -->
      <!-- <link media="all" type="text/css" rel="stylesheet" href="{@$App::PATH_FRONT_CSS@}theme-pokemon.css">
         -->
      <!-- Themes end -->
      <!-- <link media="all" type="text/css" rel="stylesheet" href="https://www.buyrealmarketing.com/assets/stylesheets/portalstyle.css">
         -->
      <!--[if IE 8]>
      <link media="all" type="text/css" rel="stylesheet" href="https://www.buyrealmarketing.com/assets/stylesheets/ie8.css">
      <![endif]-->
      <!--[if lt IE 8]>
      <link media="all" type="text/css" rel="stylesheet" href="https://www.buyrealmarketing.com/assets/stylesheets/ie7-lower.css">
      <![endif]-->
      <script src="{@$App::PATH_FRONT_JS@}js2/jquery.js"></script>
      <style type="text/css">
         /*@import  url(https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800);*//*font-family: 'Open Sans', sans-serif;*/
         /*@import  url(https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700); *//* font-family: 'Source Sans Pro', sans-serif; */
         h1,h2,h3,h4,h5,h6,p,div,ul,li,a,span { font-family: 'source_sans_pro', sans-serif !important; }
         .qty-con p.qty-new { font-weight:700; }
         .glyphicon { font-family: 'Glyphicons Halflings' !important; }
      </style>
      <!--<link media="all" type="text/css" rel="stylesheet" href="https://www.buyrealmarketing.com/assets/stylesheets/portalstyle.css">
         <!-- end Mixpanel -->	
   </head>
   <body>
      <input type="hidden" class="referer" value="">
      <div id="main-container">
         <div class="top-header hidden-xs">
            <style type="text/css">
               .hellobar { background: url("{@$App::PATH_FRONT_IMAGES@}bg-us-flag.jpg") no-repeat center; color:#000; font-size:14px; font-weight:400; line-height:22px; padding:6px 0; height:56px; text-align:center; }
               .hellobar p { font-size:14px; color:#FFF; }
               .hellobar p span { font-size:14px; }
               .hellobar a { background-color:#ff6600; color:#FFF; margin:0 4px; padding:6px; border-radius:4px; display:inline-block; }
               .hellobar a:hover { background-color:#dc5800; }
               @media  only screen and (min-width : 768px) {
               .hellobar { line-height:36px; }
               .hellobar p { font-size:21px; color:#FFF; }
               .hellobar p span { font-size:23px; }
               }
               @media  only screen and (min-width : 1200px) {
               .hellobar { background-size:100%; }
               .hellobar p { font-size:26px; color:#FFF; }
               }
            </style>
            <!-- Start Hello Bar -->
            <!-- <div id="hellobar" style="display: none;">
               <div id="hellobar-cont">
                 <p>BRM is celebrating its 5 years of service with all its employees! Customer service will be interrupted from 6:00PM EST to 4:00AM EST. Thank you for understanding.</p>
               </div>
               </div> -->
            <!-- End Hello Bar -->
            <header class="hidden-sm top-header-nav">
               <div class="container container-new ie-container">
                  <div class="row">
                     <div class="col-lg-7 col-md-6 head-left ie-head-left clearfix">
                        <div class="purchase-ticker hidden-md latest-purchase">
                        </div>
                        <div class="arrow-down hidden-md"></div>
                        <ul>
                           <li><a href="index.htm" class="visible-md"><img src="{@$App::PATH_FRONT_IMAGES@}new-brm-logo.png" alt="Buy Real Marketing"></a></li>
                           <li class="live-help">Live help 24/7</li>
                           <li class="ie8-telno"><img src="{@$App::PATH_FRONT_IMAGES@}phone-ico.png" alt="phone icon"> 1-844-920-2043</li>
                        </ul>
                     </div>
                     <div class="col-lg-5 col-md-6 clearfix right-menu">
                        <ul>
                           <li><a href="javascript:void(0)" class="join-for-free" data-toggle="modal" data-target="#requestAccount">Signup and get 50 BRM Coins</a></li>
                           <li><a href="javascript:void(0)" class="login-link" data-toggle="modal" data-target="#requestLogin">Customer Login</a></li>
                           <li>
                              <a href="cart.htm">
                                 <div class="bg-cart cart-new ">
                                    <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="BRM Cart">
                                    <span>Cart</span>
                                    (<span class="cart-orders totalItem">0</span>)
                                    $<span class="cart-price totalPrice">0</span>
                                 </div>
                                 <!-- End Cart -->
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </header>
            <section class="top-header-nav visible-sm">
               <div class="container container-new">
                  <div class="row">
                     <div class="col-sm-5">
                        <ul>
                           <li><a href="index.htm"><img src="{@$App::PATH_FRONT_IMAGES@}new-brm-logo.png" alt="Buy Real Marketing"></a></li>
                           <li class="live-help"><img src="{@$App::PATH_FRONT_IMAGES@}phone-ico.png" alt="phone icon"> 1-844-920-2043</li>
                        </ul>
                     </div>
                     <div class="col-sm-7 right-menu">
                        <ul>
                           <li>
                              <a href="javascript:void(0)" class="join-for-free" data-toggle="modal" data-target="#requestAccount">Signup and get 50 BRM Coins</a>
                           </li>
                           <li>
                              <a href="javascript:void(0)" class="login-link" data-toggle="modal" data-target="#requestLogin">Customer Login</a>
                           </li>
                           <li>
                              <a href="cart.htm">
                                 <!-- Start Cart -->
                                 <div class="bg-cart cart-new">
                                    <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="BRM Cart">
                                    <span>Cart</span>
                                    (<span class="cart-orders totalItem">0</span>)
                                    $<span class="cart-price totalPrice">0</span>
                                 </div>
                                 <!-- End Cart -->
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </section>
            <script type="text/javascript">
               jQuery(document).ready(function() {
                 jQuery('.join-for-free').click(function() {
                   jQuery('#requestAccount').css('visibility', 'visible');
                 });
               
                 jQuery('.dismiss-signup-modal').click(function() {
                   jQuery('#requestAccount').css('visibility', 'hidden');
                 });
               
                 jQuery('.close-btn').click(function() {
                   jQuery('#requestAccount').css('visibility', 'hidden');
                 });
               
                });
            </script>
            <script type="text/javascript">
               jQuery(document).ready(function() {
               
                 jQuery('.login-link').click(function() {
                   jQuery('#requestLogin').css('visibility', 'visible');
                 });
               
                 jQuery('.dismiss-login-modal').click(function() {
                   jQuery('#requestLogin').css('visibility', 'hidden');
                 });
               
                  jQuery('.from-login').click(function() {
                   jQuery('#requestLogin').css('visibility', 'hidden');
                 });
               
                });
            </script>
            <!-- Start Variation Sub Menu Desktop -->
            <!-- <style type="text/css">
               .hellobar { display:none; }
               </style> -->
            <!-- Start Desktop -->
            <section class="nav-section hidden-sm container-navigation variation-nav">
               <div class="container container-new2 ie-container">
                  <div class="row">
                     <div class="col-lg-2 ie-logo-container hidden-md">
                        <a href="index.htm"><img src="{@$App::PATH_FRONT_IMAGES@}new-brm-logo.png" alt="Buy Real Marketing"></a>
                        <div class="btn-group lang hide">
                           <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
                           select language <span class="caret"></span>
                           </button>
                        </div>
                     </div>
                     <div class="col-lg-10 ie-nav">
                        <nav class="prox-bold hidden-xs">
                           <ul class="nav nav-pills">
                              <li class="dropdown fb moveto">
                                 <a class="dropdown-toggle " href="buy-facebook-likes.htm">
                                 <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="facebook" class="icon-nav-fb">Facebook
                                 </a>
                                 <ul role="menu" class="dropdown-menu">
                                    <li>
                                       <a href="buy-facebook-likes.htm" title="Likes">Likes</a>
                                    </li>
                                    <li>
                                       <a href="buy-facebook-post-likes.htm" title="Post Likes">Post Likes</a>
                                    </li>
                                    <li>
                                       <a href="buy-facebook-shares.htm" title="Shares">Shares</a>
                                    </li>
                                    <li>
                                       <a href="buy-facebook-five-star-ratings.htm" title="Five Star Ratings">Five Star Ratings</a>
                                    </li>
                                    <li>
                                       <a href="buy-facebook-comments.htm" title="Comments">Comments</a>
                                    </li>
                                    <li>
                                       <a href="buy-facebook-friends.htm" title="Friends">Friends</a>
                                    </li>
                                    <li>
                                       <a href="buy-facebook-followers.htm" title="Followers">Followers</a>
                                    </li>
                                    <li>
                                       <a href="buy-facebook-daily-likes.htm" title="Daily Likes">Daily Likes</a>
                                    </li>
                                    <li>
                                       <a href="buy-facebook-daily-post-likes.htm" title="Daily Post Likes">Daily Post Likes</a>
                                    </li>
                                    <li>
                                       <a href="buy-facebook-post-haha-reactions.htm" title="Post Haha Reactions">Post Haha Reactions</a>
                                    </li>
                                    <li>
                                       <a href="buy-facebook-post-love-reactions.htm" title="Post Love Reactions">Post Love Reactions</a>
                                    </li>
                                    <li>
                                       <a href="buy-facebook-post-wow-reactions.htm" title="Post Wow Reactions">Post Wow Reactions</a>
                                    </li>
                                    <li>
                                       <a href="buy-facebook-video-views.htm" title="Video Views">Video Views</a>
                                    </li>
                                    <!-- Add facebook management page -->
                                    <li><a href="facebook-page-management.htm" title="facebook-page-management">Fanpage Management</a></li>
                                 </ul>
                              </li>
                              <li class="dropdown twit moveto">
                                 <a class="dropdown-toggle " href="buy-twitter-followers.htm">
                                 <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="twitter" class="icon-nav-twit">Twitter
                                 </a>
                                 <ul role="menu" class="dropdown-menu">
                                    <li>
                                       <a href="buy-twitter-followers.htm" title="Followers">Followers</a>
                                    </li>
                                    <li>
                                       <a href="buy-twitter-retweets.htm" title="Retweets">Retweets</a>
                                    </li>
                                    <li>
                                       <a href="buy-twitter-usa-followers.htm" title="USA Followers">USA Followers</a>
                                    </li>
                                    <li>
                                       <a href="buy-twitter-daily-followers.htm" title="Daily Followers">Daily Followers</a>
                                    </li>
                                    <li>
                                       <a href="buy-twitter-favorites.htm" title="Favorites">Favorites</a>
                                    </li>
                                    <!-- Add facebook management page -->
                                    <li><a href="twitter-account-management.htm" title="facebook-page-management">Account Management</a></li>
                                 </ul>
                              </li>
                              <li class="dropdown yt moveto">
                                 <a class="dropdown-toggle " href="buy-youtube-views.htm">
                                 <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="youtube" class="icon-nav-yt">Youtube
                                 </a>
                                 <ul role="menu" class="dropdown-menu">
                                    <li>
                                       <a href="buy-youtube-views.htm" title="Regular Views">Regular Views</a>
                                    </li>
                                    <li>
                                       <a href="buy-youtube-comments.htm" title="Comments">Comments</a>
                                    </li>
                                    <li>
                                       <a href="buy-youtube-subscribers.htm" title="Subscribers">Subscribers</a>
                                    </li>
                                    <li>
                                       <a href="buy-youtube-likes.htm" title="Likes">Likes</a>
                                    </li>
                                    <li>
                                       <a href="buy-youtube-usa-views.htm" title="USA Views">USA Views</a>
                                    </li>
                                    <li>
                                       <a href="buy-youtube-favorites.htm" title="Favorites">Favorites</a>
                                    </li>
                                    <li>
                                       <a href="buy-youtube-daily-regular-views.htm" title="Daily Regular Views">Daily Regular Views</a>
                                    </li>
                                    <li>
                                       <a href="buy-youtube-daily-likes.htm" title="Daily Likes">Daily Likes</a>
                                    </li>
                                    <li>
                                       <a href="buy-youtube-shares.htm" title="Shares">Shares</a>
                                    </li>
                                    <!-- Add facebook management page -->
                                 </ul>
                              </li>
                              <li class="dropdown gplus moveto">
                                 <a class="dropdown-toggle " href="buy-googleplus-circle-followers.htm">
                                 <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="googleplus" class="icon-nav-gplus">Googleplus
                                 </a>
                                 <ul role="menu" class="dropdown-menu">
                                    <li>
                                       <a href="buy-googleplus-circle-followers.htm" title="Circle Followers">Circle Followers</a>
                                    </li>
                                    <li>
                                       <a href="buy-googleplus-post-plus-ones.htm" title="Post Plus 1">Post Plus 1</a>
                                    </li>
                                    <li>
                                       <a href="buy-googleplus-daily-circle-followers.htm" title="Daily Circle Followers">Daily Circle Followers</a>
                                    </li>
                                    <li>
                                       <a href="buy-googleplus-daily-post-plus1.htm" title="Daily Post Plus1">Daily Post Plus1</a>
                                    </li>
                                    <!-- Add facebook management page -->
                                 </ul>
                              </li>
                              <li class="dropdown vimeo moveto">
                                 <a class="dropdown-toggle " href="buy-vimeo-views.htm">
                                 <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="vimeo" class="icon-nav-vimeo">Vimeo
                                 </a>
                                 <ul role="menu" class="dropdown-menu">
                                    <li>
                                       <a href="buy-vimeo-views.htm" title="Views">Views</a>
                                    </li>
                                    <li>
                                       <a href="buy-vimeo-followers.htm" title="Followers">Followers</a>
                                    </li>
                                    <li>
                                       <a href="buy-vimeo-likes.htm" title="Likes">Likes</a>
                                    </li>
                                    <!-- Add facebook management page -->
                                 </ul>
                              </li>
                              <li class="dropdown sc moveto">
                                 <a class="dropdown-toggle " href="buy-soundcloud-plays.htm">
                                 <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="soundCloud" class="icon-nav-sc">SoundCloud
                                 </a>
                                 <ul role="menu" class="dropdown-menu">
                                    <li>
                                       <a href="buy-soundcloud-plays.htm" title="Plays">Plays</a>
                                    </li>
                                    <li>
                                       <a href="buy-soundcloud-downloads.htm" title="Downloads">Downloads</a>
                                    </li>
                                    <li>
                                       <a href="buy-soundcloud-followers.htm" title="Followers">Followers</a>
                                    </li>
                                    <li>
                                       <a href="buy-soundcloud-favorites.htm" title="Favorites">Favorites</a>
                                    </li>
                                    <li>
                                       <a href="buy-soundcloud-comments.htm" title="Comments">Comments</a>
                                    </li>
                                    <li>
                                       <a href="buy-soundcloud-repost.htm" title="Repost">Repost</a>
                                    </li>
                                    <li>
                                       <a href="buy-soundcloud-daily-followers.htm" title="Daily Followers">Daily Followers</a>
                                    </li>
                                    <!-- Add facebook management page -->
                                 </ul>
                              </li>
                              <li class="dropdown insta moveto">
                                 <a class="dropdown-toggle " href="buy-instagram-followers.htm">
                                 <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="instagram" class="icon-nav-insta">Instagram
                                 </a>
                                 <ul role="menu" class="dropdown-menu">
                                    <li>
                                       <a href="buy-instagram-followers.htm" title="Followers">Followers</a>
                                    </li>
                                    <li>
                                       <a href="buy-instagram-likes.htm" title="Likes">Likes</a>
                                    </li>
                                    <li>
                                       <a href="buy-instagram-comments.htm" title="Comments">Comments</a>
                                    </li>
                                    <li>
                                       <a href="buy-instagram-daily-likes.htm" title="Daily Likes">Daily Likes</a>
                                    </li>
                                    <li>
                                       <a href="buy-instagram-daily-followers.htm" title="Daily Followers">Daily Followers</a>
                                    </li>
                                    <li>
                                       <a href="buy-instagram-video-views.htm" title="Video Views">Video Views</a>
                                    </li>
                                    <!-- Add facebook management page -->
                                 </ul>
                              </li>
                              <li class="dropdown in moveto">
                                 <a class="dropdown-toggle " href="buy-linkedin-page-followers.htm">
                                 <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="linkedin" class="icon-nav-in">Linkedin
                                 </a>
                                 <ul role="menu" class="dropdown-menu">
                                    <li>
                                       <a href="buy-linkedin-page-followers.htm" title="Page Followers">Page Followers</a>
                                    </li>
                                    <li>
                                       <a href="buy-linkedin-company-followers.htm" title="Company Followers">Company Followers</a>
                                    </li>
                                    <!-- Add facebook management page -->
                                 </ul>
                              </li>
                              <!-- MORE SERVICES -->
                              <li class="dropdown more moveto">
                                 <a href="index.htm" class=" active ">More Services</a>
                                 <ul role="menu" class="dropdown-menu">
                                    <li><a href="buy-pinterest-followers.htm" class="pin">Pinterest</a></li>
                                    <li><a href="buy-datpiff-streams.htm" class="datp">Datpiff</a></li>
                                 </ul>
                              </li>
                           </ul>
                        </nav>
                     </div>
                  </div>
               </div>
            </section>
            <section class="sub-menu hidden-sm">
               <div class="container container-new2">
                  <div class="row">
                     <div class="col-lg-2"></div>
                     <div class="col-lg-10">
                        <ul class="sub-more">
                           <li>
                              <a href="buy-pinterest-followers.htm" class="pin">
                              <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="pinterest" class="icon-nav-pin"> Pinterest
                              </a>
                           </li>
                           <li>
                              <a href="buy-datpiff-streams.htm" class="datp">
                              <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="datpiff" class="icon-nav-datp"> Datpiff
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </section>
            <!-- End Desktop -->
            <!-- Start IPad -->
            <section class="nav-section visible-sm variation-nav">
               <div class="container container-new2 p-0">
                  <div class="row">
                     <div class="col-sm-12">
                        <nav class="prox-bold hidden-xs">
                           <ul class="nav nav-pills">
                              <li class="dropdown fb moveto">
                                 <a class="dropdown-toggle" href="buy-facebook-likes.htm">
                                 <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="Facebook" class="icon-nav-fb"></a>
                                 <ul role="menu" class="dropdown-menu">
                                    <!-- set exemptions -->
                                    <li><a href="buy-facebook-likes.htm" title="likes">Likes</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-facebook-post-likes.htm" title="likes">Post Likes</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-facebook-shares.htm" title="likes">Shares</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-facebook-five-star-ratings.htm" title="likes">Five Star Ratings</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-facebook-comments.htm" title="likes">Comments</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-facebook-friends.htm" title="likes">Friends</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-facebook-followers.htm" title="likes">Followers</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-facebook-daily-likes.htm" title="likes">Daily Likes</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-facebook-daily-post-likes.htm" title="likes">Daily Post Likes</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-facebook-post-haha-reactions.htm" title="likes">Post Haha Reactions</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-facebook-post-love-reactions.htm" title="likes">Post Love Reactions</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-facebook-post-wow-reactions.htm" title="likes">Post Wow Reactions</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-facebook-video-views.htm" title="likes">Video Views</a></li>
                                 </ul>
                              </li>
                              <li class="dropdown twit moveto">
                                 <a class="dropdown-toggle" href="buy-twitter-followers.htm">
                                 <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="Twitter" class="icon-nav-twit"></a>
                                 <ul role="menu" class="dropdown-menu">
                                    <!-- set exemptions -->
                                    <li><a href="buy-twitter-followers.htm" title="likes">Followers</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-twitter-retweets.htm" title="likes">Retweets</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-twitter-usa-followers.htm" title="likes">USA Followers</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-twitter-daily-followers.htm" title="likes">Daily Followers</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-twitter-favorites.htm" title="likes">Favorites</a></li>
                                 </ul>
                              </li>
                              <li class="dropdown yt moveto">
                                 <a class="dropdown-toggle" href="buy-youtube-views.htm">
                                 <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="Youtube" class="icon-nav-yt"></a>
                                 <ul role="menu" class="dropdown-menu">
                                    <!-- set exemptions -->
                                    <li><a href="buy-youtube-views.htm" title="likes">Regular Views</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-youtube-comments.htm" title="likes">Comments</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-youtube-subscribers.htm" title="likes">Subscribers</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-youtube-likes.htm" title="likes">Likes</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-youtube-usa-views.htm" title="likes">USA Views</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-youtube-favorites.htm" title="likes">Favorites</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-youtube-daily-regular-views.htm" title="likes">Daily Regular Views</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-youtube-daily-likes.htm" title="likes">Daily Likes</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-youtube-shares.htm" title="likes">Shares</a></li>
                                 </ul>
                              </li>
                              <li class="dropdown gplus moveto">
                                 <a class="dropdown-toggle" href="buy-googleplus-circle-followers.htm">
                                 <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="Googleplus" class="icon-nav-gplus"></a>
                                 <ul role="menu" class="dropdown-menu">
                                    <!-- set exemptions -->
                                    <li><a href="buy-googleplus-circle-followers.htm" title="likes">Circle Followers</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-googleplus-post-plus-ones.htm" title="likes">Post Plus 1</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-googleplus-daily-circle-followers.htm" title="likes">Daily Circle Followers</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-googleplus-daily-post-plus1.htm" title="likes">Daily Post Plus1</a></li>
                                 </ul>
                              </li>
                              <li class="dropdown vimeo moveto">
                                 <a class="dropdown-toggle" href="buy-vimeo-views.htm">
                                 <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="Vimeo" class="icon-nav-vimeo"></a>
                                 <ul role="menu" class="dropdown-menu">
                                    <!-- set exemptions -->
                                    <li><a href="buy-vimeo-views.htm" title="likes">Views</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-vimeo-followers.htm" title="likes">Followers</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-vimeo-likes.htm" title="likes">Likes</a></li>
                                 </ul>
                              </li>
                              <li class="dropdown sc moveto">
                                 <a class="dropdown-toggle" href="buy-soundcloud-plays.htm">
                                 <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="SoundCloud" class="icon-nav-sc"></a>
                                 <ul role="menu" class="dropdown-menu">
                                    <!-- set exemptions -->
                                    <li><a href="buy-soundcloud-plays.htm" title="likes">Plays</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-soundcloud-downloads.htm" title="likes">Downloads</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-soundcloud-followers.htm" title="likes">Followers</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-soundcloud-favorites.htm" title="likes">Favorites</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-soundcloud-comments.htm" title="likes">Comments</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-soundcloud-repost.htm" title="likes">Repost</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-soundcloud-daily-followers.htm" title="likes">Daily Followers</a></li>
                                 </ul>
                              </li>
                              <li class="dropdown insta moveto">
                                 <a class="dropdown-toggle" href="buy-instagram-followers.htm">
                                 <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="Instagram" class="icon-nav-insta"></a>
                                 <ul role="menu" class="dropdown-menu">
                                    <!-- set exemptions -->
                                    <li><a href="buy-instagram-followers.htm" title="likes">Followers</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-instagram-likes.htm" title="likes">Likes</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-instagram-comments.htm" title="likes">Comments</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-instagram-daily-likes.htm" title="likes">Daily Likes</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-instagram-daily-followers.htm" title="likes">Daily Followers</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-instagram-video-views.htm" title="likes">Video Views</a></li>
                                 </ul>
                              </li>
                              <li class="dropdown in moveto">
                                 <a class="dropdown-toggle" href="buy-linkedin-page-followers.htm">
                                 <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="Linkedin" class="icon-nav-in"></a>
                                 <ul role="menu" class="dropdown-menu">
                                    <!-- set exemptions -->
                                    <li><a href="buy-linkedin-page-followers.htm" title="likes">Page Followers</a></li>
                                    <!-- set exemptions -->
                                    <li><a href="buy-linkedin-company-followers.htm" title="likes">Company Followers</a></li>
                                 </ul>
                              </li>
                              <!-- <li class="deals">
                                 <a href="https://www.buyrealmarketing.com/deals">
                                   <i class="fa fa-clock-o"></i><span>NEW</span>
                                 </a>
                                 </li> -->
                              <!-- MORE SERVICES -->
                              <li class="dropdown more moveto">
                                 <a href="index.htm" class="dropdown-toggle  active ">
                                    More<!-- <i class="fa fa-caret-down"></i> -->
                                 </a>
                              </li>
                           </ul>
                        </nav>
                     </div>
                  </div>
               </div>
            </section>
            <section class="sub-menu visible-sm">
               <div class="container container-new2">
                  <div class="row">
                     <div class="col-lg-2"></div>
                     <div class="col-lg-10">
                        <ul class="sub-more">
                           <li>
                              <a href="buy-pinterest-followers.htm" class="pin">
                              <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="pinterest" class="icon-nav-pin"> Pinterest
                              </a>
                           </li>
                           <li>
                              <a href="buy-datpiff-streams.htm" class="datp">
                              <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="datpiff" class="icon-nav-datp"> Datpiff
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </section>
            <!-- End IPad -->
            <!-- End Variation Sub Menu Desktop -->
         </div>
         <div id="mobile-nav" class="visible-xs">
            <!-- Start Hello Bar -->
            <!-- End Hello Bar -->
            <div class="container top-header-nav">
               <div class="row">
                  <div class="col-md-12">
                     <a rel="follow" href="#menu" class="mbtn"></a>
                     <ul class="mobile-header">
                        <li><a href="index.htm"><img src="{@$App::PATH_FRONT_IMAGES@}new-brm-logo.png" class="mobile-logo" alt="BuyReal Marketing" title="BuyReal Marketing"></a></li>
                        <li>
                           <a href="cart.htm">
                              <!-- Start Cart -->
                              <div class="bg-cart cart-new ">
                                 <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="BRM Cart">
                                 <span>Cart</span>
                                 (<span class="cart-orders totalItem">0</span>)
                                 $<span class="cart-price totalPrice">0</span>
                              </div>
                              <!-- End Cart -->
                           </a>
                        </li>
                     </ul>
                     <nav id="menu">
                        <ul>
                           <li><a rel="follow" href="index.htm">Home</a></li>
                           <li class="slidemove">
                              <a rel="follow" href="javascript:void(0)">Login to your account</a>
                              <ul>
                                 <form id="cx-mobile-nav-login" role="form" method="POST" action="https://www.buyrealmarketing.com/session/login">
                                    <li>
                                       <div class="form-group">
                                          <label for="email" class="col-sm-2 control-label">Email Address</label>
                                          <div class="col-sm-10">
                                             <input type="email" class="form-control" name="email" placeholder="Email Address">
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="form-group">
                                          <div class="form-group">
                                             <label for="password" class="col-sm-2 control-label">Password</label>
                                             <div class="col-sm-10">
                                                <input type="password" class="form-control" name="password" placeholder="Password">
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="form-group">
                                          <div class="col-sm-offset-2 col-sm-10">
                                             <button type="button" class="btn btn-primary btn-login-now">Login</button>
                                             <a class="pull-right" href="password\remind.htm">Forgot Password</a>
                                          </div>
                                       </div>
                                    </li>
                                 </form>
                              </ul>
                           </li>
                           <li class="slidemove">
                              <a rel="follow" href="javascript:void(0)">Signup and get 50 BRMCoins</a>
                              <ul>
                                 <form role="form" method="POST" action="https://www.buyrealmarketing.com/home/requestaccountsignup">
                                    <li>
                                       <div class="form-group">
                                          <p class="mob-free">Get a Free 50 BRMCoins just for signing up!</p>
                                          <label for="inputName" class="col-sm-2 control-label">Enter your name</label>
                                          <div class="col-sm-10">
                                             <input type="text" class="form-control c_name_m" id="inputName" placeholder="Enter your name" required=""><!-- required field -->
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="form-group">
                                          <label for="inputEmail" class="col-sm-2 control-label">Enter your email address</label>
                                          <div class="col-sm-10">
                                             <input type="email" class="form-control c_email_m" id="inputEmail" placeholder="Enter your email address" required=""><!-- required field -->
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="col-sm-10">
                                          <div class="alert alert-success succ-req-acct display-none">Please check your email for your confirmation key.</div>
                                          <div class="alert alert-danger failed-req-acct display-none">Opps! Something went wrong.</div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="form-group">
                                          <div class="col-sm-offset-2 col-sm-10">
                                             <button type="button" class="btn btn-primary btn-request-account-mobile">Signup Now!</button>
                                             &nbsp;<img src="{@$App::PATH_FRONT_IMAGES@}loading-gif.gif" class="loading-gif display-none" alt="loading icon">
                                          </div>
                                       </div>
                                    </li>
                                 </form>
                              </ul>
                           </li>
                           <li class="slidemove">
                              <a rel="follow" href="javascript:void(0)">Facebook</a>
                              <ul>
                                 <li><a rel="follow" href="buy-facebook-likes.htm">Likes</a></li>
                                 <li><a rel="follow" href="buy-facebook-post-likes.htm">Post Likes</a></li>
                                 <li><a rel="follow" href="buy-facebook-shares.htm">Shares</a></li>
                                 <li><a rel="follow" href="buy-facebook-five-star-ratings.htm">Five Star Ratings</a></li>
                                 <li><a rel="follow" href="buy-facebook-comments.htm">Comments</a></li>
                                 <li><a rel="follow" href="buy-facebook-friends.htm">Friends</a></li>
                                 <li><a rel="follow" href="buy-facebook-followers.htm">Followers</a></li>
                                 <li><a rel="follow" href="buy-facebook-daily-likes.htm">Daily Likes</a></li>
                                 <li><a rel="follow" href="buy-facebook-daily-post-likes.htm">Daily Post Likes</a></li>
                                 <li><a rel="follow" href="buy-facebook-post-haha-reactions.htm">Post Haha Reactions</a></li>
                                 <li><a rel="follow" href="buy-facebook-post-love-reactions.htm">Post Love Reactions</a></li>
                                 <li><a rel="follow" href="buy-facebook-post-wow-reactions.htm">Post Wow Reactions</a></li>
                                 <li><a rel="follow" href="buy-facebook-video-views.htm">Video Views</a></li>
                              </ul>
                           </li>
                           <li class="slidemove">
                              <a rel="follow" href="javascript:void(0)">Twitter</a>
                              <ul>
                                 <li><a rel="follow" href="buy-twitter-followers.htm">Followers</a></li>
                                 <li><a rel="follow" href="buy-twitter-retweets.htm">Retweets</a></li>
                                 <li><a rel="follow" href="buy-twitter-usa-followers.htm">USA Followers</a></li>
                                 <li><a rel="follow" href="buy-twitter-daily-followers.htm">Daily Followers</a></li>
                                 <li><a rel="follow" href="buy-twitter-favorites.htm">Favorites</a></li>
                              </ul>
                           </li>
                           <li class="slidemove">
                              <a rel="follow" href="javascript:void(0)">Youtube</a>
                              <ul>
                                 <li><a rel="follow" href="buy-youtube-views.htm">Regular Views</a></li>
                                 <li><a rel="follow" href="buy-youtube-comments.htm">Comments</a></li>
                                 <li><a rel="follow" href="buy-youtube-subscribers.htm">Subscribers</a></li>
                                 <li><a rel="follow" href="buy-youtube-likes.htm">Likes</a></li>
                                 <li><a rel="follow" href="buy-youtube-usa-views.htm">USA Views</a></li>
                                 <li><a rel="follow" href="buy-youtube-favorites.htm">Favorites</a></li>
                                 <li><a rel="follow" href="buy-youtube-daily-regular-views.htm">Daily Regular Views</a></li>
                                 <li><a rel="follow" href="buy-youtube-daily-likes.htm">Daily Likes</a></li>
                                 <li><a rel="follow" href="buy-youtube-shares.htm">Shares</a></li>
                              </ul>
                           </li>
                           <li class="slidemove">
                              <a rel="follow" href="javascript:void(0)">Googleplus</a>
                              <ul>
                                 <li><a rel="follow" href="buy-googleplus-circle-followers.htm">Circle Followers</a></li>
                                 <li><a rel="follow" href="buy-googleplus-post-plus-ones.htm">Post Plus 1</a></li>
                                 <li><a rel="follow" href="buy-googleplus-daily-circle-followers.htm">Daily Circle Followers</a></li>
                                 <li><a rel="follow" href="buy-googleplus-daily-post-plus1.htm">Daily Post Plus1</a></li>
                              </ul>
                           </li>
                           <li class="slidemove">
                              <a rel="follow" href="javascript:void(0)">Vimeo</a>
                              <ul>
                                 <li><a rel="follow" href="buy-vimeo-views.htm">Views</a></li>
                                 <li><a rel="follow" href="buy-vimeo-followers.htm">Followers</a></li>
                                 <li><a rel="follow" href="buy-vimeo-likes.htm">Likes</a></li>
                              </ul>
                           </li>
                           <li class="slidemove">
                              <a rel="follow" href="javascript:void(0)">SoundCloud</a>
                              <ul>
                                 <li><a rel="follow" href="buy-soundcloud-plays.htm">Plays</a></li>
                                 <li><a rel="follow" href="buy-soundcloud-downloads.htm">Downloads</a></li>
                                 <li><a rel="follow" href="buy-soundcloud-followers.htm">Followers</a></li>
                                 <li><a rel="follow" href="buy-soundcloud-favorites.htm">Favorites</a></li>
                                 <li><a rel="follow" href="buy-soundcloud-comments.htm">Comments</a></li>
                                 <li><a rel="follow" href="buy-soundcloud-repost.htm">Repost</a></li>
                                 <li><a rel="follow" href="buy-soundcloud-daily-followers.htm">Daily Followers</a></li>
                              </ul>
                           </li>
                           <li class="slidemove">
                              <a rel="follow" href="javascript:void(0)">Pinterest</a>
                              <ul>
                                 <li><a rel="follow" href="buy-pinterest-followers.htm">Followers</a></li>
                                 <li><a rel="follow" href="buy-pinterest-daily-followers.htm">Daily Followers</a></li>
                              </ul>
                           </li>
                           <li class="slidemove">
                              <a rel="follow" href="javascript:void(0)">Instagram</a>
                              <ul>
                                 <li><a rel="follow" href="buy-instagram-followers.htm">Followers</a></li>
                                 <li><a rel="follow" href="buy-instagram-likes.htm">Likes</a></li>
                                 <li><a rel="follow" href="buy-instagram-comments.htm">Comments</a></li>
                                 <li><a rel="follow" href="buy-instagram-daily-likes.htm">Daily Likes</a></li>
                                 <li><a rel="follow" href="buy-instagram-daily-followers.htm">Daily Followers</a></li>
                                 <li><a rel="follow" href="buy-instagram-video-views.htm">Video Views</a></li>
                              </ul>
                           </li>
                           <li class="slidemove">
                              <a rel="follow" href="javascript:void(0)">Datpiff</a>
                              <ul>
                                 <li><a rel="follow" href="buy-datpiff-streams.htm">Streams</a></li>
                                 <li><a rel="follow" href="buy-datpiff-downloads.htm">Downloads</a></li>
                                 <li><a rel="follow" href="buy-datpiff-views.htm">Views</a></li>
                              </ul>
                           </li>
                           <li class="slidemove">
                              <a rel="follow" href="javascript:void(0)">Linkedin</a>
                              <ul>
                                 <li><a rel="follow" href="buy-linkedin-page-followers.htm">Page Followers</a></li>
                                 <li><a rel="follow" href="buy-linkedin-company-followers.htm">Company Followers</a></li>
                              </ul>
                           </li>
                        </ul>
                     </nav>
                  </div>
               </div>
            </div>
         </div>
         <!-- Mobile Navigation -->
         <!-- Modal -->
         <div class="modal modal-blog fade modal-signup" id="requestAccount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header">
                     Sign up for our exclusive customer portal<a href="#" class="dismiss-signup-modal" data-dismiss="modal">&times;</a>
                  </div>
                  <div class="modal-body">
                     <section class="blog-register">
                        <div class="side-logo">
                           <img src="{@$App::PATH_FRONT_IMAGES@}logo-login.png" alt="Buy Real Marketing Logo">
                        </div>
                        <div class="side-boost">
                           <p class="free-fund">Get a <strong>Free 50 BRM Coins</strong> just for signing up!</p>
                           <h3>BOOST YOUR</h3>
                           <h4>SOCIAL MEDIA FAN BASE</h4>
                        </div>
                        <div class="side-form">
                           <h5>
                              <span class="line">--------------</span>&nbsp;&nbsp;&nbsp;&nbsp;
                              REGISTER TODAY FOR A FREE ACCOUNT
                              &nbsp;&nbsp;&nbsp;<span class="line">-------------</span>
                           </h5>
                           <form id="cx-header-signup-modal" class="form-horizontal" method="post" action="https://www.buyrealmarketing.com/session/signup">
                              <div class="control-group">
                                 <div class="controls">
                                    <label class="field-label-name">Your Name</label><br>
                                    <input type="text" name="name" class="c_name_d" id="inputName" placeholder="Enter your name" required=""><!-- required field -->
                                 </div>
                              </div>
                              <div class="control-group">
                                 <div class="controls">
                                    <label class="field-label-email">Your Email Address</label><br>
                                    <input type="text" name="email" class="c_email_d" id="inputEmail" placeholder="Enter your email address" required=""><!-- required field -->
                                 </div>
                              </div>
                              <!-- <div class="control-group">
                                 <div class="controls">
                                   <input type="text" class="c_website" id="inputWebsite" placeholder="www.buyrealmarketing.com">
                                 </div>
                                 </div>
                                 <div class="control-group">
                                 <div class="controls">
                                   <input type="text" class="c_number" id="inputNumber" placeholder="1-617-590-0425">
                                 </div>
                                 </div> -->
                              <div class="control-group">
                                 <div class="alert alert-success succ-req-acct display-none">Please check your email for your credentials and to get your FREE 50 BRMCoins.</div>
                                 <div class="alert alert-danger failed-req-acct display-none">Opps! Something went wrong.</div>
                              </div>
                              <button type="button" class="btn btn-signup btn-request-account">
                              SIGN UP NOW <span class="js-signup-spinner" style="display: none;"><i class="fa fa-spinner fa-spin fa-pulse fa-fw"></i></span>
                              </button>
                              <p>
                                 <input type="checkbox">
                                 <label>Yes, I would like to receive future service updates from BRM.</label>
                              </p>
                           </form>
                           <p>
                              By clicking "SIGN UP NOW" you agree to our <a href="terms.htm">Terms of Use</a>.
                           </p>
                           <button type="button" class="btn btn-default close-btn" data-dismiss="modal">Close</button>
                        </div>
                     </section>
                  </div>
               </div>
            </div>
         </div>
         <!-- Modal -->
         <div class="modal modal-blog modal-signup fade" id="requestLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header">
                     Log in to our exclusive customer portal<a href="javascript:void(0)" class="dismiss-login-modal" data-dismiss="modal">&times;</a>
                  </div>
                  <div class="modal-body">
                     <section class="blog-register">
                        <div class="side-logo">
                           <img src="{@$App::PATH_FRONT_IMAGES@}logo-login.png" alt="Buy Real Marketing Logo">
                        </div>
                        <div class="side-boost">
                           <h3>BOOST YOUR</h3>
                           <h4>SOCIAL MEDIA FAN BASE</h4>
                        </div>
                        <div class="side-form">
                           <form class="form-horizontal" id="cx-header-login-modal" method="post" action="https://www.buyrealmarketing.com/session/login">
                              <input name="_token" type="hidden" value="4p96ByvRnJSzoKZBtqzyqSuhT1AJOPUM11HFqBRi">
                              <div class="control-group">
                                 <div class="controls">
                                    <input type="text" class="" name="email" id="exampleInputPassword1" placeholder="Enter your email address" required="required">
                                 </div>
                              </div>
                              <div class="control-group">
                                 <div class="controls">
                                    <input type="password" class="" name="password" id="exampleInputPassword1" placeholder="Enter your password" required="required">
                                 </div>
                              </div>
                              <button type="button" onclick="" class="btn btn-signup btn-request-login">LOG IN</button>
                              <div class="mt-10">
                                 <span class="pull-left"><a href="password\remind.htm">Forgot your password?</a></span>
                                 <span class="pull-right"><a rel="follow" href="javascript:void(0)" class="join-for-free from-login" data-toggle="modal" data-target="#requestAccount" data-dismiss="modal">Not a member? Request access.</a></span>
                              </div>
                           </form>
                        </div>
                     </section>
                  </div>
               </div>
            </div>
         </div>
         <!-- <div class="theme-hw theme-bfr theme-cm theme-pokemon"> --> <!-- Themes Wrapper -->
         <div class="theme-hw theme-bfr theme-cybrmonday theme-cm theme-eastersunday split-bg split-bg-hp">
            <!-- Themes Wrapper -->
            <div class="services-pack">
               <section class="heading-title top-pack-pad">
                  <h1>Boost Your Authority in Social Media</h1>
                  <h2>One Stop Shop for Social Media Services with 24/7 Live Help</h2>
               </section>
            </div>
            <section class="home-pack services-pack">
               <ul class="mob-pack">
                  <li class="common-pack box-shadow" itemtype="https://schema.org/Product" itemscope="">
                     <span class="pos-absolute vis-hide" itemprop="name">Instagram Followers</span>
                     <span class="pos-absolute vis-hide" itemprop="description">Buy 500 Instagram Followers at a very low price. Grow and Optimize Your Social Media Accounts now. We provide live Customer Service to Help You 24 Hours a Day, 7 Days a Week.</span>
                     <span url="https://www.buyrealmarketing.com/assets/images/side-logo.png" itemprop="logo"></span>
                     <ul>
                        <li class="mob-serv-logo">
                           <span itemscope="" itemtype="https://schema.org/ImageObject">
                              <div><img src="{@$App::PATH_FRONT_IMAGES@}instagram-pack-ico.png" itemprop="contentUrl" alt="Instagram" title="Instagram"></div>
                           </span>
                        </li>
                        <li class="mob-pack-right">
                           <div class="clearfix" itemprop="aggregateRating" itemscope="" itemtype="https://schema.org/AggregateRating">
                              <div class="mob-serv-qty">
                                 <p class="qty-new">500</p>
                                 <p class="service-new"><span itemprop="name">Buy <span class="display-none">500</span> Instagram Followers</span></p>
                              </div>
                              <span itemprop="ratingValue" class="fz1 pos-absolute vis-hide">4.7</span>
                              <span itemprop="reviewCount" class="fz1 pos-absolute vis-hide">684</span>
                              <div class="mob-price">
                                 <span class="dollar-sym">$</span>
                                 <span class="service-price-new">7</span>
                              </div>
                              <div class="s3-strike hidden-xs">
                                 <img src="{@$App::PATH_FRONT_IMAGES@}price-strike-new.png" alt="strike" title="strike">
                                 <div class="">
                                    <span>$</span>11
                                 </div>
                              </div>
                           </div>
                           <span style="position: absolute; visiblity: hidden; " itemprop="offers" itemscope="" itemtype="https://schema.org/Offer"><span itemprop="price" class="fz1">7</span></span>
                           <div class="btn-try-now2 mt-15 " data-s="web">
                              <a href="buy-500-instagram-followers" title="buy 500 Instagram Followers" rel="nofollow"><span class="glyphicon glyphicon-star mr-5"></span>Make me Famous</a>
                           </div>
                           <div class="deliver hidden-xs">
                              Delivered within 5 to 6 days
                           </div>
                        </li>
                     </ul>
                  </li>
                  <li class="common-pack box-shadow" itemtype="https://schema.org/Product" itemscope="">
                     <span class="pos-absolute vis-hide" itemprop="name">Twitter Followers</span>
                     <span class="pos-absolute vis-hide" itemprop="description">Buy 1,000 Twitter Followers at a very low price. Grow and Optimize Your Social Media Accounts now. We provide live Customer Service to Help You 24 Hours a Day, 7 Days a Week.</span>
                     <span url="https://www.buyrealmarketing.com/assets/images/side-logo.png" itemprop="logo"></span>
                     <ul>
                        <li class="mob-serv-logo">
                           <span itemscope="" itemtype="https://schema.org/ImageObject">
                              <div><img src="{@$App::PATH_FRONT_IMAGES@}twitter-pack-ico.png" itemprop="contentUrl" alt="Twitter" title="Twitter"></div>
                           </span>
                        </li>
                        <li class="mob-pack-right">
                           <div class="clearfix" itemprop="aggregateRating" itemscope="" itemtype="https://schema.org/AggregateRating">
                              <div class="mob-serv-qty">
                                 <p class="qty-new">1,000</p>
                                 <p class="service-new"><span itemprop="name">Buy <span class="display-none">1,000</span> Twitter Followers</span></p>
                              </div>
                              <span itemprop="ratingValue" class="fz1 pos-absolute vis-hide">4.7</span>
                              <span itemprop="reviewCount" class="fz1 pos-absolute vis-hide">684</span>
                              <div class="mob-price">
                                 <span class="dollar-sym">$</span>
                                 <span class="service-price-new">12</span>
                              </div>
                              <div class="s3-strike hidden-xs">
                                 <img src="{@$App::PATH_FRONT_IMAGES@}price-strike-new.png" alt="strike" title="strike">
                                 <div class="">
                                    <span>$</span>17
                                 </div>
                              </div>
                           </div>
                           <span style="position: absolute; visiblity: hidden; " itemprop="offers" itemscope="" itemtype="https://schema.org/Offer"><span itemprop="price" class="fz1">12</span></span>
                           <div class="btn-try-now2 mt-15 " data-s="web">
                              <a href="buy-1000-twitter-followers" title="buy 1000 Twitter Followers" rel="nofollow"><span class="glyphicon glyphicon-star mr-5"></span>Make me Famous</a>
                           </div>
                           <div class="deliver hidden-xs">
                              Delivered within 5 to 6 days
                           </div>
                        </li>
                     </ul>
                  </li>
                  <li class="common-pack box-shadow" itemtype="https://schema.org/Product" itemscope="">
                     <span class="pos-absolute vis-hide" itemprop="name">Youtube Views</span>
                     <span class="pos-absolute vis-hide" itemprop="description">Buy 10,000 Youtube Views at a very low price. Grow and Optimize Your Social Media Accounts now. We provide live Customer Service to Help You 24 Hours a Day, 7 Days a Week.</span>
                     <span url="https://www.buyrealmarketing.com/assets/images/side-logo.png" itemprop="logo"></span>
                     <ul>
                        <li class="mob-serv-logo">
                           <span itemscope="" itemtype="https://schema.org/ImageObject">
                              <div><img src="{@$App::PATH_FRONT_IMAGES@}youtube-pack-ico.png" itemprop="contentUrl" alt="Youtube" title="Youtube"></div>
                              <img src="{@$App::PATH_FRONT_IMAGES@}best-seller-ico.png" alt="best seller" title="best seller" class="sprites-home best-seller-icon hidden-xs">
                              <div class="mob-best-seller visible-xs">BEST SELLER</div>
                           </span>
                        </li>
                        <li class="mob-pack-right">
                           <div class="clearfix" itemprop="aggregateRating" itemscope="" itemtype="https://schema.org/AggregateRating">
                              <div class="mob-serv-qty">
                                 <p class="qty-new">10,000</p>
                                 <p class="service-new"><span itemprop="name">Buy <span class="display-none">10,000</span> Youtube Views</span></p>
                              </div>
                              <span itemprop="ratingValue" class="fz1 pos-absolute vis-hide">4.7</span>
                              <span itemprop="reviewCount" class="fz1 pos-absolute vis-hide">684</span>
                              <div class="mob-price">
                                 <span class="dollar-sym">$</span>
                                 <span class="service-price-new">47</span>
                              </div>
                              <div class="s3-strike hidden-xs">
                                 <img src="{@$App::PATH_FRONT_IMAGES@}price-strike-new.png" alt="strike" title="strike">
                                 <div class="">
                                    <span>$</span>64
                                 </div>
                              </div>
                           </div>
                           <span style="position: absolute; visiblity: hidden; " itemprop="offers" itemscope="" itemtype="https://schema.org/Offer"><span itemprop="price" class="fz1">47</span></span>
                           <div class="btn-try-now2 mt-15 " data-s="web">
                              <a href="buy-10000-youtube-views" title="buy 10000 Youtube Views" rel="nofollow"><span class="glyphicon glyphicon-star mr-5"></span>Make me Famous</a>
                           </div>
                           <div class="deliver hidden-xs">
                              Delivered within 10 to 11 days
                           </div>
                        </li>
                     </ul>
                  </li>
                  <li class="common-pack box-shadow" itemtype="https://schema.org/Product" itemscope="">
                     <span class="pos-absolute vis-hide" itemprop="name">Facebook Likes</span>
                     <span class="pos-absolute vis-hide" itemprop="description">Buy 2,000 Facebook Likes at a very low price. Grow and Optimize Your Social Media Accounts now. We provide live Customer Service to Help You 24 Hours a Day, 7 Days a Week.</span>
                     <span url="https://www.buyrealmarketing.com/assets/images/side-logo.png" itemprop="logo"></span>
                     <ul>
                        <li class="mob-serv-logo">
                           <span itemscope="" itemtype="https://schema.org/ImageObject">
                              <div><img src="{@$App::PATH_FRONT_IMAGES@}facebook-pack-ico.png" itemprop="contentUrl" alt="Facebook" title="Facebook"></div>
                           </span>
                        </li>
                        <li class="mob-pack-right">
                           <div class="clearfix" itemprop="aggregateRating" itemscope="" itemtype="https://schema.org/AggregateRating">
                              <div class="mob-serv-qty">
                                 <p class="qty-new">2,000</p>
                                 <p class="service-new"><span itemprop="name">Buy <span class="display-none">2,000</span> Facebook Likes</span></p>
                              </div>
                              <span itemprop="ratingValue" class="fz1 pos-absolute vis-hide">4.7</span>
                              <span itemprop="reviewCount" class="fz1 pos-absolute vis-hide">684</span>
                              <div class="mob-price">
                                 <span class="dollar-sym">$</span>
                                 <span class="service-price-new">69</span>
                              </div>
                              <div class="s3-strike hidden-xs">
                                 <img src="{@$App::PATH_FRONT_IMAGES@}price-strike-new.png" alt="strike" title="strike">
                                 <div class="">
                                    <span>$</span>99
                                 </div>
                              </div>
                           </div>
                           <span style="position: absolute; visiblity: hidden; " itemprop="offers" itemscope="" itemtype="https://schema.org/Offer"><span itemprop="price" class="fz1">69</span></span>
                           <div class="btn-try-now2 mt-15 " data-s="web">
                              <a href="buy-2000-facebook-likes" title="buy 2000 Facebook Likes" rel="nofollow"><span class="glyphicon glyphicon-star mr-5"></span>Make me Famous</a>
                           </div>
                           <div class="deliver hidden-xs">
                              Delivered within 10 to 11 days
                           </div>
                        </li>
                     </ul>
                  </li>
               </ul>
            </section>
         </div>
         <!-- End of Themes Wrapper -->
         <!-- Themes Images -->
         <!-- Themes Images -->
         <!-- check current controller -->
         <section class="promo-bar">
            <div class="container">
               <div class="row">
                  <div class="col-lg-12 text-center">
                     <ul>
                        <li>
                           <img src="{@$App::PATH_FRONT_IMAGES@}badge-since-2009.png" alt="since 2009">
                        </li>
                        <li class="ie-promo-text">
                           <div class="recommend-txt">97% of customers recommend Buy Real Marketing</div>
                           <span class="hidden-xs"><a href="#check-customer">Check what our customers are saying</a></span>
                        </li>
                        <li><img src="{@$App::PATH_FRONT_IMAGES@}moneyback-ico.png" alt="moneyback"></li>
                     </ul>
                  </div>
                  <script type="text/javascript">
                     jQuery(document).ready(function() {
                       //scroll down "check what our customers are saying"
                       jQuery('.check-link').click(function() {
                       jQuery('html,body').animate({ scrollTop: jQuery(this.hash).offset().top + 50}, 1000);
                       return false;
                       e.preventDefault();
                       });
                     });
                  </script>
               </div>
            </div>
         </section>
         <!-- Start Home Testimonials -->
         <section class="sp2-ordinary-page sp2-testimonial hidden-xs">
            <div class="container bg-trans no-border pb-0">
               <div class="row mb-40">
                  <h3 class="header-testimonials">Heart-warming testimonials from the most awesome people.</h3>
                  <div class="flexslider brm-hp-testi-slider">
                     <ul class="slides">
                        <li data-thumb="https://www.buyrealmarketing.com/assets/images/testimonial-1.png" alt="Andy - Marketing Manager" title="Andy - Marketing Manager">
                           <div class="body-slider">
                              <img src="{@$App::PATH_FRONT_IMAGES@}testimonial-big-img-1.png" class="img-responsive" alt="Andy - Marketing Manager" title="Andy - Marketing Manager">
                              <p class="testimonial-content fs-18 t-justify">Thank you once again for taking the time out to attend to my videos, means a lot to know your guys are bond by your word.<span class="close-quote"></span></p>
                              <p class="testimonial-title fs-18">Andy - Marketing Manager</p>
                           </div>
                        </li>
                        <!--
                           <li data-thumb="https://www.buyrealmarketing.com/assets/images/testimonial-2.png" alt="Adrien - Online Marketer, DJ" title="Adrien - Online Marketer, DJ">
                             <div class="body-slider">
                               <img src="https://www.buyrealmarketing.com/assets/images/testimonial-big-img-2.png" class="img-responsive" alt="Adrien - Online Marketer, DJ" title="Adrien - Online Marketer, DJ">
                               <p class="testimonial-content fs-18 t-justify">I wasn't too sure about these types of services. But when I saw 24/7 customer support I felt that if something went wrong, at least I could speak to someone. Everything went well will my first order and I'm super satisfied with the results. Thanks guys.<span class="close-quote"></span></p>
                               <p class="testimonial-title fs-18">Adrien - Online Marketer, DJ</p>
                             </div>
                           </li>
                           <li data-thumb="https://www.buyrealmarketing.com/assets/images/testimonial-3.png" alt="Carl - Digital Nomad, Blogger" title="Carl - Digital Nomad, Blogger">
                             <div class="body-slider">
                               <img src="https://www.buyrealmarketing.com/assets/images/testimonial-big-img-3.png" class="img-responsive" alt="Carl - Digital Nomad, Blogger" title="Carl - Digital Nomad, Blogger">
                               <p class="testimonial-content fs-18 t-justify">Thanks for the boost and for the awesome customer service. There was a little glitch with the order (my fault) and I was able to fix it right away by speaking with Jason. I'll be back for more..<span class="close-quote"></span></p>
                               <p class="testimonial-title fs-18">Carl - Digital Nomad, Blogger</p>
                             </div>
                           </li>
                           <li data-thumb="https://www.buyrealmarketing.com/assets/images/testimonial-4.png" alt="Sherie - Music Entrepreneur" title="Sherie - Music Entrepreneur">
                             <div class="body-slider">
                               <img src="https://www.buyrealmarketing.com/assets/images/testimonial-big-img-4.png" class="img-responsive" alt="Sherie - Music Entrepreneur" title="Sherie - Music Entrepreneur">
                               <p class="testimonial-content fs-18 t-justify">I love the weekly promos and the fact that I can order easily through the customer portal. No hassles, quick delivery. I friggin' love it.<span class="close-quote"></span></p>
                               <p class="testimonial-title fs-18">Sherie - Music Entrepreneur</p>
                             </div>
                           </li>
                           <li data-thumb="https://www.buyrealmarketing.com/assets/images/testimonial-5.png" alt="Clark - Filmmaker" title="Clark - Filmmaker">
                             <div class="body-slider">
                               <img src="https://www.buyrealmarketing.com/assets/images/testimonial-big-img-5.png" class="img-responsive" alt="Clark - Filmmaker" title="Clark - Filmmaker">
                               <p class="testimonial-content fs-18 t-justify">The Youtube views helped me kickstart my campaign for my indie film. I bought the first 10k views and with a bit of added marketing, I was at 48k in two weeks.<span class="close-quote"></span></p>
                               <p class="testimonial-title fs-18">Clark - Filmmaker</p>
                             </div>
                           </li>
                           <li data-thumb="https://www.buyrealmarketing.com/assets/images/testimonial-6.png" alt="Administrative Assistant" title="Administrative Assistant">
                             <div class="body-slider">
                               <img src="https://www.buyrealmarketing.com/assets/images/testimonial-big-img-6.png" class="img-responsive" alt="Administrative Assistant" title="Administrative Assistant">
                               <p class="testimonial-content fs-18 t-justify">When I went to you guys, it helped me, a part-time singer, increase my followers. Through that, a lot of people knew about me and my music.<span class="close-quote"></span></p>
                               <p class="testimonial-title fs-18">Helen - Administrative Assistant</p>
                             </div>
                           </li>
                           <li data-thumb="https://www.buyrealmarketing.com/assets/images/testimonial-7.png" alt="Tim - Travel Agent" title="Tim - Travel Agent">
                             <div class="body-slider">
                               <img src="https://www.buyrealmarketing.com/assets/images/testimonial-big-img-7.png" class="img-responsive" alt="Tim - Travel Agent" title="Tim - Travel Agent">
                               <p class="testimonial-content fs-18 t-justify">I tried a lot of different service like this and always got burned. Your site is nice, easy to use and you customer service people are very helpful. I was able to reach someone on a Saturday at 2 AM. That's awesome - a website that follows through with what it states.<span class="close-quote"></span></p>
                               <p class="testimonial-title fs-18">Tim - Travel Agent</p>
                             </div>
                           </li>
                           <li data-thumb="https://www.buyrealmarketing.com/assets/images/testimonial-8.png" alt="Karen - Social Media Agency" title="Karen - Social Media Agency">
                             <div class="body-slider">
                               <img src="https://www.buyrealmarketing.com/assets/images/testimonial-big-img-8.png" class="img-responsive" alt="Karen - Social Media Agency" title="Karen - Social Media Agency">
                               <p class="testimonial-content fs-18 t-justify">I run a small social media company, I have about 8 clients to manage. You guys make me look good and when I had an issue with one Twitter account, you guys slowed up the delivery to make it look smooth and natural.<span class="close-quote"></span></p>
                               <p class="testimonial-title fs-18">Karen - Social Media Agency</p>
                             </div>
                           </li>
                           -->
                     </ul>
                  </div>
               </div>
            </div>
            <!-- /.container -->
         </section>
         <!-- flexslider js -->
         <script src="{@$App::PATH_FRONT_JS@}modernizr.js"></script>
         <script src="{@$App::PATH_FRONT_JS@}jquery.flexslider-min.js"></script>
         <script type="text/javascript">
            jQuery(window).load(function(){
              jQuery('.flexslider').flexslider({
                animation: "slide",
                controlNav: "thumbnails",
                // start: function(slider){}
              });
            });
            
            jQuery(document).ready(function() {
              jQuery('.testimonials-send').click(function() {
                jQuery('.testimonial-success').removeClass('dn');
              });
            });
            
         </script>
         <!-- End Home Testimonials -->
         <!-- Start New Content Structure 1 -->
         <section class="home-sec1">
            <div class="container">
               <div class="row mb-20">
                  <div class="col-md-12">
                     <h3 class="mb-40">We&#39;ve helped thousands of personal brands and companies<br>kickstart their social media campaigns</h3>
                     <p class="mb-60 text-center"><em>If you have not fully realized the power of social media advertising yet, then you�ve got to look around and see the actual trend. If you are still not certain about this, we offer 100% money back guarantee to fully give reassurance to your success.</em></p>
                  </div>
                  <div class="col-xs-12 col-md-3">
                     <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="Music Artists" title="Music Artists" class="sprites-home musica-img mb-10">
                  </div>
                  <div class="col-xs-12 col-md-9">
                     <h4>Music Artists</h4>
                     <p class="hidden-xs">Buy Real Marketing can be of help to music artists who have long wanted to show off their musical talents to the world. If becoming the next favorite music icon is your dream, then we can help you turn that into reality. Start making a name by maximizing the power of social network. Get known on YouTube, Facebook, Twitter, Instagram, etc. Become the most famous version of yourself and become free to unleash your talent in front of the whole world.</p>
                  </div>
               </div>
               <div class="row mb-20">
                  <div class="col-xs-12 col-md-3">
                     <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="Entrepreneurs" title="Entrepreneurs" class="sprites-home entre-img mb10">
                  </div>
                  <div class="col-xs-12 col-md-9">
                     <h4>Entrepreneurs</h4>
                     <p class="hidden-xs">Advertising has now strongly penetrated the online world. Your market revolves around the internet. If you want your products or services to get ahead of the curve, you need a strong digital back-up to serve as your anchor. We can help you out in capturing your target market better. We can help your business get to the next level through boosting your social media metrics. Embrace our advanced tactics and see soaring results right in front of you.</p>
                  </div>
               </div>
               <div class="row mb-20">
                  <div class="col-xs-12 col-md-3">
                     <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="Creatives" title="Creatives" class="sprites-home crea-img mb-10">
                  </div>
                  <div class="col-xs-12 col-md-9">
                     <h4>Creatives</h4>
                     <p class="hidden-xs">If art naturally runs through your veins, then producing magnificent artworks is utterly effortless for you. The cyber world is now more appreciative of what aesthetics brings. The world needs your talent and your talent deserves to be shown off. Pursue your passion powered by creativity and rely on us in preparing the right audience for you.</p>
                  </div>
               </div>
               <div class="row mb-20">
                  <div class="col-xs-12 col-md-3">
                     <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="Entertainers" title="Entertainers" class="sprites-home enter-img mb-10">
                  </div>
                  <div class="col-xs-12 col-md-9">
                     <h4>Entertainers</h4>
                     <p class="hidden-xs">Bringing joy to people is the true essence of being an entertainer. Let the entire planet be filled with smiles as you do your tricks and ignite the complete mood. Share happiness far and wide. Get more followers and acquire more fans whose days will be complete with your amusing online presence. Begin your digital entertainment journey today and see an absolute change tomorrow.</p>
                  </div>
               </div>
               <div class="row mb-20">
                  <div class="col-xs-12 col-md-3">
                     <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="Bloggers" title="Bloggers" class="sprites-home blogger-img mb-10">
                  </div>
                  <div class="col-xs-12 col-md-9">
                     <h4>Bloggers</h4>
                     <p class="hidden-xs">Never underestimate the power of your words. You do not know how greatly they have influenced your readers. If you believe that your content is significantly substantial, you do not have any reasons not to make your blogs� readership get stretched to the maximum. Allow the whole planet to read your pieces and share your thoughts to every life you have touched.</p>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12"><span>Look, we understand the hesitation when buying views and followers but this doesn&#39;t have to be the case. Not only do we provide 24/7 support but we also offer a 100% money back guarantee.</span></div>
               </div>
            </div>
         </section>
         <!-- End New Content Structure 1 -->
         <!-- Start New Structure 5 -->
         <section class="home-sec2">
            <div class="container">
               <div class="row">
                  <div class="col-lg-12">
                     <h3>Why Buy From Us<br>4 reasons our customers trust us to boost their online credibility</h3>
                  </div>
               </div>
               <div class="row md-break">
                  <div class="wrap-why-buy-content col-md-6 col-sm-6">
                     <div class="row">
                        <div class="col-md-2 ie-our-credibility">
                           <icon class="fa fa-star bg1"></icon>
                        </div>
                        <div class="col-md-9">
                           <h3>Our Credibility</h3>
                           <p>We&#39;ve been here since 2009 and have served over 60,000 people since then. We&#39;ve also found that 80% of our customer base are so satisfied that they refer their friends and even return to buy more.</p>
                        </div>
                     </div>
                  </div>
                  <div class="wrap-why-buy-content col-md-6 col-sm-6">
                     <div class="row">
                        <div class="col-md-2 ie-our-speed">
                           <icon class="fa fa-thumbs-up bg2"></icon>
                        </div>
                        <div class="col-md-9">
                           <h3>Our Money Back Guarantee</h3>
                           <p>If we fail to deliver what has been promised then you are entitled to a full refund. Just get in contact and we will oblige, no questions asked!.</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="wrap-why-buy-content col-md-6 col-sm-6">
                     <div class="row">
                        <div class="col-md-2 ie-our-money-back">
                           <icon class="fa fa-user bg3"></icon>
                        </div>
                        <div class="col-md-9">
                           <h3>Our Speed of Delivery</h3>
                           <p>We strive to execute orders as soon as they are received. Our team constantly delivers on even the biggest of orders within a few days.</p>
                        </div>
                     </div>
                  </div>
                  <div class="wrap-why-buy-content col-md-6 col-sm-6">
                     <div class="row">
                        <a href="contact.htm">
                           <div class="col-md-2 ie-our-customer">
                              <icon class="fa fa-phone bg4"></icon>
                           </div>
                        </a>
                        <div class="col-md-9">
                           <h3>Our 24/7 Customer Service</h3>
                           <p>You should never have to settle for bad or slow customer support. Buy Real Marketing is the only social media provider offering 24/7 customer service. Our team will go above and beyond to help you with any issues, resolving them in a quick and efficient manner.</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- End New Structure 5 -->
         <section class="service-bottom">
            <div class="container">
               <h3 class="text-center">All Buy Real Marketing Customers Benefit from our user-friendly Dashboard.<br>Get lower prices on all our services in the Client Portal</h3>
               <div class="row">
                  <div class="col-md-6 col-sm-12">
                     <h3 class="portal-steps-title">
                        The only all-in-one social media solution that provides active and premium quality followers, views and plays.
                     </h3>
                     <div class="portal-steps-btn">
                        <a href="tour.htm" class="portal-tour blue-btn" id="user_login" title="tour">Take the Tour</a>
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#requestAccount" class="cta-signup-btn portal-signup join-for-free" id="user_signup" title="buyeal client portal">Sign up for Free</a>
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-12 portal-img hidden-sm ">
                     <span class="hidden-xs"><img src="{@$App::PATH_FRONT_IMAGES@}img-portal.png" width="580" alt="Client Portal" title="Client Portal"></span>
                  </div>
               </div>
            </div>
         </section>
         <!-- Start Testimonials -->
         <div class="testimonials-wrapper-main">
            <section class="footer-testimonials" id="check-customer">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-12">
                        <div class="test-title">Testimonial</div>
                        <p><img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="Benjamin, Digital Marketer, California, USA" title="Benjamin, Digital Marketer, California, USA" class="sprites-home testimonial-img mb-20"></p>
                        <p class="testimonial-name">Benjamin, Digital Marketer</p>
                        <p class="testimonial-address mb-20">California, USA</p>
                        <p class="testimonial-text">The process was quick and everything was right to the point. From start to finish it was simple and easy to check out.</p>
                        <p class="testimonial-text"><a rel="follow" href="testimonials.htm" class="text-center">Read more testimonials</a></p>
                     </div>
                  </div>
               </div>
            </section>
         </div>
         <!-- End Testimonials -->
         <!-- Start Footer -->
         <section class="footer">
            <div class="container">
               <div class="row visible-xs">
                  <div class="col-md-12">
                     <div class="footer-toll-free">
                        <i class="fa fa-phone"></i>1-844-920-2043
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-5 col-sm-6 col-xs-12">
                     <div class="mobile-footer">
                        <div class="row">
                           <div class="col-xs-6 col-sm-12">
                              <ul>
                                 <li><a href="index.htm">Home</a></li>
                                 <li><a href="about.htm">About</a></li>
                                 <li><a href="faq.htm">FAQ</a></li>
                                 <li><a href="contact.htm">Contact</a></li>
                                 <li><a href="testimonials.htm">Testimonials</a></li>
                                 <li><a href="guest-post-opportunity.htm">Guest Post Opportunity</a></li>
                              </ul>
                           </div>
                           <div class="col-xs-6 col-sm-12">
                              <ul>
                                 <li><a href="tour.htm">Tour</a></li>
                                 <li><a href="about-client-portal.htm">Client Portal</a></li>
                                 <li><a href="compare-us.htm">Compare Us</a></li>
                                 <li><a href="why-choose-us.htm">Why Choose Us</a></li>
                                 <li><a href="affiliate-program.htm">Affiliate Program</a></li>
                                 <li><a href="https://www.buyrealmarketing.com/blog">Blog</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="copyright">
                        <p>� 2009 - 2017 Buy Real Marketing Inc.
                           <a href="terms.htm">Terms</a> / <a href="policy.htm">Privacy</a> / <a href="refund-policy.htm">Refunds</a>
                        </p>
                        <p>Don't copy us, it's (really) Bad Karma</p>
                     </div>
                     <div class="flags">
                        <a href="http://www.brmsocial.es" target="_blank" class="mr-8"><img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="Instagram Followers Spanish" class="sprites-home es-flag"></a>
                     </div>
                  </div>
                  <div class="col-md-7 col-sm-6 col-xs-12 stamps">
                     <img src="{@$App::PATH_FRONT_IMAGES@}blank.gif" alt="certified" title="certified" class="sprites-home footer-cstamps">
                  </div>
               </div>
               <!-- <div class="row">
                  <div class="col-md-12">
                    <div class="social-media">
                      <p>Join us on..</p>
                      <a href="https://www.youtube.com/user/brmsocial"><img src="https://www.buyrealmarketing.com/assets/images/footer-yt.jpg" alt="youtube"></a>
                      <a href="https://www.facebook.com/brmsocial"><img src="https://www.buyrealmarketing.com/assets/images/footer-fb.jpg" alt="facebook"></a>
                      <a href="https://plus.google.com/101263621102960725030"><img src="https://www.buyrealmarketing.com/assets/images/footer-gplus.jpg" alt="google plus"></a>
                      <a href="https://www.pinterest.com/brmsocial"><img src="https://www.buyrealmarketing.com/assets/images/footer-pin.jpg" alt="pinterest"></a>
                    </div>
                  </div>
                  </div> -->
            </div>
         </section>
         <!-- End Footer -->
         <!-- <script src='https://code.jquery.com/ui/1.10.3/jquery-ui.min.js'></script> -->
         <script src="{@$App::PATH_FRONT_JS@}jquery-ui.min.js"></script>
         <script src="{@$App::PATH_FRONT_JS@}application.min.js"></script>
         <script src="{@$App::PATH_FRONT_JS@}jquery.mmenu.min.js"></script>
         <script type="text/javascript">
            var base_url = "https://www.buyrealmarketing.com";
             jQuery(function() {
                 jQuery('nav#menu').mmenu();
             });
         </script>
         <!-- start INCLUDES -->
         <script type="text/javascript">
            // (function() {
            //   window._pa = window._pa || {};
            //   // _pa.orderId = "myOrderId"; // OPTIONAL: attach unique conversion identifier to conversions
            //   // _pa.revenue = "19.99"; // OPTIONAL: attach dynamic purchase values to conversions
            //   // _pa.productId = "myProductId"; // OPTIONAL: Include product ID for use with dynamic ads
            //   var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;
            //   pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//tag.perfectaudience.com/serve/540d506127924ebaa8000035.js";
            //   var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);
            // })();
         </script>
         <script type="text/javascript">
            (function() {
              window._pa = window._pa || {};
                  
              // _pa.revenue = "19.99"; // OPTIONAL: attach dynamic purchase values to conversions
              // _pa.productId = "myProductId"; // OPTIONAL: Include product ID for use with dynamic ads
              var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;
              pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//tag.marinsm.com/serve/540d506127924ebaa8000035.js";
              var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);
            })();
         </script>    <!-- end INCLUDES -->
         <!-- Start Javascript Responsive -->
         <script src="{@$App::PATH_FRONT_JS@}js2/bootstrap.min.js"></script>
         <script src="{@$App::PATH_FRONT_JS@}cart-min.js"></script>
         <!-- Inclue mixpanel tracking functions -->
         <script src="{@$App::PATH_FRONT_JS@}mixpanel-event-tracking.js"></script>
         <script>
            (function($){
              $(window).load(function(){
                $(".custom-scroll").mCustomScrollbar({
                  theme:"dark-3"
                });
              });
              $(function(){
                $('.moveto').click(function() {
                  var url = $(this).children('ul').find('li a:first').attr('href');
                  window.location = url;
                });
                /*slick menu whole clickable*/
                $('.slidemove').click(function(){
                	$(this).find('a').click();
                });
              });
            })(jQuery);
         </script>
         <script type="text/javascript">
            adroll_adv_id = "VQKHTOF5ZJDDVPAP6OX5KG";
            adroll_pix_id = "JMY76LWZRVB55L2AUKCLLE";
            /* OPTIONAL: provide email to improve user identification */
            /* adroll_email = "username@example.com"; */
            (function () {
                var _onload = function(){
                    if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
                    if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
                    var scr = document.createElement("script");
                    var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
                    scr.setAttribute('async', 'true');
                    scr.type = "text/javascript";
                    scr.src = host + "/j/roundtrip.js";
                    ((document.getElementsByTagName('head') || [null])[0] ||
                        document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
                };
                if (window.addEventListener) {window.addEventListener('load', _onload, false);}
                else {window.attachEvent('onload', _onload)}
            }());
         </script>
         <!-- REFERSION TRACKING: BEGIN -->
         <script src="//www.refersion.com/tracker/v3/pub_a7270edea85cacdc5918.js"></script>
         <script>_refersion();</script>
         <!-- REFERSION TRACKING: END -->
      </div>
   </body>
</html>
