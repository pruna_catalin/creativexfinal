<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * DAOUsers: CreativeX && noValue
 * Date: 10/4/2017
 * Time: 5:12 PM
 */
namespace CreativeX\Model\Data;

class DataGroup{

	public $id				= NULL;
	public $id_permision	= NULL;
	public $name			= NULL;
	public $create_by		= NULL;
	public $create_at		= NULL;
	public $modified_by		= NULL;
	public $modified_at		= NULL;

	public function __construct($class = NULL ,$id = NULL,$id_permision = NULL, $name = NULL,$create_by = NULL, $create_at = NULL, $modified_by = NULL,	$modified_at = NULL){
		 if(is_a($class, __CLASS__)) { 
			 $this->setId($id);
			 $this->setIdpermision($id_permision);
			 $this->setName($name);
			 $this->setCreateBy($create_by);
			 $this->setCreateAt($create_at);
			 $this->setModifiedBy($modified_by);
			 $this->setModifiedAt($modified_at);
		 }
	}
	public function  setId($id){
        $this->id = $id;
        return $this;
    }

    public function getId(){
        return $this->id;
    }
	public function setIdpermision($id_permision){
		$this->id_permision = $id_permision;
		return $this;
	}
	public function getIdpermision(){
		return $this->id_permision;
	}

	public function setName($name){
		$this->name = $name;
		return $this;
	}

	public function getName(){
		return $this->name;
	}
	public function setCreateBy($create_by){
		$this->create_by = $create_by;
		return $this;
	}

	public function getCreateBy(){
		return $this->create_by;
	}

	public function setCreateAt($create_at){
		$this->create_at= $create_at;
		return $this;
	}

	public function getCreateAt(){
		return $this->create_at;
	}

	public function setModifiedBy($modified_by){
		$this->modified_by = $modified_by;
		return $this;
	}

	public function setModifiedAt($modified_at){
		return $this->modified_by;
	}
}