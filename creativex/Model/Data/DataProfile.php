<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * DAOProfile: CreativeX && noValue
 * Date: 10/18/2017
 * Time: 10:23 AM
 */

namespace CreativeX\Model\Data;

class DataProfile{

    public $id = NULL;
    public $name = NULL;
    public $skill = NULL;
	public $language = NULL;

    public function __construct($class = NULL ,$id = NULL, $name = NULL, $skill = NULL,	$language = NULL){
        if(is_a($class, __CLASS__)) {
            $this->setId($id);
            $this->setName($name);
            $this->setSkill($skill);
			$this->setLanguage($language);
        }
    }
    public function  setId($id){
        $this->id = $id;
        return $this;
    }
    public function getId(){
        return $this->id;
    }
    public function  setName($name){
        $this->name = $name;
        return $this;
    }
    public function getName(){
        return $this->name;
    }
    public function  setSkill($skill){
        $this->skill = $skill;
        return $this;
    }
    public function getSkill(){
        return $this->skill;
    }
	public function setLanguage($language){
		$this->language = $language;
		return $this;
	}
	public function getLanguage(){
		return $this->language;
	}


}
