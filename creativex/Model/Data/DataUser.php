<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 10/18/2017
 * Time: 10:30 AM
 */

namespace CreativeX\Model\Data;

class DataUser
{
	public $id = NULL ;
	public $id_group = NULL ;
	public $username = NULL ;
	public $password = NULL ;
	public $email = NULL ;
	public $id_profile = NULL ;
	public $active = NULL ;
	public $reload_permisions;

    public function __construct($class = NULL ,$id = NULL, $id_group = NULL,$id_profile = NULL, $username = NULL, $password = NULL, $email = NULL,  $active = NULL,$reload_permisions = NULL){
        if(is_a($class, __CLASS__)) {
			$this->setId($id);
			$this->setId_group($id_group);
			$this->setUsername($username);
			$this->setPassword($password);
			$this->setEmail($email);
			$this->setId_profile($id_profile);
			$this->setActive($active);
			$this->setReloadPermisions($reload_permisions);
        }
    }

    public function setId($id){
		$this->id  = $id;
	}
	public function setId_group($id_group){
		$this->id_group  = $id_group;
	}
	public function setUsername($username){
		$this->username  = $username;
	}
	public function setPassword($password){
		$this->password  = $password;
	}
	public function setEmail($email){
		$this->email  = $email;
	}
	public function setId_profile($id_profile){
		$this->id_profile  = $id_profile;
	}
	public function setActive($active){
		$this->active  = $active;
	}
	public function setReloadPermisions($reload_permisions){
		$this->reload_permisions  = $reload_permisions;
	}

}
class TableUser{

    /*
     *  Table Name 
     */

    public $table_name = "users";

    /*
     *  Column id
     */

    public $id = [
            "type"=>"int(11)", //type
            "null"=>"IsNotNull", // null or not
            "default"=>"", //default
            "extra"=>"autoincrement" //extra
    ];

    /*
     *  Column name
     */

    public $name = [
           "type"=>"varchar(255)", //type
           "null"=>"IsNUll", // null or not
           "default"=>"", //default
           "extra"=>"" //extra
    ];

    /*
     *  Column username
     */

    public $username = [
           "type"=>"varchar(255)", //type
           "null"=>"IsNUll", // null or not
           "default"=>"", //default
           "extra"=>"" //extra
    ];

    /*
     *  Column password
     */

    public $password = [
           "type"=>"varchar(255)", //type
           "null"=>"IsNUll", // null or not
           "default"=>"", //default
           "extra"=>"" //extra
    ];
    
}