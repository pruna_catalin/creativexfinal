<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 10/18/2017
 * Time: 10:30 AM
 */

namespace CreativeX\Model\Data;
class DataOrder{

	public $id				= NULL;

	public $id_customer		= NULL;

	public $id_item			= NULL;

	public $quantity		= NULL;

	public $price			= NULL;

	public $status			= NULL;

	public $create_by		= NULL;

	public $create_at		= NULL;
	
	public $modified_by		= NULL;

	public $modified_at		= NULL;


	public function __construct($class = NULL ,$id = NULL, $id_customer = NULL,  $id_item = NULL, $quantity = NULL, $price = NULL,$status = NULL, $create_by = NULL, $create_at = NULL, $modified_by = NULL,	$modified_at = NULL ){
        if(is_a($class, __CLASS__)) {
            $this->setId($id);
			$this->setId_customer($id_customer);
			$this->setIdItem($id_item);
			$this->setQuantity($quantity);
			$this->setPrice($price);
			$this->setStatus($status);
			$this->setCreateBy($create_by);
			$this->setCreateAt($create_at);
			$this->setModifiedBy($modified_by);
			$this->setModifiedAt($modified_at);
        }
    }

	public function setId($id){
		$this->id = $id;
		return $this;
	}

	public function setId_customer($id_customer){
		$this->id_customer = $id_customer;
		return $this;
	}

	public function setIdItem($id_item){
		$this->id_item = $id_item;
		return $this;
	}

	public function setQuantity($quantity){
		$this->quantity = $quantity;
		return $this;
	}

	public function setPrice($price){
		$this->price = $price;
		return $this;
	}

	public function setCreateBy($create_by){
		$this->create_by = $create_by;
		return $this;
	}

	public function setCreateAt($create_at){
		$this->create_at = $create_at;
		return $this;
	}

	public function setModifiedBy($modified_by){
		$this->modified_by = $modified_by;
		return $this;
	}

	public function setModifiedAt($modified_at){
		$this->modified_at = $modified_at;
		return $this;
	}

	public function setStatus($status){
		$this->status = $status;
		return $this;
	}

	
	public function getId(){
		
		return $this->id;
	}

	public function getId_customer(){
		
		return $this->id_customer;
	}

	public function getIdItem(){
		
		return $this->id_item;
	}

	public function getQuantity(){
		
		return $this->quantity;
	}

	public function getPrice(){
		
		return $this->price;
	}

	public function getCreateBy(){
		
		return $this->create_by;
	}

	public function getCreateAt(){
		
		return $this->create_at;
	}

	public function getModifiedBy(){
		
		return $this->modified_by;
	}

	public function getModifiedAt(){
		
		return $this->modified_at;
	}

	public function getStatus(){

		return	$this->status;
	}
}