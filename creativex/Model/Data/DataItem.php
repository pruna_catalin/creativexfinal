<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * DAOUsers: CreativeX && noValue
 * Date: 10/4/2017
 * Time: 5:12 PM
 */
namespace CreativeX\Model\Data;
class DataItem{
	public $id				= NULL;
	public $id_page 		= NULL;
	public $name			= NULL;
	public $image			= NULL;
	public $quantity 		= NULL;
	public $description 	= NULL;
	public $price  			= NULL;
	public $discount  		= NULL;
	public $special   		= NULL;
	public $create_by		= NULL;
	public $create_at		= NULL;
	public $modified_by		= NULL;
	public $modified_at		= NULL;

	public function __construct($class = NULL ,$id = NULL,$id_page = NULL, $name = NULL,$image = NULL, $quantity = NULL ,$description = NULL, $price = NULL, $discount= NULL, $special = NULL, $create_by = NULL, $create_at = NULL, $modified_by = NULL,	$modified_at = NULL){
		 if(is_a($class, __CLASS__)) {  
			 $this->setid($id);
			 $this->setid_page($id_page);
			 $this->setname($name);
			 $this->setimage($image);
			 $this->setquantity($quantity);
			 $this->setdescription($description);
			 $this->setprice($price);
			 $this->setdiscount($discount);
			 $this->setspecial($special);
			 $this->setcreate_by($create_by);
			 $this->setcreate_at($create_at);
			 $this->setmodified_by($modified_by);
			 $this->setmodified_at($modified_at);
		 }
	}
	public function setid($id){
		$this->id = $id;
		return $this;
	}
	public function setid_page($id_page){
		$this->id_page = $id_page;
		return $this;
	}
	public function setname($name){
		$this->name = $name;
		return $this;
	}
	public function setimage($image){
		$this->image = $image;
		return $this;
	}
	public function setquantity($quantity){
		$this->quantity = $quantity;
		return $this;
	}
	public function setdescription($description){
		$this->description = $description;
		return $this;
	}
	public function setprice($price){
		$this->price = $price;
		return $this;
	}
	public function setdiscount($discount){
		$this->discount = $discount;
		return $this;
	}
	public function setspecial($special){
		$this->special = $special;
		return $this;
	}
	public function setcreate_by($create_by){
		$this->create_by = $create_by;
		return $this;
	}
	public function setcreate_at($create_at){
		$this->create_at = $create_at;
		return $this;
	}
	public function setmodified_by($modified_by){
		$this->modified_by = $modified_by;
		return $this;
	}
	public function setmodified_at($modified_at){
		$this->modified_at = $modified_at;
		return $this;
	}
	public function getid(){
		return $this->id;
	}
	public function getid_page(){
		return $this->id_page;
	}
	public function getname(){
		return $this->name;
	}
	public function getimage(){
		return $this->image;
	}
	public function getquantity(){
		return $this->quantity;
	}
	public function getdescription(){
		return $this->description;
	}
	public function getprice(){
		return $this->price;
	}
	public function getdiscount(){
		return $this->discount;
	}
	public function getspecial(){
		return $this->special;
	}
	public function getcreate_by(){
		return $this->create_by;
	}
	public function getcreate_at(){
		return $this->create_at;
	}
	public function getmodified_by(){
		return $this->modified_by;
	}
	public function getmodified_at(){
		return $this->modified_at;
	}

}