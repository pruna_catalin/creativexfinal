<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 10/18/2017
 * Time: 10:30 AM
 */

namespace CreativeX\Model\Data;

class DataCustomer
{
    public $id				= NULL;

    public $id_profile		= NULL;

    public $username		= NULL;

    public $password		= NULL;

    public $activation_link = NULL;

	public $create_by		= NULL;

	public $create_at		= NULL;
	
	public $modified_by		= NULL;

	public $modified_at		= NULL;

    public function __construct($class = NULL ,$id = NULL, $id_profile = NULL,  $username = NULL, $password = NULL, $activation_link = NULL, $create_by = NULL, $create_at = NULL, $modified_by = NULL,	$modified_at = NULL ){
        if(is_a($class, __CLASS__)) {
            $this->setId($id);
			$this->setIdProfile($id_profile);
            $this->setUsername($username);
            $this->setPassword($password);
			$this->setActivationLink($activation_link);
			$this->setCreateBy($create_by);
			$this->setCreateAt($create_at);
			$this->setModifiedBy($modified_by);
			$this->setModifiedAt($modified_at);
        }
    }

    public function  setId($id){
        $this->id = $id;
        return $this;
    }

    public function getId(){
        return $this->id;
    }

	public function  setIdProfile($id_profile){
        $this->id_profile = $id_profile;
        return $this;
    }

	public function  getIdProfile(){
        return  $this->id_profile;
    }

    public function setUsername($username){
        $this->username = $username;
        return $this;
    }

    public function getUsername(){
        return $this->username;
    }

    public function setPassword($password){
        $this->password = $password;
        return $this;
    }

    public function getPassword(){
        return $this->password;
    }

	public function setActivationLink($activation_link){
		$this->activation_link = $activation_link;
		return $this;
	}

	public function getActivationLink(){
		return $this->activation_link;
	}

	public function setCreateBy($create_by){
		$this->create_by = $create_by;
		return $this;
	}

	public function getCreateBy(){
		return $this->create_by;
	}

	public function setCreateAt($create_at){
		$this->create_at= $create_at;
		return $this;
	}

	public function getCreateAt(){
		return $this->create_at;
	}

	public function setModifiedBy($modified_by){
		$this->modified_by = $modified_by;
		return $this;
	}

	public function setModifiedAt($modified_at){
		return $this->modified_by;
	}
}