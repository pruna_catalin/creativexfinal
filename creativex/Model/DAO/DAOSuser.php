﻿<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 2017-12-04
 * Time: 09:28:06 AM
 */

namespace CreativeX\Model\DAO;

use CreativeX\Model\Data\DataSuser;
use CreativeX\Model\Drivers\SqlPDO;
use CreativeX\modules\util\Debugger;
class DAOSuser extends DAOAbstract {
	public static $order = "id ASC";
	private static function TableName() {
		return "s_user";
	}

	/* 
     * @params  condition , resultType  = 'assoc|num|object|lazy' , null or empty return OBJECT
	 * @ return false on rows 0/ SUCCESS RETURN Suser WITH DATA
	 */
	public static function Find($data, $resultType = NULL) {
		$db = new SqlPDO('');
		$result = [];
		if ($data instanceof DataSuser) {
			$result = DAOTools::FindByModel($db, $data, self::tableName(), self::$order)->fetch($db::SwitchResult($resultType));
		} else {
			if ($data != "") {
				$db->select('*')->from(self::tableName())->where($data)->orderby(self::$order)->prepare();
				$request = $db->execute();
				$result = $request->fetch($db::SwitchResult($resultType));
			}
		}
		return $result;
	}

	/* 
     * @params  condition , limit / row , resultType  = 'assoc|num|object|lazy' , null or empty return OBJECT
     * @ return false on rows 0
     */
	public static function FindAll($data, $limit = "", $resultType = NULL) {
		$db = new SqlPDO('');
		$result = [];
		$prepare = [];
		if ($data instanceof DataSuser) {
			$prepare = DAOTools::FindByModel($db, $data, self::tableName(), self::$order,$limit)->fetchAll($db::SwitchResult($resultType));
		} else {
			if ($data != "") {
				if($limit != "")
					$db->select('*')->from(self::tableName())->where($data)->orderby(self::$order)->limit($limit)->prepare();
				else
					$db->select('*')->from(self::tableName())->where($data)->orderby(self::$order)->prepare();
				$request = $db->execute();
				$prepare = $request->fetchAll($db::SwitchResult($resultType));
			}else{
				if($limit != "")
					$db->select('*')->from(self::tableName())->orderby(self::$order)->limit($limit)->prepare();
				else
					$db->select('*')->from(self::tableName())->orderby(self::$order)->prepare();
				$request = $db->execute();
				$prepare = $request->fetchAll($db::SwitchResult($resultType));
            }
		}
		
        foreach($prepare as $data){
			$id = $data->id;
			$id_group = $data->id_group;
			$id_profile = $data->id_profile;
			$username = $data->username;
			$password = $data->password;
			$email = $data->email;
			$active = $data->active;
			$reload_permisions = $data->reload_permisions;

            $prepareObject = new DataSuser(null,$id ,$id_group ,$id_profile ,$username ,$password ,$email ,$active ,$reload_permisions);
            array_push($result,new DataSuser($prepareObject,$id ,$id_group ,$id_profile ,$username ,$password ,$email ,$active ,$reload_permisions));
        }
		return (sizeof($result) == 0 ) ? FALSE : $result;
	}
    /* 
     * @params  condition | Suser   
     * @ return false on FAILD
     */
	public static function Insert($model) {
		$dataParse = DAOTools::InsertModel($model);
		$data = array('input' => $dataParse[0]);
		if(sizeof($dataParse[0]) > 0 ){
			$sql = new SqlPDO($data);
			$sql->insert(self::tableName())->columns($dataParse[1])
				->values($dataParse[2])->prepare();
			$sql->execute();
			return $sql->lastInsertId();
		}else{
			return FALSE;
		}
	}
    /* 
     * @params  condition | Suser
     * @ return false on FAILD
     */
	public static function Update($model,$condition) {
		$dataParse = DAOTools::UpdateModel($model,$condition);
		if(sizeof($dataParse[1]) == 0){
			$data = $model;
		}else{
			$data = array('prepare' => array(':SET' =>$dataParse[0],':WHERE' => $condition.'=:'.$condition),
				'input' => $dataParse[1]);
		}
		$sql = new SqlPDO($data);
		$sql->update(self::tableName())->prepare();
        $result = $sql->execute();
		return ($result->rowCount() > 0 ) ? TRUE : FALSE;
	}

	public static function Delete($data) {
		$sql = new SqlPDO('');
		$sql->delete("")->from(self::tableName())->where($data)->prepare();
        $result = $sql->execute();
		return ($result->rowCount() > 0 ) ? TRUE : FALSE;
	}

	public static function Count(){
		$db = new SqlPDO('');
		$db->select('*')->from(self::tableName())->orderby(self::$order)->prepare();
		$request = $db->execute();
		$prepare = $request->fetchAll();
		return count($prepare);
	}
    


}