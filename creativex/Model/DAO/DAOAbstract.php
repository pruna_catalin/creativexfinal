<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 10/18/2017
 * Time: 10:57 AM
 */

namespace CreativeX\Model\DAO;


abstract class DAOAbstract {
	public abstract static function Find($data, $resultType = NULL);

	public abstract static function FindAll($data,$limit,$resultType = NULL);

	public abstract static function Insert($data);

	public abstract static function Update($data,$condition);

	public abstract static function Delete($data);

}