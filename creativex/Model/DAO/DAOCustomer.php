<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * DAOUsers: CreativeX && noValue
 * Date: 10/4/2017
 * Time: 5:12 PM
 */

namespace CreativeX\Model\DAO;

use CreativeX\Model\Data\DataCustomer;
use CreativeX\Model\Drivers\SqlPDO;

class DAOCustomer
{
	
	public static $order = "id ASC";


	private static function TableName() {
		return "n_customer";
	}

	/* 
	 * @params  condition , resultType  = 'assoc|num|object|lazy' , null or empty return OBJECT
	 * @ return false on rows 0/ SUCCESS RETURN MODEL WITH DATA
	 */
	public static function Find($data, $resultType = NULL) {
		$db = new SqlPDO('');
		$result = [];
		if ($data instanceof DataCustomer) {
			$result = DAOTools::FindByModel($db, $data, self::tableName(), self::$order)->fetch($db::SwitchResult($resultType));
		} else {
			if ($data != "") {
				$db->select('*')->from(self::tableName())->where($data)->orderby(self::$order)->prepare();
				$request = $db->execute();
				$result = $request->fetch($db::SwitchResult($resultType));
			}
		}
		return $result;
	}

	/* 
	 * @params  condition , limit / row , resultType  = 'assoc|num|object|lazy' , null or empty return OBJECT
	 * @ return false on rows 0
	 */
	public static function FindAll($data, $limit, $resultType = NULL) {
		$db = new SqlPDO('');
		$result = [];
		$prepare = [];
		if ($data instanceof DataCustomer) {
			$prepare = DAOTools::FindByModel($db, $data, self::tableName(), self::$order)->fetchAll($db::SwitchResult($resultType));
		} else {
			if ($data != "") {
				$db->select('*')->from(self::tableName())->where($data)->orderby(self::$order)->prepare();
				$request = $db->execute();
				$prepare = $request->fetchAll($db::SwitchResult($resultType));
			}else{
                $db->select('*')->from(self::tableName())->orderby(self::$order)->prepare();
				$request = $db->execute();
				$prepare = $request->fetchAll($db::SwitchResult($resultType));
            }
		}
        foreach($prepare as $data){
            $id = $data->id;
            $id_profile = $data->id_profile;
            $username = $data->username;
            $password = $data->password;
			$activation_link = $data->activation_link;
			$create_by = $data->create_by;
			$create_at = $data->create_at;
			$modified_by = $data->modified_by;
			$modified_at = $data->modified_at;

            $prepareObject = new DataCustomer(null,$id,$id_profile,$username,$password,$activation_link,$create_by,$create_at,$modified_by,$modified_at);
            array_push($result,new DataCustomer($prepareObject,$id,$id_profile,$username,$password,$activation_link,$create_by,$create_at,$modified_by,$modified_at));
        }
		return (sizeof($result) == 0 ) ? FALSE : $result;
	}
    /* 
	 * @params  condition | MODEL   
	 * @ return false on FAILD
	 */
	public static function Insert($model) {
		$dataParse = DAOTools::InsertModel($model);
		$data = array('input' => $dataParse[0]);
		if(sizeof($dataParse[0]) > 0 ){
			$sql = new SqlPDO($data);
			$sql->insert(self::tableName())->columns($dataParse[1])
				->values($dataParse[2])->prepare();
			$sql->execute();
			return $sql->lastInsertId();
		}else{
			return FALSE;
		}
	}
    /* 
	 * @params  condition | MODEL
	 * @ return false on FAILD
	 */
	public static function Update($model,$condition) {
		$dataParse = DAOTools::UpdateModel($model,$condition);
		if(sizeof($dataParse[1]) == 0){
			$data = $model;
		}else{
			$data = array('prepare' => array(':SET' =>$dataParse[0],':WHERE' => $condition.'=:'.$condition),
				'input' => $dataParse[1]);
		}
		$sql = new SqlPDO($data);
		$sql->update(self::tableName())->prepare();
        $result = $sql->execute();
		return ($result->rowCount() > 0 ) ? TRUE : FALSE;
	}

	public static function Delete($data) {
		$sql = new SqlPDO('');
		$sql->delete("")->from(self::tableName())->where($data)->prepare();
        $result = $sql->execute();
		return ($result->rowCount() > 0 ) ? TRUE : FALSE;
	}

	public static function Count(){
		$db = new SqlPDO('');
		$db->select('*')->from(self::tableName())->orderby(self::$order)->prepare();
		$request = $db->execute();
		$prepare = $request->fetchAll();
		return count($prepare);
	}
	public static function FindOrderCustomer($id){
		$db = new SqlPDO('');
		$result = [];
		$prepare = [];
		$db->select('t1.*,t2.*')->from(self::tableName()." as t1 left join n_customer_profile as t2 on t1.id_profile = t2.id")->where("t1.id=".$id)->prepare();
		$request = $db->execute();
		$prepare = $request->fetchAll($db::SwitchResult(""));
		foreach($prepare as $data){
           
            array_push($result,$data);
        }
		return (sizeof($result) == 0 ) ? FALSE : $result[0];

	} 
	public static function FindCustomerByName($name){
		$db = new SqlPDO('');
		$result = [];
		$prepare = [];
		$db->select('t1.*,t2.*')->from(self::tableName()." as t1 left join n_customer_profile as t2 on t1.id_profile = t2.id")->where("t2.name like '%".$name."%'")->prepare();
		$request = $db->execute();
		$prepare = $request->fetchAll($db::SwitchResult(""));
		foreach($prepare as $data){
			
            array_push($result,$data);
        }
		return (sizeof($result) == 0 ) ? FALSE : $result;

	} 
}