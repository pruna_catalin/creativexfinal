<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 10/18/2017
 * Time: 12:03 PM
 */

namespace CreativeX\Model\DAO;


class DAOTools
{
    /*
         *@params $model , $table
         *@return result of sql instance
         *@create sql syntax by Class Model
         */
    public static function FindByModel($db,$model,$table,$order,$limit = ""){
       
        $condition = array();
        foreach($model as $key => $value){
            if(isset($value)){
                if(is_array($value)){
                    if(isset($value['in'])){
                        if(is_array($value['in'])){
                            $condition[] = $key." IN('".implode("','",$value['in'])."') ";
                        }
                    }
                    if(isset($value['or'])){
                        if(is_array($value['or'])){
                            $Or = " (";
                            for($i = 0;$i<sizeof($value['or']);$i++){
                                if($i  == sizeof($value['or']) - 1) {
                                    $Or .= $key."='".$value['or'][$i]."' ";
                                }else{
                                    $Or .= $key."='".$value['or'][$i]."' OR ";
                                }
                            }
                            $Or .= " ) ";
                            $condition[] = $Or;
                        }
                    }
                    if(isset($value['like'])){
                        $like = "";
                        if(is_array($value['like'])){
                            $like = "";
                            for($i = 0;$i<sizeof($value['like']);$i++){
                                if($i  == sizeof($value['like']) - 1) {
                                    $like .= $key." LIKE '".$value['like'][$i]."' ";
                                }else{
                                    $like .= $key." LIKE '".$value['like'][$i]."' OR ";
                                }
                            }
                        }
                        $condition[] = $like;
                    }
                }else{
                    $condition[] = $key."='".$value."'";
                }
            }
        }
        if(sizeof($condition) > 0){
            $conditionString  = implode(" AND ",$condition);
            if($order != ""){
				if($limit != "")
					$db->select('*')->from($table)->where($conditionString)->orderBy($order)->limit($limit)->prepare();
				else
					$db->select('*')->from($table)->where($conditionString)->orderBy($order)->prepare();
            }else{
				if($limit != "")
					$db->select('*')->from($table)->where($conditionString)->limit($limit)->prepare();
				else
					$db->select('*')->from($table)->where($conditionString)->prepare();
            }
        }else{
            if($order != "") {
				if($limit != "")
					$db->select('*')->from($table)->orderBy($order)->limit($limit)->prepare();
				else
					$db->select('*')->from($table)->orderBy($order)->prepare();

            }else{
				if($limit != "")
					$db->select('*')->from($table)->limit($limit)->prepare();
				else
					$db->select('*')->from($table)->prepare();
            }
        }
        return $db->execute();
    }
    public static function UpdateModel($model,$condition){
		$dataSet = "";
		$dataInput = [];
		$i = 0;
		if(sizeof( (array)$model) > 0){
			if(isset($model->{$condition})){
				$dataInput[":".$condition] = $model->{$condition};
				unset($model->{$condition});
				foreach($model as $item => $value){
                        $dataInput[":".$item] = $value;
                        if($i == sizeof( (array)$model) - 1){
                            $dataSet .= $item."=:".$item;
                        }else{
                            $dataSet .= $item."=:".$item.",";
                        }					
					$i++;
				}
			}
		}
		return [$dataSet,$dataInput];
	}
    public static function InsertModel($model){
		$dataInput = [];
		$dataValues = [];
		$dataColumns = "";

		if(sizeof( (array)$model) > 0){
			$dataColumns = implode(",",array_keys((array)$model));
			foreach($model as $item => $value){
				$dataInput[":".$item] = $value;
				$dataValues[]  = ":".$item;
			}
			if(sizeof($dataValues) > 0){
				$dataValues = implode(",",$dataValues);
			}
		}
		return [$dataInput,$dataColumns,$dataValues];
	}
}